Project: Inhibiting human aversive memory by transcranial theta-burst stimulation to primary sensory cortex

Project authors: Karita E. Ojala (1,2), Matthias Staib (1,2), Samuel Gerster (1), Christian C. Ruff (2,3), Dominik R. Bach (1,2,4)

1. Computational Psychiatry Research, Department of Psychiatry, Psychotherapy and Psychosomatics, Psychiatric Hospital, University of Zurich, Switzerland
2. Neuroscience Centre Zurich, University of Zurich, Switzerland
3. Zurich Center for Neuroeconomics (ZNE), Department of Economics, University of Zurich, Switzerland
4. Wellcome Centre for Human Neuroimaging and Max-Planck UCL Centre for Computational Psychiatry and Ageing Research, University College London, UK

Corresponding author: Karita Ojala, k.ojala@uke.de (as of 6/2021)

*Abstract*

Background

Predicting adverse events from past experience is fundamental for many biological organisms. However, some individuals suffer from maladaptive memories that impair behavioral control and wellbeing, e.g. after psychological trauma. Inhibiting the formation and maintenance of such memories would have high clinical relevance. Previous pre-clinical research has focused on systemically administered pharmacological interventions, which cannot be targeted to specific neural circuits in humans. Here, investigated the potential of non-invasive neural stimulation on human sensory cortex in inhibiting aversive memory in a laboratory threat conditioning model.

Methods

We build on an emerging non-human literature suggesting that primary sensory cortices may be crucially required for threat memory formation and consolidation. Immediately before conditioning innocuous somatosensory stimuli (conditioned stimuli, CS) to aversive electric stimulation, healthy human participants received continuous theta-burst transcranial magnetic stimulation (cTBS) to individually localized primary somatosensory cortex in the CS-contralateral (experimental) or CS-ipsilateral (control) hemisphere. We measured fear-potentiated startle to infer threat memory retention on the next day, as well as skin conductance and pupil size during learning.

Results

After overnight consolidation, threat memory was attenuated in the experimental compared to the control cTBS group. There was no evidence that this differed between simple and complex CS, or that CS identification or initial learning were affected by cTBS.

Conclusions

Our results suggest that cTBS to primary sensory cortex inhibits threat memory, likely by an impact on post-learning consolidation. We propose that non-invasive targeted stimulation of sensory cortex may provide a new avenue for interfering with aversive memories in humans.

Published article: https://doi.org/10.1016/j.biopsych.2022.01.021

Preprint: https://www.biorxiv.org/content/10.1101/2021.06.09.447685v1

Bach lab website: bachlab.org
Psychophysiological Modelling (PsPM) toolbox: bachlab.github.io/PsPM/

----------------------------------------------------------------------

Scripts written by Karita Ojala, 2017-2021

This collection of Matlab scripts forms an analysis pipeline that cleans up, preprocesses (including import of data into PsPM, filtering, artefact correction, selection of valid trials etc.), runs PsPM model-based response estimation for, and visualizes skin conductance (SCR), pupil size (PSR) and startle eye-blink response (SEBR) data. 
The pipeline can be started from psychophys_start.m script, where the user can edit which steps of the pipeline and which experimental sessions (acquisition/conditioning on experimental day 1, memory retention test on day 2, relearning/retest on day 2). 

The original data files that work with the pipeline will be publicly available anonymized and in full when the manuscript is published. 

Also included are the scripts for the experiment itself, as well as the fMRI analysis to extract "regions-of-interest" (a sphere in left and right primary somatosensory cortex around the peak activity from localizer scans) for each participant individually as the TMS target region. 

----------------------------------------------------------------------

For a comprehensive explanation of the process for each psychophysiological modality, please refer to the latest PsPM manual from the website and the following articles: 

Psychphysiological Modelling in general: 
Bach DR, Castegnetti G, Korn CW, Gerster S, Melinscak F, Moser T (2018) Psychophysiological modeling: Current state and future directions. Psychophysiology 55:e13209.
Bach DR, Melinscak F (2020) Psychophysiological modelling and the measurement of fear conditioning. Behav Res Ther 127:103576.

SCR:
Bach DR, Friston KJ (2013) Model-based analysis of skin conductance responses: Towards causal models in psychophysiology. Psychophysiology 50:15–22.
Staib M, Castegnetti G, Bach DR (2015) Optimising a model-based approach to inferring fear learning from skin conductance responses. J Neurosci Methods 255:131–138.

PSR:
Korn CW, Staib M, Tzovara A, Castegnetti G, Bach DR (2017) A pupil size response model to assess fear learning. Psychophysiology 54:330–343.

SEBR: 
Khemka S, Tzovara A, Gerster S, Quednow B, Bach DR (2017) Modelling startle eye-blink electromyogram to assess fear learning. Psychophysiology 54:204–214.