function markernum = physio_sound_markers(fname,sIDX)
       
options.createchannel   = true;
options.diagnostics     = false;
options.channel_action  = 'replace';
options.channel_output  = 'all';
options.threshold       = .2;

switch sIDX
    case 32; options.roi = [20,920];
    case 22; options.roi = [40,490];
    case 24; options.roi = [105,450];
    case {25 29}; options.roi = [80,430];
    case 31; options.roi = [40,390];
    case {36 49}; options.roi = [200,600];
    case {28 42 51 53}; options.roi = [100,500]; 
    case 41; options.roi = [20,380];
    case 43; options.roi = [10,350];
    case 45; options.roi = [50,400];
    case 46; options.roi = [50,390];
    case 47; options.roi = [30,370];
    case 57; options.roi = [20,360];
    case 58; options.roi = [40,380];
    case 61; options.roi = [60,400];
    case 95; options.roi = [130,480];
    case 106; options.roi = [25,370]; 
    case 137; options.roi = [40,385];
    otherwise; options.roi = [0,600];
end

options.maxdelay      = 4;
options.mindelay      = 1;
options.sndchannel    = 2;
pspm_find_sounds(fname,options);

[~,~,data] = pspm_load_data(fname);

% pspm_find_sounds does not add a markerinfo marker names field
% add manually
markers = data{3,1}.data;

newdata{1} = data{3};

if sIDX == 106 % pspm_find_sounds cannot do the job so manual workaround
    sound = data{2,1}.data;
    sr = data{2,1}.header.sr;
    baseline = mean(sound(1:25*sr));
    foundMarker = find(diff(sound-baseline)>0.1)/sr;
    foundMarker(diff(foundMarker)<2) = [];
    if numel(foundMarker) > 24; incl_markers = 2:length(foundMarker); end % remove first startle probe
    markers = foundMarker(incl_markers); 
    newdata{1,1}.data = markers;
    newdata{1,1}.markerinfo.value = sound(floor(foundMarker(incl_markers)*sr));
elseif numel(markers) > 24
    markers(1) = []; % remove first startle probe
    newdata{1,1}.data = markers;
    newdata{1,1}.markerinfo.value = data{3,1}.markerinfo.value(2:end);
end

newdata{1,1}.markerinfo.name = sprintfc('%d',1:length(markers))';
options.channel = 3;
pspm_write_channel(fname,newdata,'replace',options);

markernum = length(markers);

end