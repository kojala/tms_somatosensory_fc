function SCR_sort_artefactperiods(opts,sessions)

for ses = sessions

    dataPath = opts.expPath; 
    sesname = opts.scr.sessionlist{ses};
    dataFile = fullfile(dataPath,['scr_markedartefacts_' sesname '.mat']);
    dataFile2 = fullfile(dataPath,['scr_markedartefacts_' sesname '_newsubs.mat']);
    dataFile3 = fullfile(dataPath,['scr_markedartefacts_' sesname '_newsubs2.mat']);
    
    data1 = load(dataFile);
    data2 = load(dataFile2);
    data3 = load(dataFile3);
    
    data = [data1.y_artefacts; data2.y_artefacts; data3.y_artefacts];
    
    [sorted, ind] = sort(cell2mat(data(:,1)));
    artefact_epochs = data(ind,:);
    
    saveFile = fullfile(dataPath,['scr_markedartefacts_' sesname '_compiled.mat']);
    save(saveFile,'artefact_epochs');
    
end


end