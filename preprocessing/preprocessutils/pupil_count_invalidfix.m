function sList_excl = pupil_count_invalidfix(ExpPath)

file = fullfile(ExpPath,'invalidfixations.mat');

if exist(file,'file')
    
    load(file,'invalidfix');
    
    data(:,1) = invalidfix(:,1);
    
    for ses = 1:3
        
        data(invalidfix(:,ses+1)>0.35,ses+1) = 1; % excluded
        data(invalidfix(:,ses+1)<=0.35,ses+1) = 0; % included
        exclusions(ses) = sum(data(:,ses+1)); % number of exlusions
        
        % Subject list with excluded subjects taken out
        sList_excl{:,ses} = data(data(:,ses+1)==0,1);
        
    end
    
    fprintf('No. of exluded subjects with > 35 percent invalid fixations: \n')
    fprintf(['Session 1: ' num2str(exclusions(1)) '\n'])
    fprintf(['Session 2: ' num2str(exclusions(2)) '\n'])
    fprintf(['Session 3: ' num2str(exclusions(3)) '\n'])
    
end

end