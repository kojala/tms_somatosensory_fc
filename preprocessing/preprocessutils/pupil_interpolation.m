function pupil_interpolation(opts,sessions)

% Options
options.overwrite = 1;
options.extrapolate = 1;
options.newfile = 1;

for ses = 1:length(sessions)
    
    sList = opts.psr.sList.sessions{ses};
    
    for sIDX = 1:length(sList)
        
        s_id = num2str(sList(sIDX));
        if sList(sIDX) < 100
            fixed_file  = fullfile(opts.psr.path.valfix,['valfix_tpspm_ss7b_0' num2str(s_id) '_eyelink_' opts.psr.sessionlist{ses} '.mat']); % Valid fixations
        else
            fixed_file  = fullfile(opts.psr.path.valfix,['valfix_tpspm_ss7b_' num2str(s_id) '_eyelink_' opts.psr.sessionlist{ses} '.mat']); % Valid fixations
        end
        
        if exist(fixed_file,'file')
            
            fprintf('Subject %3.0f, Session %3.0f \n', sList(sIDX), ses)
            [~, ~] = pspm_interpolate(fixed_file, options);
        end
        
    end
    
end

fprintf('Interpolation done')