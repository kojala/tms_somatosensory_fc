function output_onsets = SCR_remove_trials_tooshortITI(opts,sessionfile,input_onsets)

data = load(sessionfile);
sr = data.data{opts.scr.channel.scr}.header.sr;
session_data = data.data{opts.scr.channel.scr}.data;
USdur = 0.5;

remove_trial = zeros(numel(input_onsets),1);
output_onsets = input_onsets;

for trial = 1:numel(input_onsets)
   
    trial_onset = ceil(input_onsets(trial));
    trial_window = trial_onset*sr:(trial_onset+opts.soa+USdur)*sr; 
    iti_window = trial_window(end):(trial_window(end)+opts.scr.minITI*sr+sr-1); % 7.5 s minimum ITI used in DCM, 8 s after US offset should be safe
    full_window = trial_window(1):iti_window(end);
    
    if full_window(end) > numel(session_data) % if last trial of the session and data too short to include at least 8 s ITI
        remove_trial(trial) = 1;
    else
        trial_data = session_data(full_window);
        iti_data = session_data(iti_window);
    end
    
    if sum(isnan(trial_data))/numel(trial_data) > opts.scr.exclude_missingperc % if overall too little data
        remove_trial(trial) = 1;
    elseif sum(isnan(iti_data))/numel(iti_data) > opts.scr.exclude_missingperc % if too little ITI data
        remove_trial(trial) = 1;
    end
    
end

output_onsets(remove_trial==1) = [];

end