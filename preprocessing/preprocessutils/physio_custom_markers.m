function markernum = physio_custom_markers(fname,data)

markers = data{2}.data;
sr = data{2}.header.sr;

% Replace negative values with the median of data
markers(data{2}.data<0) = median(data{2}.data);

% Find markers
foundMarker = find(diff(markers)>2)/sr;
% Remove double markers
% Some trigger slopes are too "slow" and had 2 datapoints in the
% same slope, if they are more or less in the middle this
% (ie. [0 0 2.5 5 5 5]) the diff will return 2 consecutive values larger
% than 2. Here, if 2 points with a Dt smaller than 2 sampling intervals
% is detected, the 2nd point is removed.
foundMarker(find(diff(foundMarker)<2/sr)+1) = [];

% NOT POSSIBLE TO FIX MARKERS ACCORDING TO COGENT FILE
% NO INFORMATION ON INDIVIDUAL TRIAL ITI OR ACTUAL TRIAL ONSET/OFFSET IN
% RELATION TO WHOLE EXPERIMENT (ONLY WTR THE BLOCK ONSET)

% Check number of markers and attempt to fix by extracting marker timing
% from the Cogent file
% Need at least one known marker timing to reconstruct the rest
% if numel(foundMarker) ~= 96 && ~isempty(foundMarker)
%     
%     fprintf('\nWrong number of markers! Subject %03.0f ... ', sIDX);
%     fprintf('\nAttempting to fix...');
% 
%     [~,name,ext] = fileparts(fname);
% 
%     bhvfile = fullfile(opts.bhv.path,[name(1:9) 'behav' name(16:end) ext]);
%     bhvdata = load(bhvfile);
% 
%     markers_cogent = bhvdata.Data(:,12);
%     
%     % assume that at least the last marker is available
%     fixedMarker = nan(1,96);
%     fixedMarker(end) = foundMarker(end);
%     
%     % Reconstruct markers block by block, trial by trial
%     for fm = length(markers_cogent)-1:-1:1
%         
%         % substract cogent marker time n from n+1
%         markerdiff = markers_cogent(fm+1)-markers_cogent(fm);
%         % substract cogent marker difference from previous labchart based marker
%         fixedMarker(fm) = fixedMarker(fm+1)-markerdiff;
%         
%         % take into account the time between blocks (including ITI of last trial)
%         
%     end
%     
%     if numel(fixedMarker) == 96
%         fprintf('\nFixed!');
%     else
%         fprintf('\nFix failed!');
%     end
%     
% end

% Save data
newdata{1}.data              = foundMarker;
newdata{1}.header.sr         = 1;
newdata{1}.header.chantype   = 'marker';
newdata{1}.header.units      = 'events';
newdata{1}.markerinfo.value  = transpose(1:length(foundMarker));
newdata{1}.markerinfo.name   = sprintfc('%d',1:length(foundMarker))';
options.channel = 3;
pspm_write_channel(fname,newdata,'add',options)

markernum = length(foundMarker);

end