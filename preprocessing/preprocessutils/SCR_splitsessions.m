function SCR_splitsessions(opts,sessions)

options.prefix = opts.scr.trimstart;
options.suffix = opts.scr.trimend_block;
options.overwrite = true;

for ses = sessions
    
    options.max_sn = opts.scr.blocks(ses);
    options.splitpoints = opts.scr.split_sessions_markers{ses};
    
    sList = opts.scr.sList.DCM{ses};
%     modPath = opts.scr.path.trim;
    modPath = opts.scr.path.artcorr;
    fname_mod = 'atpspm';
    modname = 'SCR';
    sesname = opts.scr.sessionlist{ses};
    
    for sIDX = 1:length(sList)
        
        s_id = num2str(sList(sIDX));
        if sList(sIDX) < 100
            fname   = fullfile(modPath, [fname_mod '_ss7b_0' s_id '_physio_' sesname '_' modname '.mat']);
        else
            fname   = fullfile(modPath, [fname_mod '_ss7b_' s_id '_physio_' sesname '_'  modname '.mat']);
        end
        
        if ~exist([fname(1:end-4) '_sn01.mat'],'file') || opts.scr.split_sessions_overwrite == 1
            pspm_split_sessions(fname,[],options)
        end
        
    end
    
end

end