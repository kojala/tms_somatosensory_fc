function preprocessEMG_pp(opts)

% Steps:
%  1. Check availability of file
%  2. Preprocess EMG signal with PsPM settings
%  3. Save preprocessed data
%__________________________________________________________________________

sList = opts.sebr.sList.orig;

for sIDX = 1:numel(sList)
    
    if sList(sIDX) < 100
        fnamesebr = fullfile(opts.sebr.path.trim,['tpspm_ss7b_0' num2str(sList(sIDX)) '_physio_retention_SEBR.mat']);
    else
        fnamesebr = fullfile(opts.sebr.path.trim,['tpspm_ss7b_' num2str(sList(sIDX)) '_physio_retention_SEBR.mat']);
    end
    
    if exist(fnamesebr,'file')
        
        fprintf('Subject # %3.0f ... \n', sList(sIDX))
        
        options.channel_action = 'replace';
        pspm_emg_pp(fnamesebr, options)
        
    end
end

end