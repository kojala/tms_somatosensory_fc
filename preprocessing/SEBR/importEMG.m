function importEMG(opts)

% Steps:
%  1. Check availability of file
%  2. Import LabChart file with PsPM
%  3. Find markers based on the sound data
%__________________________________________________________________________

sList = opts.sebr.sList.orig;

for sIDX = 1:numel(sList)
    
    % File names
    if sList(sIDX) < 100
        fname      = fullfile(opts.sebr.path.raw,['ss7b_0' num2str(sList(sIDX)) '_physio_retention.mat']); % Original
        fnamesebr  = fullfile(opts.sebr.path.import,['pspm_ss7b_0' num2str(sList(sIDX)) '_physio_retention_SEBR.mat']); % Imported
        fnamesebrt = fullfile(opts.sebr.path.trim,['tpspm_ss7b_0' num2str(sList(sIDX)) '_physio_retention_SEBR.mat']); % Trimmed
    else
        fname      = fullfile(opts.sebr.path.raw,['ss7b_' num2str(sList(sIDX)) '_physio_retention.mat']);
        fnamesebr  = fullfile(opts.sebr.path.import,['pspm_ss7b_' num2str(sList(sIDX)) '_physio_retention_SEBR.mat']);
        fnamesebrt = fullfile(opts.sebr.path.trim,['tpspm_ss7b_' num2str(sList(sIDX)) '_physio_retention_SEBR.mat']);
    end
    
    if exist(fname,'file') && ~exist(fnamesebr,'file')
        
        % Import raw LabChart files
        %----------------------------------------------------------
        datatype = 'labchartmat_in';
        import{1}.type = 'emg';
        import{1}.channel = 3;
        import{2}.type = 'custom';
        import{2}.channel = 2;
        options.overwrite = 1;
        imported_files = pspm_import(fname, datatype, import, options);
        
        % Some subjects might have split data due to a break in the
        % labchart file, need to check for and fix that
        if size(imported_files,2) > 1
            importedfn = physio_fix_splitdata(imported_files);
        else
            importedfn = imported_files{:};
        end
        
        % Rename file with SCR only, moving it to a new folder
        pspm_ren(importedfn, fnamesebr);
        
    end
    
    if exist(fnamesebr,'file') && ~exist(fnamesebrt,'file')
        
        % Find triggers from sound and add marker channel
        %----------------------------------------------------------
        markernum = physio_sound_markers(fnamesebr,sList(sIDX));
        
        if markernum ~= 24
            fprintf('\nWrong number of markers! Subject %03.0f, %03.0f markers... ', sList(sIDX), markernum ); 
            fprintf('\n');
        end
        % Remember that 1st marker is the startle probe, not an actual trial
        
        % Trim the imported pspm file
        clear options
        options.overwrite = true;
        trimmedfn = pspm_trim(fnamesebr,opts.sebr.trimstart,opts.sebr.trimend,'marker',options);
        
        pspm_ren(trimmedfn, fnamesebrt);
        
    end
    
end

end