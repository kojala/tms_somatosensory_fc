function pupil_correct(opts,sessions)

options.screen_size_px = opts.psr.screen_resolution;
options.screen_size_mm = [opts.psr.eyelink_width*10 opts.psr.eyelink_height*10];
options.mode = 'auto';
%options.C_z = 625;
options.channel_action = 'replace';
options.plot_data = false;

for ses = sessions
    
    sList = opts.psr.sList.sessions{ses};
    
    for sIDX = 1:numel(sList)
        
        %pupilchan = opts.psr.channel.pupil;
        s_id = sList(sIDX);
        sprintf('\n--\nCorrecting... Subject %s, Session %d \n--\n', num2str(s_id), ses)
        
        if sList(sIDX) < 100
            pupilfile  = fullfile(opts.psr.path.trim,['tpspm_ss7b_0' num2str(s_id) '_eyelink_' opts.psr.sessionlist{ses} '.mat']);
        else
            pupilfile  = fullfile(opts.psr.path.trim,['tpspm_ss7b_' num2str(s_id) '_eyelink_' opts.psr.sessionlist{ses} '.mat']);
        end
        
        if exist(pupilfile,'file')
            
            % Correct the measures eye for participant
            if ismember(s_id,opts.psr.sList.left)
                options.channel = 'pupil_l';
                %channel_pp = 'pupil_l_pp';
                %[sts, out_chan] = pspm_pupil_correct_eyelink(pupilfile, options);
                %assert(sts == 1);
            else
                options.channel = 'pupil_r';
                %channel_pp = 'pupil_r_pp';
                %[sts, out_chan] = pspm_pupil_correct_eyelink(pupilfile, options);
                %assert(sts == 1);
            end
            
            % Preprocessing
            [sts, options.custom_settings] = pspm_pupil_pp_options();
            assert(sts == 1);
%             options.custom_settings.raw.dilationSpeedFilter_MadMultiplier = 10; % lowering the value decreases the threshold for detecting speed changes
%             options.custom_settings.raw.dilationSpeedFilter_maxGap_ms = 200;
%             options.custom_settings.raw.gapPadding_backward = 75; % increasing this value will remove more residual artefact edges before the gap
%             options.custom_settings.raw.gapPadding_forward = 75; % increasing this value will remove more after the gap
            options.custom_settings.raw.residualsFilter_passes = 7; % adding more passes might remove additional outlying values
            
%             options.custom_settings.raw.PupilDiameter_Min = 1.5;
%             options.custom_settings.raw.PupilDiameter_Max = 10;
%             options.custom_settings.valid.interp_maxGap = 200;
            
            %options.channel = channel_pp;
            [sts, ~] = pspm_pupil_pp(pupilfile, options);
            assert(sts == 1);
            
        end
        
    end
    
end

end