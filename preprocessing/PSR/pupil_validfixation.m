function pupil_validfixation(opts,sessions)

% Loop over sessions
for ses = sessions
    
    sList = opts.psr.sList.sessions{ses};
    
    % Loop over subjects
    for sIDX = 1:length(sList)
        
        s_id = num2str(sList(sIDX));
        
        % Data file
        if sList(sIDX) < 100
            fnamepsrt   = fullfile(opts.psr.path.trim,['tpspm_ss7b_0' num2str(s_id) '_eyelink_' opts.psr.sessionlist{ses} '.mat']); % Trimmed
            fixed_file  = fullfile(opts.psr.path.valfix,['valfix_tpspm_ss7b_0' num2str(s_id) '_eyelink_' opts.psr.sessionlist{ses} '.mat']); % Valid fixations
        else
            fnamepsrt   = fullfile(opts.psr.path.trim,['tpspm_ss7b_' num2str(s_id) '_eyelink_' opts.psr.sessionlist{ses} '.mat']);
            fixed_file  = fullfile(opts.psr.path.valfix,['valfix_tpspm_ss7b_' num2str(s_id) '_eyelink_' opts.psr.sessionlist{ses} '.mat']); % Valid fixations
        end
        
        fixation_opt = opts.psr.fixation_opt;
        
        if ismember(sList(sIDX),opts.psr.sList.left) % some subjects have left eye option in EyeLink even though right eye actually
            fixation_opt.eyes = 'left';
        end
        fixation_opt.newfile = fixed_file;
        fixation_opt.channels = opts.psr.channel.correct;
        
        fprintf(['Subject ' num2str(sList(sIDX)) ' Session ' num2str(ses) '\n'])
        
        if exist(fnamepsrt,'file')% && ~exist(fixed_file,'file')
            
            [~,infos,pupildata] = pspm_load_data(fnamepsrt);
            
            % Fix observed eyes for two subjects who had two eyes measured
            % in EyeLink but actually data only for one eye (right)
            if sList(sIDX) == 26 || sList(sIDX) == 30
                data = pupildata;
                infos.source.eyesObserved = 'r';
                save(fnamepsrt,'data','infos')
            end
            
            %% Convert gaze data to cm if not done yet
            gaze_chan_px = [opts.psr.channel.gaze_x opts.psr.channel.gaze_y];
            
            if length(pupildata) == opts.psr.channel.correct % if pupil data does not contain more gaze channels (cm) yet
                options.channel_action = 'add';
                pspm_convert_pixel2unit(fnamepsrt, gaze_chan_px, opts.psr.convert_unit, opts.psr.eyelink_width, opts.psr.eyelink_height, opts.psr.distance.screen/10, options);
            end
            
            %% Define fixation point
            if strcmp(opts.psr.fixation_type,'default') % default option
                fixation_opt.fixation_point = [0.5 0.5];
                
            elseif strcmp(opts.psr.fixation_type,'biased') % biased fixation
                
                fixation_opt.fixation_point = opts.psr.fixation_point_biased;
                
            elseif strcmp(opts.psr.fixation_type,'gazemedian') % median of gaze coordinates
                median_gazex = nanmedian(pupildata{gaze_chan_px(1)}.data); % median x coordinate
                median_gazey = nanmedian(pupildata{gaze_chan_px(2)}.data); % median y coordinate
                x_coord = median_gazex;
                inverted_y_coord = infos.source.gaze_coords.ymax-median_gazey;
                fixation_opt.fixation_point = [x_coord inverted_y_coord];
                % should be given in eyelink units, the same units as the resolution
                % need to invert y-coordinate because eyelink reports coordinates in an inverted system
                
            end
            
            %% Find valid fixations
          
            pspm_find_valid_fixations(fnamepsrt, opts.psr.box_degrees, opts.psr.distance.screen, opts.psr.distance_unit, fixation_opt);
            
        end
        
    end
    
    fprintf('---- \n')
    
end

end