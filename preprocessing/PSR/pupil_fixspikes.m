function pupil_fixspikes(opts,sessions)
% Does not fix upward spikes unless immediately adjacent to downward spike

% Loop over sessions
for ses = sessions
    
    sList = opts.psr.sList.fixspikes{ses};
    
    % Loop over subjects
    for sIDX = 1:length(sList)
        
        s_id = num2str(sList(sIDX));
        
        % Data file
        if sList(sIDX) < 100
            fixed_file  = fullfile(opts.psr.path.valfix,['valfix_tpspm_ss7b_0' num2str(s_id) '_eyelink_' opts.psr.sessionlist{ses} '.mat']); % Valid fixations
        else
            fixed_file  = fullfile(opts.psr.path.valfix,['valfix_tpspm_ss7b_' num2str(s_id) '_eyelink_' opts.psr.sessionlist{ses} '.mat']); % Valid fixations
        end
        
        fprintf(['Subject ' num2str(sList(sIDX)) ' Session ' num2str(ses) '\n'])
        
        if exist(fixed_file,'file')
            
            [~,~,pupildata] = pspm_load_data(fixed_file);
            
            % Visual inspection of whether this subject is problematic
            channel = opts.psr.channel.valfix;
            
            if ismember(0,pupildata{channel}.data) % if data contains zeros, there are spikes present
                
                figure; plot(pupildata{channel}.data); % assume similar problem with spikes for both eyes
                title(['Subject ' num2str(sList(sIDX)) ' Session ' num2str(ses)])
                limit = input('Input (lower) cutoff for spikes: ');
                close all
                
                data = pupildata{channel}.data;
                fixed_data = data;
                fixed_data(fixed_data == 0) = NaN; % already remove zeros
                spikes = fixed_data < limit; % find approximate remaining spike data
                
                spike_ind = find(spikes); % find spike indices
                
                for spike = 1:numel(spike_ind) % loop over spikes
                    spike_start = spike_ind(spike)-60; % 60 samples before
                    spike_end = spike_ind(spike)+60; % 60 samples after
                    if spike_start < 0; spike_start = 1; end % avoid indices before the start of data
                    if spike_end > length(data); spike_end = length(data); end % avoid indices after the end of data
                    fixed_data(spike_start:spike_end) = NaN; % replace spike interval with NaNs
                end
                
                %if sList(s) == 179 || sList(s) == 180; fixed_data(end) = []; end % for some reason 1 sample too much for infos duration
                
                figure; plot(data);
                hold on; plot(fixed_data);
                
                newdata{1}.data = fixed_data;
                newdata{1}.header = pupildata{channel}.header;
                
                options.overwrite = 1;
                options.channel = channel;
                action = 'replace';
                pspm_write_channel(fixed_file,newdata,action,options); % overwrite old data
                
                clear data fixed_data
                close all
                
            end
            
            % If data still contains spikes after removing the most obvious
            % downward zero spikes
            [~,~,pupildata] = pspm_load_data(fixed_file);
            
            data = pupildata{channel}.data;
            data_mean = nanmean(data);
            data_std = nanstd(data);
            
            check_limit = data_mean+data_std*7;
            
            if sum(data>check_limit) || sum(data<-check_limit) > 0 % initial check for extreme data values
                
                fixed_data = data;
                
                sr = pupildata{channel}.header.sr;
                sr_divided = sr/2; % 0.5 s
                window_length = ceil(length(data)/sr_divided);
                window_ind = 1:sr_divided;
                median_interval = sr/2-sr/2/2; % 1 s total interval including the window itself
                spike_limit_perc = 0.15; % percentage of median +- median as spike definition
                
                % Go through data in windows of 1 second
                for window = 1:window_length
                    
                    if window_ind(end) > length(data)
                        difference = window_ind(end)-length(data);
                        window_ind = window_ind(1:end-difference);
                    end
                    
                    big_window_start = window_ind(1)-median_interval;
                    big_window_end = window_ind(end)+median_interval;
                    if big_window_start < 1; big_window_start = 1; end
                    if big_window_end > length(data); big_window_end = length(data); end
                    window_median = nanmedian(fixed_data(big_window_start:big_window_end));
                    spike_limit_up = window_median+window_median*spike_limit_perc;
                    spike_limit_down = window_median-window_median*spike_limit_perc;
                    
                    spikes = (fixed_data(window_ind) > spike_limit_up) | (fixed_data(window_ind) < spike_limit_down); % find approximate remaining spike data
                    spike_ind = find(spikes); % find spike indices
                    
                    spikes_all(window) = length(spike_ind);
                    
                    for spike = 1:length(spike_ind)
                        spike_start = spike_ind(spike)-10; % 60 samples before
                        spike_end = spike_ind(spike)+10; % 60 samples after
                        if spike_start < 1; spike_start = 1; end % avoid indices before the start of data
                        if spike_end > length(window_ind); spike_end = length(window_ind); end % avoid indices after the end of data
                        fixed_window = fixed_data(window_ind);
                        fixed_window(spike_start:spike_end) = NaN;
                        fixed_data(window_ind) = fixed_window; % replace spike interval with NaNs
                    end
                    
                    window_ind = window_ind + sr_divided;
                    
                end
                
                if sum(spikes_all) > 0
                    figure; plot(data);
                    hold on; plot(fixed_data);
                    
                    newdata{1}.data = fixed_data;
                    newdata{1}.header = pupildata{channel}.header;
                    
                    options.overwrite = 1;
                    options.channel = channel;
                    action = 'replace';
                    pspm_write_channel(fixed_file,newdata,action,options); % overwrite old data
                end
                
                clear data fixed_data
                close all
                
            end
            
        end
        
    end
    
end

fprintf('---- \n')

end