function pupil_import_trim(opts,sessions)

for session = sessions
    
    sList = opts.psr.sList.sessions{session};
    
    for sIDX = 1:length(sList)
        
        s_id = sList(sIDX);
        
        if sList(sIDX) < 100
            fname       = fullfile(opts.psr.path.raw,['ss7b_0' num2str(s_id) '_eyelink_' opts.psr.sessionlist{session} '.asc']); % Original
            fnamepsr    = fullfile(opts.psr.path.import,['pspm_ss7b_0' num2str(s_id) '_eyelink_' opts.psr.sessionlist{session} '.mat']); % Imported
            fnamepsrt   = fullfile(opts.psr.path.trim,['tpspm_ss7b_0' num2str(s_id) '_eyelink_' opts.psr.sessionlist{session} '.mat']); % Trimmed
        else
            fname       = fullfile(opts.psr.path.raw,['ss7b_' num2str(s_id) '_eyelink_' opts.psr.sessionlist{session} '.asc']);
            fnamepsr    = fullfile(opts.psr.path.import,['pspm_ss7b_' num2str(s_id) '_eyelink_' opts.psr.sessionlist{session} '.mat']);
            fnamepsrt   = fullfile(opts.psr.path.trim,['tpspm_ss7b_' num2str(s_id) '_eyelink_' opts.psr.sessionlist{session} '.mat']);
        end
        
        if exist(fname,'file') && ~exist(fnamepsr,'file')
            
            datatype = 'eyelink';
            
            % some subjects had eye accidentally measured as left side in
            % EyeLink (but actually the right eye)
            if ismember(s_id,opts.psr.sList.left)
                import{opts.psr.channel.pupil}.type = 'pupil_l';
                import{opts.psr.channel.pupil}.eyelink_trackdist = opts.psr.distance.eyetracker;
                import{opts.psr.channel.pupil}.distance_unit = opts.psr.distance_unit;
                import{opts.psr.channel.gaze_x}.type = 'gaze_x_l';
                import{opts.psr.channel.gaze_y}.type = 'gaze_y_l';
                import{opts.psr.channel.marker}.type = 'marker';
            else
                import{opts.psr.channel.pupil}.type = 'pupil_r';
                import{opts.psr.channel.pupil}.eyelink_trackdist = opts.psr.distance.eyetracker;
                import{opts.psr.channel.pupil}.distance_unit = opts.psr.distance_unit;
                import{opts.psr.channel.gaze_x}.type = 'gaze_x_r';
                import{opts.psr.channel.gaze_y}.type = 'gaze_y_r';
                import{opts.psr.channel.marker}.type = 'marker';
            end
            
            options.overwrite = true;
            
            imported_file = pspm_import(fname, datatype, import, options);
            
            % Rename file with SCR only, moving it to a new folder
            pspm_ren(imported_file{:}, fnamepsr);
            
        end
        
        if exist(fnamepsr,'file') && ~exist(fnamepsrt,'file')
            
            %% Check markers
            
            [~, ~, markerdata] = pspm_load_data(fnamepsr,'events');
            
            % markers from EyeLink
            markerCS = markerdata{1}.data;
            
            % remove extra markers
            marker_names = markerdata{1}.markerinfo.name;
            marker_values = markerdata{1}.markerinfo.value;
            
            markers_to_delete = ~contains(marker_names,'CS');
            markerCS(markers_to_delete) = [];
            marker_names(markers_to_delete) = [];
            marker_values(markers_to_delete) = [];
            
            newdata{1}.data = markerCS;
            newdata{1}.header = markerdata{1}.header;
            newdata{1}.markerinfo.name = marker_names;
            newdata{1}.markerinfo.value = marker_values;
            options.channel = opts.psr.channel.marker;
            pspm_write_channel(fnamepsr,newdata,'replace',options)
            
            if (session == 1 || session == 3) && length(markerCS) ~= opts.psr.trials
                sprintf('Wrong number of EyeLink markers, %d! Subject %s, Session %d', length(markerCS), num2str(s_id), session)
            end
            
            %% Trim
            clear options
            options.overwrite = true;
            trimmedfn = pspm_trim(fnamepsr,opts.psr.trimstart,opts.psr.trimend,'marker',options);
            
            pspm_ren(trimmedfn, fnamepsrt);
            
        end
        
    end
    
end

end