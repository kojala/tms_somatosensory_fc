function pupil_validtrials(opts,sessions)

% Loop over sessions
for ses = sessions
    
    sList = opts.psr.sList.sessions{ses};
    
    %% Segment extraction and finding valid trials
    for sIDX = 1:length(sList) % Loop over subjects
        
        s_id = sList(sIDX);
        
        % Data file
        if sList(sIDX) < 100
            fixed_file  = fullfile(opts.psr.path.valfix,['valfix_tpspm_ss7b_0' num2str(s_id) '_eyelink_' opts.psr.sessionlist{ses} '.mat']); % Valid fixations
            eventfile = fullfile(opts.bhv.path,['ss7b_0' num2str(s_id) '_behav_' opts.psr.sessionlist{ses} '.mat']);
        else
            fixed_file  = fullfile(opts.psr.path.valfix,['valfix_tpspm_ss7b_' num2str(s_id) '_eyelink_' opts.psr.sessionlist{ses} '.mat']); % Valid fixations
            eventfile = fullfile(opts.bhv.path,['ss7b_' num2str(s_id) '_behav_' opts.psr.sessionlist{ses} '.mat']);
        end
        
        %% Load file with valid fixations remaining (invalid: NaN)
        if exist(fixed_file,'file')
            
            % Split valfix file to sessions
            splitoptions.overwrite = 1;
            splitoptions.splitpoints = opts.psr.split_sessions_markers{ses}; % split points for blocks
            splitoptions.prefix = opts.psr.trimstart;
            splitoptions.suffix = opts.psr.trimend;
            splitfiles = pspm_split_sessions(fixed_file,opts.psr.channel.marker,splitoptions);
            
            %timing.names = {'CSs1'; 'CSs2'; 'CSc1'; 'CSc2'};
            timing.names = {'CS+';'CS-'};
            options.timeunit = 'seconds';
            if ses == 2
                options.length = 3; % only CS to US interval due to startle artefacts in retention
            else
                options.length = opts.psr.trialtime; % Total time for trial
            end
            options.plot = 0;
            options.overwrite = 1;
            options.marker_chan = opts.psr.channel.marker;
            
            sub_validperc = [];
            
            for splitfile = 1:length(splitfiles) % Loop over runs within a session
                
                splitfn = splitfiles{splitfile};
                [~,name,ext] = fileparts(splitfn);
                options.outputfile = fullfile(opts.psr.path.valfix, ['seg_' name ext]); % output to file
                if splitfile < length(splitfiles); splitpoint = splitoptions.splitpoints(splitfile); else; splitpoint = splitoptions.splitpoints(splitfile-1)+opts.psr.trialsperblock(ses); end
                onsets = physio_get_onsets(ses,splitfn,eventfile,splitpoint); % get onsets for trials
                timing.onsets = onsets;
                
                datachan = opts.psr.channel.valfix;
                [~, nan_segments] = pspm_extract_segments('manual', splitfn, datachan, timing, options);
                
                all_trials_nan = [];
                for cond = 1:length(nan_segments.segments)
                    all_trials_nan = [all_trials_nan nan_segments.segments{cond}.trial_nan_percent];
                end

                % Save NaN %
                nan_perc_trials = all_trials_nan;
                
                % Save valid trial
                sub_validperc(splitfile) = mean(nan_perc_trials <= (1-opts.psr.minpercdata)*100);
                
            end
            
            %% Check valid trials and exclude/include sub
            % See if subject has enough data
            validruns = find(sub_validperc >= opts.psr.minpercdata);
            
            validsubs(ses).session(sIDX,1) = sList(sIDX);
            
            for block = 1:length(sub_validperc)
                if ismember(block,validruns) % enough data to include (at least 25% data remaining overall)
                    validsubs(ses).session(sIDX,block+1) = 1;
                else % not enough data
                    validsubs(ses).session(sIDX,block+1) = 0;
                end
            end
            validsubs(ses).session(sIDX,block+2) = sum(validruns>0);
            
            fprintf(['Subject ' num2str(sList(sIDX)) ' Session ' num2str(ses) '\n'])
            fprintf(['Valid runs: ' num2str(validruns) '\n'])
            
        end
        
    end
    
    save(fullfile(opts.psr.path.main,[opts.psr.validtrials.validrunsfn '_' opts.psr.sessionlist{ses} '.mat']),'validsubs')
    
    fprintf('---- \n')
   
end

end