function pupil_preprocess_trace(opts,sessions)

for ses = sessions
    
    for group = 1:2
        
        if group == 1
            sList = opts.psr.sList.GLM_control{ses};
        else
            sList = opts.psr.sList.GLM_experimental{ses};
        end
        
        %pupil_alldata_USp = NaN(41,samples,3,length(sList));
        %pupil_alldata_USm = NaN(41,samples,3,length(sList));
        %pupil_alldata_CS = NaN(samples,4,length(sList));
        
        for sIDX = 1:numel(sList)
            
            s_id = num2str(sList(sIDX));
            
            if opts.psr.plot.valfix
                pupilchan = opts.psr.channel.valfix;
                if sList(sIDX) < 100
                    pupilfile  = fullfile(opts.psr.path.valfix,['valfix_tpspm_ss7b_0' num2str(s_id) '_eyelink_' opts.psr.sessionlist{ses} '.mat']); % Valid fixations
                    eventfile = fullfile(opts.bhv.path,['ss7b_0' num2str(s_id) '_behav_' opts.psr.sessionlist{ses} '.mat']);
                else
                    pupilfile  = fullfile(opts.psr.path.valfix,['valfix_tpspm_ss7b_' num2str(s_id) '_eyelink_' opts.psr.sessionlist{ses} '.mat']); % Valid fixations
                    eventfile = fullfile(opts.bhv.path,['ss7b_' num2str(s_id) '_behav_' opts.psr.sessionlist{ses} '.mat']);
                end
            else
                pupilchan = opts.psr.channel.pupil;
                if sList(sIDX) < 100
                    pupilfile  = fullfile(opts.psr.path.trim,['tpspm_ss7b_0' num2str(s_id) '_eyelink_' opts.psr.sessionlist{ses} '.mat']);
                    eventfile = fullfile(opts.bhv.path,['ss7b_0' num2str(s_id) '_behav_' opts.psr.sessionlist{ses} '.mat']);
                else
                    pupilfile  = fullfile(opts.psr.path.trim,['tpspm_ss7b_' num2str(s_id) '_eyelink_' opts.psr.sessionlist{ses} '.mat']);
                    eventfile = fullfile(opts.bhv.path,['ss7b_' num2str(s_id) '_behav_' opts.psr.sessionlist{ses} '.mat']);
                end
            end
            
            if exist(pupilfile,'file')
                
                [~, ~, pupildata] = pspm_load_data(pupilfile);
                
                markers = pupildata{opts.psr.channel.marker}.data;
                sr = pupildata{pupilchan}.header.sr;
                
                %% Interpolate
                options.extrapolate = 1;
                
                [~, pupil_interp] = pspm_interpolate(pupildata{pupilchan}.data,options);
                
                %% Filtering
                filter = [];
                filter.sr = sr;
%                 filter.lpfreq = 25; 
%                 filter.lporder = 1;
%                 filter.hpfreq = 'none';
%                 filter.hporder = nan;
%                 filter.direction = 'uni';
%                 filter.down = opts.psr.plot.sr;
                filter.lpfreq = 50;
                filter.lporder = 1;
                filter.hpfreq = 'none';
                filter.hporder = nan;
                filter.direction = 'bi';
                filter.down = opts.psr.glm.sr;
                [~, pupil, sr] = pspm_prepdata(pupil_interp, filter);
                
                samples = opts.psr.plot.trialtime*sr;
                %             pupil = pupil_interp;
                
                %pupil = pupildata{pupilchan}.data;
                
                %% Retrieve markers from Cogent data
                
                trials2take = 1:opts.psr.trials2take(ses); % which trials are included
                
                bhvdata = load(eventfile);
                US = bhvdata.Data(trials2take,4);
                CSPrediction = bhvdata.Data(trials2take,3);
                CSComplexity = bhvdata.Data(trials2take,6);
                
                CS = nan(length(US),1);
                CS(CSPrediction == 1 & CSComplexity == 0) = 1; % Simple CS+
                CS(CSPrediction == 2 & CSComplexity == 0) = 2; % Simple CS-
                CS(CSPrediction == 1 & CSComplexity == 1) = 3; % Complex CS+
                CS(CSPrediction == 2 & CSComplexity == 1) = 4; % Complex CS-
                
                for iTrl = 1:opts.psr.trials(ses)
                    trlonset = markers(iTrl);
                    trldata  = pupil(floor(trlonset*sr) + (1:samples));
                    trldata = trldata - trldata(1); % baseline correction, 1st timepoint as baseline
                    pupil_subdata(iTrl,:) = trldata;
                end
                
                pupil_subdata_incl = pupil_subdata(trials2take,:);
                
                % For plotting purposes, separating the CS-US conditions
                if ses == 1 || ses == 3 % only acquisition and retest
                    CSslot = 1;
                    for CStype = [1 3] % US+ possible only for CS+ trials
                        trials = CS == CStype & US == 1;
                        pupil_alldata_USp(1:sum(trials),:,CSslot,sIDX) = pupil_subdata_incl(trials,:);
                        CSslot = CSslot + 1;
                    end
                else
                    pupil_alldata_USp = NaN;
                end
                
                for CStype = 1:length(unique(CS))
                    trials = CS == CStype & US == 0;
                    pupil_alldata_USm(1:sum(trials),:,CStype,sIDX) = pupil_subdata_incl(trials,:);
                    no_CSp_trials = opts.psr.trials/opts.psr.blocks;
                    if sum(trials) == no_CSp_trials % if 12 trials (CS+US-) instead of 24 (CS-), replace the extra zeros in matrix with NaN
                        pupil_alldata_USm(no_CSp_trials+1:no_CSp_trials*2,:,CStype,sIDX) = NaN;
                    end
                end
                
                % For R to create ANOVA data for permutation test
                for CStype = 1:length(unique(CS))
                    trialdata = pupil_subdata_incl(CS == CStype,:);
                    pupil_alldata_CS(:,CStype,sIDX) = nanmean(trialdata); % take mean over trials of each type within the subject
                end
                
            end
        end
        
        % Save data for plotting in Matlab
        if length(trials2take) == opts.psr.trials(ses)
            preproc_file = fullfile(opts.psr.path.main,['pupil_preproc_' opts.psr.sessionlist{ses} '_' opts.psr.groupnames{group} '.mat']);
            save(preproc_file,'pupil_alldata_USp','pupil_alldata_USm','sr')
        else
            preproc_file = fullfile(opts.psr.path.main,['pupil_preproc_' opts.psr.sessionlist{ses} '_' opts.psr.groupnames{group} '_' num2str(length(trials2take)) 'trials.mat']);
            save(preproc_file,'pupil_alldata_USp','pupil_alldata_USm','sr')
        end
        
        % Save data for permuted ANOVA tests in R
        if length(trials2take) == opts.psr.trials(ses)
            preproc_file_R = fullfile(opts.psr.path.main,['pupil_preproc_ses' num2str(ses) '_' opts.psr.groupnames{group} '_R.mat']);
            save(preproc_file_R,'pupil_alldata_CS')
        else
            preproc_file_R = fullfile(opts.psr.path.main,['pupil_preproc_ses' num2str(ses) '_' opts.psr.groupnames{group} '_' num2str(length(trials2take)) 'trials_R.mat']);
            save(preproc_file_R,'pupil_alldata_CS')
        end
        
    end
end

end