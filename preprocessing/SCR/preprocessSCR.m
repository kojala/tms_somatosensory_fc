function preprocessSCR(opts,sessions)

% Visual inspection of SCR for CS+ (US+ and US-) and CS- of acquisition.
% Steps:
%  1. Check availability of file
%  2. Import LabChart file with PsPM
%  3. Find markers and create marker channel manually
%  4. Trim the file
%  5. Find artefacts in the data and change datapoint to NaN
%  6. Save artefact removed data
%__________________________________________________________________________

artefact_on = opts.scr.artefact_on;
newsubs = opts.scr.newsubs;

sesListSCR = {'acquisition','retention','retest','training1'};

for session = sessions
    
    if newsubs
        sList = intersect(opts.scr.sList.sessions{session},[46]);
    else
        sList = opts.scr.sList.sessions{session};
    end
    
    % Define artefacts file
    if artefact_on
        if newsubs
            artefact_file = fullfile(opts.expPath,['scr_markedartefacts_' sesListSCR{session} '_newsubs4.mat']);
        else
            artefact_file = fullfile(opts.expPath,['scr_markedartefacts_' sesListSCR{session} '.mat']);
        end
        
        if ~exist(artefact_file,'file')
            y_artefacts = cell(length(sList),2);
        else
            artefacts = load(artefact_file); y_artefacts = artefacts.y_artefacts;
        end
    end
    
    for sIDX = 1:numel(sList)
        
        % File names
        if sList(sIDX) < 100
            fname      = fullfile(opts.scr.path.raw,['ss7b_0' num2str(sList(sIDX)) '_physio_' sesListSCR{session} '.mat']); % Original
            fnamescr   = fullfile(opts.scr.path.import,['pspm_ss7b_0' num2str(sList(sIDX)) '_physio_' sesListSCR{session} '_SCR.mat']); % Imported
            fnamescrt  = fullfile(opts.scr.path.trim,['tpspm_ss7b_0' num2str(sList(sIDX)) '_physio_' sesListSCR{session} '_SCR.mat']); % Trimmed
            fnamescrta = fullfile(opts.scr.path.artcorr,['atpspm_ss7b_0' num2str(sList(sIDX)) '_physio_' sesListSCR{session} '_SCR.mat']); % Artefact corrected
        else
            fname      = fullfile(opts.scr.path.raw,['ss7b_' num2str(sList(sIDX)) '_physio_' sesListSCR{session} '.mat']);
            fnamescr   = fullfile(opts.scr.path.import,['pspm_ss7b_' num2str(sList(sIDX)) '_physio_' sesListSCR{session} '_SCR.mat']);
            fnamescrt  = fullfile(opts.scr.path.trim,['tpspm_ss7b_' num2str(sList(sIDX)) '_physio_' sesListSCR{session} '_SCR.mat']);
            fnamescrta = fullfile(opts.scr.path.artcorr,['atpspm_ss7b_' num2str(sList(sIDX)) '_physio_' sesListSCR{session} '_SCR.mat']);
        end
        
        if exist(fname,'file') && ~exist(fnamescr,'file')
            
            % Import raw LabChart files
            %----------------------------------------------------------
            datatype = 'labchartmat_in';
            import{1}.type = 'scr';
            import{1}.channel = 1;
            import{1}.transfer = 'none';
            import{2}.type = 'custom';
            import{2}.channel = 2;
            options.overwrite = 1;
            imported_files = pspm_import(fname, datatype, import, options);
            
            % Some subjects might have split data due to a break in the
            % labchart file, need to check for and fix that
            if size(imported_files,2) > 1
                importedfn = physio_fix_splitdata(imported_files);
            else
                importedfn = imported_files{:};
            end
            
            % Rename file with SCR only, moving it to a new folder
            pspm_ren(importedfn, fnamescr);
            
        end
        
        if exist(fnamescr,'file') && ~exist(fnamescrt,'file')
            % Find markers and add marker channel manually
            %----------------------------------------------------------
            clear data infos markers foundMarker newdata options
            [~,~,data] = pspm_load_data(fnamescr);
            
            if length(data) < 3 % only if marker channel has not been created yet
                
                if session == 1 || session == 3 % acquisition and retest TTL channel triggers
                    
                    markernum = physio_custom_markers(fnamescr,data);
                    if markernum ~= 96; fprintf('\nWrong number of markers! Subject %03.0f ... ', sList(sIDX)); end
                    
                elseif session == 2  % retention sound triggers
                    
                    markernum = physio_sound_markers(fnamescr,sList(sIDX));
                    if markernum ~= 24; fprintf('\nWrong number of markers! Subject %03.0f ... ', sList(sIDX)); end
                    % Remember that 1st marker is the startle probe, not an actual trial
                    
                elseif session == 4 % training
                    
                    markernum = physio_custom_markers(fnamescr,data);
                    if markernum ~= 24; fprintf('\nWrong number of markers! Subject %03.0f ... ', sList(sIDX)); end
                    
                end
            end
            
            % Trim the imported pspm file
            clear options
            options.overwrite = true;
            trimmedfn = pspm_trim(fnamescr,opts.scr.trimstart,opts.scr.trimend,'marker',options);
            
            pspm_ren(trimmedfn, fnamescrt);
            
        end
        
        if exist(fnamescrt,'file') && artefact_on && ~exist(fnamescrta,'file')
            
            clear data
            [~,~,data] = pspm_load_data(fnamescrt);
            
            % Remove artefacts
            %------------------------------------------------------------------
            
            y  = data{1,1}.data;
            
            if exist(artefact_file,'file') % edit existing file
                artefacts = load(artefact_file);
                y_artefacts = artefacts.y_artefacts;
            end
            
            if isempty(y_artefacts{sIDX,2})
                y_artefacts{sIDX,1} = sList(sIDX);
                y_artefacts{sIDX,2} = pspm_data_editor(y);
            end
            
            save(artefact_file,'y_artefacts');
            
            % Correct artefacts and save data file if does not exist yet
            if ~isempty(y_artefacts{sIDX,2})
                
                y_edited = y;
                
                % Change values in the marked artefact epochs to NaN
                for i=1:size(y_artefacts{sIDX,2},1)
                    y_edited(y_artefacts{sIDX,2}(i,1):y_artefacts{sIDX,2}(i,2)) = NaN;
                end
                
                % Plot unprocessed SCR data
                figure,plot(y,'b'); title(['Subject ' int2str(sList(sIDX))])
                % Plot artefact removed signal
                figure,plot(y_edited,'r'); title(['Subject ' int2str(sList(sIDX))])
                
                % Save artefact corrected data
                newdata = data;
                newdata{1,1}.data = y_edited;
                
                copyfile(fnamescrt,fnamescrta);
                pspm_write_channel(fnamescrta,newdata,'replace')
            
            else
                
                % If no artefacts to correct, save old data with new name
                copyfile(fnamescrt,fnamescrta);
%                 [~,newname,ext] = fileparts(fnamescrta);
%                 pspm_ren(fnamescrta, newname);
                
            end
            
        end
        
    end
    
end

end