function SCR_preprocess(opts,sessions)

trials2take = opts.scr.trials2take; % which trials are included
                
for ses = sessions
    
    for group = 1:2
        
        clear scr_subdata scr_alldata_USp scr_alldata_USm scr_alldata_CS
        
        if group == 1
            sList = opts.scr.sList.DCM_control{ses};
        else
            sList = opts.scr.sList.DCM_experimental{ses};
        end
        
        for sIDX = 1:numel(sList)
            
            s_id = num2str(sList(sIDX));
            datachan = opts.scr.channel.scr;
            
            fname_mod = 'atpspm';
            modname = 'SCR';
            
            if sList(sIDX) < 100
                filename   = fullfile(opts.scr.path.artcorr, [fname_mod '_ss7b_0' s_id '_physio_' opts.sessionlist{ses} '_' modname '.mat']);
                eventfile = fullfile(opts.bhv.path,['ss7b_0' s_id '_behav_' opts.sessionlist{ses} '.mat']);
            else
                filename   = fullfile(opts.scr.path.artcorr, [fname_mod '_ss7b_' s_id '_physio_' opts.sessionlist{ses} '_'  modname '.mat']);
                eventfile = fullfile(opts.bhv.path,['ss7b_' s_id '_behav_' opts.sessionlist{ses} '.mat']);
            end
            
            if exist(filename,'file')
                
                [~, ~, data] = pspm_load_data(filename);
                
                markers = data{opts.scr.channel.marker}.data;
                sr = data{datachan}.header.sr;
                
                %% Interpolate
                options.extrapolate = 1;
                
                [~, scr_interp] = pspm_interpolate(data{datachan}.data,options);
                
                %% Filtering
                filter = struct('sr', sr, 'lpfreq', 5, 'lporder', 1,  ...  
                'hpfreq', 0.0159, 'hporder', 1, 'down', opts.scr.plot.sr, ...
                'direction', 'bi');
%                 filter.down = opts.psr.plot.sr;
                [~, scr, sr] = pspm_prepdata(scr_interp, filter);
                
                scr = scr*opts.scr.datamultiplier;
                samples = opts.scr.plot.trialtime*sr;

                %% Retrieve markers from Cogent data
                
                bhvdata = load(eventfile);
                US = bhvdata.Data(trials2take,4);
                CSPrediction = bhvdata.Data(trials2take,3);
                CSComplexity = bhvdata.Data(trials2take,6);
                
                CS = nan(length(US),1);
                CS(CSPrediction == 1 & CSComplexity == 0) = 1; % Simple CS+
                CS(CSPrediction == 2 & CSComplexity == 0) = 2; % Simple CS-
                CS(CSPrediction == 1 & CSComplexity == 1) = 3; % Complex CS+
                CS(CSPrediction == 2 & CSComplexity == 1) = 4; % Complex CS-
                
                % Fix missing marker
                if (sList(sIDX) == 98) && (length(markers) < opts.scr.trials)
                    missing_marker = markers(90)+(bhvdata.Data(91,8)-bhvdata.Data(90,8));
                    newmarkers = [markers(1:90); missing_marker; markers(91:end)];
                    markers = newmarkers;
                end
                
                for iTrl = 1:opts.scr.trials
                    trlonset = markers(iTrl);
                    trldata  = scr(floor(trlonset*sr) + (1:samples));
                    tsrlonset_sample = floor(trlonset*sr);
                    baseline = nanmean(scr(tsrlonset_sample-(1*sr):tsrlonset_sample));
                    trldata = trldata - baseline; % baseline correction, 1st timepoint as baseline
                    scr_subdata(iTrl,:) = trldata;
                end
                
                scr_subdata_incl = scr_subdata(trials2take,:);
                
                % For plotting purposes, separating the CS-US conditions
                CSslot = 1;
                for CStype = [1 3] % US+ possible only for CS+ trials
                    trials = CS == CStype & US == 1;
                    scr_alldata_USp(1:sum(trials),:,CSslot,sIDX) = scr_subdata_incl(trials,:);
                    CSslot = CSslot + 1;
                end
                
                for CStype = 1:length(unique(CS))
                    trials = CS == CStype & US == 0;
                    scr_alldata_USm(1:sum(trials),:,CStype,sIDX) = scr_subdata_incl(trials,:);
                    no_CSp_trials = opts.scr.trials/opts.scr.blocks(ses);
                    if sum(trials) == no_CSp_trials % if 12 trials (CS+US-) instead of 24 (CS-), replace the extra zeros in matrix with NaN
                        scr_alldata_USm(no_CSp_trials+1:no_CSp_trials*2,:,CStype,sIDX) = NaN;
                    end
                end
                
                % Condition-wise mean
                for CStype = 1:length(unique(CS))
                    trialdata = scr_subdata_incl(CS == CStype,:);
                    scr_alldata_CS(:,CStype,sIDX) = nanmean(trialdata); % take mean over trials of each type within the subject
                end
                
%                 if opts.scr.standardize == 1
%                     CSm_avg = nanmean(scr_alldata_USm(:,:,:,sIDX));
%                     CSm_avg = nanmean(CSm_avg,3);
%                     scr_alldata_USp(:,:,:,sIDX) = scr_alldata_USp(:,:,:,sIDX)./CSm_avg;
%                     scr_alldata_USm(:,:,:,sIDX) = scr_alldata_USm(:,:,:,sIDX)./CSm_avg;
%                     scr_alldata_CS(:,:,sIDX) = scr_alldata_CS(:,:,sIDX)./CSm_avg';
%                 end
                
            end
        end
        
        % Save data for plotting in Matlab
        if length(trials2take) == opts.scr.trials
            preproc_file = fullfile(opts.scr.path.main,['SCR_preproc_' opts.sessionlist{ses} '_' opts.groupnames{group} '.mat']);
            save(preproc_file,'scr_alldata_USp','scr_alldata_USm','sr')
        else
            preproc_file = fullfile(opts.scr.path.main,['SCR_preproc_' opts.sessionlist{ses} '_' opts.groupnames{group} '_' num2str(length(trials2take)) 'trials.mat']);
            save(preproc_file,'scr_alldata_USp','scr_alldata_USm','sr')
        end
        
        % Save data for permuted ANOVA tests in R
        if length(trials2take) == opts.scr.trials
            preproc_file_R = fullfile(opts.scr.path.main,['SCR_preproc_ses' num2str(ses) '_' opts.groupnames{group} '_R.mat']);
            save(preproc_file_R,'scr_alldata_CS')
        else
            preproc_file_R = fullfile(opts.scr.path.main,['SCR_preproc_ses' num2str(ses) '_' opts.groupnames{group} '_' num2str(length(trials2take)) 'trials_R.mat']);
            save(preproc_file_R,'scr_alldata_CS')
        end
        
    end
end

end