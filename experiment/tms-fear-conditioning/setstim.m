% SETSTIM, script to set pain threshold
%
% Two calibration procedures:
% 1. Shock intensity can be modified by the Experimenter in ascending
% order.
% 2. Delivers shocks in random order. User is asked to provide pain
% evaluation.
%
% After the random procedure, a regression result for the estimates is
% shown.
%_________________________________________________________________________
% based on AEC 3 script, Dominik R Bach 21.01.2013

clear all;
addpath(fullfile(pwd,'utils'));
addpath('D:\labuser\Documents\MATLAB\@ParallelPort\private')

config_io
address = hex2dec('DF98');
outp(address,0);
TTL.shock = 2^0;

%enter subject name
name=input('Subject number? ','s');
outfile = fullfile(pwd,'data','pain',['pain_', name, '.mat']);
[s,m]   = mkdir(fullfile(pwd,'data','pain'));
if exist(outfile,'file'), fullfile(pwd,'data','pain',['pain_', name, 'b.mat']); end;

rep = 1;
while rep == 1
    reps = input ('Repeat? (y/n, default = y)', 's');
    if ~isempty(reps)
        if reps == 'n'
            rep = 0;
        end;
    end;
    % first argument: frequency * 2
    if rep == 1, deliver (200, 0.5, TTL.shock); end;
end;

%set maximum current to be used, determined from the initial ramping procedure
high=input('enter maximum current ');

%generate a random sequence of current intensities to be used, ignoring
%very low values
for i=1:10
    level(i)=high/10*i;
end
range = level(4:10);
index = [randperm(length(range)) randperm(length(range))];

disp('Use the following currents:');
disp([1:14;range(index)]');

% collect data from subject
for j=1:length(index)
    current(j)=range(index(j));
    %give=current(j)
    disp(['Number: ' num2str(j) ' Current: ' num2str(current(j))]);
    %input('When ready, press any key followed by enter?','s')
    disp('Press any key when ready');
    pause;
    deliver(200,0.5,TTL.shock);
    dummy=['n'];
    while isempty (str2num(dummy))
        dummy=input('enter rating (in the range 0-100)  ', 's');
    end;
    rating(j, 1)=str2num(dummy);
end

data = [current', rating];

save(outfile, 'data');

%% find optimal value

try
    r = linspace(min(data(:,1))-1, max(data(:,1))+2,100);
    p = polyfit(data(:,1),data(:,2),1);
    f = polyval(p,r);
    upper = r(f>90);
    f2 = polyval(p,upper);
    
    [R,P,RLO,RUP]=corrcoef(data(:,1),data(:,2));
    disp([R(2,1) P(2,1)])
    try
        figure
        hold on
        line([min(r) max(r)], [90 90], 'Linestyle','--', 'Color','k')
        plot(data(:,1),data(:,2),'o',r,f,'-',r(f>90),f2,'dr')
        title(['I(90) = ' num2str(upper(1)) 'mA'])
        ylim([min(data(:,2)) 100])
        xlabel('mA')
        ylabel('evaluation')
    end
    
    %% use recommended shock
    
    disp(['Start with: I(90) = ' num2str(upper(1)) 'mA'])
end

rep = 1;
while rep == 1
    reps = input ('Repeat? (y/n, default = y)', 's');
    if ~isempty(reps)
        if reps == 'n'
            rep = 0;
        end;
    end;
    if rep == 1, deliver (200, 0.5, TTL.shock); end;
end;
