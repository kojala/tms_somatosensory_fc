function deliver(f,d,tb)
% function deliver(f,d,tb) uses PTB commands instead of cogent functions and
% works the way the shocks are delivered in present_visual_experiment
%
%_________________________________________________________________________
% (C) 2014 Matthias Staib (Zurich University Hospital for Psychiatry)
%
% v001 ms 28.05.2014 initial version

% example:
% f = 200;
% d = 0.5;
% tb = 2^0;

stimvec       = repmat([tb 0],1,f*d);
nshocks       = numel(stimvec);
shocktimes    = linspace(0,d,nshocks); % time of pulse
shock         = 0;                     % initializing incrementing counter
t0            = GetSecs;               % reference time
address       = hex2dec('DF98');

while shock < nshocks
    
    % send 1 pulse in time until nshocks were delivered
    if  GetSecs-t0 >= shocktimes(shock+1)
        shock = shock+1;
        outp(address,stimvec(shock)); % eneable pulse
    end
end