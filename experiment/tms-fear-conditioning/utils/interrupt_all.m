function interrupt_all

ShowCursor;
ListenChar(1);
sca;

try
    Eyelink('Stoprecording');
    Eyelink('CloseFile');
    Eyelink('Shutdown');
end