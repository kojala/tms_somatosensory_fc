function generate_noise(amp_fct)
% GENERATE_NOISE
%
% customize:
% - amplitude
% - duration
% - rise and fall duration
%
% 300ms of zero padding at the end to avoid speaker artefacts
%_________________________________________________________________________
% (C) 2013 Matthias Staib (Zurich University Hospital for Psychiatry)
%
% v001 ms 12.12.2013 initial version
% v002 ms 22.07.2015 included playback options

% check for loudness and sound pressure level: http://www.sengpielaudio.com/Rechner-pegelaenderung.htm
% dB +10  ->  factor 2 on loudness
% dB +20  ->  factor 2 on sound pressure level

tsound_list   = [.05 2] ;  % duration in seconds

for idx = 1:2
    
    tsound   = tsound_list(idx);  % duration in seconds
    tfade    = .002; % duration of rise and fall in seconds (see Blumenthal&Berg, 1986)
    
    % general signal parameters
    Fs         = 44100;        % sampling frequency (> twice max sound frequency, Nyquist)
    dt         = 1/Fs;
    t_sounds   = dt:dt:tsound;
    tpadding   = .5;           % zero padding at the end (in seconds)
    
    % prepare amplitude fade (rise and fall)
    ramp = linspace(0,1,tfade/dt);
    
    % Uniformly distributed pseudorandom numbers; use whole range from -1 to 1
    white=amp_fct*(rand(Fs*tsound,1)*2-1);
    
    % add rise and fall of amplitude to beginning and end
    tfade_offset = [ramp ones(1,numel(t_sounds) - 2*numel(ramp)) fliplr(ramp)];
    white = white'.*tfade_offset;
    
    % zero padding to avoid clicking sound error at the end of playback
    white = [white zeros(1,tpadding*Fs)];
    
    % save as audio file
            filename = ['white_noise_uniform_' num2str(tsound) 'sec_x' num2str(amp_fct) '.wav'];
    
    try % not available on some matlab distributions
        audiowrite(filename,white,Fs,'Title','white noise uniform');
        info = audioinfo(filename);
        disp(info)
    catch
        wavwrite(white,Fs,filename); % old matlab version
    end
    
    % plays sounds using different playback functions
    
    % Matlab built-in
    [y,Fs] = audioread(filename);
    player = audioplayer(y,Fs);
    play(player);
    
    pause(tsound_list(idx)+.5)
    
    % cogent option 1
    cgsound('open')
    m = wavread(filename);
    cgsound('matrixSND',10,m',44100)
    cgsound('play',10)
    
    pause(tsound_list(idx)+.5)
    
    % cogent option 2
    config_display; config_keyboard; config_sound(2, 16, 44100, 1)
        
    start_cogent
    wait(1000)
    loadsound(filename, 1);
    playsound(1);
    wait((tsound_list(idx)+.5)*1000)
    
    stop_cogent
    
end
