function Introduction_Texts_retention

% Get all base workspace variables into this function.
T = evalin('base','whos');
for ii = 1:length(T)
    C_ =  evalin('base',[T(ii).name ';']);
    eval([T(ii).name,'=C_;']);
end
clear T C_ ii

Screen('Preference', 'TextAntiAliasing', 2); % high quality text
Screen('TextSize', expWin, tsize_small);

% first
myText = ['Willkommen zum dritten und letzten Messtag.\n\n\n', ...
    'Zur Erinnerung zeigen wir Ihnen auf der n�chsten Seite noch einmal,\n\n', ...
    'bei welchen Mustern Sie LINKS oder RECHTS dr�cken m�ssen.'];
DrawFormattedText(expWin, myText, 'center', 'center', 255);
DrawFormattedText(expWin, 'weiter mit ENTER', 'center', sr(2)*.8, 255);
Screen('Flip', expWin);
KbWait([], 3); WaitSecs(0.3);


% second
b1 = header.som.order(1);
b2 = header.som.order(2);
Image1  = imread(sprintf('stim%02i.png',header.options.pattidx(b1)));
Imgtex1 = Screen('MakeTexture',expWin,Image1);
Image2  = imread(sprintf('stim%02i.png',header.options.pattidx(b2)));
Imgtex2 = Screen('MakeTexture',expWin,Image2);
yshift = 80;
myText = [ ...
    '\n\n\n\n' ...
    'PAAR 1:'];
Screen('DrawTexture',expWin,Imgtex1,[],[250 380-yshift 1250 530-yshift])
DrawFormattedText(expWin, [header.task.arrowlabels{header.task.order(1)} 'E Pfeiltaste'], 'center', 330-yshift, 255);
DrawFormattedText(expWin, 'Zeigefinger', 20, 400-yshift, 255);
DrawFormattedText(expWin, 'Mittelfinger', 20, 480-yshift, 255);
Screen('DrawTexture',expWin,Imgtex2,[],[250 650-yshift 1250 800-yshift])
DrawFormattedText(expWin, [header.task.arrowlabels{header.task.order(2)} 'E Pfeiltaste'], 'center', 600-yshift, 255);
DrawFormattedText(expWin, 'Zeigefinger', 20, 670-yshift, 255);
DrawFormattedText(expWin, 'Mittelfinger', 20, 750-yshift, 255);
DrawFormattedText(expWin, myText, 'center', 150-yshift, 255);
DrawFormattedText(expWin, 'weiter mit ENTER', 'center', sr(2)*.8, 255);
Screen('Flip', expWin);
KbWait([], 3); WaitSecs(0.3);


% third
b1 = header.som.order(3);
b2 = header.som.order(4);
Image1  = imread(sprintf('stim%02i.png',header.options.pattidx(b1)));
Imgtex1 = Screen('MakeTexture',expWin,Image1);
Image2  = imread(sprintf('stim%02i.png',header.options.pattidx(b2)));
Imgtex2 = Screen('MakeTexture',expWin,Image2);
yshift = 80;
myText = [ ...
    '\n\n\n\n' ...
    'PAAR 2:'];
Screen('DrawTexture',expWin,Imgtex1,[],[250 380-yshift 1250 530-yshift])
DrawFormattedText(expWin, [header.task.arrowlabels{header.task.order(1)} 'E Pfeiltaste'], 'center', 330-yshift, 255);
DrawFormattedText(expWin, 'Zeigefinger', 20, 400-yshift, 255);
DrawFormattedText(expWin, 'Mittelfinger', 20, 480-yshift, 255);
Screen('DrawTexture',expWin,Imgtex2,[],[250 650-yshift 1250 800-yshift])
DrawFormattedText(expWin, [header.task.arrowlabels{header.task.order(2)} 'E Pfeiltaste'], 'center', 600-yshift, 255);
DrawFormattedText(expWin, 'Zeigefinger', 20, 670-yshift, 255);
DrawFormattedText(expWin, 'Mittelfinger', 20, 750-yshift, 255);
DrawFormattedText(expWin, myText, 'center', 150-yshift, 255);
DrawFormattedText(expWin, 'weiter mit ENTER', 'center', sr(2)*.8, 255);
Screen('Flip', expWin);
KbWait([], 3); WaitSecs(0.3);


% forth
myText = [ ...
    'In den n�chsten vier (k�rzeren) Bl�cken treten zus�tzlich\n\n', ...
    'gelegentlich Ger�usche auf.\n\n\n\n', ...
    'Dr�cken Sie ENTER, um jetzt ein solches Ger�usch einmal zu h�ren.'];
DrawFormattedText(expWin, myText, 'center', 'center', 255);
DrawFormattedText(expWin, 'weiter mit ENTER, Aufgabe beginnt dann automatisch', 'center', sr(2)*.8, 255);
Screen('Flip', expWin);
KbWait([], 3); WaitSecs(0.3);

PsychPortAudio('Start', pahandle, 1, 0, 1);

WaitSecs(2);

% first
myText = [ ...
    'Sehr gut!\n\n', ...
    'Der weitere Ablauf ist genau wie in den letzten 8 Bl�cken gestern.'];
DrawFormattedText(expWin, myText, 'center', 'center', 255);
DrawFormattedText(expWin, 'Es geht automatisch weiter', 'center', sr(2)*.8, 255);
Screen('Flip', expWin);

WaitSecs(6);
