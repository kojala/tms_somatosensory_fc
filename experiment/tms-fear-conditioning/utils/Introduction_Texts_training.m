function Introduction_Texts_training

% Get all base workspace variables into this function.
T = evalin('base','whos');
for ii = 1:length(T)
    C_ =  evalin('base',[T(ii).name ';']);
    eval([T(ii).name,'=C_;']);
end
clear T C_ ii

Screen('Preference', 'TextAntiAliasing', 2); % high quality text
Screen('TextSize', expWin, tsize_small);

% set background color for the block
% Screen('FillRect', expWin, rectColor, baseRect);

b1 = header.som.order(1+2*(training==2));
b2 = header.som.order(2+2*(training==2));

Image1  = imread(sprintf('stim%02i.png',options.pattidx(b1)));
Imgtex1 = Screen('MakeTexture',expWin,Image1);
Image2  = imread(sprintf('stim%02i.png',options.pattidx(b2)));
Imgtex2 = Screen('MakeTexture',expWin,Image2);

% first
yshift = 80;
myText = [ ...
    'In diesem Teil des Trainings ist es Ihre Aufgabe,\n\nzwei Muster zu unterscheiden.\n\n', ...
    'Je nach Muster dr�cken Sie bitte entweder die Taste LINKS oder RECHTS.'];
Screen('DrawTexture',expWin,Imgtex1,[],[250 380-yshift 1250 530-yshift])
DrawFormattedText(expWin, [header.task.arrowlabels{header.task.order(1)} 'E Pfeiltaste'], 'center', 330-yshift, 255);
DrawFormattedText(expWin, 'Zeigefinger', 20, 400-yshift, 255);
DrawFormattedText(expWin, 'Mittelfinger', 20, 480-yshift, 255);
Screen('DrawTexture',expWin,Imgtex2,[],[250 650-yshift 1250 800-yshift])
DrawFormattedText(expWin, [header.task.arrowlabels{header.task.order(2)} 'E Pfeiltaste'], 'center', 600-yshift, 255);
DrawFormattedText(expWin, 'Zeigefinger', 20, 670-yshift, 255);
DrawFormattedText(expWin, 'Mittelfinger', 20, 750-yshift, 255);
DrawFormattedText(expWin, myText, 'center', 150-yshift, 255);
DrawFormattedText(expWin, 'weiter mit ENTER', 'center', sr(2)*.8, 255);
Screen('Flip', expWin);
KbWait([], 3); WaitSecs(0.3);


keyCode = zeros(1,500);
while ~keyCode(KbName('Return'))
    myText = [ ...
        'Testen Sie jetzt die verschiedenen Muster.\n\n', ...
        'Durch Dr�cken der LINKS oder RECHTS Taste\n\n', ...
        'wird Ihnen nun das jeweilige Muster pr�sentiert.\n\n', ...
        'Probieren Sie die verschiedenen Muster ein paarmal aus.'];
    Screen('DrawTexture',expWin,Imgtex1,[],[250 380-yshift 1250 530-yshift])
    DrawFormattedText(expWin, [header.task.arrowlabels{header.task.order(1)} 'E Pfeiltaste'], 'center', 330-yshift, 255);
    DrawFormattedText(expWin, 'Zeigefinger', 20, 400-yshift, 255);
    DrawFormattedText(expWin, 'Mittelfinger', 20, 480-yshift, 255);
    Screen('DrawTexture',expWin,Imgtex2,[],[250 650-yshift 1250 800-yshift])
    DrawFormattedText(expWin, [header.task.arrowlabels{header.task.order(2)} 'E Pfeiltaste'], 'center', 600-yshift, 255);
    DrawFormattedText(expWin, 'Zeigefinger', 20, 670-yshift, 255);
    DrawFormattedText(expWin, 'Mittelfinger', 20, 750-yshift, 255);
    DrawFormattedText(expWin, myText, 'center', 150-yshift, 255);
    DrawFormattedText(expWin, 'PFEILTASTE zum Abspielen, weiter mit ENTER', 'center', sr(2)*.8, 255);
    Screen('Flip', expWin);
    [~, keyCode, ~] = KbWait(); WaitSecs(0.3);
    
    
    if keyCode(header.key.arrows(header.task.order(1))), sidx=b1;
    elseif keyCode(header.key.arrows(header.task.order(2))), sidx=b2; else sidx = NaN; end
    % deliver stimulus
    if ~isnan(sidx)
        pattern  = Stimuli{sidx}.stims(2:end);
        tpattern = Stimuli{sidx}.tstims(2:end);
        pinsetup = Stimuli{sidx}.pinsetup;
        if trigger, outp(address,pinsetup); end; % reset port
        somcount = 0;
        ttrial = GetSecs+.5;
        % deliver stimulus
        while (GetSecs-ttrial) < 4
            if  somcount<length(tpattern) && GetSecs-ttrial >= tpattern(somcount+1)
                somcount = somcount+1;
                if trigger, outp(address,pattern(somcount)); end;
            end
        end
    end
end

% second
myText = ['Entscheiden Sie sich so schnell wie m�glich,\n\n' ...
    'auch wenn Sie nicht ganz sicher sind.\n\n\n\n' ...
    'Bitte beachten Sie:\n\n' ...
    'Wenn sich das + im Zentrum rot f�rbt, haben Sie\n\n' ...
    'eine falsche Taste gedr�ckt oder zu sp�t reagiert.\n\n'];
DrawFormattedText(expWin, myText, 'center', 'center', 255);
DrawFormattedText(expWin, 'Beginn der Aufgabe mit ENTER', 'center', sr(2)*.8, 255);
Screen('Flip', expWin);
KbWait([], 3);
