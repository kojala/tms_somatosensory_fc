% plays sounds using different playback functions
close all
clear
clc

filename = fullfile('F:\Experiment_ss5b','soundfiles','startle','white_noise_uniform_2sec_x0.037.wav');


%% Matlab built-in
[y,Fs] = audioread(filename);
player = audioplayer(y,Fs);

n = 0;

t0 = GetSecs;

while n ~= 50000
    n = n+1;
    
    if n == 10000
        play(player);
    end
    
    t1(n) = GetSecs-t0;
    
end

pause(2)
%%


config_display; config_keyboard; config_sound(2, 16, 44100, 1)

start_cogent
cgsound('open')
m = wavread(filename);
cgsound('matrixSND',10,m',44100)


n = 0;

t0 = GetSecs;

while n ~= 50000
    n = n+1;
    
    if n == 10000
        cgsound('play',10)
    end
    
    t2(n) = GetSecs-t0;
    
end

stop_cogent

pause(2)
%%

config_display; config_keyboard; config_sound(2, 16, 44100, 1)

start_cogent
loadsound(filename, 1);

n = 0;

t0 = GetSecs;

while n ~= 50000
    n = n+1;
    
    if n == 10000
        playsound(1);
    end
    
    t3(n) = GetSecs-t0;
    
end

stop_cogent

%%

figure, plot(diff(t1))
figure, plot(diff(t2))
figure, plot(diff(t3))