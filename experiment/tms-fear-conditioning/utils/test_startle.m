%% prepare startle

fnameST     = fullfile('D:\Users\03_Christian_Ruff_Group\MatthiasStaib\Experiment_ss7b\','soundfiles','startle','white_noise_uniform_2sec_x0.037.wav');
[wNoise,Fs] = audioread(fnameST);
nrchannels  = 2;     % stereo
sugLat      = 0.05;  % soundcard latency

InitializePsychSound; % Initialize Sounddriver
pahandle = PsychPortAudio('Open', [], [], 0, Fs, nrchannels, [], sugLat);
% load startle sound (white noise) to buffer
PsychPortAudio('FillBuffer', pahandle, [wNoise' ; wNoise']);

%% repeat this step

PsychPortAudio('Start', pahandle, 1, 0, 1);

%% close
PsychPortAudio('Close');