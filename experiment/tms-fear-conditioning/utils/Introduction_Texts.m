function Introduction_Texts

% Get all base workspace variables into this function.
T = evalin('base','whos');
for ii = 1:length(T)
    C_ =  evalin('base',[T(ii).name ';']);
    eval([T(ii).name,'=C_;']);
end
clear T C_ ii

Screen('Preference', 'TextAntiAliasing', 2); % high quality text
Screen('TextSize', expWin, tsize_small);

% first
myText = [ ...
    'In dieser Studie untersuchen wir die Wahrnehmung der Hand.\n\n', ...
    'Ihre Aufgabe ist es, so schnell wie m�glich auf\n\n' ...
    'verschiedene Muster zu reagieren. Dabei interessieren\n\n' ...
    'wir uns f�r den Effekt von elektrischer Stimulation auf Ihre\n\n' ...
    'Hautleitf�higkeit.\n\n\n\n', ...
    'Sie werden feststellen, dass nach bestimmten Mustern gelegentlich\n\n' ...
    'eine elektrische Stimulation folgt!'];
DrawFormattedText(expWin, myText, 'center', 'center', 255);
DrawFormattedText(expWin, 'weiter mit ENTER', 'center', sr(2)*.8, 255);
Screen('Flip', expWin);
KbWait([], 3); WaitSecs(0.3);

% second
myText = [ ...
    'Genau wie im Training, ist es Ihre Aufgabe,\n\njeweils zwei Muster voneinander zu unterscheiden.\n\n', ...
    'Je nach Muster dr�cken Sie bitte entweder die Taste LINKS oder RECHTS.\n\n\n\n' ...
    'In jedem der acht Bl�cke kommen entweder Muster aus PAAR 1 oder\n\n' ...
    'Muster aus PAAR 2 vor.\n\n\n\n' ...
    'Bitte pr�gen Sie sich die Paare auf den n�chsten Bildschirmen gut ein.'];
DrawFormattedText(expWin, myText, 'center', 'center', 255);
DrawFormattedText(expWin, 'weiter mit ENTER', 'center', sr(2)*.8, 255);
Screen('Flip', expWin);
KbWait([], 3); WaitSecs(0.3);


% third
b1 = header.som.order(1);
b2 = header.som.order(2);
Image1  = imread(sprintf('stim%02i.png',header.options.pattidx(b1)));
Imgtex1 = Screen('MakeTexture',expWin,Image1);
Image2  = imread(sprintf('stim%02i.png',header.options.pattidx(b2)));
Imgtex2 = Screen('MakeTexture',expWin,Image2);
yshift = 80;
myText = [ ...
    '\n\n\n\n' ...
    'PAAR 1:'];
Screen('DrawTexture',expWin,Imgtex1,[],[250 380-yshift 1250 530-yshift])
DrawFormattedText(expWin, [header.task.arrowlabels{header.task.order(1)} 'E Pfeiltaste'], 'center', 330-yshift, 255);
DrawFormattedText(expWin, 'Zeigefinger', 20, 400-yshift, 255);
DrawFormattedText(expWin, 'Mittelfinger', 20, 480-yshift, 255);
Screen('DrawTexture',expWin,Imgtex2,[],[250 650-yshift 1250 800-yshift])
DrawFormattedText(expWin, [header.task.arrowlabels{header.task.order(2)} 'E Pfeiltaste'], 'center', 600-yshift, 255);
DrawFormattedText(expWin, 'Zeigefinger', 20, 670-yshift, 255);
DrawFormattedText(expWin, 'Mittelfinger', 20, 750-yshift, 255);
DrawFormattedText(expWin, myText, 'center', 150-yshift, 255);
DrawFormattedText(expWin, 'weiter mit ENTER', 'center', sr(2)*.8, 255);
Screen('Flip', expWin);
KbWait([], 3); WaitSecs(0.3);


% forth
b1 = header.som.order(3);
b2 = header.som.order(4);
Image1  = imread(sprintf('stim%02i.png',header.options.pattidx(b1)));
Imgtex1 = Screen('MakeTexture',expWin,Image1);
Image2  = imread(sprintf('stim%02i.png',header.options.pattidx(b2)));
Imgtex2 = Screen('MakeTexture',expWin,Image2);
yshift = 80;
myText = [ ...
    '\n\n\n\n' ...
    'PAAR 2:'];
Screen('DrawTexture',expWin,Imgtex1,[],[250 380-yshift 1250 530-yshift])
DrawFormattedText(expWin, [header.task.arrowlabels{header.task.order(1)} 'E Pfeiltaste'], 'center', 330-yshift, 255);
DrawFormattedText(expWin, 'Zeigefinger', 20, 400-yshift, 255);
DrawFormattedText(expWin, 'Mittelfinger', 20, 480-yshift, 255);
Screen('DrawTexture',expWin,Imgtex2,[],[250 650-yshift 1250 800-yshift])
DrawFormattedText(expWin, [header.task.arrowlabels{header.task.order(2)} 'E Pfeiltaste'], 'center', 600-yshift, 255);
DrawFormattedText(expWin, 'Zeigefinger', 20, 670-yshift, 255);
DrawFormattedText(expWin, 'Mittelfinger', 20, 750-yshift, 255);
DrawFormattedText(expWin, myText, 'center', 150-yshift, 255);
DrawFormattedText(expWin, 'weiter mit ENTER', 'center', sr(2)*.8, 255);
Screen('Flip', expWin);
KbWait([], 3); WaitSecs(0.3);

% fifth
myText = ['Wenn sich das + im Zentrum rot f�rbt, haben Sie\n\n' ...
    'eine falsche Taste gedr�ckt oder zu sp�t reagiert.'];
DrawFormattedText(expWin, myText, 'center', 'center', 255);
DrawFormattedText(expWin, 'weiter mit ENTER', 'center', sr(2)*.8, 255);
Screen('Flip', expWin);
KbWait([], 3);

myText = ['Bitte beachten Sie:\n\n\n' ...
    'Ob ein Elektroshock auftritt h�ngt nicht davon ab,\n\n' ...
    'wie schnell oder korrekt sie antworten, sondern von dem Muster.'];
DrawFormattedText(expWin, myText, 'center', 'center', 255);
DrawFormattedText(expWin, 'Beginn der Aufgabe mit ENTER', 'center', sr(2)*.8, 255);
Screen('Flip', expWin);
KbWait([], 3);
