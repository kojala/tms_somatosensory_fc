function Introduction_Texts_retest

% Get all base workspace variables into this function.
T = evalin('base','whos');
for ii = 1:length(T)
    C_ =  evalin('base',[T(ii).name ';']);
    eval([T(ii).name,'=C_;']);
end
clear T C_ ii

Screen('Preference', 'TextAntiAliasing', 2); % high quality text
Screen('TextSize', expWin, tsize_small);

% first
myText = [ ...
    'Der letzte Abschnitt besteht aus 8 weiteren Bl�cken.\n\n', ...
    'Der Ablauf bleibt unver�ndert.\n\n\n\n', ...
    'In diesem Abschnitt sollten elektrische Stimulationen\n\n', ...
    'am Fuss klar sp�rbar sein.\n\n\n', ...
    'BITTE SETZEN SIE DIE KOPFH�RER VOR BEGINN AB!'];
DrawFormattedText(expWin, myText, 'center', 'center', 255);
DrawFormattedText(expWin, 'Beginn der Aufgabe mit ENTER', 'center', sr(2)*.8, 255);
Screen('Flip', expWin);
KbWait([], 3); WaitSecs(0.3);