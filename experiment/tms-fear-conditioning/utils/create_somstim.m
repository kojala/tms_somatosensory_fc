function S = create_somstim
% CREATE_SOMSTIM creates stimuli for somatosensory experiment
%
% Output
%  S:             structure containing details of n stimuli
%  S.stims{n}:    sequence (s) of pins that are sent via outp(address,s)
%  S.tstims{n}:   timing vector from 0 to stimduration with
%                 numel(S.tstims{n})/duration = play rate in Hz
%  S.descr{n}:    short descritpion of stimulus
%  S.pinsetup{n}: relay setup that should be initialized before stimulus
%                 is delivered, because relay update takes 10 milliseconds
%
%
% Manual:
%
% To deliver stimuli through a specific mapping from a digitimer to an
% output, set relay pins according to the table below and activate pin(s)
% for activation of digitimer(s).
%   Note: Do not reset pins to zero carelessly during stimulus delivery!
%
% Pin slots used:
%   (d = digitimer, r = relay, t = trigger, G = ground, US = pain)
%
%  _______________________________________
% ( -  -  -  -  r4 r3 r2 r1 d1 d2 t US  - )
%  \  G  G  -  -  -  -  -  -  -  -  -  - /
%   \___________________________________/
%
% referring to positions (i.e. exponents of 2)
%  _______________________________________
% ( -  -  -  -  7  6  5  4  3  2  1  0  - )
%  \ -  -  -  -  -  -  -  -  -  -  -  -  /
%   \___________________________________/
%
% Outputs are controlled either by d1 or d2, depending on relay
% status
%  ___________
% |           |
% | O3     O1 |
% |           |
% | O4     O2 |
% |___________|
%
% mapping from digitimer to outputs according to relay setup:
%
%      r1 | r2 | r3 | r4  ->  O1 | O2 | O3 | O4
%      ---|----|----|----    ----|----|----|----
%  1.   0 |  0 |  0 |  0      d1 | d1 | d1 | d1
%  2.   0 |  0 |  0 |  1      d2 | d1 | d1 | d1
%  3.   0 |  0 |  1 |  0      d1 | d2 | d1 | d1
%  4.   0 |  0 |  1 |  1      d2 | d2 | d1 | d1
%  5.   0 |  1 |  0 |  0      d1 | d1 | d2 | d1
%  6.   0 |  1 |  0 |  1      d2 | d1 | d2 | d1
%  7.   0 |  1 |  1 |  0      d1 | d2 | d2 | d1
%  8.   0 |  1 |  1 |  1      d2 | d2 | d2 | d1
%  9.   1 |  0 |  0 |  0      d1 | d1 | d1 | d2
% 10.   1 |  0 |  0 |  1      d2 | d1 | d1 | d2
% 11.   1 |  0 |  1 |  0      d1 | d2 | d1 | d2  <- currently used
% 12.   1 |  0 |  1 |  1      d2 | d2 | d1 | d2
% 13.   1 |  1 |  0 |  0      d1 | d1 | d2 | d2
% 14.   1 |  1 |  0 |  1      d2 | d1 | d2 | d2
% 15.   1 |  1 |  1 |  0      d1 | d2 | d2 | d2
% 16.   1 |  1 |  1 |  1      d2 | d2 | d2 | d2
%
% to deliver to specific electrodes via specific digitimer, look up stats
% of r1-r4 in the table above and set relays accordingly.
%
% example: set pin 3 true, calling: outp(888,2^3) -> delivers a US
%          set all pins false with: outp(888,0)   -> sets O1-O2 to d1
%
% examples of preparing sequences to send via outp(888,x):
%  - activate r1 and r3 and deliver through digitimer 1:
%       1. reset pins: x = 0
%       2. set relay pins: x = 2^4 + 2^6 = 80
%       3. switch d1 on and off while keeping r1 and r3 active:
%            x = 2^4 + 2^6 + 2^0 = 81
%            -> switch between 81 80 81 80 81 80 ...
%               O1%O3 on/off by d1
%  - activate r1 and r3 and deliver through digitimer 1 and 2:
%       1. reset pins: x = 0
%       2. set relay pins: x = 2^4 + 2^6 = 80
%       3. switch d1+d2 on and off while keeping r1 and r3 active:
%            x = 2^4 + 2^6 + 2^0 + 2^2 = 85
%            -> switch between 85 80 85 80 85 80 ...
%               O1%O3 on/off by d1; O2%O4 on/off by d2
%  - same as above but send trigger in the beginning:
%       1. add 2^1 to first outp command:
%            x = 2^4 + 2^6 + 2^0 + 2^2 + 2^1 = 87
%            -> switch between 87 80 85 80 85 80 ...
%
%
%_________________________________________________________________________
% (C) 2015 Matthias Staib (Zurich University Hospital for Psychiatry)
%
% v001 ms 03.06.2015 initial version
% v002 ms 04.06.2015 updated for use with 2 electrodes per hand
% v003 ms 25.06.2015 s13 and s14 squeezed and s14 now starts with index finger
% v004 ms 28.10.2015 s3 and s4 now are applied to one finger only

plot_on   = 0; % plot sequences
test_stim = 0; % test them straight away

rng('default') % reset random number generator
rng('shuffle') % reset random number generator

% set relay settings
%--------------------------------------------------------------------------

% relay pins, set 0 for first state
rp = [2^4, 2^5, 2^6, 2^7];

% stimulus pin to activate somatosensory digitimer 1
d1 = 2^3;

% stimulus pin to activate somatosensory digitimer 2
d2 = 2^2;

% trigger pin (CS)
t = 2^1;

% stimulus pin to activate pain digitimer
US = 2^0;

% create logical mapping table (lmt), column-wise
lmt = logical([ ...
    repmat([zeros(8,1);ones(8,1)],1,1), ...
    repmat([zeros(4,1);ones(4,1)],2,1), ...
    repmat([zeros(2,1);ones(2,1)],4,1), ...
    repmat([zeros(1,1);ones(1,1)],8,1)]);

% stimulus setting
%--------------------------------------------------------------------------

% CS duration (sec)
tCS = 4;

%% prepare sequence for each stimulus
%--------------------------------------------------------------------------

% stimuli for calibration
%--------------------------------------------------------------------------

% -- stimulus 1 --
% O1, high frequency (10 Hz):
% O1 by d1, O2&O3&O4 not used
descr1   = 'O1, high frequency (10 Hz)';
sr       = 200;                % frequency of stimulus delivery update (twice of element frequency)
r1       = sum(rp(lmt(11,:))); % relay setup: O1-d1, O2-d2, O3-d1, O4-d2
pbase_d1 = repmat([d1 0],1,5); % building block d1 in 100 Hz: .05 sec element with 100 Hz stimulation
pbase_d2 = repmat([d2 0],1,5); % building block d2 in 100 Hz: .05 sec element with 100 Hz stimulation
pbase_0  = zeros(1,10);        % building block, no stimulation in 100 Hz: .05 sec element
s1       = repmat([pbase_d1 pbase_0], ...
    1,5) + r1;             % sequence for tCS seconds
t1       = 0:(1/sr):(length(s1)/sr-(1/sr));


% -- stimulus 2 --
% O2, high frequency (10 Hz):
% O2 by d2, O1&O3&O4 not used
descr2   = 'O2, high frequency (10 Hz)';
r2       = r1;                 % relay setup: O1 by d1, O2 by d2
s2       = repmat([pbase_d2 pbase_0], ...
    1,5) + r2;             % sequence for tCS seconds
t2       = t1;


% stimuli for simple discrimination training
%--------------------------------------------------------------------------


% -- stimulus 3 --
% O1, low frequency (3 Hz):
%
%   1_1_1_1_1_1_1_1_1_1_1_1_1_1_1_1_1_1_1_1_
%   ________________________________________
%
%   |---------|---------|---------|---------|--> time (sec)
%   0         1         2         3         4
%
% O1 by d1, O2 by d2, O3&O4 not used
descr3   = 'O1, low frequency (3 Hz)';
r3       = r1;                 % relay setup: O1 by d1, O2 by d2
s3       = repmat([pbase_d1 pbase_0 pbase_0 pbase_0], ...
    1,tCS*10) + r3;             % sequence for tCS seconds
s3(1)    = s3(1) + t;          % send trigger on first pulse
s3       = s3(1:sr*tCS);
t3       = 0:(1/sr):(tCS-(1/sr));

% -- stimulus 4 --
% O2, low frequency (3 Hz):
%
%   ________________________________________
%   2_2_2_2_2_2_2_2_2_2_2_2_2_2_2_2_2_2_2_2_
%
%   |---------|---------|---------|---------|--> time (sec)
%   0         1         2         3         4
%
% O1 by d1, O2 by d2, O3&O4 not used
descr4   = 'O2, low frequency (3 Hz)';
r4       = r1;                 % relay setup: O1 by d1, O2 by d2
s4       = repmat([pbase_d2 pbase_0 pbase_0 pbase_0], ...
    1,tCS*10) + r4;             % sequence for tCS seconds
s4(1)    = s4(1) + t;          % send trigger on first pulse
s4       = s4(1:sr*tCS);
t4       = 0:(1/sr):(tCS-(1/sr));


% stimuli for advanced discrimination training
%--------------------------------------------------------------------------


% -- stimulus 7 --
% travel from O2 to O1 with triplets, repeated 4 times:
% O1 by d1, O2 by d2, O3&O4 not used
descr7   = 'travel from O2 to O1 with triplets, repeated 4 times';
r7       = r1;
s7       = repmat([ ...
    pbase_d2 pbase_0 pbase_d2 pbase_0 pbase_d2 pbase_0 ...
    pbase_d1 pbase_0 pbase_d1 pbase_0 pbase_d1 pbase_0 ...
    pbase_0 pbase_0 pbase_0 pbase_0 pbase_0 pbase_0 pbase_0 pbase_0 ...
    ],1,4) + r7;               % sequence for tCS seconds
s7(1)    = s7(1) + t;          % send trigger on first pulse
t7       = 0:(1/sr):(tCS-(1/sr));


% -- stimulus 8 --
% travel from O1 to O2 with triplets, repeated 4 times:
% O1 by d1, O2 by d2, O3&O4 not used
descr8   = 'travel from O1 to O2 with triplets, repeated 4 times';
r8       = r1;
s8       = [pbase_d2 pbase_0 pbase_d2 pbase_0 pbase_d2 pbase_0 pbase_0 pbase_0 ...
    pbase_0 pbase_0 pbase_0 pbase_0 pbase_0 pbase_0 ...
    repmat([ ...
    pbase_d1 pbase_0 pbase_d1 pbase_0 pbase_d1 pbase_0 ...
    pbase_d2 pbase_0 pbase_d2 pbase_0 pbase_d2 pbase_0 ...
    pbase_0 pbase_0 pbase_0 pbase_0 pbase_0 pbase_0 pbase_0 pbase_0 ...
    ],1,4)] + r8;               % sequence for tCS seconds
s8       = s8(1:numel(s7));
s8(1)    = s8(1) + t;          % send trigger on first pulse
t8       = 0:(1/sr):(tCS-(1/sr));


% -- stimulus 9 --
% travel from O1 to O2 with triplets, repeated 4 times:
% O1 by d1, O2 by d2, O3&O4 not used
descr9   = 'travel from O1 to O2 with triplets, repeated 4 times';
r9       = r1;
s9       = repmat([ ...
    pbase_d1 pbase_0 pbase_d1 pbase_0 pbase_d1 pbase_0 ...
    pbase_d2 pbase_0 pbase_d2 pbase_0 pbase_d2 pbase_0 ...
    pbase_0 pbase_0 pbase_0 pbase_0 pbase_0 pbase_0 pbase_0 pbase_0 ...
    ],1,4) + r9;               % sequence for tCS seconds
s9(1)    = s9(1) + t;          % send trigger on first pulse
t9       = 0:(1/sr):(tCS-(1/sr));


% -- stimulus 10 --
% travel from O2 to O1 with triplets, repeated 4 times:
% O1 by d1, O2 by d2, O3&O4 not used
descr10   = 'travel from O2 to O1 with triplets, repeated 4 times';
r10       = r1;
s10       = [pbase_d1 pbase_0 pbase_d1 pbase_0 pbase_d1 pbase_0 pbase_0 pbase_0 ...
    pbase_0 pbase_0 pbase_0 pbase_0 pbase_0 pbase_0 ...
    repmat([ ...
    pbase_d2 pbase_0 pbase_d2 pbase_0 pbase_d2 pbase_0 ...
    pbase_d1 pbase_0 pbase_d1 pbase_0 pbase_d1 pbase_0 ...
    pbase_0 pbase_0 pbase_0 pbase_0 pbase_0 pbase_0 pbase_0 pbase_0 ...
    ],1,4)] + r10;               % sequence for tCS seconds
s10       = s10(1:numel(s9));
s10(1)    = s10(1) + t;          % send trigger on first pulse
t10       = 0:(1/sr):(tCS-(1/sr));

%% store for quick access

% trsining
stims{1}  = s1;  tstims{1}  = t1;  descr{1}  = descr1;  pinsetup{1}  = r1;
stims{2}  = s2;  tstims{2}  = t2;  descr{2}  = descr2;  pinsetup{2}  = r2;

% simple
stims{3}  = s3;  tstims{3}  = t3;  descr{3}  = descr3;  pinsetup{3}  = r3;
stims{4}  = s4;  tstims{4}  = t4;  descr{4}  = descr4;  pinsetup{4}  = r4;

% complex
stims{7}  = s7;  tstims{7}  = t7;  descr{7}  = descr7;  pinsetup{7}  = r7;
stims{8}  = s8;  tstims{8}  = t8;  descr{8}  = descr8;  pinsetup{8}  = r8;
stims{9}  = s9;  tstims{9}  = t9;  descr{9}  = descr9;  pinsetup{9}  = r9;
stims{10} = s10; tstims{10} = t10; descr{10} = descr10; pinsetup{10} = r10;

S.stims    = stims;    % LPT port commands
S.tstims   = tstims;   % timings
S.descr    = descr;    % description
S.pinsetup = pinsetup; % pin setting

if plot_on
    
    cbox = ones(1,3);
    
    for s = 1:numel(stims)
        try
        f = figure('Position',[50 50+(s*20) 1000 150]);
        foo    = stims{s}  - pinsetup{s};
        foo(1) = foo(1) - t;
        clear seq
        seq(1,:) = conv(double(foo == d1 | foo == d1+d2),cbox,'same');
        seq(2,:) = conv(double(foo == d2 | foo == d1+d2),cbox,'same');
        imagesc(tstims{s},[1 2],~seq); colormap gray
        hold on
        line([0 4], [1.5 1.5],'Color',[1 1 1],'Linewidth',10)
        %         set(gca,'XTick',[],'YTick',1:2,'YTicklabel',{'Zeigefinger';'Mittelfinger'})
        set(gca,'XTick',[],'YTick',[])
        %         title(descr{s})
        set(gca,'Position',[0 0 1 1],'units','normalized')
        
        hgexport(f, fullfile(pwd,sprintf('stim%02i',s)),  ...
            hgexport('factorystyle'), 'Format', 'png');
        end
    end
    
end

%% deliver stimuli
%--------------------------------------------------------------------------

if test_stim
    
    % configure parallel port
    addpath C:\ParPort
    address = hex2dec('378');     % standard LPT1 output port address (0x378), as decimal
    config_io; outp(address, 0)   % install and reset port
    
    input('ENTER to start')
    
    trialstart  = 1;
    trial       = trialstart;
    idx         = NaN;
    off         = false;
    sidx        = input('Pattern: ');
    
    while ~off
        
        % prepare stimulus ----------------------------------------
        pattern  = S.stims{sidx};
        tpattern = S.tstims{sidx};
        pinsetup = S.pinsetup{sidx};
        desc     = S.descr{sidx};
        npulses  = length(tpattern);
        outp(address,pinsetup); % reset port
        somcount = 0;
        disp(desc)
        k        = NaN;
        
        ttrial = GetSecs+.5;
        % deliver stimulus
        while (GetSecs-ttrial) < 4
            if  somcount<npulses && GetSecs-ttrial >= tpattern(somcount+1)
                somcount = somcount+1;
                outp(address,pattern(somcount))
                if pattern(somcount)~=pinsetup && k ~= pattern(somcount), k = pattern(somcount); fprintf('%i ',pattern(somcount)-pinsetup); end
            end
        end
        disp(' ')
        idx = input('ENTER to continue, 0 to exit or new index: ');
        if idx==0 off = true;
        elseif ~isnan(idx)
            sidx = idx;
            input('Updated, ENTER to continue')
        else
            idx = NaN;
        end
    end
end

