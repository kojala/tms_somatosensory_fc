function Eval_probability = probability_evaluation
% probability evaluation

% Get all base workspace variables into this function.
T = evalin('base','whos');
for ii = 1:length(T)
    C_ =  evalin('base',[T(ii).name ';']);
    eval([T(ii).name,'=C_;']);
end
clear T C_ ii

stim = 1:4;

Screen('TextSize', expWin, tsize_small);

myText = [ ...
    'Wir m�chten Sie nun bitten, die verschiedenen Muster zu bewerten.\n\n', ...
    'Die unangenehmen Stimulationen am Fuss folgten nach\n\n', ...
    'Pr�sentation der Muster.\n\n', ...
    'Aber war so ein Schock f�r alle Muster gleich wahrscheinlich?\n\n', ...
    'Ihnen werden die Muster noch einmal einzeln gezeigt\n\n', ...
    '(ohne elektrische Stimulation).\n\n', ...
    'Bitte versuchen Sie auf einer Skala von 0% (niemals) bis 100% (immer)\n\n', ...
    'zu sch�tzen, in wie vielen F�llen auf ein Muster eine\n\n' ...
    'elektrische Stimulation folgte.\n\n\n\n', ...
    'Nutzen Sie daf�r die Tasten 1 bis 5 auf dem Nummernpad.'];
Screen('FillRect', expWin, rectColor, baseRect);
DrawFormattedText(expWin, myText, 'center', 'center', 255);
DrawFormattedText(expWin, 'Beginn mit ENTER', 'center', sr(2)*.8, 255);
Screen('Flip', expWin);
KbWait([], 3); WaitSecs(0.3)

c = 1;
for stim_idx = stim
    
    Screen('FillRect', expWin, rectColor, baseRect);
    DrawFormattedText(expWin, 'Bitte achten Sie auf das Muster!\n\n(noch keine Taste dr�cken)', 'center', 'center');
    Screen('Flip', expWin);
    
    % prepare stimulus ----------------------------------------
    pattern  = Stimuli{stim_idx,1}.stims(2:end); % delete first pulse to avoid setting a trigger
    tpattern = Stimuli{stim_idx,1}.tstims(2:end);
    pinsetup = Stimuli{stim_idx,1}.pinsetup;
    npulses  = length(tpattern);
    outp(address,pinsetup); % reset port
    somcount = 0;
    k        = NaN;
    
    ttrial = GetSecs+2;
    % deliver stimulus
    while (GetSecs-ttrial) < (max(tpattern)*1.1)
        if  somcount<npulses && GetSecs-ttrial >= tpattern(somcount+1)
            somcount = somcount+1;
            outp(address,pattern(somcount))
            if pattern(somcount)~=pinsetup && k ~= pattern(somcount), k = pattern(somcount); fprintf('%i ',pattern(somcount)-pinsetup); end
        end
    end
    
    WaitSecs(2)
    
    keyIsDown = 0;
    while ~keyIsDown
        
        [keyIsDown, secs, keyCode, deltaSecs] = KbCheck;
        
        myText1  = ['Muster ' num2str(c)];
        myText0 = 'Wie h�ufig kam nach dem Muster eine elektrische Stimulation vor?';
        myText2 = 'Bitte w�hlen Sie:';
        M1 = '1\n\n\n0%'; M2 = '2\n\n\n25%'; M3 = '3\n\n\n50%';
        M4 = '4\n\n\n75%'; M5 = '5\n\n\n100%';
        
        Screen('FillRect', expWin, rectColor, baseRect);
        
        xposbox = [sr(2)*.5 sr(2)*.6 sr(2)*.7 sr(2)*.8 sr(2)*.9];
        DrawFormattedText(expWin, myText1, 'center', sr(2)*.1);
        DrawFormattedText(expWin, myText0, 'center', sr(2)*.3);
        DrawFormattedText(expWin, myText2, sr(2)*.05, sr(2)*.5, 255);
        DrawFormattedText(expWin, M1, xposbox(1), sr(2)*.5, 255);
        DrawFormattedText(expWin, M2, xposbox(2), sr(2)*.5, 255);
        DrawFormattedText(expWin, M3, xposbox(3), sr(2)*.5, 255);
        DrawFormattedText(expWin, M4, xposbox(4), sr(2)*.5, 255);
        DrawFormattedText(expWin, M5, xposbox(5), sr(2)*.5, 255);
        
        if keyIsDown
            WaitSecs(.3)
            
            pressedKey   = find(keyCode);
            pressedKey   = pressedKey(1);
            %                 disp(KbName(pressedKey))
            
            [v,ind] = ismember(pressedKey,header.key.eval);
            if v
                Screen('FrameRect', expWin, [1 1 1], [xposbox(ind)-30, sr(2)*.55-60, xposbox(ind)+75, sr(2)*.55+50], 6);
            else
%                 DrawFormattedText(expWin, KbName(pressedKey), mx, my, 255);
%                 Screen('Flip', expWin);
%                 WaitSecs(2)
                keyIsDown = 0;
            end
        end
        
        Screen('Flip', expWin);
        if keyIsDown, WaitSecs(.6); end
        
    end
    
    Eval_probability.key(c) = pressedKey;
    c = c+1;
end

Eval_probability.stim = stim;