% PRESENT_STIM delivers stimuli by user's choice
%
%_________________________________________________________________________
% (C) 2015 Matthias Staib (Zurich University Hospital for Psychiatry)
%
% v001 ms 03.06.2015 initial version

close all
clear all
clc

%% add paths and toolboxes

if ~strcmp(version,'8.2.0.701 (R2013b)'); disp('Wrong MATLAB version. Abort!'); return; end
addpath(fullfile(pwd,'utils'));
addpath('D:\labuser\Documents\MATLAB\@ParallelPort\private')

[mPath, ~, ~] = fileparts(which(mfilename));
[s, m]        = mkdir(fullfile(mPath,'data')); % to save file

cd(mPath)
addpath(fullfile(mPath,'utils')) % add utility path

%% subject and experiment details

% default settings, don't edit
options.grayonly   = 0; % one background color
options.stimset    = 1; % 1 = plaids; 2 = high freq. gratings
options.speedup    = 0; % TEMPORARY!!!!!
options.cond1      = 1; %input('O (1-4): '); % which orientations are CS+/CS- resp. reinforced/safe
options.cond2      = 1; %input('K (1-2): '); % keys
options.cond3      = 1; %input('C (1-2): '); % color
options.cond4      = 1;                  % (unused) order of stimulus complexity
options.cond5      = 1;                  % (unused) quadrant of target (bottom left or right)
options.pattidx(1) = 1; %input('M1: ');
options.pattidx(2) = 2; %input('M2: ');

[Data, header, Stimuli] = prepare_som(options);

%% prepare trigger

address = hex2dec('DF98');
config_io; outp(address, 0);  % install and reset port


%%

try
    
    %% preparation of presentation
    
    % Make sure the script is running on Psychtoolbox-3:
    AssertOpenGL;
    
    % configure keyboard
    KbName('UnifyKeyNames');
    KbCheck;
    olddebuglevel      = Screen('Preference', 'VisualDebuglevel', 3);
    
    header.key.arrows  = [KbName('UpArrow') KbName('DownArrow')];
    header.key.escape  = KbName('ESCAPE');
    
    % configure screen
    screens       = Screen('Screens');
    screenNumber  = max(screens);
    oldresolution = Screen('Resolution', screenNumber, header.screenres(1), header.screenres(2));
    sr            = header.screenres;
    
    % set background color
    [expWin,rect] = Screen('OpenWindow',screenNumber,255/2,[],[],[],[],4);
    ifi           = Screen('GetFlipInterval', expWin); % for exact timing
    vbl           = Screen('Flip', expWin);
    [mx, my]      = RectCenter(rect);
    
    % set text size
    tsize_small = round(sr(2)/864*18);
    tsize_large = round(sr(2)/864*60);
    
    % disable keyboard and hide cursor
    ListenChar(0);
    HideCursor; % ShowCursor;
    
    Screen('Preference', 'TextAntiAliasing', 2); % high quality text
    Screen('TextSize', expWin, 40);
    
    % first
    myText = [ ...
        'Pfeiltaste OBEN: Zeigefinger\n\n' ...
        'Pfeiltaste UNTEN: Mittelfinger\n\n'];
    DrawFormattedText(expWin, myText, 'center', 'center', 255);
    DrawFormattedText(expWin, 'Ende mit ESCAPE', 'center', sr(2)*.8, 255);
    Screen('Flip', expWin);
    
    %%
    while 1
        
        [secs, keyCode, deltaSecs] = KbWait;
        
        p = false;
        while ~p
            if find(keyCode)==header.key.arrows(1), kp = 1; p = true;
            elseif find(keyCode)==header.key.arrows(2), kp = 2; p = true;
            elseif find(keyCode)==header.key.escape; interrupt_all; return;
            else [secs, keyCode, deltaSecs] = KbWait; end
            
            if p
                % prepare stimulus ----------------------------------------
                pattern  = Stimuli{kp}.stims;
                tpattern = Stimuli{kp}.tstims;
                pinsetup = Stimuli{kp}.pinsetup;
                npulses  = length(tpattern);
                outp(address,pinsetup); % reset port
                somcount = 0;
                disp(Stimuli{1}.descr)
                k        = NaN;
                
                ttrial = GetSecs+.2;
                % deliver stimulus
                while (GetSecs-ttrial) < (max(tpattern)*1.1)
                    if  somcount<npulses && GetSecs-ttrial >= tpattern(somcount+1)
                        somcount = somcount+1;
                        outp(address,pattern(somcount))
                        if pattern(somcount)~=pinsetup && k ~= pattern(somcount), k = pattern(somcount); fprintf('%i ',pattern(somcount)-pinsetup); end
                    end
                end
            end
        end
    end
    
    %% end of PTB script
    
    Priority(0);
    
    % cleanup
    ShowCursor;
    ListenChar(1);
    sca;
    
catch ME
    disp([ME.stack.line])
    disp([ME.identifier])
    disp([ME.message])
    
    % cleanup
    interrupt_all
end