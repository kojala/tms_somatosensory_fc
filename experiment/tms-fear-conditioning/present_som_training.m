% PRESENT_SOM_TRAINING delivers stimuli according to delay fear
% conditioning paradigm but with shorter inter-stimuli intervals.
% Requires PTB.
%
% Records Eye-Tracking
%
% Subject has to press an arrow key.
% Feedback is given in each trial by changing color of the fixation cross
% after the end of stimulus presentation.
%
%_________________________________________________________________________
% (C) 2015 Matthias Staib (Zurich University Hospital for Psychiatry)
%
% v001 ms 10.07.2015 initial version

close all
clear all
clc

if ~strcmp(version,'8.2.0.701 (R2013b)'); disp('Wrong MATLAB version. Abort!'); return; end
addpath(fullfile(pwd,'utils'));
addpath('D:\labuser\Documents\MATLAB\@ParallelPort\private')

%% subject name

subj             = input('Subject (2 digits): ','s');
training         = input('Training (1-2): ');
if training == 1, setpreset = input('Preset  (1-24): '); end

%% presets from which to pick

preset.pattidx(1,:) = [3 4 9 10];
preset.pattidx(2,:) = [3 4 7 8];
preset.conds(:,1)   = repmat(1:4,1,6);
preset.conds(:,2)   = repmat([1 1 2 2 2 2 1 1],1,3);
preset.conds(:,4)   = [ones(1,12) ones(1,12)*2];
preset.patts        = repmat([1 1 1 1 2 2 2 2],1,3);

%% set parameters

% experiment parameters
options.params.ntrials           = 12;   % number of trials per block
options.params.nruns             = 1;    % number of blocks (defined by one complexity setting)
options.speedup                  = 0; % TEMPORARY!!!!!

options.params.prob_CSp          = .5;  % probability of CS+
options.params.prob_CSpp         = 0;   % probability of US+ given CS+
options.params.prob_STp          = 0;   % probability of ST+ given CS

% timing parameters
options.params.CS_duration       = 4;    % duration of stimulus presentation
options.params.SOA               = 3.5;  % time between CS onset and US onset
options.params.response_window   = 3;    % allowed time to reply
options.params.initial_info      = 2;    % time of block# display
options.params.t0                = 5;    % time before first trial
options.params.blockend          = 8;    % delay after last trial in a block
options.params.error_duration    = 2;    % time of error display
options.params.itis              = [5:9  7]; % ITI

%% add paths and toolboxes

trigger = 1;

[mPath, ~, ~] = fileparts(which(mfilename));
[s, m]        = mkdir(fullfile(mPath,'data')); % to save file

cd(mPath)
addpath(fullfile(mPath,'utils')) % add utility path

%% get subject id and set params

% set filename
fx     = true;
fcount = 0;
while fx
    fcount = fcount+1;
    subject = sprintf('training%i_ss7b%s#%i',training,subj,fcount);
    fx = exist(fullfile(mPath,'data',sprintf('%s.mat',subject)),'file');
end

if training == 1 && fcount==1
    options.pattidx       = preset.pattidx(preset.patts(setpreset),:); % which patterns from create_somstim.m are used?
    options.cond1         = preset.conds(setpreset,1); %input('P (1-4): '); % which patterns are CS+/CS-
    options.cond2         = preset.conds(setpreset,2); %input('K (1-2): '); % keys
    options.cond3         = mod(str2double(subj),2)+1; % background color
    options.cond4         = preset.conds(setpreset,4); %input('C (1-2): '); % order of complexity blocks
else
    load(fullfile(mPath,'data',sprintf('training%i_ss7b%s#%i.mat',1,subj,1)))
    options = header.options;
end
options.training = training;
fprintf('\nP: %i, K: %i, B: %i, C: %i\n',options.cond1,options.cond2,options.cond3,options.cond4)

%% subject and experiment details

% create all data
[Data, header, Stimuli] = prepare_som(options);

header.subject.name   = subject;
c            = clock;
header.date  = c;
header.time  = sprintf('%0.0f.%0.0f.%0.0f', c(4), c(5), c(6));

% total time of stimulus presentation
foo = sort(Data(:,8),'descend') + header.delays.blockend;
header.est_totaltime = sum(foo(1:header.nruns));

blockstart   = 1; % from which block to start?

%% save all m scripts to structure

flist1 = dir(fullfile(mPath,'*.m'));
flist2 = dir(fullfile(mPath,'utils','*.m'));

for f = 1:numel(flist1)
    header.mfiles.(flist1(f).name(1:end-2)) = fileread(fullfile(mPath,flist1(f).name)); end
for f = 1:numel(flist2)
    header.mfiles.utils.(flist2(f).name(1:end-2)) = fileread(fullfile(mPath,'utils',flist2(f).name)); end

%% prepare trigger

if trigger
    address = hex2dec('DF98'); % standard LPT1 output port address (0x378), as decimal
    config_io
    outp(address, 0)           % install and reset port
else
    warning('Trigger/Digitimer disabled!!')
    Screen('Preference', 'SkipSyncTests', 1);
end

%% initialize eyetracking

efname = ['t' num2str(training) 's' subj 'x' num2str(fcount)];

Eyelink('Initialize');
Eyelink('OpenFile',efname);
header.eyelink.version = Eyelink('GetTrackerVersion');
Eyelink('Startrecording');

%% save all information

save(fullfile(mPath,'data',sprintf('%s.mat',header.subject.name)),'Data', 'header')

try
    
    %% preparation of presentation
    
    % Make sure the script is running on Psychtoolbox-3:
    AssertOpenGL;
    
    % configure keyboard
    KbName('UnifyKeyNames');
    KbCheck;
    olddebuglevel      = Screen('Preference', 'VisualDebuglevel', 3);
    
    header.key.arrows  = [KbName('LeftArrow') KbName('RightArrow')];
    header.key.eval    = [KbName('End') KbName('DownArrow') ...
        KbName('PageDown') KbName('LeftArrow') KbName('Clear')]; % numblock 1-5
    header.key.eval    = [KbName('1') KbName('2') KbName('3') KbName('4') KbName('5')]; % numblock 1-5
    header.key.trigger = KbName('t');
    header.key.escape  = KbName('ESCAPE');
    key_order          = header.key.arrows(header.task.order);
    
    % configure screen
    screens       = Screen('Screens');
    screenNumber  = max(screens);
    oldresolution = Screen('Resolution', screenNumber, header.screenres(1), header.screenres(2));
    sr            = header.screenres;
    
    % set background color
    [expWin,rect] = Screen('OpenWindow',screenNumber,255/2,[],[],[],[],4);
    ifi           = Screen('GetFlipInterval', expWin); % for exact timing
    vbl           = Screen('Flip', expWin);
    [mx, my]      = RectCenter(rect);
    
    % set text size
    tsize_small = round(sr(2)/864*18);
    tsize_large = round(sr(2)/864*60);
    
    % disable keyboard and hide cursor
    ListenChar(0);
    HideCursor; % ShowCursor;
    
    % error screen after failed fixation
    rectColor_Err = 50;
    
    % normal fixation window
    fix_color1 = [150 150 150];    % normal fixation cross
    % error fixation cross
    fix_color3 = [200 100 100]; % prepratory fixation cross
    
    
    %% start trials -------------------------------------------------------
    
    topPriorityLevel = MaxPriority(expWin);
    Priority(topPriorityLevel);
    
    tblock      = inf;     % time of block start
    trialstart  = header.ntrials*(blockstart-1)+1;
    fooKey(header.key.escape) = 0;
    Screen('TextSize', expWin, tsize_large);
    
    tdelay       = 0; % will be used to correct timing of display
    trial        = trialstart;
    shownIntro   = 0; % Intro texts
    
    while trial <= Data(end,1)
        
        % reset time stamps and keys that are defined within each trial
        % multiple booleans are defined that ensure that each screen
        % will be called only once
        all_pressedKeys    = [];
        all_rt             = [];
        waspressed         = 0; % only one key press will be counted
        shownb             = 0; % block information
        shownf             = 0; % fixation cross
        shownff            = 0; % lighter fixation cross before stimulus
        showns             = 0; % stimulus
        trialover          = 0; % end of trial
        aborted            = 0;
        ttrial             = GetSecs; % trial reference time
        somcount           = 0;
        % prepare stimulus ----------------------------------------
        pattern  = Stimuli{Data(trial,7)}.stims;
        tpattern = Stimuli{Data(trial,7)}.tstims;
        pinsetup = Stimuli{Data(trial,7)}.pinsetup;
        npulses  = length(tpattern);
        if trigger, outp(address,pinsetup); end % reset port
        
        % display introduction texts
        if ~shownIntro
            Introduction_Texts_training; shownIntro = 1;
            Screen('TextSize', expWin, tsize_large);
        end
        
        % set reference time of block
        if mod(trial,header.ntrials)==1
            tblock = GetSecs;
        end
        
        % infinite while loop with permanent escape option
        while ~trialover && ~fooKey(header.key.escape)
            
            [ ~, ~, fooKey ] = KbCheck; % check pressed keys at all times
            
            % show block info for 2 seconds -------------------------------
            if mod(trial,header.ntrials)==1 && GetSecs-tblock <= 2
                
                % set background color for the block
                rectColor    = header.context.color(Data(trial,2),:);
                baseRect     = [0 0 sr(1) sr(2)];
                
                holdscreen1 = 1; % delays fixation cross until block info was shown
                if ~shownb
                    shownb = 1;
                    
                    Screen('FillRect', expWin, rectColor, baseRect);
                    DrawFormattedText(expWin, '\n\nStart Training', 'center', 'center');
                    DrawFormattedText(expWin, '+', 'center', 'center', fix_color1);
                    Screen('Flip', expWin);
                end
            else
                holdscreen1 = 0;
            end
            
            
            % initial fixation cross --------------------------------------
            if ~shownf && ~holdscreen1
                shownf = 1;
                DrawFormattedText(expWin, '+', 'center', 'center', fix_color1);
                tfixation = Screen('Flip', expWin);
                disp(Stimuli{Data(trial,7)}.descr)
            end
            
            
            % stimulus time window
            %--------------------------------------------------------------
            if GetSecs-tblock >= Data(trial,8)
                
                if ~showns
                    showns = 1;
                    Data(trial,12) = GetSecs-tblock;
                    disp(['stimulus onset ' num2str(Data(trial,12))])
                end
                
                % deliver stimulus
                if  somcount<npulses ...
                        && GetSecs-tblock >= Data(trial,8)+tpattern(somcount+1) ...
                        && GetSecs-tblock < Data(trial,8)+header.delays.CS_duration
                    somcount = somcount+1;
                    if trigger, outp(address,pattern(somcount)); end
                end
                
                % record pressed key
                [keyIsDown, timeSecs, keyCode] = KbCheck;
                
                if keyIsDown && ~waspressed
                    waspressed   = 1;
                    pressedKey   = find(keyCode);
                    pressedKey   = pressedKey(1);
                    all_pressedKeys(trial) = pressedKey;
                    all_rt(trial)          = timeSecs-tblock - Data(trial,8);
                    disp(['key pressed: ' KbName(pressedKey) ', ' ...
                        num2str(GetSecs-tblock) ', RT = ' num2str(all_rt(trial))])
                    Data(trial,13) = pressedKey;
                    Data(trial,14) = all_rt(trial);
                end
            end
            
            
            % end of trial and stimulus presentation: show fixation cross
            if ~trialover && GetSecs-tblock >= Data(trial,8)+header.delays.CS_duration+.5
                
                trialover = 1;
                
                % show feedback -------------------------------------------
                
                % correct key for each trial depends on CS type and keyorder
                key_correct = header.key.arrows(header.task.order(Data(trial,3)));
                
                fprintf('\ntrial %02d: %i\n', trial,Data(trial,7))
                
                % feedback code
                if ~waspressed
                    Data(trial,15) = 3; % no key
                    fprintf('\n------------------------\nWRONG: no button presssed\n------------------------\n\n')
                    if ~aborted
                        % fixation cross turns red
                        Screen('FillRect', expWin, rectColor, baseRect);
                        DrawFormattedText(expWin, '+', 'center', 'center', fix_color3);
                        terror = Screen('Flip', expWin);
                        WaitSecs(header.delays.error_duration);
                        Screen('FillRect', expWin, rectColor, baseRect);
                        DrawFormattedText(expWin, '+', 'center', 'center', fix_color1);
                        Screen('Flip', expWin);
                    end
                elseif key_correct == pressedKey(1)
                    Data(trial,15) = 1; % correct response
                    fprintf('\n------------------------\nCORRECT\n------------------------\n\n')
                else
                    Data(trial,15) = 0; % wrong key
                    fprintf('\n------------------------\nWRONG: misidentified\n------------------------\n\n')
                    if ~aborted
                        % fixation cross turns red
                        Screen('FillRect', expWin, rectColor, baseRect);
                        DrawFormattedText(expWin, '+', 'center', 'center', fix_color3);
                        terror = Screen('Flip', expWin);
                        WaitSecs(header.delays.error_duration);
                        Screen('FillRect', expWin, rectColor, baseRect);
                        DrawFormattedText(expWin, '+', 'center', 'center', fix_color1);
                        Screen('Flip', expWin);
                    end
                end
                
            end
        end
        
        if fooKey(header.key.escape)
            interrupt_all
            return
        end; % exit experiment upon ESC press
        
        % backup data
        save(fullfile(mPath,'data',sprintf('%s.mat',header.subject.name)),'Data', 'header')
        
        % block end % -----------------------------------------------------
        
        % Evaluation and goodbye screens after last trial
        if ~aborted && trial == header.ntrials
            
            WaitSecs(header.delays.blockend/2);
            save(fullfile(mPath,'data',sprintf('%s.mat',header.subject.name)),'Data', 'header')
            
            Screen('TextSize', expWin, tsize_small);
            
            myText = sprintf(['Das Training ist beendet!\n\n\n\n', ...
                'Auswertung:\n\n\n', ...
                'Sie lagen in %3.1f%% der F�lle richtig.\n\n', ...
                'Sie haben in %3.1f%% keine Taste gedr�ckt oder zu sp�t reagiert.\n\n', ...
                'Sie lagen in %3.1f%% falsch.'], ...
                sum(Data(:,15)==1)/header.ntrials*100, ...
                sum(Data(:,15)==3)/header.ntrials*100, ...
                sum(Data(:,15)==0)/header.ntrials*100);
            DrawFormattedText(expWin, myText, 'center', 'center', 255);
            Screen('Flip', expWin);
            
            % backup stimuli
            header.Stimuli = Stimuli;
            save(fullfile(mPath,'data',sprintf('%s.mat',header.subject.name)),'Data', 'header')
            
            WaitSecs(2)
            
            Eyelink('Stoprecording');
            Eyelink('CloseFile');
            Eyelink('Shutdown');
            
            KbWait([], 3);
            
            Screen('TextSize', expWin, tsize_large);
            
            shownIntro  = 0; % reset to show intro for next part
        end
        
        
        % loop through trials, but repeat if wrong fixation was made
        if ~aborted
            trial = trial+1;
            disp(['next trial ' num2str(trial)])
        else
            % add duration of broken trial
            tblock = tblock + (GetSecs-ttrial);
        end
    end
    
    Priority(0);
    
    
    %% end of experiment --------------------------------------------------
    
    disp(['EXPERIMENT END ' num2str(GetSecs-tblock)]);
    
    %% end of PTB script
    
    % cleanup
    ShowCursor;
    ListenChar(1);
    sca;
    
catch ME
    disp([ME.stack.line])
    disp([ME.identifier])
    disp([ME.message])
    
    % cleanup
    interrupt_all
end


%% evaluation

fprintf('\nPerformance:   %3.1f%',mean(Data(:,15)==1)*100)
fprintf('\nReaction time: %3.1f%',mean(Data(~isnan(Data(:,14)),14)))