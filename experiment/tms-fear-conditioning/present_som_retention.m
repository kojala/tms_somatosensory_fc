% PRESENT_SOM_RETENTION exctinction paradigm using
% different patterns, presented in mini blocks. Requires PTB.
%
%
% Phys recordings:
% - SCR electrodes: thenar/hypothenar of the non-dominant hand
% - Eye motion using eyelink
% - blink startle
%
% The experiment is composed of:
%       two trial types (CS+, CS-)
%     x two complexity conditions (traveling wave or frequency)
%
% Subject has to press an arrow key (balanced over subjects).
% The button press task is congruent with the CS condition.
% Feedback is given in each trial by changing color of the fixation cross
% after the end of stimulus presentation.
%
%_________________________________________________________________________
% (C) 2015 Matthias Staib (Zurich University Hospital for Psychiatry)
%
% v001 ms 03.06.2015 initial version
% v002 ms 29.10.2015 adapted for new version

% rows of data structure (see also header.datakeys):
% header.datakeys{1}  = 'trial index';
% header.datakeys{2}  = 'context type          1: reinforcement1; 2: reinforcement2';
% header.datakeys{3}  = 'CS type               1: CS+; 2: CS- (ignored in in context 2)';
% header.datakeys{4}  = 'US occurrence         0: no US; 1: US (shock, 500ms)';
% header.datakeys{5}  = 'ST occurrence         0: no ST; 1: ST (white noise, 50ms)';
% header.datakeys{6}  = 'stimulus complexity   0: simple; 1: complex';
% header.datakeys{7}  = 'stimulus id           eight different combinations (complexity x f1 x f2)';
% header.datakeys{8}  = 'time of CS onset      in milliseconds';
% header.datakeys{9}  = 'time of fixation cross onset';
% header.datakeys{10} = 'time of US onset';
% header.datakeys{11} = 'time of ST onset';
% header.datakeys{12} = 'real time of CS onset';
% header.datakeys{13} = 'id of pressed button';
% header.datakeys{14} = 'reaction time';
% header.datakeys{15} = 'feedback of button press   0: wrong, 1: correct', 3: no response;

close all
clear all
clc

%% set parameters

if ~strcmp(version,'8.2.0.701 (R2013b)'); disp('Wrong MATLAB version. Abort!'); return; end
eyetracking = 1;
trig        = 1;

input('+++++ VOR BEGINN: TRIGGER KANAL UMSTECKEN!!! +++++','s')

% experiment parameters
options.params.ntrials           = 6;  % number of trials per block
options.params.nruns             = 4;  % number of blocks (defined by one complexity setting)
options.speedup                  = 0;  % TEMPORARY!!!!!

options.params.prob_CSp          = .5; % probability of CS+
options.params.prob_CSpp         = 0;  % probability of US+ given CS+
options.params.prob_STp          = 1;  % probability of ST+ given CS

% timing parameters
options.params.CS_duration       = 3.5;  % duration of stimulus presentation
options.params.SOA               = 3.5;  % time between CS onset and US onset
options.params.response_window   = 3;    % allowed time to reply
options.params.initial_info      = 2;    % time of block# display
options.params.t0                = 5;    % time before first trial
options.params.blockend          = 8;    % delay after last trial in a block
options.params.error_duration    = 2;    % time of error display
options.params.itis              = [7:11 9]; % ITI

%% add paths and toolboxes

addpath(fullfile(pwd,'utils'));
addpath('D:\labuser\Documents\MATLAB\@ParallelPort\private')

if ~trig, warning('Trigger disabled!'); end

% check if Eyetracker can be used
if ~eyetracking
    Screen('Preference', 'SkipSyncTests', 1);
    warning('Eyetracking disabled!');
end

[mPath, ~, ~] = fileparts(which(mfilename));
[s, m]        = mkdir(fullfile(mPath,'data')); % to save file

cd(mPath)
addpath(fullfile(mPath,'utils')) % add utility path

%% get subject id and set params

subj    = input('Subject (2 digits): ','s');
subject = sprintf('ss7b%s',subj);

if ~exist(fullfile(mPath,'data',sprintf('retention_%s.mat',subject)),'file')
    
    expsubject = sprintf('%s',subject);
    fname = fullfile(mPath,'data',sprintf('%s.mat',expsubject));
    if ~exist(fname,'file')
        disp('Execute experiment first!')
        return
    end
    
    % load experiment data
    expdata = load(fname);
    
    options.pattidx = expdata.header.options.pattidx;
    options.cond1   = expdata.header.options.cond1;
    options.cond2   = expdata.header.options.cond2;
    options.cond3   = expdata.header.options.cond3;
    options.cond4   = expdata.header.options.cond4;
    options.grayonly = 0;
    options.training = 3;
    
    [Data, header, Stimuli] = prepare_som(options);
    
    header.subject  = expdata.header.subject;
    
    c            = clock;
    header.date  = c;
    header.time  = sprintf('%0.0f.%0.0f.%0.0f', c(4), c(5), c(6));
    
    % total time of stimulus presentation, excluding evaluations
    foo = sort(Data(:,8),'descend') + header.delays.blockend;
    header.est_totaltime = sum(foo(1:header.nruns));
    
    blockstart   = 1; % from which block to start?
    
    eye_prefix  = [];
    
else
    disp('Subject exists. Loading data.')
    load(fullfile(mPath,'data',sprintf('retention_%s.mat',subject)))
    [~, ~, Stimuli] = prepare_som(header.options);
    solved = find(~isnan(Data(:,end)),1,'last'); if isempty(solved);solved=0;end;
    finishedblock = floor(solved/header.ntrials);
    blockstart  = finishedblock+1; % from which block to start?
    eye_prefix  = ['B' num2str(blockstart)];
end
header.eyetracking = eyetracking;
%% save all m scripts to structure

flist1 = dir(fullfile(mPath,'*.m'));
flist2 = dir(fullfile(mPath,'utils','*.m'));

for f = 1:numel(flist1)
    header.mfiles.(flist1(f).name(1:end-2)) = fileread(fullfile(mPath,flist1(f).name)); end
for f = 1:numel(flist2)
    header.mfiles.utils.(flist2(f).name(1:end-2)) = fileread(fullfile(mPath,'utils',flist2(f).name)); end

%% prepare trigger

address = hex2dec('DF98');     % standard LPT1 output port address (0x378), as decimal
if trig, config_io; outp(address, 0); end   % install and reset port

US_label = {'US-';'US+'}; % for printout only
CS_label = {'CS+','CS-'};

%% prepare startle

fnameST     = fullfile(mPath,'soundfiles','startle','white_noise_uniform_0.05sec_x0.037.wav');
[wNoise,Fs] = audioread(fnameST);
nrchannels  = 2;     % stereo
sugLat      = 0.05;  % soundcard latency

InitializePsychSound; % Initialize Sounddriver
pahandle = PsychPortAudio('Open', [], [], 0, Fs, nrchannels, [], sugLat);
PsychPortAudio('Volume', pahandle, 1.3)
% load startle sound (white noise) to buffer
PsychPortAudio('FillBuffer', pahandle, [wNoise' ; wNoise']);

%% initialize eyetracking

efname = [subject 'r'];
header.subject.eye_fname = efname;

if header.eyetracking
    Eyelink('Initialize');
    Eyelink('OpenFile',efname);
    header.eyelink.version = Eyelink('GetTrackerVersion');
end

%% save all information

save(fullfile(mPath,'data',sprintf('retention_%s.mat',header.subject.name)),'Data', 'header')

try
    
    %% preparation of presentation
    
    % Make sure the script is running on Psychtoolbox-3:
    AssertOpenGL;
    
    % configure keyboard
    KbName('UnifyKeyNames');
    KbCheck;
    olddebuglevel      = Screen('Preference', 'VisualDebuglevel', 3);
    
    header.key.arrows  = [KbName('LeftArrow') KbName('RightArrow')];
    header.key.eval    = [KbName('End') KbName('DownArrow') ...
        KbName('PageDown') KbName('LeftArrow') KbName('Clear')]; % numblock 1-5
    header.key.eval    = [KbName('1') KbName('2') KbName('3') KbName('4') KbName('5')]; % numblock 1-5
    header.key.trigger = KbName('t');
    header.key.escape  = KbName('ESCAPE');
    
    % configure screen
    screens       = Screen('Screens');
    screenNumber  = max(screens);
    oldresolution = Screen('Resolution', screenNumber, header.screenres(1), header.screenres(2));
    sr            = header.screenres;
    
    % set background color
    [expWin,rect] = Screen('OpenWindow',screenNumber,255/2,[],[],[],[],4);
    ifi           = Screen('GetFlipInterval', expWin); % for exact timing
    vbl           = Screen('Flip', expWin);
    [mx, my]      = RectCenter(rect);
    
    % set text size
    tsize_small = round(sr(2)/864*18);
    tsize_large = round(sr(2)/864*60);
    
    % disable keyboard and hide cursor
    ListenChar(0);
    HideCursor; % ShowCursor;
    
    % EYELINK detail
    %----------------------------------------------------------------------
    
    if header.eyetracking
        el = EyelinkInitDefaults(expWin);
        
        [v, vs] = Eyelink('GetTrackerVersion');
        fprintf('Running experiment on a ''%s'' tracker.\n', vs );
        
        % start recording
        %------------------------------------------------------------------
        
        Eyelink('Startrecording');
    end
    
    % fixation cross
    fix_color1 = [150 150 150]; % normal fixation cross
    fix_color3 = [200 100 100]; % error fixation cross
    
    
    %% start trials -------------------------------------------------------
    
    topPriorityLevel = MaxPriority(expWin);
    Priority(topPriorityLevel);
    
    tblock      = inf;     % time of block start
    trialstart  = header.ntrials*(blockstart-1)+1;
    fooKey(header.key.escape) = 0;
    Screen('TextSize', expWin, tsize_large);
    
    tdelay       = 0; % will be used to correct timing of display
    trial        = trialstart;
    shownIntro   = 0; % Intro texts
    
    while trial <= Data(end,1)
        
        % set parameters
        %------------------------------------------------------------------
        % reset time stamps and keys that are defined within each trial
        % multiple booleans are defined that ensure that each screen
        % will be called only once
        all_pressedKeys    = [];
        all_rt             = [];
        waspressed         = 0; % only one key press will be counted
        shownb             = 0; % block information
        shownf             = 0; % fixation cross
        shownff            = 0; % lighter fixation cross before stimulus
        showns             = 0; % stimulus
        trialover          = 0; % end of trial
        aborted            = 0;
        ttrial             = GetSecs; % trial reference time
        somcount           = 0;
        startled           = 0; % startle probe was played
        
        % prepare stimulus
        pattern  = Stimuli{Data(trial,7),1+Data(trial,4)}.stims; % CS+|US+ in second column
        tpattern = Stimuli{Data(trial,7)}.tstims;
        pinsetup = Stimuli{Data(trial,7)}.pinsetup;
        npulses  = length(tpattern);
        if trig, outp(address,pinsetup); end; % reset port
        
        % display introduction texts
        if ~shownIntro
            Introduction_Texts_retention; shownIntro = 1;
            Screen('TextSize', expWin, tsize_large);
            if header.eyetracking, Eyelink('Message','GO');end % send trigger
        end
        
        % set reference time of block
        if mod(trial,header.ntrials)==1
            tblock = GetSecs;
        end
        
        % infinite while loop with permanent escape option
        while ~trialover && ~fooKey(header.key.escape)
            
            [ ~, ~, fooKey ] = KbCheck; % check pressed keys at all times
            
            % show block info for 2 seconds -------------------------------
            if mod(trial,header.ntrials)==1 && GetSecs-tblock <= 2
                
                % set background color for the block
                rectColor    = header.context.color(Data(trial,2),:);
                baseRect     = [0 0 sr(1) sr(2)];
                
                holdscreen1 = 1; % delays fixation cross until block info was shown
                if ~shownb
                    shownb = 1;
                    
                    Screen('FillRect', expWin, rectColor, baseRect);
                    if Data(trial,2)==1
                        DrawFormattedText(expWin, ['\n\nStart Block ' num2str(floor(trial/header.ntrials)+9)], 'center', 'center');
                    elseif Data(trial,2)==2
                        DrawFormattedText(expWin, ['\n\nStart Block ' num2str(floor(trial/header.ntrials)+9) '\n\n(ohne elektrische Stimulation)'], 'center', 'center');
                    end
                    DrawFormattedText(expWin, '+', 'center', 'center', fix_color1);
                    Screen('Flip', expWin);
                end
            else
                holdscreen1 = 0;
            end
            
            
            % initial fixation cross --------------------------------------
            if ~shownf && ~holdscreen1
                shownf = 1;
                DrawFormattedText(expWin, '+', 'center', 'center', fix_color1);
                tfixation = Screen('Flip', expWin);
                disp(['Trial ' num2str(trial)])
                disp(Stimuli{Data(trial,7)}.descr)
            end
            
            
            % stimulus time window
            %--------------------------------------------------------------
            if GetSecs-tblock >= Data(trial,8)
                
                if ~showns
                    if header.eyetracking, Eyelink('Message',['CS' num2str(Data(trial,3)) '-' num2str(Data(trial,4)) '-' num2str(Data(trial,6))]);end % send trigger
                    showns = 1;
                    Data(trial,12) = GetSecs-tblock;
                    disp(['stimulus onset time: ' num2str(Data(trial,12)) 'sec'])
                end
                
                % deliver stimulus
                if  somcount<npulses ...
                        && GetSecs-tblock >= Data(trial,8)+tpattern(somcount+1) ...
                        && GetSecs-tblock < Data(trial,8)+header.delays.CS_duration
                    if somcount==1, Eyelink('Message',['p' num2str(Data(trial,7))]); end
                    somcount = somcount+1;
                    if trig, outp(address,pattern(somcount)); end
                end
                
                % startle sound event -----------------------------------------
                if ~aborted && ~startled && Data(trial,5) && GetSecs-tblock >= Data(trial,11)-ifi
                    if header.eyetracking, Eyelink('Message',['ST' num2str(Data(trial,3))]);end % send trigger
                    tST0 = GetSecs;
                    PsychPortAudio('Start', pahandle, 1, 0, 1);
                    tST1 = GetSecs;
                    TTT(trial) = tST1-tblock-Data(trial,12);
                    startled = 1;
                    disp(['++ STARTLE ++  onset:  ' num2str(tST0-tblock)])
                    disp(['               offset: ' num2str(tST1-tblock-Data(trial,8))])
                end
                
                % record pressed key
                [keyIsDown, timeSecs, keyCode] = KbCheck;
                
                if keyIsDown && ~waspressed
                    waspressed   = 1;
                    pressedKey   = find(keyCode);
                    pressedKey   = pressedKey(1);
                    all_rt(trial)          = timeSecs-tblock - Data(trial,8);
                    disp(['key pressed: ' KbName(pressedKey) ', ' ...
                        num2str(GetSecs-tblock) ', RT = ' num2str(all_rt(trial))])
                    Data(trial,13) = pressedKey;
                    Data(trial,14) = all_rt(trial);
                end
            end
            
            
            % end of trial and stimulus presentation: show fixation cross
            if ~trialover && GetSecs-tblock >= Data(trial,8)+header.delays.CS_duration*1.5
                
                trialover = 1;
                
                % show feedback -------------------------------------------
                
                % correct key for each trial depends on CS type and keyorder
                key_correct = header.key.arrows(header.task.order(Data(trial,3)));
                
                fprintf('\ntrial %02d: %s|%s\n', trial,CS_label{Data(trial,3)},US_label{Data(trial,4)+1})
                
                % feedback code
                if ~waspressed
                    Data(trial,15) = 3; % no key
                    fprintf('\n------------------------\nWRONG: no button presssed\n------------------------\n\n')
                    if ~aborted
                        % fixation cross turns red
                        Screen('FillRect', expWin, rectColor, baseRect);
                        DrawFormattedText(expWin, '+', 'center', 'center', fix_color3);
                        terror = Screen('Flip', expWin);
                        WaitSecs(header.delays.error_duration);
                        Screen('FillRect', expWin, rectColor, baseRect);
                        DrawFormattedText(expWin, '+', 'center', 'center', fix_color1);
                        Screen('Flip', expWin);
                    end
                elseif key_correct == pressedKey(1)
                    Data(trial,15) = 1; % correct response
                    fprintf('\n------------------------\nCORRECT\n------------------------\n\n')
                else
                    Data(trial,15) = 0; % wrong key
                    fprintf('\n------------------------\nWRONG: misidentified\n------------------------\n\n')
                    if ~aborted
                        % fixation cross turns red
                        Screen('FillRect', expWin, rectColor, baseRect);
                        DrawFormattedText(expWin, '+', 'center', 'center', fix_color3);
                        terror = Screen('Flip', expWin);
                        WaitSecs(header.delays.error_duration);
                        Screen('FillRect', expWin, rectColor, baseRect);
                        DrawFormattedText(expWin, '+', 'center', 'center', fix_color1);
                        Screen('Flip', expWin);
                    end
                end
                
            end
        end
        
        if fooKey(header.key.escape)
            interrupt_all
            save(fullfile(mPath,'data',sprintf('retention_%s_INTERRUPTED_trial%i.mat',header.subject.name,trial)),'Data', 'header')
            return
        end; % exit experiment upon ESC press
        
        % backup data
        save(fullfile(mPath,'data',sprintf('retention_%s.mat',header.subject.name)),'Data', 'header')
        
        % block end % -----------------------------------------------------
        if ~aborted && mod(trial,header.ntrials)==0 && trial~=header.ntrials*header.nruns
            Screen('TextSize', expWin, tsize_small);
            WaitSecs(header.delays.blockend);
            myText = 'Block beendet.';
            Screen('FillRect', expWin, rectColor, baseRect);
            DrawFormattedText(expWin, myText, 'center', 'center', 255);
            DrawFormattedText(expWin, 'Es geht automatisch weiter', 'center', sr(2)*.8, 255);
            Screen('Flip', expWin);
            WaitSecs(3)
            Screen('TextSize', expWin, tsize_large);
        end
        
        % Evaluation and goodbye screens after last trial
        if ~aborted && trial == header.ntrials*header.nruns
            
            Screen('TextSize', expWin, tsize_small);
            
            WaitSecs(header.delays.blockend);
            
            myText = ['Abschnitt beendet!\n\n' ...
                'Nach einer kurzen Pause geht es weiter.'];
            DrawFormattedText(expWin, 'Der Versuchsleiter informiert Sie gleich.', 'center', sr(2)*.8, 255);
            DrawFormattedText(expWin, myText, 'center', 'center', 255);
            Screen('Flip', expWin);
            
            if header.eyetracking
                Eyelink('Stoprecording');
                Eyelink('CloseFile');
                Eyelink('Shutdown');
            end
            
            % backup stimuli
            header.Stimuli = Stimuli;
            save(fullfile(mPath,'data',sprintf('retention_%s.mat',header.subject.name)),'Data', 'header')
            
            KbWait([], 3);
            
            Screen('TextSize', expWin, tsize_large);
        end
        
        
        % loop through trials, but repeat if wrong fixation was made
        if ~aborted
            trial = trial+1;
        else
            % add duration of broken trial
            tblock = tblock + (GetSecs-ttrial);
        end
    end
    
    Priority(0);
    
    
    %% end of experiment --------------------------------------------------
    
    disp(['EXPERIMENT END ' num2str(GetSecs-tblock)]);
    
    %% end of PTB script
    
    % cleanup
    ShowCursor;
    ListenChar(1);
    sca;
    
catch ME
    disp([ME.stack.line])
    disp([ME.identifier])
    disp([ME.message])
    
    % cleanup
    interrupt_all
end

PsychPortAudio('Close');

%% evaluation

for b = 1:header.nruns
    p = mean(Data((1:header.ntrials) + header.ntrials*(b-1),15) == 1)*100;
    fprintf('\nblock %i: %3.1f%%',b,p)
end

p = mean(Data(Data(:,6)==1,15)==1)*100;
fprintf('\n\npair 1: %3.1f%%',p)

p = mean(Data(Data(:,6)==0,15)==1)*100;
fprintf('\npair 2: %3.1f%%',p)

