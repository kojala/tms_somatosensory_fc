% PRESENT_STIMULI applies weak but perceiveable 4 sec stimulations to
% fingers, repeatedly in the following order:
%  - D1: middle left
%  - D2: index left
%  - D3: index right
%  - D4: middle right
% Random order of stimulation.
%
%__________________________________________________________________________
% Matthias Staib, Zurich, 14.03.2016

close all
clear; clc
rng('shuffle')

addpath(genpath(fullfile(pwd,'utils')))

% set parameters
%--------------------------------------------------------------------------
subj  = input('Subject # (three digits): ','s');
rIDX  = input('Run # (1, 2 or 3):        ','s');
subj  = [subj '_localizer_R' rIDX];
if exist(fullfile(pwd,'data',[subj '.mat']),'file'), disp('ABORT'); return; end

TR    = 2;
nvols = 150;


% load stimuli
%--------------------------------------------------------------------------
options = [];
[~,~, Stimuli] = prepare_som(options);


% setup PTB
%--------------------------------------------------------------------------
KbName('UnifyKeyNames');
keys.scanner = KbName('t');


% prepare parallel port
%--------------------------------------------------------------------------
address = hex2dec('D070');     % standard LPT1 output port address (0x378), as decimal
config_io; outp(address, 0)   % install and reset port


% generate stimulus order and timings
%--------------------------------------------------------------------------
    maxTime   = nvols*TR; % 2 sec * 300 volumes = 600 sec
    stimduration = 4;
    
if str2double(rIDX) < 3
    
    % for runs 1 and 2, use a fixed order and ITI between 6 and 9 seconds,
    % which is optimal for the main effect of each finger individually

    timing    = repmat(6:9,1,20);
    timing    = timing(randperm(length(timing)));
    timing    = cumsum(timing+stimduration)-stimduration;
    timing(timing>=maxTime) = [];
    
    % fixed stimulus order
    stimvec   = repmat([1 3 2 4],1,ceil(length(timing)/4));
    stimvec   = stimvec(1:numel(timing));
    
else
    
    % for run 3, use a random order and ITI between 2 and 4 seconds,
    % which is optimal for the contrast left minus right
    
    timing    = repmat(2:4,1,60);
    timing    = timing(randperm(length(timing)));
    timing    = cumsum(timing+stimduration)-stimduration;
    timing(timing>=maxTime) = [];
    
    stimvec   = repmat([1 3 2 4],1,ceil(length(timing)/4));
    stimvec   = stimvec(1:numel(timing));
    % randomize stimulus order, without 3 repititions
    stimvec   = stimvec(randperm(numel(stimvec)));
    while any(diff(diff(stimvec))==0)
        stimvec = stimvec(randperm(numel(stimvec)));
    end
end
disp('Ready to begin, waiting for scanner trigger (t)')

% save settings
%--------------------------------------------------------------------------
[~,~] = mkdir(fullfile(pwd,'data'));
save(fullfile(pwd,'data',[subj '.mat']),'stimduration','stimvec','timing');


%--------------------------------------------------------------------------
% begin experiment
%--------------------------------------------------------------------------

% wait for scanner trigger
keyCode = zeros(1,500);
while ~keyCode(keys.scanner),
    [keyIsDown, secs, keyCode, deltaSecs] = KbCheck;
end
disp('start')

t0 = GetSecs;

trial = 1;
while trial <= length(timing)
    
    % reset time stamps and keys that are defined within each trial
    trialover          = 0; % end of trial
    somcount           = 0;
    tstamp             = 0;
    
    % prepare stimulus
    %----------------------------------------------------------------------
    pattern  = Stimuli{stimvec(trial)}.stims;
    tpattern = Stimuli{stimvec(trial)}.tstims;
    pinsetup = Stimuli{stimvec(trial)}.pinsetup;
    npulses  = length(tpattern);
    outp(address,pinsetup); % reset port
    
    % infinite while loop
    while ~trialover
        
        % stimulus time window
        %------------------------------------------------------------------
        if GetSecs-t0 >= timing(trial)
            % actual presentation timing
            if ~tstamp
                timingtrial(trial) = GetSecs-t0; tstamp = 1;
                fprintf('\nFinger: D%i, time: %3.1f sec', ...
                    stimvec(trial),timingtrial(trial));
            end
            % deliver stimulus
            if  somcount<npulses ...
                    && GetSecs-t0 >= timing(trial)+tpattern(somcount+1) ...
                    && GetSecs-t0 < timing(trial)+stimduration
                somcount = somcount+1;
                outp(address,pattern(somcount));
            end
        end
        
        % end of trial
        if ~trialover && GetSecs-t0 >= timing(trial)+stimduration+.1
            fprintf(', ... stimulation end.');
            trialover = 1;
            trial     = trial+1;
        end
    end
end

% save settings including real timings
%--------------------------------------------------------------------------
save(fullfile(pwd,'data',[subj '.mat']),'stimduration','stimvec','timing','timingtrial');
save(fullfile(pwd,'data',[subj '_bkp.mat']));