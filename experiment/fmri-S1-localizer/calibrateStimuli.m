% calibrateCS should be used to find optimal intensity settings for weak
% stimulation. Fingers should be attached in the following order:
%  - D1: middle left
%  - D2: index left
%  - D3: index right
%  - D4: middle right
%
% Perform this script when participant is positioned in the scanner.
%
%__________________________________________________________________________
% Matthias Staib, Zurich, 14.03.2016

close all
clear; clc
addpath(genpath(fullfile(pwd,'utils')))

disp('Make sure to run Matlab 2014b 64 Bit from the desktop!')

% set parameters
%--------------------------------------------------------------------------
% subj  = input('Subject name (three digits): ','s');
% subj  = [subj '_CScalibration'];

% instructions
%--------------------------------------------------------------------------
disp('Make sure that fingers are attached correctly:');
disp('  - D1: middle left');
disp('  - D2: index left');
disp('  - D3: index right');
disp('  - D4: middle right');


% load stimuli
%--------------------------------------------------------------------------
options = [];
[~,~, Stimuli] = prepare_som(options);
stimduration = 1;


% setup PTB
%--------------------------------------------------------------------------
KbName('UnifyKeyNames');
keys.scanner = KbName('t');

% prepare parallel port
%--------------------------------------------------------------------------
address = hex2dec('D070');     % standard LPT1 output port address (0x378), as decimal
config_io; outp(address, 0)   % install and reset port


%--------------------------------------------------------------------------
% begin experiment
%--------------------------------------------------------------------------

stimID = 1;
keyCode = zeros(1,256);
while 1
    
    % set stimulus
    s = input('send/set stimulus 1-4 (ENTER = same, q = Quit): ','s');
    if ~isempty(s)
        stimID = str2double(s);
        if strcmp(s,'q'), break; end
    end
    
    trialover          = 0; % end of trial
    ttrial             = GetSecs; % trial reference time
    somcount           = 0;
    
    % prepare stimulus
    %----------------------------------------------------------------------
    pattern  = Stimuli{stimID}.stims;
    tpattern = Stimuli{stimID}.tstims;
    pinsetup = Stimuli{stimID}.pinsetup;
    npulses  = length(tpattern);
    outp(address,pinsetup); % reset port
    
    % infinite while loop during stimulus
    while ~trialover
        
        % check for escape
        [keyIsDown, secs, keyCode, deltaSecs] = KbCheck;
        
        % stimulus time window
        %------------------------------------------------------------------
        if GetSecs-ttrial >= .2
            
            % deliver stimulus
            if  somcount<npulses ...
                    && GetSecs-ttrial >= tpattern(somcount+1) ...
                    && GetSecs-ttrial < stimduration
                somcount = somcount+1;
                outp(address,pattern(somcount));
            end
        end
        
        if GetSecs-ttrial >= stimduration+.5
            trialover = 1;
        end
    end
end


% % Enter intensity and time from Digitimer
% %--------------------------------------------------------------------------
% disp(' ')
% disp('--- ENTER INTENSITY AND PULSE DURATION: ---')
% disp(' ')
% for didx = 1:4
%     DI(didx) = input(['Intensity D' num2str(didx) ': ']);
%     DT(didx) = input(['Time      D' num2str(didx) ': ']);
% end


% save
%--------------------------------------------------------------------------
% [~,~] = mkdir(fullfile(pwd,'data'));
% save(fullfile(pwd,'data',[subj '.mat']),'DI','DT');

