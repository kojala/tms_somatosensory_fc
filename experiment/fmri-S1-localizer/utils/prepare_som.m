function [Data, header, Stimuli] = prepare_som(options)

% PREPARE_SOM creates the header, conditions and timings for
% the experiment 'present_som_experiment.m'.
%
% requires options as input with the following fields
%  cond1: conditions (1-4)
%  cond2: button mapping (1-2)
%  cond3: background color (1-2)
%  cond4: order of complexity (1-2)
%  grayonly: coloured background or gray (1 0)
%  speedup: for testing only! divide all timings by speedfactor
%
%_________________________________________________________________________
% (C) 2014 Matthias Staib (Zurich University Hospital for Psychiatry)
%
% v001 01.12.2014 initial version
% v002 25.06.2015 random settings instead of fRMI order
% v003 28.10.2015 generalized for training, experiment and extinction

%% get parameters

try
    cond1    = options.cond1;
    cond2    = options.cond2;
    cond3    = options.cond3;
    cond4    = options.cond4;
    training = options.training;
catch
    options.training      = 2;
    options.pattidx       = [1 2 3 4]; % which patterns from create_somstim.m are used?
    options.cond1         = 1; %input('P (1-4): '); % which patterns are CS+/CS-
    options.cond2         = 1; %input('K (1-2): '); % keys
    options.cond3         = 1; % background color
    options.cond4         = 1; %input('C (1-2): '); % order of complexity blocks
    
    % experiment parameters
    options.params.ntrials           = 12;   % number of trials per block
    options.params.nruns             = 1;    % number of blocks (defined by one complexity setting)
    options.speedup                  = 0; % TEMPORARY!!!!!
    
    options.params.prob_CSp          = .5;  % probability of CS+
    options.params.prob_CSpp         = 0;   % probability of US+ given CS+
    options.params.prob_STp          = 0;   % probability of ST+ given CS
    
    % timing parameters
    options.params.CS_duration       = 4;    % duration of stimulus presentation
    options.params.SOA               = 3.5;  % time between CS onset and US onset
    options.params.response_window   = 3;    % allowed time to reply
    options.params.initial_info      = 2;    % time of block# display
    options.params.t0                = 5;    % time before first trial
    options.params.blockend          = 8;    % delay after last trial in a block
    options.params.error_duration    = 2;    % time of error display
    options.params.itis              = [5:9  7]; % ITI
end

cond1    = options.cond1;
cond2    = options.cond2;
cond3    = options.cond3;
cond4    = options.cond4;
training = options.training;

% very first trial is reinforced CS+ (only for experiment)
if options.training == 0, first = 1; max_rep_US = 3; else first = 0; max_rep_US = inf; end

ntrials         = options.params.ntrials;           % number of trials per block
nruns           = options.params.nruns;             % number of blocks (defined by one complexity setting)
prob_CSp        = options.params.prob_CSp;          % probability of CS+
prob_CSpp       = options.params.prob_CSpp;         % probability of US+ given CS+
prob_STp        = options.params.prob_STp;          % probability of US+ given CS+
CS_duration     = options.params.CS_duration;       % duration of stimulus presentation
SOA             = options.params.SOA;               % time between CS onset and US onset
response_window = options.params.response_window;   % allowed time to reply
initial_info    = options.params.initial_info;      % time of block# display
t0              = options.params.t0;                % time before first trial
blockend        = options.params.blockend;          % delay after last trial in a block
error_duration  = options.params.error_duration;    % time of error display
itis            = options.params.itis;              % ITI

grayonly = 0;
speedup  = options.speedup;
header.options = options;

rng('default') % reset random number generator
rng('shuffle') % reset random number generator

% surround settings, HAVE TO BE CHECKED BEFORE STARTING THE EXPERIMENT
header.viewDist        = 55; % how far sits subj from screen
header.screenHeightCm  = 24; % height in cm of the display on screen
% screen resolution
screenres        = get(0,'screensize'); % [left upper corner to right lower corner]
header.screenres = screenres(3:4);

% US parameters
USpin                      = 2^0;  % which pin to activate in addition?
header.shock.fq            = 100;  % frequency (Hz)
header.shock.d             = .5;   % duration (seconds)

%% balancing all conditions

% 1: complex, then simple; 2: reverse
header.complexity.order = [1 0];
if cond4==2, header.complexity.order = fliplr(header.complexity.order); end
complexity.order = header.complexity.order;

% training always starts with simple, then complex
if training == 1; complexity.order = [1 0];
elseif training == 2; complexity.order = [0 1]; end

% four possible permutations of condition-pattern mapping
%  - 2x reordering of traveling waves
%  - 2x reordering of frequency

orders = [ ...
    1 2 3 4; ... % original
    2 1 3 4; ... % swap CS for simple
    1 2 4 3; ... % swap CS for complex
    2 1 4 3; ... % swap CS for both
    ];
header.som.order_idx = cond1; % pick condition
header.som.order     = orders(header.som.order_idx,:); % reordered condition-som mapping
header.som.names     = 'CSps  CSms CSpc  CSmc';

% button press instructions
foo = [1 2; 2 1];
header.task.order = foo(cond2,:); % defines whether LEFT or RIGHT key is assigned to CS+ stimuli
header.task.arrowlabels = {'LINK', 'RECHT'};

% order of contexts
header.context.order       = ones(1,options.params.nruns); % always reinforcement

% background colors for contexts
context_color              = [95 76 121; 99 94 62];
if grayonly; context_color = [255 255 255; 255 255 255]/2; end;
header.context.color_order = foo(cond3,:);
header.context.color = context_color(foo(cond3,:),:);
label                = {'lila';'gelb'};
header.context.label = label(foo(cond3,:));

% prepare stimuli
%--------------------------------------------------------------------------

% load stimuli
I = create_somstim;
% extract stimuli
for s = 1:4
    Stimuli{s,1}.stims    = I.stims{options.pattidx(s)};
    Stimuli{s,1}.tstims   = I.tstims{options.pattidx(s)};
    Stimuli{s,1}.descr    = I.descr{options.pattidx(s)};
    Stimuli{s,1}.pinsetup = I.pinsetup{options.pattidx(s)};
end

% add US to CS (CS coded is in 200 Hz frequency)
nshocks = header.shock.fq*header.shock.d;
US      = [zeros(1,SOA*200) ...
    repmat([USpin zeros(1,(200/header.shock.fq)-1)],1,nshocks)];
for s = 1:size(Stimuli,1)
    try,Stimuli{s,2}.stims = Stimuli{s,1}.stims+US;end
end

% prepare runs
%--------------------------------------------------------------------------

Data = [];
for run_idx = 1:nruns % run through blocks
    D = [];
    
    % first trial CS+US+ only for first occurrence
    if run_idx > 2, first = 0; end
    
    %   2.  context type          1: reinforcement1
    D(:,2) = ones(ntrials,1)*header.context.order(run_idx);
    
    %   3.  CS type               1: CS+; 2: CS-
    CS_tmp = minrep_order([ones(ntrials*prob_CSp,1); ... % complexity 1
        ones(ntrials*(1-prob_CSp),1)*2],3,first)';
    D(:,3) = CS_tmp;
    
    %   4.  US occurrence         0: no US; 1: US (shock, 500ms)
    US_tmp = minrep_order([ones(ntrials*prob_CSp*prob_CSpp,1); ... % CS+ trials
        zeros(ntrials*(prob_CSp)*(1-prob_CSpp),1)],max_rep_US,first)';
    D(D(:,3)==1,4) = US_tmp;
    
    %   5. ST occurrence
    D(:,5) = NaN(ntrials,1);
    if prob_STp
        D(:,5) = ones(ntrials,1);
        D(D(:,3)==1,5) = ~US_tmp; % never CS and US at the same time
    end
    
    %   6.  stimulus complexity   0: simple; 1: complex
    iscomplex = ~complexity.order(mod(run_idx,2)+1);
    D(:,6) = ones(ntrials,1)*iscomplex;
    
    %   7. stimulus id, (complexity x CS)
    D(D(:,3)==1,7) = header.som.order(1+2*~iscomplex); % CS+
    D(D(:,3)==2,7) = header.som.order(2+2*~iscomplex); % CS-
    
    %   8.  time of CS onset      in seconds
    iti = repmat(itis,1,ntrials/numel(itis));
    iti = iti(randperm(ntrials));
    D(:,8) = cumsum([t0 iti(1:end-1)+CS_duration]);
    
    %   9.  time of fixation cross onset preceding the CS
    D(:,9) = [0; D(1:end-1,8)+CS_duration];
    
    Data = [Data; D];
end

% disp(Data([1:3 13:16],:))
% keyboard

%   1.  trial index
Data(:,1) = 1:nruns*ntrials;

%   10.  time of US onset
Data(:,10) = NaN(1,ntrials*nruns);
Data(Data(:,4)==1,10) = Data(Data(:,4)==1,8)+SOA;

%   11.  time of ST onset
Data(:,11) = NaN(1,ntrials*nruns);
Data(Data(:,5)==1,11) = Data(Data(:,5)==1,8)+SOA;

%   12.  actual time of CS onset
Data(:,12) = NaN(1,ntrials*nruns);
%   13. id of pressed button
Data(:,13) = NaN(1,ntrials*nruns);
%   14. time of button press
Data(:,14) = NaN(1,ntrials*nruns);
%   15. feedback of button press   0: wrong, 1: correct
Data(:,15) = NaN(1,ntrials*nruns);

% save parameters
header.ntrials                  = ntrials;
header.nruns                    = nruns;
header.delays.CS_duration       = CS_duration;
header.delays.ITI               = itis;
header.delays.t0                = t0;
header.delays.response_window   = response_window;
header.delays.initial_info      = initial_info;
header.delays.blockend          = blockend;
header.delays.SOA               = SOA;
header.delays.error_duration    = error_duration;

% rows of data structure:
header.datakeys{1}  = 'trial index';
header.datakeys{2}  = 'context type          1: reinforcement1; 2: reinforcement2';
header.datakeys{3}  = 'CS type               1: CS+; 2: CS- (ignored in in context 2)';
header.datakeys{4}  = 'US occurrence         0: no US; 1: US (shock, 500ms)';
header.datakeys{5}  = 'ST occurrence         0: no ST; 1: ST (white noise, 50ms)';
header.datakeys{6}  = 'stimulus complexity   0: simple; 1: complex';
header.datakeys{7}  = 'stimulus id           eight different combinations (complexity x f1 x f2)';
header.datakeys{8}  = 'time of CS onset      in milliseconds';
header.datakeys{9}  = 'time of fixation cross onset';
header.datakeys{10} = 'time of US onset';
header.datakeys{11} = 'time of ST onset';
header.datakeys{12} = 'real time of CS onset';
header.datakeys{13} = 'id of pressed button';
header.datakeys{14} = 'reaction time';
header.datakeys{15} = 'feedback of button press   0: wrong, 1: correct, 3: no response';


if speedup
    speedfactor = 20;
    Data(:,[8:11]) = Data(:,[8:11])/speedfactor;
    header.delays.CS_duration = header.delays.CS_duration/speedfactor;
    header.delays.blockend = header.delays.blockend/speedfactor;
    header.delays.initial_info = header.delays.initial_info/speedfactor;
    header.delays.ITI = header.delays.ITI/speedfactor;
    header.delays.error_duration = header.delays.error_duration/speedfactor;
    header.delays.response_window = header.delays.response_window/speedfactor;
    header.delays.t0 = header.delays.t0/speedfactor;
    disp('!!!!TESTING!!!! faster speed!')
end

end


% generate sequences with at most max_rep consecutive repetitions
function order = minrep_order(labelvector, max_rep,first)

isEqualSequence = 1;
while isEqualSequence
    % create random sequence
    tmp = randperm(numel(labelvector));
    
    order = labelvector(tmp); % shuffle trials
    
    % find start of sequences with pairwise different values
    p = find([true;diff(order)~=0;true]); % one if neighbours are different, zero else
    
    % find where these sequences are [max_rep] values apart by checking the
    % difference of the indices of start and end.
    isEqualSequence = ~isempty(find(diff(p)>=max_rep+1,1));
    
    % should the vector's first position be defined by first labelvector entry?
    if first isEqualSequence = isEqualSequence || ~(order(1)==labelvector(1)); end
    
end

end

