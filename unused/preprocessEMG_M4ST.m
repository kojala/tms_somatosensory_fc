function preprocessEMG_M4ST(ExpPath,sList)

% Steps:
%  1. Check availability of file
%  2. Preprocess EMG signal with Saurabh's M4ST settings
%  3. Save preprocessed data
%__________________________________________________________________________

EMGpath = fullfile(ExpPath,'data','scr');

% To do: include a part to fix for potential stops in the LabChart file,
% that is, to only take the last block of data. 
    
for sIDX = 1:numel(sList)
    
    if sList(sIDX) < 100
        fname = fullfile(EMGpath,['pspm_0' num2str(sList(sIDX)) '_retention.mat']);
    else
        fname = fullfile(EMGpath,['pspm_' num2str(sList(sIDX)) '_retention.mat']);
    end
    
    if exist(fname,'file')
        
        clear data
        [sts, infos, data] = pspm_load_data(fname,'emg');
        
        % Pre-process EMG data
        %------------------------------------------------------------------
        sr = data{1}.header.sr; % Sampling rate
        y  = data{1}.data;      % Data
        
        % Plot unprocessed EMG data
        %figure,plot(y,'r'); title(['Subject ' int2str(sIDX)])
        
        % M4ST settings
        
        % Bandpass
        [b,a] = butter(4,[50 470]/(sr/2));  % 4th order, cutoff 50-470 Hz
        y_filtered = filter(b,a,y);
        
        % Notch filter
%         L   = 20;       % Band
%         BW  = 5;        % Bandwidth
%         GBW = -3.0103;  %
%         Nsh = 1;        %
%         h = fdesign.comb('Notch', 'L,BW,GBW,Nsh', L, BW, GBW, Nsh, sr);
%         Hd = design(h, 'butter');
%         y_filtered = filter(Hd.Numerator,Hd.Denominator,y_filtered);
        
        % A notch filter with a Q-factor of 35 (an arbitrary value)
        wo = 50/(sr/2); bw = wo/35;
        [b, a] = iirnotch(wo, bw);
        y_filtered = filter(b, a, y_filtered);
            
        % Rectify and smooth signal
        y_filtered = abs(y_filtered);
        %y_filtered_M4ST = smooth(y_filtered_M4ST, 0.020*sr);
%         [b2, a2] = butter(4, 1/(2*pi*0.004*sr/2), 'low');
%         y_filtered = filter(b2,a2,y_filtered);
        [b,a]=butter(4,53.05/(sr/2), 'low');
        y_filtered = filter(b,a,y_filtered);
        
        % Plot processed EMG data
        %figure,plot(y_filtered,'b'); title(['Subject ' int2str(sIDX)])
        
        % Save preprocessed EMG data
        % add channel
        newdata = struct('data', y_filtered(:));
        newdata.header.sr = sr;
        newdata.header.chantype = 'emg';
        newdata.header.units = data{1}.header.units;
        
        action = 'add';
        options.msg.prefix = 'Preprocessed EMG';
        pspm_write_channel(fname, newdata, action, options);
    
%         data{5,1}.data = y_filtered;
%         data{5,1}.header.chantype = 'preprocessed EMG';
%         
%         save(fnamescr,'data','infos')
        
    end
end

end