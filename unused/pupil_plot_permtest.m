function pupil_plot_permtest(opts,sessions)

indir = opts.psr.path.main;

for ses = sessions
    
    test_type = opts.psr.permtest.test_type;
    test_no = opts.psr.permtest.test_no;
  
    for test = 1:test_no
        if strcmp(test_type,'anova')
            datafile_clusterp = fullfile(indir,['pupil_permuted_anova_pertimebin_clusterp' num2str(test) '.mat']);
        elseif strcmp(test_type,'ttest')
            datafile_clusterp = fullfile(indir,['pupil_permuted_ttest_pertimebin_clusterp' num2str(test) '.mat']);
        end
        permtest_data{test} = load(datafile_clusterp);
    end

    fig = figure('DefaultAxesFontSize', 12, 'DefaultAxesFontName', 'Arial', 'DefaultTextFontName', 'Arial');
    
    figwidth = 17.6; % 1/2 pdf column = 6, 1 pdf column = 8.5 cm, 1.5 pdf column = 11.6, 2 pdf columns = 17.6
    figheight = 8;
    
    set(fig, 'Units', 'Centimeters',...
        'PaperUnits', 'Centimeters', ...
        'PaperPositionMode','auto')
    
    set(fig,'Position', [0, 0, figwidth, figheight])
    
    set(gca,'LineWidth',1) % axes line width
    
    %     ymin = min(min(plotdata))-0.1;
    %     ymax = max(max(plotdata))+0.1;
    ymin = -0.2;
    ymax = 0.6;
    ylim([ymin ymax])
    xlim([0 11])
    line([6 6],[ymin ymax],'LineStyle','--','LineWidth',1.5,'Color',[192 192 192]./255,'HandleVisibility','off')
    
    hold on
    
    for cond = 1:conditions
        x = (1:size(plotdata,1))/sr;
        y = plotdata(:,cond);
        hl(cond) = boundedline(x, y, errorsem(:,cond), 'cmap', clr(cond,:),'alpha');
        %plot((1:size(plotdata,1))/sr, plotdata(:,CStype), 'Color', clr(CStype,:), 'LineWidth', 1.5);
    end
    
    hold on
    
    % Significance lines for t-tests
    sigpoints(1).ttest = [];
    for test = 2:4
        cluster_p = load(fullfile(opts.psr.linregPath,['pupil_pCS_permuted_ttest_pertimebin_clusterp' num2str(test) '.mat']),'cluster_p');
        cluster_p = cluster_p.cluster_p;
        sigpoints(test).ttest = find(cluster_p>=0)*5; % correct sampling rate
        sigpoints(test).ttest(sigpoints(test).ttest >= 300) = []; % remove significance points from US onset
    end
    
    for cond = 1:4
        x = (1:size(plotdata,1))/sr;
        y = plotdata(:,cond);
        plot(x, y, '-d', 'LineStyle', 'none', 'MarkerIndices', sigpoints(cond).ttest, ...
            'MarkerFaceColor', clr(cond,:), 'MarkerEdgeColor', clr(cond,:), 'MarkerSize', 2.5);
        hold on
    end
    
    % Significance lines for ANOVA
    cluster_p = load(fullfile(opts.psr.path.main,'pupil_preproc_acquisition_permtestdata'),'cluster_p');
    cluster_p = cluster_p.cluster_p;
    sigpoints_ANOVA = find(cluster_p>=0)*5; % correct sampling rate
    sigpoints_ANOVA(sigpoints_ANOVA >= 300) = []; % remove significance points from US onset
    
    x = (1:size(plotdata,1))/sr;
    y = -0.17*ones(1,length(x));
    plot(x, y, '-*', 'LineStyle', 'none', 'MarkerIndices', sigpoints_ANOVA, ...
        'MarkerFaceColor', 'm', 'MarkerEdgeColor', [128 128 128]./255, 'MarkerSize', 2.5);
    
    legend([hl(1) hl(2) hl(3) hl(4)],'String',{'Simple CS+US-', 'Simple CS-US-', 'Complex CS+US-', 'Complex CS-US-'},'Location','northwest');
    legend boxoff
    
    
    set(gca,'XTick',0:1:11,'FontSize',12)
    set(gca,'YTick',-0.2:0.1:0.6,'FontSize',12)
    
    xlabel('Time since trial onset (sec)','FontSize',12,'FontWeight','normal')
    ylabel('/delta Pupil size from baseline (mm)','FontSize',12,'FontWeight','normal')
    
end

end