% Visual inspection of SCR for CS+ (US+ and US-) and CS- of acquisition.
% Steps:
%  1. Check availability of file
%  2. Import LabChart file with PsPM
%  3. Cut out windows
%  4. Sort window by CS type
%  5. Plot
%__________________________________________________________________________
close all;clear;clc

% PsPM
% restoredefaultpath
addpath 'D:\PsPM_3.1\'

% Set paths
%--------------------------------------------------------------------------
ExpPath = 'D:\TMS_Experiment\SCR_EMG_data_scripts\';
SCRpath = fullfile(ExpPath,'data','scr');
Bhvpath = fullfile(ExpPath,'data','datainfo');

% All possible subjects
sList = [20 22:80];
% 41, 43, 64 no proper US response
% 42, 53 small or weird US response

s = 1;

scr_init
scr_jobman('initcfg');

for sIDX = 1:numel(sList)
    
    fname    = fullfile(SCRpath,['0' num2str(sList(sIDX)) '_retest.mat']);
    fnamescr = fullfile(SCRpath,['scr_0' num2str(sList(sIDX)) '_retest.mat']);
    fnamebhv = fullfile(Bhvpath,['retest_ss7b' num2str(sList(sIDX)) '.mat']);
    
    if exist(fname,'file') && ~exist(fnamescr,'file')
        
        % Import raw LabChart files
        %----------------------------------------------------------
        matlabbatch{1}.pspm{1}.prep{1}.import.datatype.labchartmat_in.datafile = {fname};
        matlabbatch{1}.pspm{1}.prep{1}.import.datatype.labchartmat_in.importtype{1}.scr.chan_nr.chan_nr_spec = 1;
        matlabbatch{1}.pspm{1}.prep{1}.import.datatype.labchartmat_in.importtype{1}.scr.transfer.none = true;
        matlabbatch{1}.pspm{1}.prep{1}.import.datatype.labchartmat_in.importtype{2}.custom.chan_nr.chan_nr_spec = 2;
        %matlabbatch{1}.pspm{1}.prep{1}.import.datatype.labchartmat_in.importtype{3}.emg.chan_nr.chan_nr_spec = 3;
        matlabbatch{1}.pspm{1}.prep{1}.import.overwrite = true;
        
        save matlabbatch.mat matlabbatch
        
        % Run batch
        scr_jobman('run', matlabbatch);
        
        %Find triggers and add marker channel manually
        %----------------------------------------------------------
        clear data infos
        load(fnamescr)
        foundTrigger = find(diff(data{2,1}.data)>2)/data{1, 1}.header.sr;
        data{4,1}.data              = foundTrigger;
        data{4,1}.header.sr         = 1;
        data{4,1}.header.chantype   = 'marker';
        data{4,1}.header.units      = 'events';
        data{4,1}.markerinfo.value  = zeros(size(foundTrigger));
        % Check number of markers! Should be... 96?
        save(fnamescr,'data','infos')
        
    end
    
    if exist(fnamescr,'file')
        clear data
        load(fnamescr)
        
        % Preprocess SCR data
        %------------------------------------------------------------------
        sr = data{1,1}.header.sr;
        y  = data{1,1}.data;
        
        % Plot unprocessed SCR data
        %figure,plot(y,'r'); title(['Subject ' int2str(sIDX)])
        
        % Bandpass & downsampling
        filt.sr = sr;
        filt.lpfreq = 5;
        filt.lporder = 1;
        filt.hpfreq = 0.0159;
        filt.hporder = 1;
        filt.direction = 'bi';
        filt.down = 10;
        
        [sts,y_filtds,newsr] = scr_prepdata(y,filt);

        %[b,a]=butter(1,[0.00159 5]/(sr/2),'bandpass');
        %y_filtered = filter(b,a,y);
        
        % Median filter to remove spike artefacts
        y_filtered = medfilt1(y_filtds,15);
        
        % Plot processed SCR data
        %figure,plot(y_filtered,'b'); title(['Subject ' int2str(sIDX)])
        
        % Collect SCR
        %------------------------------------------------------------------
        triggers = data{4,1}.data;
        for trial = 1:numel(triggers)
            t0 = round(triggers(trial)*newsr);
            t1 = t0+120; %12000 for original sr
            scr(:,trial,sIDX) = y_filtered(t0:t1);
            scrmaxampl(trial,sIDX) = max(y_filtered(t0:t1));
        end
        
        
        % Separate for CS+ (US+ and US-) and CS-
        %------------------------------------------------------------------
        % Load behavioural file with event IDs
        load(fnamebhv)
        
        CSpUSp = Data(:,3) == 1 & Data(:,4) == 1;
        CSpUSm = Data(:,3) == 1 & Data(:,4) == 0;
        CSm = Data(:,3) == 2;
        US = Data(:,4) == 1;
        scrCSpUSp(:,:,sIDX) = scr(:,CSpUSp,sIDX);
        scrCSpUSm(:,:,sIDX) = scr(:,CSpUSm,sIDX);
        scrCSm(:,:,sIDX) = scr(:,CSm,sIDX);
        
        scrmaxamplUS(:,sIDX) = scrmaxampl(US,sIDX);
        
%         % For simple stimuli
%         CSpUSp_s = Data(:,3) == 1 & Data(:,4) == 1 & Data(:,6) == 0;
%         CSpUSm_s = Data(:,3) == 1 & Data(:,4) == 0 & Data(:,6) == 0;
%         CSm_s = Data(:,3) == 2 & Data(:,6) == 0;
%         scrCSpUSp_s(:,:,sIDX) = scr(:,CSpUSp_s,sIDX);
%         scrCSpUSm_s(:,:,sIDX) = scr(:,CSpUSm_s,sIDX);
%         scrCSm_s(:,:,sIDX) = scr(:,CSm_s,sIDX);
%         
%         % For complex stimuli
%         CSpUSp_c = Data(:,3) == 1 & Data(:,4) == 1 & Data(:,6) == 1;
%         CSpUSm_c = Data(:,3) == 1 & Data(:,4) == 0 & Data(:,6) == 1;
%         CSm_c = Data(:,3) == 2 & Data(:,6) == 1;
%         scrCSpUSp_c(:,:,sIDX) = scr(:,CSpUSp_c,sIDX);
%         scrCSpUSm_c(:,:,sIDX) = scr(:,CSpUSm_c,sIDX);
%         scrCSm_c(:,:,sIDX) = scr(:,CSm_c,sIDX);
        
        % Keeping a list of the real subject numbers
        sList2(s) = sList(sIDX);
        s = s + 1;
        
    end
end


% Summarize and plot
%--------------------------------------------------------------------------
meanscrmaxamplUS = mean(scrmaxamplUS);
meanscrmaxamplUS(meanscrmaxamplUS==0) = [];
figure,bar(meanscrmaxamplUS); xlabel('Subject number'); ylabel('SCR average max amplitude of a trial');
figure,hist(meanscrmaxamplUS); xlabel('SCR average max amplitude of a trial'); ylabel('Count');

allMeansCSpUSp = squeeze(mean(scrCSpUSp(:,:,:),2));
allMeansCSpUSp(:,sum(allMeansCSpUSp)==0) = [];
allMeansCSpUSm = squeeze(mean(scrCSpUSm(:,:,:),2));
allMeansCSpUSm(:,sum(allMeansCSpUSm)==0) = [];
allMeansCSm = squeeze(mean(scrCSm(:,:,:),2));
allMeansCSm(:,sum(allMeansCSm)==0) = [];

% CSpUSm & CSm in general have 1 column missing (so data for 1 subject)!
% Subject 51?

figure
title('Fear retest - response to the US')
for sIDX = 1:size(allMeansCSpUSp,2)
    subplot(ceil(sqrt(size(allMeansCSpUSp,2))),ceil(sqrt(size(allMeansCSpUSp,2))),sIDX)
    title(['Subject ' int2str(sList2(sIDX))])
    hold on
    plot(allMeansCSpUSp(:,sIDX),'k')
    plot(allMeansCSpUSm(:,sIDX),'r')
    plot(allMeansCSm(:,sIDX),'b')
end
legend('CS+/US+','CS+/US-','CS-','Location','northwest'); legend boxoff

% % Simple stimuli
% allMeansCSpUSp_s = squeeze(mean(scrCSpUSp_s(:,:,:),2));
% allMeansCSpUSp_s(:,sum(allMeansCSpUSp_s)==0) = [];
% allMeansCSpUSm_s = squeeze(mean(scrCSpUSm_s(:,:,:),2));
% allMeansCSpUSm_s(:,sum(allMeansCSpUSm_s)==0) = [];
% allMeansCSm_s = squeeze(mean(scrCSm_s(:,:,:),2));
% allMeansCSm_s(:,sum(allMeansCSm_s)==0) = [];
%
% figure
% title('Fear retest with simple stimuli')
% for sIDX = 1:size(allMeansCSpUSm_s,2)
%     subplot(ceil(sqrt(size(allMeansCSpUSp_s,2))),ceil(sqrt(size(allMeansCSpUSp_s,2))),sIDX)
%     title(['Subject ' int2str(sList2(sIDX))])
%     hold on
%     plot(allMeansCSpUSp_s(:,sIDX),'k')
%     plot(allMeansCSpUSm_s(:,sIDX),'r')
%     plot(allMeansCSm_s(:,sIDX),'b')
% end
% legend('CS+/US+','CS+/US-','CS-','Location','northwest'); legend boxoff
% 
% % Complex stimuli
% allMeansCSpUSp_c = squeeze(mean(scrCSpUSp_c(:,:,:),2));
% allMeansCSpUSp_c(:,sum(allMeansCSpUSp_c)==0) = [];
% allMeansCSpUSm_c = squeeze(mean(scrCSpUSm_c(:,:,:),2));
% allMeansCSpUSm_c(:,sum(allMeansCSpUSm_c)==0) = [];
% allMeansCSm_c = squeeze(mean(scrCSm_c(:,:,:),2));
% allMeansCSm_c(:,sum(allMeansCSm_c)==0) = [];
% 
% figure
% title('Fear retest with complex stimuli')
% for sIDX = 1:size(allMeansCSpUSp_c,2)
%     subplot(ceil(sqrt(size(allMeansCSpUSp_c,2))),ceil(sqrt(size(allMeansCSpUSp_c,2))),sIDX)
%     title(['Subject ' int2str(sList2(sIDX))])
%     hold on
%     plot(allMeansCSpUSp_c(:,sIDX),'k')
%     plot(allMeansCSpUSm_c(:,sIDX),'r')
%     plot(allMeansCSm_c(:,sIDX),'b')
% end
% legend('CS+/US+','CS+/US-','CS-','Location','northwest'); legend boxoff

% Mean over all subjects
%--------------------------------------------------------------------------
overallMeanCSpUSp = mean(allMeansCSpUSp,2);
overallMeanCSpUSm = mean(allMeansCSpUSm,2);
overallMeanCSm = mean(allMeansCSm,2);

figure
hold on
plot(overallMeanCSpUSp,'k')
plot(overallMeanCSpUSm,'r')
plot(overallMeanCSm,'b')
title(['Mean SCR during fear retest, N = ', int2str(sIDX)])
xlabel('Time (ms)')
ylim([-5*(10^-7);20*(10^-7)])
ylabel('SCR (S)')
legend('CS+/US+','CS+/US-','CS-','Location','northwest'); legend boxoff

% % Stimple stimuli
% overallMeanCSpUSp_s = mean(allMeansCSpUSp_s,2);
% overallMeanCSpUSm_s = mean(allMeansCSpUSm_s,2);
% overallMeanCSm_s = mean(allMeansCSm_s,2);
% 
% figure
% hold on
% plot(overallMeanCSpUSp_s,'k')
% plot(overallMeanCSpUSm_s,'r')
% plot(overallMeanCSm_s,'b')
% title(['Mean SCR during fear retest (day 3) for simple stimuli, N = ', int2str(sIDX)])
% xlabel('Time (ms)')
% ylim([-5*(10^-7);20*(10^-7)])
% ylabel('SCR (S)')
% legend('CS+/US+','CS+/US-','CS-','Location','northwest'); legend boxoff
% 
% % Complex stimuli
% overallMeanCSpUSp_c = mean(allMeansCSpUSp_c,2);
% overallMeanCSpUSm_c = mean(allMeansCSpUSm_c,2);
% overallMeanCSm_c = mean(allMeansCSm_c,2);
% 
% figure
% hold on
% plot(overallMeanCSpUSp_c,'k')
% plot(overallMeanCSpUSm_c,'r')
% plot(overallMeanCSm_c,'b')
% title(['Mean SCR during fear retest (day 3) for complex stimuli, N = ', int2str(sIDX)])
% xlabel('Time (ms)')
% ylim([-5*(10^-7);20*(10^-7)])
% ylabel('SCR (S)')
% legend('CS+/US+','CS+/US-','CS-','Location','northwest'); legend boxoff