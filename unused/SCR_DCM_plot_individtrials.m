function SCR_DCM_plot_individtrials(opts,sessions,group)

for ses = sessions
    
    if group == 0
        %sList = [opts.scr.sList.DCM_control{ses} opts.scr.sList.DCM_experimental{ses}];
        groupname = 'all';
    elseif group == 1
        %sList = opts.scr.sList.DCM_control{ses};
        groupname = 'control';
    else
        %sList = opts.scr.sList.DCM_experimental{ses};
        groupname = 'experimental';
    end
    
    sesname = opts.scr.sessionlist{ses};
    
    dcmPath = fullfile(opts.scr.path.dcm, sesname);
    bhvPath = opts.bhv.path;
    
    %% Retrieve GLM stats
    
    dcmfile = ['DCM_trial_summary_' groupname '_' sesname '.mat'];
    data = load(fullfile(dcmPath,dcmfile));
    dcm_data = data.dcm_data_trial;
    
    dcm_est_cs1 = dcm_data.cs{1};
    dcm_est_cs2 = dcm_data.cs{2};
    dcm_est_cs3 = dcm_data.cs{3};
    dcm_est_cs4 = dcm_data.cs{4};
    dcm_est_cs5 = dcm_data.cs{5};
    dcm_est_cs6 = dcm_data.cs{6};
    
    dcm_est_us1 = dcm_data.us{1};
    dcm_est_us2 = dcm_data.us{2};
    dcm_est_us3 = dcm_data.us{3};
    dcm_est_us4 = dcm_data.us{4};
    dcm_est_us5 = dcm_data.us{5};
    dcm_est_us6 = dcm_data.us{6};
    
    %% Organize data
    
    simple_csp_usp = dcm_est_cs2;
    simple_csm_usm = dcm_est_cs3;
    
    complex_csp_usp = dcm_est_cs5;
    complex_csm_usm = dcm_est_cs6;
    
%     %% Individual trials plots
%     
%     colors = [0 102 244; 255 153 153; 51 153 255;...
%         255 51 51; 153 204 255; 204 0 0]/255;
%     
%     % CR
%     % US+ trials
%     figure
%     subplot(2,1,1)
%     title('US+ trials')
%     hold on
%     plot(1:3:(18*3),dcm_est_cs_usp(1:3:end,:),'o','MarkerFaceColor',colors(2,:),'MarkerEdgeColor','k','MarkerSize',8);
%     plot(2:3:(18*3),dcm_est_cs_usp(2:3:end,:),'o','MarkerFaceColor',colors(4,:),'MarkerEdgeColor','k','MarkerSize',8);
%     plot(3:3:(18*3),dcm_est_cs_usp(3:3:end,:),'o','MarkerFaceColor',colors(6,:),'MarkerEdgeColor','k','MarkerSize',8);
%     xlim([0 (18*3)+1])
%     set(gca,'XTick',2:3:18*3)
%     set(gca,'XTickLabel',1:18)
%     % cond2 = plot(dcm_est_cs2,'o','MarkerFaceColor',colors(2,:),'MarkerEdgeColor','k','MarkerSize',8);
%     % cond4 = plot(dcm_est_cs4,'o','MarkerFaceColor',colors(4,:),'MarkerEdgeColor','k','MarkerSize',8);
%     % cond6 = plot(dcm_est_cs6,'o','MarkerFaceColor',colors(6,:),'MarkerEdgeColor','k','MarkerSize',8);
%     % xlim([0 19])
%     % set(gca,'XTick',1:18)
%     % set(gca,'XTickLabel',1:18)
%     xlabel('Subjects')
%     ylabel('Coefficient (a.u.)')
%     
%     subplot(2,1,2)
%     title('US- trials')
%     hold on
%     plot(1:3:(18*3),dcm_est_cs_usm(1:3:end,:),'o','MarkerFaceColor',colors(1,:),'MarkerEdgeColor','k','MarkerSize',8);
%     plot(2:3:(18*3),dcm_est_cs_usm(2:3:end,:),'o','MarkerFaceColor',colors(3,:),'MarkerEdgeColor','k','MarkerSize',8);
%     plot(3:3:(18*3),dcm_est_cs_usm(3:3:end,:),'o','MarkerFaceColor',colors(5,:),'MarkerEdgeColor','k','MarkerSize',8);
%     xlim([0 (18*3)+1])
%     set(gca,'XTick',2:3:18*3)
%     set(gca,'XTickLabel',1:18)
%     % cond1 = plot(dcm_est_cs1,'o','MarkerFaceColor',colors(1,:),'MarkerEdgeColor','k','MarkerSize',8);
%     % cond3 = plot(dcm_est_cs3,'o','MarkerFaceColor',colors(3,:),'MarkerEdgeColor','k','MarkerSize',8);
%     % cond5 = plot(dcm_est_cs5,'o','MarkerFaceColor',colors(5,:),'MarkerEdgeColor','k','MarkerSize',8);
%     % xlim([0 19])
%     % set(gca,'XTick',1:18)
%     % set(gca,'XTickLabel',1:18)
%     xlabel('Subjects')
%     ylabel('Coefficient (a.u.)')
%     
%     suptitle('SCR individual DCM CR estimates, all trials Maint+Acq');
%     
%     % CR
%     titles = {'CS(1/3)US+ trials' 'CS(2/3)US+ trials' 'CS(1)US+ trials'...
%         'CS(0)US- trials' 'CS(1/3)US- trials' 'CS(2/3)US- trials'};
%     figure
%     order = [2 4 6 1 3 5];
%     index = 1;
%     for cr = 1:2
%         if cr == 1; plotdata = dcm_est_cs_usp; else; plotdata = dcm_est_cs_usm; end
%         for sp = 1:3
%             ind = index;
%             subplot(2,3,ind)
%             title(titles{ind})
%             hold on
%             plot(plotdata(sp:3:end,:),'o','MarkerFaceColor',colors(order(ind),:),'MarkerEdgeColor','k','MarkerSize',8); index = index + 1;
%             xlim([0 19])
%             set(gca,'XTick',1:18)
%             set(gca,'XTickLabel',1:18)
%             xlabel('Subjects')
%             ylabel('Coefficient (a.u.)')
%         end
%     end
%     suptitle('SCR individual DCM CR estimates, all trials Maint+Acq')
%     
%     % UR
%     % US+
%     figure
%     subplot(2,1,1)
%     title('US+ trials')
%     hold on
%     plot(1:3:(18*3),dcm_est_us_usp(1:3:end,:),'o','MarkerFaceColor',colors(2,:),'MarkerEdgeColor','k','MarkerSize',8);
%     plot(2:3:(18*3),dcm_est_us_usp(2:3:end,:),'o','MarkerFaceColor',colors(4,:),'MarkerEdgeColor','k','MarkerSize',8);
%     plot(3:3:(18*3),dcm_est_us_usp(3:3:end,:),'o','MarkerFaceColor',colors(6,:),'MarkerEdgeColor','k','MarkerSize',8);
%     xlim([0 (18*3)+1])
%     set(gca,'XTick',2:3:18*3)
%     set(gca,'XTickLabel',1:18)
%     % cond2 = plot(dcm_est_us2,'o','MarkerFaceColor',colors(2,:),'MarkerEdgeColor','k','MarkerSize',8);
%     % cond4 = plot(dcm_est_us4,'o','MarkerFaceColor',colors(4,:),'MarkerEdgeColor','k','MarkerSize',8);
%     % cond6 = plot(dcm_est_us6,'o','MarkerFaceColor',colors(6,:),'MarkerEdgeColor','k','MarkerSize',8);
%     % xlim([0 19])
%     % set(gca,'XTick',1:18)
%     % set(gca,'XTickLabel',1:18)
%     xlabel('Subjects')
%     ylabel('Coefficient (a.u.)')
%     
%     subplot(2,1,2)
%     title('US- trials')
%     hold on
%     plot(1:3:(18*3),dcm_est_us_usm(1:3:end,:),'o','MarkerFaceColor',colors(1,:),'MarkerEdgeColor','k','MarkerSize',8);
%     plot(2:3:(18*3),dcm_est_us_usm(2:3:end,:),'o','MarkerFaceColor',colors(3,:),'MarkerEdgeColor','k','MarkerSize',8);
%     plot(3:3:(18*3),dcm_est_us_usm(3:3:end,:),'o','MarkerFaceColor',colors(5,:),'MarkerEdgeColor','k','MarkerSize',8);
%     xlim([0 (18*3)+1])
%     set(gca,'XTick',2:3:18*3)
%     set(gca,'XTickLabel',1:18)
%     % cond1 = plot(dcm_est_us1,'o','MarkerFaceColor',colors(1,:),'MarkerEdgeColor','k','MarkerSize',8);
%     % cond3 = plot(dcm_est_us3,'o','MarkerFaceColor',colors(3,:),'MarkerEdgeColor','k','MarkerSize',8);
%     % cond5 = plot(dcm_est_us5,'o','MarkerFaceColor',colors(5,:),'MarkerEdgeColor','k','MarkerSize',8);
%     % xlim([0 19])
%     % set(gca,'XTick',1:18)
%     % set(gca,'XTickLabel',1:18)
%     xlabel('Subjects')
%     ylabel('Coefficient (a.u.)')
%     
%     suptitle('SCR individual DCM UR estimates, all trials Maint+Acq');
%     
%     % UR
%     figure
%     order = [2 4 6 1 3 5];
%     index = 1;
%     for cr = 1:2
%         if cr == 1; plotdata = dcm_est_us_usp; else; plotdata = dcm_est_us_usm; end
%         for sp = 1:3
%             ind = index;
%             subplot(2,3,ind)
%             title(titles{ind})
%             hold on
%             plot(plotdata(sp:3:end,:),'o','MarkerFaceColor',colors(order(ind),:),'MarkerEdgeColor','k','MarkerSize',8); index = index + 1;
%             xlim([0 19])
%             set(gca,'XTick',1:18)
%             set(gca,'XTickLabel',1:18)
%             xlabel('Subjects')
%             ylabel('Coefficient (a.u.)')
%         end
%     end
%     suptitle('SCR individual DCM UR estimates, all trials Maint+Acq')
    
end

end