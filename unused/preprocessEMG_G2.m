function preprocessEMG_G2(ExpPath,sList)

% Steps:
%  1. Check availability of file
%  2. Preprocess EMG signal with G2 settings
%  3. Save preprocessed data
%__________________________________________________________________________

EMGpath = fullfile(ExpPath,'data','scr');

% To do: include a part to fix for potential stops in the LabChart file,
% that is, to only take the last block of data. 
    
for sIDX = 1:numel(sList)
    
    if sList(sIDX) < 100
        fnamescr = fullfile(EMGpath,['pspm_0' num2str(sList(sIDX)) '_retention.mat']);
    else
        fnamescr = fullfile(EMGpath,['pspm_' num2str(sList(sIDX)) '_retention.mat']);
    end
    
    if exist(fnamescr,'file')
        
        fprintf('Subject # %3.0f ... \n', sList(sIDX))
        
        clear data
        
        [~, infos, data] = pspm_load_data(fnamescr, 'emg');
        
        % Pre-process EMG data
        %------------------------------------------------------------------
        sr = data{1}.header.sr; % Sampling rate
        y  = data{1}.data;      % Data
        
        % Plot unprocessed EMG data
        %figure,plot(y,'r'); title(['Subject ' int2str(sIDX)])
        
        % G2 filter settings
        filt = struct('modality', 'emg', ... % modality name
            'bandfreq', 'none',... % filter parameters
            'lpfreq', 'none', ...
            'hpfreq', 'none', ...
            'stopfreq', 'none', ...
            'notchfreq', 50, 'down', 'none', ...
            'filtertype', 'band');
        
        filt.bandorder      = 2; % Order of the filter
        filt.bandfreq       = [30 490]; % Filter band
        
        % Give to a filter function
        [y_filtered,~]      = startle_filter_emg(y,sr,filt);
        
        % Rectify and smooth using a 20-ms moving average
        y_filtered          = abs(y_filtered);
        y_filtered          = smooth(y_filtered, 0.020*sr);

        % Plot processed EMG data
        %figure,plot(y_filtered,'b'); title(['Subject ' int2str(sIDX)])
        
        % Save preprocessed EMG data
        data{1}.data              = y_filtered;
        data{1}.header.chantype   = 'emg';
        data{1}.header.units      = 'unknown';
        data{1}.header.sr         = sr;
        
        pspm_write_channel(fnamescr, data, 'add');
        
    end
end

end