function pupil_plot_clusterpermtrace(opts,sessions)

conditions = 4;
clr = [61, 145, 194; 150, 168, 207; 143, 59, 92; 186, 155, 167]./255; % CS+ simple, CS- simple, CS+ complex, CS- complex
sr = 100;
indir = opts.psr.path.main;

for ses = sessions
    
    %datafile_clusterp = fullfile(indir,'pupil_permuted_ttest_pertimebin_clusterp.mat');
    %data_clusterp = load(datafile_clusterp);
    %clusterp = data_clusterp.cluster_p;
    
    for group = 1:2
        
        if group == 1
            groupname = 'control';
            %grouptests = 1:2;
        else
            groupname = 'experimental';
            %grouptests = 3:4;
        end
        
        filename = fullfile(indir,['pupil_preproc_ses' num2str(ses) '_' groupname '_R.mat']);
        data = load(filename);
        
        pupildata = data.pupil_alldata_CS;
        
        % Pupil data per condition (CS+ simple, CS- simple, CS+ complex, CS-
        % complex)
        for cond = 1:conditions
            plotdata(:,cond) = nanmean(squeeze(pupildata(:,cond,:)),2);
        end
        
        
        %% Plotting
        
        % Within-subject error bars for all conditions and time points
        subavg = squeeze(nanmean(pupildata,2)); % mean over conditions for each sub for each timepoint
        grandavg = mean(subavg,2); % mean over subjects and conditions for each timepoint
        newvalues = nan(size(pupildata));
        subjects = size(pupildata,3);
        
        % normalization of subject values
        for cond = 1:conditions
            meanremoved = squeeze(pupildata(:,cond,:))-subavg; % remove mean of conditions from each condition value for each sub for each timepoint
            newvalues(:,cond,:) = meanremoved+repmat(grandavg,[1 subjects 1]); % add grand average over subjects to the values where individual sub average was removed
        end
        
        newvar = (cond/(cond-1))*nanvar(newvalues,[],3);
        tvalue = tinv(1-0.05, subjects-1);
        errorsem = squeeze(tvalue*(sqrt(newvar)./sqrt(subjects))); % calculate error bars
        
        % Draw figure
        fig = figure('DefaultAxesFontSize', 12, 'DefaultAxesFontName', 'Arial', 'DefaultTextFontName', 'Arial');
        
        figwidth = 8.5; % 1/2 pdf column = 6, 1 pdf column = 8.5 cm, 1.5 pdf column = 11.6, 2 pdf columns = 17.6
        figheight = 8;
        
        set(fig, 'Units', 'Centimeters',...
            'PaperUnits', 'Centimeters', ...
            'PaperPositionMode','auto')
        
        set(fig,'Position', [0, 0, figwidth, figheight])
        
        set(gca,'LineWidth',1) % axes line width
        
        %     ymin = min(min(plotdata))-0.1;
        %     ymax = max(max(plotdata))+0.1;
        ymin = -0.1;
        ymax = 0.5;
        ylim([ymin ymax])
        xmax = 4.5;
        xlim([0 xmax])
        line([3.5 3.5],[ymin ymax],'LineStyle','--','LineWidth',1.5,'Color',[192 192 192]./255,'HandleVisibility','off')
        
        hold on
        
        for cond = 1:conditions
            x = (1:size(plotdata,1))/sr;
            y = plotdata(:,cond);
            if cond > 2 % complex stimuli
                hl(cond) = boundedline(x, y, errorsem(:,cond), 'linewidth', 2, 'cmap', clr(cond,:),'alpha');
            else % simple stimuli normal line
                hl(cond) = boundedline(x, y, errorsem(:,cond), 'linewidth', 2, 'cmap', clr(cond,:),'alpha');
            end
            %plot((1:size(plotdata,1))/sr, plotdata(:,CStype), 'Color', clr(CStype,:), 'LineWidth', 1.5);
        end
        
        hold on
        
%         % Significance lines for t-tests
%         sigpoints(1).ttest = [];
%         for test = 1:2
%             clusterp_test = clusterp(grouptests(test),:);
%             sigpoints(test).ttest = find(clusterp_test>=0)*10; % correct sampling rate
%             %sigpoints(test).ttest(sigpoints(test).ttest >= 350) = []; % remove
%             %significance points from US onset (not needed anymore, done in R)
%         end
%         
%         cond2plot = [1 3]; % only plot permutation test results on CS+ traces
%         for cond = 1:2
%             x = (1:size(plotdata,1))/sr;
%             y = plotdata(:,cond2plot(cond));
%             plot(x, y, '-d', 'LineStyle', 'none', 'MarkerIndices', sigpoints(cond).ttest, ...
%                 'MarkerFaceColor', clr(cond2plot(cond),:), 'MarkerEdgeColor', clr(cond2plot(cond),:), 'MarkerSize', 3.5);
%             hold on
%         end
%         
%         % ANOVA results
%         datafile_clusterp_anova = fullfile(indir,'pupil_permuted_anova_pertimebin_clusterp.mat');
%         clusterp_anova = load(datafile_clusterp_anova);
%         clusterp_anova = clusterp_anova.cluster_p;
%         
%         % Significance lines for ANOVA
%         ycoords = [-0.01 -0.025 -0.04 -0.055 -0.07 -0.085 0];
%         anova_clr = [0, 0, 204; 255, 0, 102; 0, 153, 51; 204, 0, 204; 51, 204, 204; 255, 191, 0; 0, 0, 0]./255;
%         for test = 1:6 % Group and CS type main effects and CS type x group interaction
%             sigpoints_ANOVA = find(clusterp_anova(test,:)>=0)*10; % correct sampling rate
%             %sigpoints_ANOVA(sigpoints_ANOVA >= 350) = []; % remove significance points from US onset
%             %(not needed because post US onset data not used in the permutation)
%             x = (1:size(plotdata,1))/sr;
%             y = ycoords(test)*ones(1,length(x));
%             h2(test) = plot(x, y, '-*', 'LineStyle', 'none', 'MarkerIndices', sigpoints_ANOVA, ...
%                 'MarkerFaceColor', anova_clr(test,:), 'MarkerEdgeColor', anova_clr(test,:), 'MarkerSize', 5);
%         end
        
        set(gca,'XTick',0:0.5:xmax,'FontSize',12)
        set(gca,'YTick',ymin:0.1:ymax,'FontSize',12)
        
        xlabel('Time since trial onset (sec)','FontSize',12,'FontWeight','normal')
        ylabel('\Delta Pupil size from baseline (mm)','FontSize',12,'FontWeight','normal')
        groupname = opts.psr.groupnames{group};
        groupname = regexprep(lower(groupname),'(\<[a-z])','${upper($1)}');
        title(groupname)
        if group == 2 % plot legend only on 2nd figure
            leg = legend([hl(1) hl(2) hl(3) hl(4)],'String',{'CS+ simple', 'CS- simple', 'CS+ complex', 'CS- complex'},'Orientation','horizontal','NumColumns',2,'Location','south','FontSize',10);
            leg.ItemTokenSize = [15,15];
            %legend([hl(1) hl(2) hl(3) hl(4) h2(1) h2(2) h2(3) h2(4) h2(5) h2(6)],'String',{'CS+ simple', 'CS- simple', 'CS+ complex', 'CS- complex', 'Group main effect', 'CS type main effect' 'CS complexity main effect' 'Group x CS type' 'Group x CS complexity' 'CS type x CS complexity'},'Location','northwest','FontSize',10);
            legend boxoff
        end
        
        PAPER = get(fig,'Position');
        set(fig,'PaperSize',[PAPER(3), PAPER(4)]);
        plotname = ['PSR_timecourse_cluster_' opts.sessionlist{ses} '_' groupname];
        savefig(fig,fullfile(opts.expPath,'Plots',plotname));
        saveas(gcf,fullfile(opts.expPath,'Plots',plotname),'png')
        saveas(gcf,fullfile(opts.expPath,'Plots',plotname),'svg')
        
    end
end

end