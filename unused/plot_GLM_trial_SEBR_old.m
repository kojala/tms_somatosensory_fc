function plot_GLM_trial_SEBR_old(opts)

sesname = 'retention';

if opts.sebr.plot_allorders
%     sList = opts.sebr.sList.experim; % only experimental subjects, with all orders
%     groupname = 'experimental';
    sList = opts.sebr.sList.control; % only control subjects, with all orders
    groupname = 'control';
    allordname = 'all';
else
    sList = opts.sebr.sList.control_sameorder; % only control subjects now, with the same order
    allordname = 'same';
end

%% Get GLM data
data = get_GLM_data(opts,sList,'save');

%% Separate into simple and complex stimuli and CS+ and CS-

newsize = size(data.amplitude);
newsize(2) = newsize(2)/4;
amplitude_simple_CSp = reshape(data.amplitude(data.stimuli_CS == 1 & data.stimuli_complex == 0),newsize);
amplitude_simple_CSm = reshape(data.amplitude(data.stimuli_CS == 0 & data.stimuli_complex == 0),newsize);
amplitude_complex_CSp = reshape(data.amplitude(data.stimuli_CS == 1 & data.stimuli_complex == 1),newsize);
amplitude_complex_CSm = reshape(data.amplitude(data.stimuli_CS == 0 & data.stimuli_complex == 1),newsize);

if opts.sebr.plot_allconds % simple and complex stimuli separately
    condnames = {'Simple CS+' 'Simple CS-' 'Complex CS+' 'Complex CS-'};
    meanresp = [mean(amplitude_simple_CSp,2) mean(amplitude_simple_CSm,2) mean(amplitude_complex_CSp,2) mean(amplitude_complex_CSm,2)];
else % simple and complex stimuli together  
    condnames = {'CS+' 'CS-'};
    meanresp = [mean([amplitude_simple_CSp amplitude_complex_CSp],2)...
        mean([amplitude_simple_CSm amplitude_complex_CSm],2)];
end

%% Calculate within-subject error bars

subavg = nanmean(meanresp,2); % mean over conditions for each sub
grandavg = nanmean(subavg); % mean over subjects and conditions

newvalues = nan(size(meanresp));

% normalization of subject values
for cond = 1:size(meanresp,2)
    meanremoved = meanresp(:,cond)-subavg; % remove mean of conditions from each condition value for each sub
    newvalues(:,cond) = meanremoved+repmat(grandavg,[length(sList) 1 1]); % add grand average over subjects to the values where individual sub average was removed
    plotdata(:,cond) = nanmean(newvalues(:,cond));
end

%cond = cond-1; % remove the filler NaN condition

newvar = (cond/(cond-1))*nanvar(newvalues);
tvalue = tinv(1-0.05, length(sList)-1);
errorbars = squeeze(tvalue*(sqrt(newvar)./sqrt(length(sList)))); % calculate error bars according to Cousineau (2005) with Morey (2005) fix

%% Plot

clr = [59 100 173; 143 162 212; 59 100 173; 143 162 212]./255; % CS+ and CS- colors (darker and lighter blue)

% Bar graph
figure('Position',[300,300,800,400]);
hold on
for b = 1:length(plotdata)
    h = bar(b,plotdata(b));
    h.FaceColor = clr(b,:);
end
% Individual values
subjects = size(meanresp,1);
xdata = repmat(1:b,[subjects 1]);
scatter(xdata(:),meanresp(:), 'filled', 'MarkerFaceColor', [102 178 255]./255, 'MarkerEdgeColor', 'k', 'jitter', 'on', 'jitterAmount', 0.2);
% Error bars
errorbar(plotdata,errorbars,'k','LineStyle','none','LineWidth',2.5,'CapSize',0)
title(['SEBR ' groupname ' group ' allordname ' orders, N = ' num2str(length(sList))])
set(gca,'xTick', 1:length(condnames))
set(gca,'xTickLabel', condnames)
ylabel('Mean amplitude (a.u.)')
%ylim([0 1.5e-4])
ylim([-0.5 3])
savefig(fullfile(opts.sebr.path.model,['SEBR_CR_mean_' sesname '_' groupname '_simple-complex_std.fig']));

% colors = [76 0 153; 0 0 153; 0 76 153; 0 153 153]/255;
% % Box plot with individual values
% figure('Position',[300,300,800,600]);
% boxplot(newvalues)
% h = findobj(gca,'Tag','Box');
% for j=1:length(h)
%     patch(get(h(j),'XData'),get(h(j),'YData'),colors(j,:),'FaceAlpha',.6);
% end
% hold on
% plot(1:length(h),newvalues,'o','MarkerFaceColor',[153 0 76]/255,'MarkerEdgeColor',[153 0 76]/255,'MarkerSize',7)
% title(['SEBR ' groupname ' group ' allordname ' orders, N = ' num2str(length(sList))])
% set(gca,'xTick', 1:length(condnames))
% set(gca,'xTickLabel', condnames)
% ylabel('Amplitude (a.u.)')

end