function pupil_GLM(ExpPath,sList)

clear matlabbatch

% Settings
glm_ps_fc.outdir = cellstr(fullfile(ExpPath,'models','pupil'));
glm_ps_fc.chan.chan_def.chan_def_right = 'pupil_r';
glm_ps_fc.timeunits.seconds = 'seconds';
glm_ps_fc.session.missing.no_epochs = 0;

conditions = {'CS+US+','CS+US-','CS-'};

for c = 1:length(conditions)
    glm_ps_fc.session.data_design.condition(c).name = conditions{c};
    glm_ps_fc.session.data_design.condition(c).durations = 0;
end

glm_ps_fc.session.nuisancefile = {''};
glm_ps_fc.latency.fixed = 'fixed';
glm_ps_fc.bf.psrf_fc1 = 1;
glm_ps_fc.norm = false;
glm_ps_fc.filter.def = 0;
glm_ps_fc.overwrite = true;

% Sessions for this study
sessions = 1:3;
sessnames = {'','r','n'}; % acquisition, retention, retest
sessnames_bhv = {'','retention_','retest_'};

for s = 1:length(sList)
    s_id = num2str(sList(s));
    
    for ses = 1:length(sessions)    
        
        filename = ['ss7b' s_id sessnames{ses}];
        file = fullfile(ExpPath, 'data', 'eyelink', ['ivalfix_tpspm_' filename '.mat']);
        bhvfile = fullfile(ExpPath, 'data', 'datainfo', [sessnames_bhv{ses} filename '.mat']);
        
        if exist(bhvfile,'file')
            
            load(bhvfile)
            CSpUSp = Data(:,3) == 1 & Data(:,4) == 1;
            CSpUSm = Data(:,3) == 1 & Data(:,4) == 0;
            CSm = Data(:,3) == 2;
            
            load(file)
            markers = data{5,1}.data;
            markers(3:2:end) = [];
            markers(1) = [];
            onset_CSpUSp = markers(CSpUSp);
            onset_CSpUSm = markers(CSpUSm);
            onset_CSm = markers(CSm);
            glm_ps_fc.session.data_design.condition(1).onsets = onset_CSpUSp;
            glm_ps_fc.session.data_design.condition(2).onsets = onset_CSpUSm;
            glm_ps_fc.session.data_design.condition(3).onsets = onset_CSm;
        
        end
        
        if exist(file,'file')
            
            fprintf('Subject %3.0f, Session %3.0f \n', sList(s), sessions(ses))
            
            glm_ps_fc.modelfile = ['GLM_ ' filename];
            glm_ps_fc.session.datafile = cellstr(file);

            matlabbatch{1}.pspm{1}.first_level{1}.ps{1}.glm_ps_fc = glm_ps_fc;

            % Run batch
            pspm_jobman('run', matlabbatch);
            
        end
        
    end
    
end

sprintf('GLM done')