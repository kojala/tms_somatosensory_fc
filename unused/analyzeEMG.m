% visual inspection of EMG blink startle response for CS+ and CS-. This is
% meant as an overall quality assessment without detailed analyses.
% Steps:
%  1. Check availability of file
%  2. Import LabChart file with PsPM
%  3. Preprocess
%  4. Cut out windows after startle
%  5. Sort window by CS type
%  6. Plot
%__________________________________________________________________________
close all;clear;clc
%clear all

% PsPM
addpath 'D:\PsPM_3.1\'

% Set paths
%--------------------------------------------------------------------------
ExpPath = 'D:\TMS_Experiment\SCR_EMG_data_scripts\';
EMGpath = fullfile(ExpPath,'data','scr');
Bhvpath = fullfile(ExpPath,'data','datainfo');

% All possible subjects
sList = [20 22:37 39:57 58 59:80];

% 32 & 64 error: no data{4,1} (markers) existing (stops in the LabChart
% file also but how does it affect this? none of the separate LabChart
% blocks have the 4th cell with markers)
% 38 error: 'The cutoff frequencies must be within the interval of (0,1).'
% for butter bandpass. 38 also missing SCR channel. 
% 20, 26, 36, 38, 47 unusable EMG data? -> old subjects, the first ones
% with wrong EMG electrodes
% 48 also very bad, before I came to to the study
% 28, 41, 42 weird response? 28 before my time, 41 and 42 my first subjects
% alone

% To do: include a part to fix for potential stops in the LabChart file,
% that is, to only take the last block of data. 

scr_init
scr_jobman('initcfg');
        
s = 1;
    
for sIDX = 1:numel(sList)
    
    fname    = fullfile(EMGpath,['0' num2str(sList(sIDX)) '_retention.mat']);
    fnamescr = fullfile(EMGpath,['scr_0' num2str(sList(sIDX)) '_retention.mat']);
    fnamebhv = fullfile(Bhvpath,['retention_ss7b' num2str(sList(sIDX)) '.mat']);
    
    if exist(fname,'file') && ~exist(fnamescr,'file')
        
        % Import raw LabChart files
        %----------------------------------------------------------
        clear matlabbatch
        matlabbatch{1}.pspm{1}.prep{1}.import.datatype.labchartmat_in.datafile = {fname};
        if sList(sIDX) ~= 38; 
            matlabbatch{1}.pspm{1}.prep{1}.import.datatype.labchartmat_in.importtype{1}.scr.chan_nr.chan_nr_spec = 1;
            matlabbatch{1}.pspm{1}.prep{1}.import.datatype.labchartmat_in.importtype{1}.scr.transfer.none = true;
            matlabbatch{1}.pspm{1}.prep{1}.import.datatype.labchartmat_in.importtype{2}.custom.chan_nr.chan_nr_spec = 2;
            matlabbatch{1}.pspm{1}.prep{1}.import.datatype.labchartmat_in.importtype{3}.emg.chan_nr.chan_nr_spec = 3;
        else
            matlabbatch{1}.pspm{1}.prep{1}.import.datatype.labchartmat_in.importtype{1}.custom.chan_nr.chan_nr_spec = 2;
            matlabbatch{1}.pspm{1}.prep{1}.import.datatype.labchartmat_in.importtype{2}.emg.chan_nr.chan_nr_spec = 3;
        end;
        matlabbatch{1}.pspm{1}.prep{1}.import.overwrite = true;

        save matlabbatch.mat matlabbatch
        
        % Run batch
        scr_jobman('run', matlabbatch);
        %             job_id = cfg_util('initjob', matlabbatch);
        %             cfg_util('run', job_id);
        %             cfg_util('deljob', job_id);
        
        
        % Find triggers from sound and add marker channel
        %----------------------------------------------------------
        clear data infos options
        options.createchannel = true;
        options.diagnostics   = false;
        options.channelaction = 'replace';
        options.channeloutput = 'all';
        options.threshold     = .1;
        options.roi = [0,600];
        % switch case structure here
        if sList(sIDX) == 32; options.roi = [620,920];
        elseif sList(sIDX) == 22; options.roi = [40,490];
        elseif sList(sIDX) == 25 || sList(sIDX) == 29; options.roi = [80,430];
            %elseif sList(sIDX) == 29; options.roi = [90,430);
        elseif sList(sIDX) == 31; options.roi = [40,390];
        elseif sList(sIDX) == 36 || sList(sIDX) == 49; options.roi = [200,600];
        elseif sList(sIDX) == 28 || sList(sIDX) == 42 || sList(sIDX) == 51 || sList(sIDX) == 53; options.roi = [100,500];
            %elseif sList(sIDX) == 28; options.roi = [120,480];
        elseif sList(sIDX) == 41; options.roi = [20,380];
        elseif sList(sIDX) == 43; options.roi = [10,350];
        elseif sList(sIDX) == 45; options.roi = [50,400];
        elseif sList(sIDX) == 46; options.roi = [50,390];
        elseif sList(sIDX) == 47; options.roi = [30,370];
        elseif sList(sIDX) == 57; options.roi = [20,360];
        elseif sList(sIDX) == 58; options.roi = [40,380];
        elseif sList(sIDX) == 61; options.roi = [60,400];
        end;
        options.maxdelay      = 3;
        options.mindelay      = 0;
%        options.resample      = 0;
        options.sndchannel    = 2;
%        options.expectedSoundCount = 25;
        scr_find_sounds(fnamescr,options);
    
    end
    
    if exist(fnamescr,'file')
        
%         clear matlabbatch
% 
%         matlabbatch{1}.pspm{1}.data_preprocessing{1}.pp_emg{1}.find_sounds.datafile = {fnamescr};
%         matlabbatch{1}.pspm{1}.data_preprocessing{1}.pp_emg{1}.find_sounds.chan.chan_nr = 2;
%         matlabbatch{1}.pspm{1}.data_preprocessing{1}.pp_emg{1}.find_sounds.threshold = 0.1;
%         if sList(sIDX) == 36 || 49; matlabbatch{1}.pspm{1}.data_preprocessing{1}.pp_emg{1}.find_sounds.roi.region = [200 600];
%         else matlabbatch{1}.pspm{1}.data_preprocessing{1}.pp_emg{1}.find_sounds.roi.whole = 0; end
%         matlabbatch{1}.pspm{1}.data_preprocessing{1}.pp_emg{1}.find_sounds.output.create_chan.channel_action = 'replace';
% 
%         save matlabbatch.mat matlabbatch
%                 
%         scr_jobman('run', matlabbatch);
%         
        
        
        clear data
        load(fnamescr)
        
        % Pre-process EMG data
        %------------------------------------------------------------------
        sr = data{3,1}.header.sr;
        y  = data{3,1}.data;
        
        % Plot unprocessed EMG data
        %figure,plot(y,'r'); title(['Subject ' int2str(sIDX)])
        
        % Bandpass
        [b,a] = butter(2,[28 250]/(sr/2)); % butter(2,[28 250]/(sr/2),'bandpass') butter(4,[70 250]/(sr/2)) -> Saurabh's filter settings
        y_filtered = filter(b,a,y);
        
        % Notch filter
        L   = 20;       % Band
        BW  = 5;        % Bandwidth
        GBW = -3.0103;  %
        Nsh = 1;        %
        h = fdesign.comb('Notch', 'L,BW,GBW,Nsh', L, BW, GBW, Nsh, sr);
        Hd = design(h, 'butter');
        y_filtered = filter(Hd.Numerator,Hd.Denominator,y_filtered);
        
        % A notch filter with a Q-factor of 35 (an arbitrary value)
        %wo = 60/(sr/2); bw = wo/35;
        %[b, a] = iirnotch(wo, bw);
        %y_filtered = filter(b, a, y_filtered);
            
        % Rectify signal
        y_filtered = abs(y_filtered);
        y_filtered = smooth(y_filtered, 0.020*sr);
        %[b2, a2] = butter(4, 1/(2*pi*0.004*sr/2), 'low');
        %y_filtered = filter(b2,a2,y_filtered);
        
        % Plot processed EMG data
        
        %figure,plot(y_filtered,'b'); title(['Subject ' int2str(sIDX)])
        
        % Collect EMG responses
        %------------------------------------------------------------------
        triggers = data{4,1}.data; % check the correct no. of markers !!
        triggersfound(sIDX,1) = sList(sIDX);
        triggersfound(sIDX,2) = numel(triggers); % should be 25 at this point
        triggers(1) = []; % first startle sound is not part of the experiment
        
        for trial = 1:numel(triggers)
            t0 = round(triggers(trial)*sr);
            t1 = t0+300;
            emg(:,trial,sIDX) = y_filtered(t0:t1);
        end
        
        % Separate for CS+ and CS-
        %-------------------time-----------------------------------------------
        % Load behavioural file with event IDs
        load(fnamebhv)
        
        CSp = Data(:,3) == 1 & Data(:,6) == 0;
        CSm = Data(:,3) == 2 & Data(:,6) == 0;
        emgCSp(:,:,sIDX) = emg(:,CSp,sIDX);
        emgCSm(:,:,sIDX) = emg(:,CSm,sIDX);
        
%         % For simple stimuli
%         CSp_s = Data(:,3) == 1 & Data(:,6) == 0;
%         CSm_s = Data(:,3) == 2 & Data(:,6) == 0;
%         emgCSp_s(:,:,sIDX) = emg(:,CSp_s,sIDX);
%         emgCSm_s(:,:,sIDX) = emg(:,CSm_s,sIDX);
%         
%         % For complex stimuli
%         CSp_c = Data(:,3) == 1 & Data(:,6) == 1;
%         CSm_c = Data(:,3) == 2 & Data(:,6) == 1;
%         emgCSp_c(:,:,sIDX) = emg(:,CSp_c,sIDX);
%         emgCSm_c(:,:,sIDX) = emg(:,CSm_c,sIDX);
      
        % Peak scoring for maximum EMG startle amplitude
        %------------------------------------------------------------------
%         options.overwrite = 1;
%         options.method = 'simple';
%         options.summary = 'amplitude';
%         options.window = [-1 0; 1 4.5];
%         scr_peakscore(datafile,regfile,modelfile,timeunits,0,3,options);

        startletrigger_CSp = triggers(CSp);
        startletrigger_CSm = triggers(CSm);

        fprintf('Subject # %3.0f ... \n', sList(sIDX))
        
        for iTrl = 1:numel(startletrigger_CSp)
            winstart = ceil(startletrigger_CSp(iTrl) * sr);
            win = winstart + sr * [0 0.2];
            peakwin = winstart + sr * [0.02 0.10];
            [mx, ind] = max(y_filtered(peakwin(1):peakwin(2)));
            emgpeak_CSp(sIDX, iTrl) = mx;
        end
        
        for iTrl = 1:numel(startletrigger_CSm)
            winstart = ceil(startletrigger_CSm(iTrl) * sr);
            win = winstart + sr * [0 0.2];
            peakwin = winstart + sr * [0.02 0.10];
            [mx, ind] = max(y_filtered(peakwin(1):peakwin(2)));
            emgpeak_CSm(sIDX, iTrl) = mx;
        end
        
        for iTrl = 1:numel(triggers)
            winstart = ceil(triggers(iTrl) * sr);
            win = winstart + sr * [0 0.2];
            peakwin = winstart + sr * [0.02 0.10];
            [mx, ind] = max(y_filtered(peakwin(1):peakwin(2)));
            emgpeak(sIDX, iTrl) = mx;
        end
        
        % Keeping a list of the real subject numbers
        sList2(s) = sList(sIDX);
        s = s + 1;
        
    end
end


% Summarize and plot
%--------------------------------------------------------------------------
allMeansCSp = squeeze(mean(emgCSp(:,:,:),2));
allMeansCSp(:,sum(allMeansCSp)==0) = [];
allMeansCSm = squeeze(mean(emgCSm(:,:,:),2));
allMeansCSm(:,sum(allMeansCSm)==0) = [];

figure
title('Fear-potentiated startle')
for sIDX = 1:size(allMeansCSp,2)
    subplot(ceil(sqrt(size(allMeansCSp,2))),ceil(sqrt(size(allMeansCSp,2))),sIDX)
    title(['Subject ' int2str(sList2(sIDX))])
    hold on
    plot(allMeansCSp(:,sIDX),'r')
    plot(allMeansCSm(:,sIDX),'b')
end
legend('CS+','CS-','Location','northeast'); legend boxoff

% % Simple stimuli
% allMeansCSp_s = squeeze(mean(emgCSp_s(:,:,:),2));
% allMeansCSp_s(:,sum(allMeansCSp_s)==0) = [];
% allMeansCSm_s = squeeze(mean(emgCSm_s(:,:,:),2));
% allMeansCSm_s(:,sum(allMeansCSm_s)==0) = [];
% 
% figure
% title('Fear-potentiated startle with simple stimuli')
% for sIDX = 1:size(allMeansCSp_s,2)
%     subplot(ceil(sqrt(size(allMeansCSp_s,2))),ceil(sqrt(size(allMeansCSp_s,2))),sIDX)
%     title(['Subject ' int2str(sList2(sIDX))])
%     hold on
%     plot(allMeansCSp_s(:,sIDX),'r')
%     plot(allMeansCSm_s(:,sIDX),'b')
% end
% legend('CS+','CS-','Location','northeast'); legend boxoff
% 
% % Complex stimuli
% allMeansCSp_c = squeeze(mean(emgCSp_c(:,:,:),2));
% allMeansCSp_c(:,sum(allMeansCSp_c)==0) = [];
% allMeansCSm_c = squeeze(mean(emgCSm_c(:,:,:),2));
% allMeansCSm_c(:,sum(allMeansCSm_c)==0) = [];
% 
% figure
% title('Fear-potentiated startle with complex stimuli')
% for sIDX = 1:size(allMeansCSp_c,2)
%     subplot(ceil(sqrt(size(allMeansCSp_c,2))),ceil(sqrt(size(allMeansCSp_c,2))),sIDX)
%     title(['Subject ' int2str(sList2(sIDX))])
%     hold on
%     plot(allMeansCSp_c(:,sIDX),'r')
%     plot(allMeansCSm_c(:,sIDX),'b')
% end
% legend('CS+','CS-','Location','northeast'); legend boxoff

% Mean over all subjects
%--------------------------------------------------------------------------
emgmeanCSp = mean(emgpeak_CSp(emgpeak_CSp(:,1)~=0),2);
emgmeanCSm = mean(emgpeak_CSm(emgpeak_CSm(:,1)~=0),2);
emgmean = mean(emgpeak(emgpeak(:,1)~=0),2);
figure; hold on
bar(emgmeanCSm,'b')
bar(emgmeanCSp,'r')
title('EMG startle peak. CS+ red, CS- blue')
ylabel('EMG amplitude')
xlabel('Subject no.')

figure; hold on
bar(emgmean,'g')
title('EMG startle peak')
ylabel('EMG amplitude')
xlabel('Subject no.')

overallMeanCSp = mean(allMeansCSp,2);
overallMeanCSm = mean(allMeansCSm,2);

figure
hold on
plot(overallMeanCSp,'r')
plot(overallMeanCSm,'b')
title(['Average startle response during fear retention (day 3), N = ',int2str(sIDX)])
xlabel('Time (ms)')
ylabel('EMG (V)')
legend('CS+','CS-','Location','northeast'); legend boxoff

% % Simple stimuli
% overallMeanCSp_s = mean(allMeansCSp_s,2);
% overallMeanCSm_s = mean(allMeansCSm_s,2);
% 
% figure
% hold on
% plot(overallMeanCSp_s,'r')
% plot(overallMeanCSm_s,'b')
% title(['Average startle response during fear retention (day 3) for simple stimuli, N = ',int2str(sIDX)])
% xlabel('Time (ms)')
% ylabel('EMG (V)')
% legend('CS+','CS-','Location','northeast'); legend boxoff
% 
% % Complex stimuli
% overallMeanCSp_c = mean(allMeansCSp_c,2);
% overallMeanCSm_c = mean(allMeansCSm_c,2);
% 
% figure
% hold on
% plot(overallMeanCSp_c,'r')
% plot(overallMeanCSm_c,'b')
% title(['Average startle response during fear retention (day 3) for complex stimuli, N = ',int2str(sIDX)])
% xlabel('Time (ms)')
% ylabel('EMG (V)')
% legend('CS+','CS-','Location','northeast'); legend boxoff