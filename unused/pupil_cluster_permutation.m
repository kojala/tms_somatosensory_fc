function pupil_cluster_permutation(opts)

% Input directory
indir = opts.psr.path.main;
% Output directory
outdir = opts.psr.path.main;
if ~exist(outdir,'dir'); mkdir(outdir); end

test_type = opts.psr.permtest.test_type;
test_no = opts.psr.permtest.test_no;

% Data file for test statistics and cluster permutation test
if strcmp(test_type,'anova')
    datafile_test       = fullfile(indir,'pupil_preproc_acquisition_permtestdata.mat');
    datafile_clusterp   = fullfile(outdir,'pupil_permuted_anova_pertimebin_clusterp');
elseif strcmp(test_type,'ttest')
    datafile_test       = fullfile(indir,'pupil_preproc_acquisition_permtestdata_ttest.mat');
    datafile_clusterp   = fullfile(outdir,'pupil_permuted_ttest_pertimebin_clusterp');
end

%% Permutation test

permtest_data = load(datafile_test);

for test = 1:test_no
    
    if strcmp(test_type,'anova')
        
        Testvalue = permtest_data.Fvalue(:,:,test);
        pvalue = permtest_data.pvalue(:,:,test);
        Testvalue_orig = permtest_data.Fvalue_orig(:,test);
        pvalue_orig = permtest_data.pvalue_orig(:,test);
        
    elseif strcmp(test_type,'ttest')
        
        Testvalue = permtest_data.Tvalue(:,:,test);
        pvalue = permtest_data.pvalue(:,:,test);
        Testvalue_orig = permtest_data.Tvalue_orig(:,test);
        pvalue_orig = permtest_data.pvalue_orig(:,test);
        
    end
    
%     figure
%     plot(Testvalue,'LineStyle','none','Marker','o','MarkerFaceColor','b','MarkerEdgeColor','k')
%     hold on
%     plot(Testvalue_orig,'LineStyle','none','Marker','o','MarkerFaceColor','r','MarkerEdgeColor','k')
%     xlabel('Time (s)')
%     set(gca,'XTick',0:10:35,'XTickLabel',0:3.5)
%     xlim([0 35])
%     ylabel('Test value')
%     text(3,15,{'Red: Original condition labels' 'Blue: Randomly shuffled condition labels'})
%     title('Test values going into the permutation test')
    
    cluster_p(test,:) = permtest(Testvalue_orig,pvalue_orig,Testvalue,pvalue);
    
end

save(fullfile([datafile_clusterp  '.mat']),'cluster_p');

end