function preprocessEMG(ExpPath,sList)

% visual inspection of EMG blink startle response for CS+ and CS-. This is
% meant as an overall quality assessment without detailed analyses.
% Steps:
%  1. Check availability of file
%  2. Import LabChart file with PsPM
%  3. Preprocess
%  4. Save preprocessed data
%__________________________________________________________________________

EMGpath = fullfile(ExpPath,'data','scr');
Bhvpath = fullfile(ExpPath,'data','datainfo');

% To do: include a part to fix for potential stops in the LabChart file,
% that is, to only take the last block of data. 
    
for sIDX = 1:numel(sList)
    
    fname    = fullfile(EMGpath,['0' num2str(sList(sIDX)) '_retention.mat']);
    fnamescr = fullfile(EMGpath,['pspm_0' num2str(sList(sIDX)) '_retention.mat']);
    
    if exist(fname,'file') && ~exist(fnamescr,'file')
        
        % Import raw LabChart files
        %----------------------------------------------------------
        clear matlabbatch
        
        matlabbatch{1}.pspm{1}.prep{1}.import.datatype.labchartmat_in.datafile = {fname};
        
        if sList(sIDX) ~= 38; 
            matlabbatch{1}.pspm{1}.prep{1}.import.datatype.labchartmat_in.importtype{1}.scr.chan_nr.chan_nr_spec = 1;
            matlabbatch{1}.pspm{1}.prep{1}.import.datatype.labchartmat_in.importtype{1}.scr.transfer.none = true;
            matlabbatch{1}.pspm{1}.prep{1}.import.datatype.labchartmat_in.importtype{2}.custom.chan_nr.chan_nr_spec = 2;
            matlabbatch{1}.pspm{1}.prep{1}.import.datatype.labchartmat_in.importtype{3}.emg.chan_nr.chan_nr_spec = 3;
        else % Subjects 38 does not have SCR channel
            matlabbatch{1}.pspm{1}.prep{1}.import.datatype.labchartmat_in.importtype{1}.custom.chan_nr.chan_nr_spec = 2;
            matlabbatch{1}.pspm{1}.prep{1}.import.datatype.labchartmat_in.importtype{2}.emg.chan_nr.chan_nr_spec = 3;
        end;
        matlabbatch{1}.pspm{1}.prep{1}.import.overwrite = true;

        save matlabbatch.mat matlabbatch
        
        % Run batch
        pspm_jobman('run', matlabbatch);
        
        % Find triggers from sound and add marker channel
        %----------------------------------------------------------
        clear data infos options
        options.createchannel = true;
        options.diagnostics   = false;
        options.channelaction = 'replace';
        options.channeloutput = 'all';
        options.threshold     = .1;
        options.roi = [0,600];
        % switch case structure here
        if sList(sIDX) == 32; options.roi = [620,920];
        elseif sList(sIDX) == 22; options.roi = [40,490];
        elseif sList(sIDX) == 24; options.roi = [108,450];
        elseif sList(sIDX) == 25 || sList(sIDX) == 29; options.roi = [80,430];
        elseif sList(sIDX) == 31; options.roi = [40,390];
        elseif sList(sIDX) == 36 || sList(sIDX) == 49; options.roi = [200,600];
        elseif sList(sIDX) == 28 || sList(sIDX) == 42 || sList(sIDX) == 51 || sList(sIDX) == 53; options.roi = [100,500];
        elseif sList(sIDX) == 41; options.roi = [20,380];
        elseif sList(sIDX) == 43; options.roi = [10,350];
        elseif sList(sIDX) == 45; options.roi = [50,400];
        elseif sList(sIDX) == 46; options.roi = [50,390];
        elseif sList(sIDX) == 47; options.roi = [30,370];
        elseif sList(sIDX) == 57; options.roi = [20,360];
        elseif sList(sIDX) == 58; options.roi = [40,380];
        elseif sList(sIDX) == 61; options.roi = [60,400];
        end;
        options.maxdelay      = 3;
        options.mindelay      = 0;
        options.sndchannel    = 2;
        pspm_find_sounds(fnamescr,options);
    
    end
    
    if exist(fnamescr,'file')
        
        clear data
        load(fnamescr)
        
        % Pre-process EMG data
        %------------------------------------------------------------------
        sr = data{3,1}.header.sr; % Sampling rate
        y  = data{3,1}.data;      % Data
        
        % Plot unprocessed EMG data
        %figure,plot(y,'r'); title(['Subject ' int2str(sIDX)])
        
        % G2 settings
        % Bandpass
        [b,a] = butter(4,[30 490]/(sr/2)); % 4th order, cutoff 30-490 Hz
        y_filtered_G2 = filter(b,a,y);
        % Notch filter to remove 50 Hz harmonics
        L   = 20;       % Band
        BW  = 5;        % Bandwidth
        GBW = -3.0103;  %
        Nsh = 1;        %
        h = fdesign.comb('Notch', 'L,BW,GBW,Nsh', L, BW, GBW, Nsh, sr);
        Hd = design(h, 'butter');
        y_filtered_G2 = filter(Hd.Numerator,Hd.Denominator,y_filtered_G2);
        % Rectify and smooth using a 20-ms moving average
        y_filtered_G2 = abs(y_filtered_G2);
        y_filtered_G2 = smooth(y_filtered_G2, 0.020*sr);
        
        % M4ST settings
        % Bandpass
        [b,a] = butter(4,[50 470]/(sr/2));  % 4th order, cutoff 50-470 Hz
        y_filtered_M4ST = filter(b,a,y);
        % Notch filter
        L   = 20;       % Band
        BW  = 5;        % Bandwidth
        GBW = -3.0103;  %
        Nsh = 1;        %
        h = fdesign.comb('Notch', 'L,BW,GBW,Nsh', L, BW, GBW, Nsh, sr);
        Hd = design(h, 'butter');
        y_filtered_M4ST = filter(Hd.Numerator,Hd.Denominator,y_filtered_M4ST);
        
        % A notch filter with a Q-factor of 35 (an arbitrary value)
        %wo = 60/(sr/2); bw = wo/35;
        %[b, a] = iirnotch(wo, bw);
        %y_filtered = filter(b, a, y_filtered);
            
        % Rectify and smooth signal
        y_filtered_M4ST = abs(y_filtered_M4ST);
        %y_filtered_M4ST = smooth(y_filtered_M4ST, 0.020*sr);
        [b2, a2] = butter(4, 1/(2*pi*0.004*sr/2), 'low');
        y_filtered_M4ST = filter(b2,a2,y_filtered_M4ST);
        
        % Plot processed EMG data
        
        %figure,plot(y_filtered,'b'); title(['Subject ' int2str(sIDX)])
        
        % Save preprocessed EMG data
        data{5,1}.data = y_filtered;
        data{5,1}.header.chantype = 'preprocessed EMG';
        
        save(fnamescr,'data','infos')
        
    end
end

end