function psychophys_import(datapath,sList)

sessions_labchart = {'experiment' 'retention' 'retest'};
sessions_cog = {'' 'retention' 'retest'};

for ses = 1:length(sessions)
    
    for s = 1:length(sList)
        
        if sList(s) <= 100
            s_id = ['0' num2str(sList(s))];
        else
            s_id = num2str(sList(s));
        end
        
        inarg.wfn = fullfile(datapath,'scr','labchartmat',[s_id '_' sessions{ses}]);         % file name
        inarg.cfn = fullfile(datapath,'datainfo',[sessions_cog{ses} '_ss7b' s_id ]);         % cogent file name
        inarg.chan = {'scr' 'emg' 'snd' 'marker'};        % cell array of channel names
        inarg.markers = [];     % 2-element vectors of markers to use for trimming
        inarg.overwrite = 1;
        inarg.scr = 1000;       % transfer constant for SCR (mV/mcS) if entered incorrectly (or non-existent) in cogent file
        %inarg.soa = 3.5;       % CS/US SOA if different from 3.5 s (needed for EMG analysis)
        inarg.trimtimes = [];   % 2 element vector of times in s used for trimming
        
        [outnames, markerno] = pspm_pipeline_prepare_labchart(inarg);
        
    end
    
end

end