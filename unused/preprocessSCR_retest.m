function preprocessSCR_retest(ExpPath,sList)

% Visual inspection of SCR for CS+ (US+ and US-) and CS- of acquisition.
% Steps:
%  1. Check availability of file
%  2. Import LabChart file with PsPM
%  3. Cut out windows
%  4. Sort window by CS type
%  5. Plot
%__________________________________________________________________________

close all;clear;clc

% Set paths
%--------------------------------------------------------------------------
SCRpath = fullfile(ExpPath,'data','scr');
Bhvpath = fullfile(ExpPath,'data','datainfo');

s = 1;

pspm_init
pspm_jobman('initcfg');

% IMPLEMENT ARTEFACT CORRECTION & DCM !

for sIDX = 1:numel(sList)
    
    fname    = fullfile(SCRpath,['0' num2str(sList(sIDX)) '_retest.mat']);
    fnamescr = fullfile(SCRpath,['pspm_0' num2str(sList(sIDX)) '_retest.mat']);
    fnamescrt = fullfile(SCRpath,['tpspm_0' num2str(sList(sIDX)) '_retest.mat']);
    fnamebhv = fullfile(Bhvpath,['retest_ss7b' num2str(sList(sIDX)) '.mat']);
    
    if exist(fname,'file') && ~exist(fnamescr,'file')
        
        % Import raw LabChart files
        %----------------------------------------------------------
        clear matlabbatch
        
        % Import
        matlabbatch{1}.pspm{1}.prep{1}.import.datatype.labchartmat_in.datafile = {fname};  
        matlabbatch{1}.pspm{1}.prep{1}.import.datatype.labchartmat_in.importtype{1}.scr.chan_nr.chan_nr_spec = 1;
        matlabbatch{1}.pspm{1}.prep{1}.import.datatype.labchartmat_in.importtype{1}.scr.transfer.none = true;
        matlabbatch{1}.pspm{1}.prep{1}.import.datatype.labchartmat_in.importtype{2}.custom.chan_nr.chan_nr_spec = 2;    
        matlabbatch{1}.pspm{1}.prep{1}.import.overwrite = true;

        save matlabbatch.mat matlabbatch
        
        % Run batch
        pspm_jobman('run', matlabbatch);
        
        % Find triggers and add marker channel manually
        %----------------------------------------------------------
        clear data infos
        load(fnamescr)
        
        markers = data{2}.data;
        sr = data{2}.header.sr;
        
        % Replace negative values with the median of data
        markers(data{2}.data<0) = median(data{2}.data);
        
        % Find markers
        foundTrigger = find(diff(markers)>2)/sr;
        % Remove double markers
        % Some trigger slopes are too "slow" and had 2 datapoints in the 
        % same slope, if they are more or less in the middle this
        % (ie. [0 0 2.5 5 5 5]) the diff will return 2 consecutive values larger
        % than 2. Here, if 2 points with a Dt smaller than 2 sampling intervals
        % is detected, the 2nd point is removed.
        foundTrigger(find(diff(foundTrigger)<2/sr)+1) = [];
        
        data{3,1}.data              = foundTrigger;
        data{3,1}.header.sr         = 1;
        data{3,1}.header.chantype   = 'marker';
        data{3,1}.header.units      = 'events';
        data{3,1}.markerinfo.value  = ones(size(foundTrigger));
        data{3,1}.markerinfo.name   = num2cell(ones(size(foundTrigger)));
        save(fnamescr,'data','infos')
        
        % Trim the imported pspm file
        clear matlabbatch
        
        matlabbatch{1}.pspm{1}.prep{1}.trim.datafile = {fnamescr};
        matlabbatch{1}.pspm{1}.prep{1}.trim.ref.ref_mrk.from = 0;
        matlabbatch{1}.pspm{1}.prep{1}.trim.ref.ref_mrk.to = 20;
        matlabbatch{1}.pspm{1}.prep{1}.trim.ref.ref_mrk.mrk_chan.chan_nr = 3;
        matlabbatch{1}.pspm{1}.prep{1}.trim.overwrite = true;
        
        pspm_jobman('run', matlabbatch);
        
    end
    
    if exist(fnamescrt,'file')
        clear data
        load(fnamescrt)
        
        % Preprocess SCR data
        %------------------------------------------------------------------
        sr = data{1,1}.header.sr;
        y  = data{1,1}.data;
        
%         if ~exist(fullfile(ExpPath,'scr_acquisition_markedartefacts.mat'),'file') && isempty(y_artefacts{sIDX}) 
%             y_artefacts{sIDX} = pspm_data_editor(y);
%         end
        
        y_edited = y;
        
%         if exist(fullfile(ExpPath,'scr_acquisition_markedartefacts.mat'),'file') && ~isempty(y_artefacts{sIDX})
%             
%             for i=1:length(y_artefacts{sIDX})
%                 y_edited(y_artefacts{sIDX}(i,1):y_artefacts{sIDX}(i,2)) = NaN;
%             end
%             
%             % Plot unprocessed SCR data
%             %figure,plot(y,'b'); title(['Subject ' int2str(sList(sIDX))])
%             % Plot artefact removed signal
%             %figure,plot(y_edited,'r'); title(['Subject ' int2str(sList(sIDX))])
%         
%         end
        
        % Bandpass & downsampling (for plotting purposes only!)
        filt.sr = sr;
        filt.lpfreq = 5;
        filt.lporder = 1;
        filt.hpfreq = 0.0159;
        filt.hporder = 1;
        filt.direction = 'bi';
        filt.down = 10;
        
        [~,y_filtered,newsr] = pspm_prepdata(y_edited,filt);
        
        % Collect SCR
        %------------------------------------------------------------------
        triggers = data{4,1}.data;
                
        triggersfound(sIDX,1) = sList(sIDX);
        triggersfound(sIDX,2) = numel(triggers); % should be 96
        
        for trial = 1:numel(triggers)
            t0 = round(triggers(trial)*newsr);
            t1 = t0+120; %12000 for original sr
            scr(:,trial,sIDX) = y_filtered(t0:t1);
            scrmaxampl(trial,sIDX) = max(y_filtered(t0:t1));
        end
        
        
        % Separate for CS+ (US+ and US-) and CS-
        %------------------------------------------------------------------
        % Load behavioural file with event IDs
        load(fnamebhv)
        
        CSpUSp = Data(:,3) == 1 & Data(:,4) == 1;
        CSpUSm = Data(:,3) == 1 & Data(:,4) == 0;
        CSm = Data(:,3) == 2;
        US = Data(:,4) == 1;
        scrCSpUSp(:,:,sIDX) = scr(:,CSpUSp,sIDX);
        scrCSpUSm(:,:,sIDX) = scr(:,CSpUSm,sIDX);
        scrCSm(:,:,sIDX) = scr(:,CSm,sIDX);
        
        scrUS(:,:,sIDX) = scr(:,US,sIDX);
        scrmaxamplUS(:,sIDX) = scrmaxampl(US,sIDX);
        
%         % For simple stimuli
%         CSpUSp_s = Data(:,3) == 1 & Data(:,4) == 1 & Data(:,6) == 0;
%         CSpUSm_s = Data(:,3) == 1 & Data(:,4) == 0 & Data(:,6) == 0;
%         CSm_s = Data(:,3) == 2 & Data(:,6) == 0;
%         scrCSpUSp_s(:,:,sIDX) = scr(:,CSpUSp_s,sIDX);
%         scrCSpUSm_s(:,:,sIDX) = scr(:,CSpUSm_s,sIDX);
%         scrCSm_s(:,:,sIDX) = scr(:,CSm_s,sIDX);
%         
%         % For complex stimuli
%         CSpUSp_c = Data(:,3) == 1 & Data(:,4) == 1 & Data(:,6) == 1;
%         CSpUSm_c = Data(:,3) == 1 & Data(:,4) == 0 & Data(:,6) == 1;
%         CSm_c = Data(:,3) == 2 & Data(:,6) == 1;
%         scrCSpUSp_c(:,:,sIDX) = scr(:,CSpUSp_c,sIDX);
%         scrCSpUSm_c(:,:,sIDX) = scr(:,CSpUSm_c,sIDX);
%         scrCSm_c(:,:,sIDX) = scr(:,CSm_c,sIDX);
        
        % Keeping a list of the real subject numbers
        sList2(s) = sList(sIDX);
        s = s + 1;
        
    end
end


% Summarize and plot
%--------------------------------------------------------------------------

% SCR to US per subject normalized to the peak amplitude
allMeansUS = squeeze(mean(scrUS(:,:,:),2));
allMeansUS(:,sum(allMeansUS)==0) = [];

maxscr = max(allMeansUS);

scrnorm=bsxfun(@rdivide,allMeansUS,maxscr);

figure
title('SCR normalized to the peak amplitude')
for sIDX = 1:size(scrnorm,2)
    subplot(ceil(sqrt(size(scrnorm,2))),ceil(sqrt(size(scrnorm,2))),sIDX)
    title(['Subject ' int2str(sList2(sIDX))])
    hold on
    plot(scrnorm(:,sIDX),'b')
end

end