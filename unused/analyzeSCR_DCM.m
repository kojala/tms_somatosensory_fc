function analyzeSCR_DCM(ExpPath,sList,session)

SCRpath = fullfile(ExpPath,'data','scr');
Bhvpath = fullfile(ExpPath,'data','datainfo');
Modelpath = fullfile(ExpPath,'models','SCR');

sesListSCR = {'_experiment','_retention','_retest'};
sesListbhv = {'','retention_','retest_'};

for sIDX = 1:length(sList)
    
    % File names
    if sList(sIDX) < 100
        fnamescr = fullfile(SCRpath,['atpspm_0' num2str(sList(sIDX)) sesListSCR{session} '.mat']);
    else % Check what is correct for when subject numbers go past 99!
        fnamescr = fullfile(SCRpath,['atpspm_' num2str(sList(sIDX)) sesListSCR{session} '.mat']);
    end
    fnamebhv     = fullfile(Bhvpath,[sesListbhv{session} 'ss7b' num2str(sList(sIDX)) '.mat']);
    fnameevents  = fullfile(Bhvpath,['event_timings_' num2str(sList(sIDX)) sesListSCR{session} '.mat']);
    fnamemodel   = fullfile(Modelpath,['dcm_' sList(sIDX) sesListSCR{session} '.mat']);
    
    if exist(fnamescr,'file') && ~exist(fnamemodel,'file')
        
        % Separate for CS+ (US+ and US-) and CS-
        %------------------------------------------------------------------
        % Load behavioural file with event IDs
        load(fnamebhv)
        
        CSpUSp = Data(:,3) == 1 & Data(:,4) == 1;
        CSpUSm = Data(:,3) == 1 & Data(:,4) == 0;
        CSm = Data(:,3) == 2;
        
        % DCM for SCR amplitude
        %------------------------------------------------------------------
        load(fnamescr)
        
        % Check number of triggers!
        
        % Create an event file
        SOA = header.delays.SOA;
        CS_onset = data{3,1}.data; % recorded triggers of CS onset in seconds
        US_onset = data{3,1}.data + SOA; % US starts 3.5 seconds after CS
        events{1} = [CS_onset US_onset];
        events{2} = US_onset;
        save(fnameevents,'events')
        
        % 1st level non-linear DCM
        clear matlabbatch dcm
        dcm.modelfile = fnamemodel;
        dcm.outdir = cellstr(Modelpath);
        dcm.chan.chan_def = 0;
        dcm.session.datafile = cellstr(fnamescr);
        dcm.session.timing.timingfile = cellstr(fnameevents);
        dcm.session.condition(1).name = 'CS+|US-';
        dcm.session.condition(1).index = find(CSpUSm==1)';
        dcm.session.condition(2).name = 'CS+|US+';
        dcm.session.condition(2).index = find(CSpUSp==1)';
        dcm.session.condition(3).name = 'CS-';
        dcm.session.condition(3).index = find(CSm==1)';
        dcm.data_options.norm = 0;
        dcm.data_options.filter.def = 0;
        dcm.resp_options.crfupdate = 0;
        dcm.resp_options.indrf = 0;
        dcm.resp_options.getrf = 0;
        dcm.resp_options.rf = 0;
        dcm.inv_options.depth = 2;
        dcm.inv_options.sfpre = 2;
        dcm.inv_options.sfpost = 5;
        dcm.inv_options.sffreq = 0.5;
        dcm.inv_options.sclpre = 2;
        dcm.inv_options.sclpost = 5;
        dcm.inv_options.ascr_sigma_offset = 0.1;
        dcm.disp_options.dispwin = 0;
        dcm.disp_options.dispsmallwin = 0;
        matlabbatch{1}.pspm{1}.first_level{1}.scr{1}.dcm = dcm;
        
        save matlabbatchdcm.mat matlabbatch
        
        %Run batch
        pspm_jobman('run', matlabbatch);

    end
    
end

end