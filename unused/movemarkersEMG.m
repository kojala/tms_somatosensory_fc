function movemarkersEMG(opts)

sList = opts.sebr.sList.orig;

for sIDX = 1:length(sList)
    
    % Data file
    if sList(sIDX) < 100
        fnamesebr = fullfile(opts.sebr.path.trim,['tpspm_ss7b_0' num2str(sList(sIDX)) '_physio_retention_SEBR.mat']);
        newfilename = fullfile(opts.sebr.path.trim,['tpspm_ss7b_0' num2str(sList(sIDX)) '_physio_retention_SEBR_movedmarkers.mat']);
    else
        fnamesebr = fullfile(opts.sebr.path.trim,['tpspm_ss7b_' num2str(sList(sIDX)) '_physio_retention_SEBR.mat']);
        newfilename = fullfile(opts.sebr.path.trim,['tpspm_ss7b_' num2str(sList(sIDX)) '_physio_retention_SEBR_movedmarkers.mat']);
    end
    
    copyfile(fnamesebr,newfilename);
    
    [~,~,data] = pspm_load_data(newfilename);
    markers = data{opts.sebr.channel.marker}.data;
    newmarkers = markers-0.02;
    
    newdata = data{opts.sebr.channel.marker};
    newdata.data = newmarkers;
    options.channel = opts.sebr.channel.marker;
    
    pspm_write_channel(newfilename,newdata,'replace',options);
    
end