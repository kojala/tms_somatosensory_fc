function SCR_GLM_cond_plot(opts,sessions)

for ses = sessions
    
    %% Plot
    condnames = {'CS+' 'CS-' 'CS+' 'CS-'};   
    markersize = 14;
    barcolor = [103,169,207; 189,201,225; 103,169,207; 189,201,225]./255;
    scattercolor = [192 192 192]./255;
    
    fig = figure('DefaultAxesFontSize', 12, 'DefaultAxesFontName', 'Arial', 'DefaultTextFontName', 'Arial');
    
    figwidth = 17.6; % 1/2 pdf column = 6, 1 pdf column = 8.5 cm, 1.5 pdf column = 11.6, 2 pdf columns = 17.6
    figheight = 6.5;
    
    set(fig, 'Units', 'Centimeters',...
        'PaperUnits', 'Centimeters', ...
        'PaperPositionMode','auto')
    
    set(fig,'Position', [0, 0, figwidth, figheight])
    
    set(gca,'LineWidth',1) % axes line width
    
    for group = 1:2
        
        if group == 1
            sList = opts.scr.sList.GLM_control{ses};
        else
            sList = opts.scr.sList.GLM_experimental{ses};
        end
        
        groupname = opts.scr.groupnames{group};
        sesname = opts.scr.sessionlist{ses};
        
        glmpath = fullfile(opts.scr.path.glm_cond,opts.scr.glm_bfname{opts.scr.estimated_response},sesname);
        
        no_conditions = (opts.scr.estimated_conditions-1)*2; % take only CS+US- and CS-US- (leave out CS+US+)
        
        %% Retrieve GLM stats
        recresp = nan(length(sList),no_conditions);
        
        for s = 1:numel(sList)
            s_id = num2str(sList(s));
            cond = 1;
            
            for stim = 1:2
                filename = [opts.scr.glm_filename.cond s_id '_' opts.scr.glm.stimname{stim} '.mat'];
                datafile = fullfile(glmpath,filename);
                
                if exist(datafile,'file')
                    [~, glm] = pspm_glm_recon(datafile);
                    recresp(s,cond:cond+1) = glm.recon(2:3);
                    cond = cond + 2;
                end
            end
        end
        
        % Data multiplier
        recresp = recresp.*opts.scr.datamultiplier;

        % Calculate within-subject error bars
        subavg = nanmean(recresp,2); % mean over conditions for each sub
        grandavg = nanmean(subavg); % mean over subjects and conditions
        
        newvalues = nan(size(recresp));
        
        % normalization of subject values
        for cond = 1:size(recresp,2)
            meanremoved = recresp(:,cond)-subavg; % remove mean of conditions from each condition value for each sub
            newvalues(:,cond) = meanremoved+repmat(grandavg,[length(sList) 1 1]); % add grand average over subjects to the values where individual sub average was removed
            plotdata(:,cond) = nanmean(newvalues(:,cond));
        end
        
        tvalue = tinv(1-0.05, length(sList)-1);
        newvar = (cond/(cond-1))*nanvar(newvalues);
        errorbars = squeeze(tvalue*(sqrt(newvar)./sqrt(length(sList)))); % calculate error bars according to Cousineau (2005) with Morey (2005) fix
        
        subplot(1,2,group)
        for b = 1:length(plotdata)
            h = bar(b,plotdata(b),'LineWidth',1.5);
            hold on
            h.FaceColor = barcolor(b,:);
        end
        
        % Individual values
        subjects = length(sList);
        xdata = repmat(1:b,[subjects 1]);
        jitter_amount = 0.2;
        jittered_xdata = xdata + (rand(size(xdata))-0.5)*(2*jitter_amount);
        %plot(jittered_xdata',sebr_group','-','Color',linecolor);
        hold on
        for c = 1:length(plotdata)
            scatter(jittered_xdata(:,c),recresp(:,c), markersize, 'filled', 'MarkerFaceColor', scattercolor, 'MarkerEdgeColor', 'k');
            hold on
        end
        
        % Error bars
        errorbar(plotdata,errorbars,'k','LineStyle','none','LineWidth',2.5,'CapSize',0)
        groupname = regexprep(lower(groupname),'(\<[a-z])','${upper($1)}');
        title(groupname)
        set(gca,'xTick', 1:length(condnames),'FontSize',12)
        set(gca,'xTickLabel', condnames,'FontSize',12)
        if group == 1
            ylabel('SCR estimate (\muS)','FontSize',12,'FontWeight','normal')
            xlabel('Simple')
        else
            xlabel('Complex')
        end
        if group == 1; set(gca,'yTick', -1:1:5,'FontSize',12); ylim([-1 5]); 
        else; set(gca,'yTick', -1:1:3,'FontSize',12); ylim([-1 3]); end
        set(gca,'box','off')
        
        
    end
    
    PAPER = get(fig,'Position');
    set(fig,'PaperSize',[PAPER(3), PAPER(4)]);
    savefig(fullfile(opts.expPath,'plots',['SCR_GLM_mean_' sesname '.fig']));
    saveas(fig,fullfile(opts.expPath,'plots',['SCR_GLM_mean_' sesname]),'png');
    saveas(fig,fullfile(opts.expPath,'plots',['SCR_GLM_mean_' sesname]),'svg');
    
end

end