function plot_GLM_trial_SEBR(opts)

sesname = 'retention';

if opts.sebr.plot_allorders
    sList = opts.sebr.sList.orig;
    %sList = opts.sebr.sList.experim; % only experimental subjects, with all orders
    groupname = 'experimental';
%     sList = opts.sebr.sList.control; % only control subjects, with all orders
%     groupname = 'control';
    allordname = 'all';
else
    sList = opts.sebr.sList.control_sameorder; % only control subjects now, with the same order
    allordname = 'same';
end

%% Get GLM data
data = get_GLM_data(opts,sList,'get');

%% Separate into simple and complex stimuli and CS+ and CS-

amplitude_simple_CSp = data.StartleAmplitude_CSps;
amplitude_simple_CSm = data.StartleAmplitude_CSms;
amplitude_complex_CSp = data.StartleAmplitude_CSpc;
amplitude_complex_CSm = data.StartleAmplitude_CSmc;

if opts.sebr.plot_allconds % simple and complex stimuli separately
    condnames = {'Simple CS+' 'Simple CS-' 'Complex CS+' 'Complex CS-'};
    meanresp = [amplitude_simple_CSp; amplitude_simple_CSm; amplitude_complex_CSp; amplitude_complex_CSm]';
else % simple and complex stimuli together  
    condnames = {'CS+' 'CS-'};
    meanresp = [mean([amplitude_simple_CSp amplitude_complex_CSp],2)...
        mean([amplitude_simple_CSm amplitude_complex_CSm],2)];
end

%% Plot

%clr = [59 100 173; 143 162 212; 59 100 173; 143 162 212]./255; % CS+ and CS- colors (darker and lighter blue)

idx{1} = find(ismember(sList,opts.sebr.sList.control));
idx{2} = find(ismember(sList,opts.sebr.sList.experim));
groupname = {'Control' 'Experimental'};
condnames = {'CS+' 'CS-' 'CS+' 'CS-'};

markersize = 14;
%barcolor = [169 169 169]; grey
barcolor = [103,169,207; 189,201,225; 103,169,207; 189,201,225]./255; % greenish blues
%scattercolor = [105 105 105]./255;
scattercolor = [192 192 192]./255;
linecolor = [105 105 105]./255;%[169 169 169]; %[211 211 211]; % light grey
    
fig = figure('DefaultAxesFontSize', 12, 'DefaultAxesFontName', 'Arial', 'DefaultTextFontName', 'Arial');

figwidth = 17.6; % 1/2 pdf column = 6, 1 pdf column = 8.5 cm, 1.5 pdf column = 11.6, 2 pdf columns = 17.6
figheight = 6;

set(fig, 'Units', 'Centimeters',...
    'PaperUnits', 'Centimeters', ...
    'PaperPositionMode','auto')

set(fig,'Position', [0, 0, figwidth, figheight])

set(gca,'LineWidth',1) % axes line width
    

for group = 1:2
    
    sebr_group = meanresp(idx{group},:);
        
    %Calculate within-subject error bars
    subavg = nanmean(sebr_group,2); % mean over conditions for each sub
    grandavg = nanmean(subavg); % mean over subjects and conditions
    
    newvalues = nan(size(sebr_group));
    
    % normalization of subject values
    for cond = 1:size(sebr_group,2)
        meanremoved = sebr_group(:,cond)-subavg; % remove mean of conditions from each condition value for each sub
        newvalues(:,cond) = meanremoved+repmat(grandavg,[length(idx{group}) 1 1]); % add grand average over subjects to the values where individual sub average was removed
        plotdata(:,cond) = nanmean(newvalues(:,cond));
    end
    
    %cond = cond-1; % remove the filler NaN condition
    
    newvar = (cond/(cond-1))*nanvar(newvalues);
    tvalue = tinv(1-0.05, length(idx{group})-1);
    errorbars = squeeze(tvalue*(sqrt(newvar)./sqrt(length(idx{group})))); % calculate error bars according to Cousineau (2005) with Morey (2005) fix

    subplot(1,2,group)
    for b = 1:length(plotdata)
        h = bar(b,plotdata(b),'LineWidth',1.5);
        hold on
        h.FaceColor = barcolor(b,:);
    end
    
    % Individual values
    subjects = size(sebr_group,1);
    xdata = repmat(1:b,[subjects 1]); 
    jitter_amount = 0.2;
    jittered_xdata = xdata + (rand(size(xdata))-0.5)*(2*jitter_amount);
    %plot(jittered_xdata',sebr_group','-','Color',linecolor);
    hold on
    for c = 1:length(plotdata)
        scatter(jittered_xdata(:,c),sebr_group(:,c), markersize, 'filled', 'MarkerFaceColor', scattercolor, 'MarkerEdgeColor', 'k');
        hold on
    end
        %     scatter(xdata(:),sebr_group(:), 'filled', 'MarkerFaceColor', [102 178 255]./255, 'MarkerEdgeColor', 'k', 'jitter', 'on', 'jitterAmount', 0.2);
    
    % Error bars
    errorbar(plotdata,errorbars,'k','LineStyle','none','LineWidth',2.5,'CapSize',0)
    title(groupname{group})
    set(gca,'xTick', 1:length(condnames),'FontSize',12)
    set(gca,'xTickLabel', condnames,'FontSize',12)
    if group == 1
        ylabel('Standardized amplitude (a.u.)','FontSize',12)
        xlabel('Simple')
    else
        xlabel('Complex')
    end
    %ylim([0 1.5e-4])
    ylim([0 3])
    set(gca,'box','off')

% colors = [76 0 153; 0 0 153; 0 76 153; 0 153 153]/255;
% % Box plot with individual values
% figure('Position',[300,300,800,600]);
% boxplot(newvalues)
% h = findobj(gca,'Tag','Box');
% for j=1:length(h)
%     patch(get(h(j),'XData'),get(h(j),'YData'),colors(j,:),'FaceAlpha',.6);
% end
% hold on
% plot(1:length(h),newvalues,'o','MarkerFaceColor',[153 0 76]/255,'MarkerEdgeColor',[153 0 76]/255,'MarkerSize',7)
% title(['SEBR ' groupname ' group ' allordname ' orders, N = ' num2str(length(sList))])
% set(gca,'xTick', 1:length(condnames))
% set(gca,'xTickLabel', condnames)
% ylabel('Amplitude (a.u.)')

end

PAPER = get(fig,'Position');
set(fig,'PaperSize',[PAPER(3), PAPER(4)]);
savefig(fullfile(opts.expPath,'plots',['SEBR_mean_' sesname '.fig']));
saveas(fig,fullfile(opts.expPath,'plots',['SEBR_mean_' sesname]),'png');
saveas(fig,fullfile(opts.expPath,'plots',['SEBR_mean_' sesname]),'svg');


%% Linked observations plot by group

scattercolor_control_line = [140, 158, 189]./255;
scattercolor_control_dot = [33, 86, 173]./255*0.8;
scattercolor_exp_line = [186, 155, 167]./255; 
scattercolor_exp_dot = [143, 59, 92]./255*0.8; 
    
fig = figure('DefaultAxesFontSize', 12, 'DefaultAxesFontName', 'Arial', 'DefaultTextFontName', 'Arial');

figwidth = 17.6; % 1/2 pdf column = 6, 1 pdf column = 8.5 cm, 1.5 pdf column = 11.6, 2 pdf columns = 17.6
figheight = 6;

set(fig, 'Units', 'Centimeters',...
    'PaperUnits', 'Centimeters', ...
    'PaperPositionMode','auto')

set(fig,'Position', [0, 0, figwidth, figheight])

set(gca,'LineWidth',1) % axes line width

% Plot data
plotdata_control_simple = meanresp(idx{1},1:2);
plotdata_control_complex = meanresp(idx{1},3:4);
plotdata_exp_simple = meanresp(idx{2},1:2);
plotdata_exp_complex = meanresp(idx{2},3:4);

% Control
subplot(1,2,1)

% Individual values
% simple
subjects = size(plotdata_control_simple,1);
xdata = repmat(1:2,[subjects 1]);
%jitter_amount = 0.2;
%jittered_xdata = xdata + (rand(size(xdata))-0.5)*(2*jitter_amount);
hold on
plot(xdata',plotdata_control_simple','-','Color',scattercolor_control_line,'LineWidth',1);
scatter(xdata(:),plotdata_control_simple(:), markersize, 'filled', 'MarkerFaceColor', scattercolor_control_line, 'MarkerEdgeColor', 'k');

% complex
subjects = size(plotdata_control_complex,1);
xdata = repmat(1:2,[subjects 1]);
%jitter_amount = 0.2;
%jittered_xdata = xdata + (rand(size(xdata))-0.5)*(2*jitter_amount);
hold on
plot(xdata',plotdata_control_complex','-','Color',scattercolor_exp_line,'LineWidth',1); hold on
scatter(xdata(:),plotdata_control_complex(:), markersize, 'filled', 'MarkerFaceColor', scattercolor_exp_line, 'MarkerEdgeColor', 'k');

% Mean lines
plot(1:2,mean(plotdata_control_simple),'-','Color',scattercolor_control_dot,'LineWidth',4);
plot(1:2,mean(plotdata_control_complex),'-','Color',scattercolor_exp_dot,'LineWidth',4);

title('Control')
set(gca,'xTick', 1:2,'FontSize',12)
set(gca,'xTickLabel',{'CS+' 'CS-'},'FontSize',12)
ylabel('Standardized amplitude (a.u.)','FontSize',12)
%ylim([0 1.5e-4])
ylim([0 3])
xlim([0.5 2.5])
set(gca,'box','off')
    
% Experimental
subplot(1,2,2)

% Individual values
% simple
subjects = size(plotdata_exp_simple,1);
xdata = repmat(1:2,[subjects 1]);
%jitter_amount = 0.2;
%jittered_xdata = xdata + (rand(size(xdata))-0.5)*(2*jitter_amount);
hold on
plot(xdata',plotdata_exp_simple','-','Color',scattercolor_control_line,'LineWidth',1);
scatter(xdata(:),plotdata_exp_simple(:), markersize, 'filled', 'MarkerFaceColor', scattercolor_control_line, 'MarkerEdgeColor', 'k');

% complex
subjects = size(plotdata_exp_complex,1);
xdata = repmat(1:2,[subjects 1]);
%jitter_amount = 0.2;
%jittered_xdata = xdata + (rand(size(xdata))-0.5)*(2*jitter_amount);
hold on
plot(xdata',plotdata_exp_complex','-','Color',scattercolor_exp_line,'LineWidth',1); hold on
scatter(xdata(:),plotdata_exp_complex(:), markersize, 'filled', 'MarkerFaceColor', scattercolor_exp_line, 'MarkerEdgeColor', 'k');

% Mean lines
meanline_control = plot(1:2,mean(plotdata_exp_simple),'-','Color',scattercolor_control_dot,'LineWidth',4);
meanline_exp = plot(1:2,mean(plotdata_exp_complex),'-','Color',scattercolor_exp_dot,'LineWidth',4);

title('Experimental')
set(gca,'xTick', 1:2,'FontSize',12)
set(gca,'xTickLabel',{'CS+' 'CS-'},'FontSize',12)
%ylim([0 1.5e-4])
ylim([0 3])
xlim([0.5 2.5])
legend([meanline_control, meanline_exp],{'Simple','Complex'},'FontSize',10)
legend('boxoff')
set(gca,'box','off')

PAPER = get(fig,'Position');
set(gcf,'PaperSize',[PAPER(3), PAPER(4)]);
savefig(gcf,fullfile(opts.expPath,'plots',['SEBR_mean_' sesname '_linkedobs.fig']));
saveas(gcf,fullfile(opts.expPath,'plots',['SEBR_mean_' sesname '_linkedobs']),'png');
saveas(gcf,fullfile(opts.expPath,'plots',['SEBR_mean_' sesname '_linkedobs']),'svg');

%% Linked observations plot by CS complexity

% scattercolor_control_line = [140, 158, 189]./255;
% scattercolor_control_dot = [33, 86, 173]./255*0.8;
% scattercolor_exp_line = [186, 155, 167]./255; 
% scattercolor_exp_dot = [143, 59, 92]./255*0.8; 
%     
% fig = figure('DefaultAxesFontSize', 12, 'DefaultAxesFontName', 'Arial', 'DefaultTextFontName', 'Arial');
% 
% figwidth = 17.6; % 1/2 pdf column = 6, 1 pdf column = 8.5 cm, 1.5 pdf column = 11.6, 2 pdf columns = 17.6
% figheight = 6;
% 
% set(fig, 'Units', 'Centimeters',...
%     'PaperUnits', 'Centimeters', ...
%     'PaperPositionMode','auto')
% 
% set(fig,'Position', [0, 0, figwidth, figheight])
% 
% set(gca,'LineWidth',1) % axes line width
% 
% % Plot data
% plotdata_control_simple = meanresp(idx{1},1:2);
% plotdata_control_complex = meanresp(idx{1},3:4);
% plotdata_exp_simple = meanresp(idx{2},1:2);
% plotdata_exp_complex = meanresp(idx{2},3:4);
% 
% % Simple CS
% subplot(1,2,1)
% 
% % Individual values
% % controls
% subjects = size(plotdata_control_simple,1);
% xdata = repmat(1:2,[subjects 1]);
% %jitter_amount = 0.2;
% %jittered_xdata = xdata + (rand(size(xdata))-0.5)*(2*jitter_amount);
% hold on
% plot(xdata',plotdata_control_simple','-','Color',scattercolor_control_line,'LineWidth',1);
% scatter(xdata(:),plotdata_control_simple(:), markersize, 'filled', 'MarkerFaceColor', scattercolor_control_line, 'MarkerEdgeColor', 'k');
% 
% % experimental
% subjects = size(plotdata_exp_simple,1);
% xdata = repmat(1:2,[subjects 1]);
% %jitter_amount = 0.2;
% %jittered_xdata = xdata + (rand(size(xdata))-0.5)*(2*jitter_amount);
% hold on
% plot(xdata',plotdata_exp_simple','-','Color',scattercolor_exp_line,'LineWidth',1); hold on
% scatter(xdata(:),plotdata_exp_simple(:), markersize, 'filled', 'MarkerFaceColor', scattercolor_exp_line, 'MarkerEdgeColor', 'k');
% 
% % Mean lines
% plot(1:2,mean(plotdata_control_simple),'-','Color',scattercolor_control_dot,'LineWidth',4);
% plot(1:2,mean(plotdata_exp_simple),'-','Color',scattercolor_exp_dot,'LineWidth',4);
% 
% title('Simple CS')
% set(gca,'xTick', 1:2,'FontSize',12)
% set(gca,'xTickLabel',{'CS+' 'CS-'},'FontSize',12)
% ylabel('Standardized amplitude (a.u.)','FontSize',12)
% %ylim([0 1.5e-4])
% ylim([0 3])
% xlim([0.5 2.5])
% set(gca,'box','off')
%     
% % Complex CS
% subplot(1,2,2)
% 
% % Individual values
% % controls
% subjects = size(plotdata_control_complex,1);
% xdata = repmat(1:2,[subjects 1]);
% %jitter_amount = 0.2;
% %jittered_xdata = xdata + (rand(size(xdata))-0.5)*(2*jitter_amount);
% hold on
% plot(xdata',plotdata_control_complex','-','Color',scattercolor_control_line,'LineWidth',1);
% scatter(xdata(:),plotdata_control_complex(:), markersize, 'filled', 'MarkerFaceColor', scattercolor_control_line, 'MarkerEdgeColor', 'k');
% 
% % experimental
% subjects = size(plotdata_exp_complex,1);
% xdata = repmat(1:2,[subjects 1]);
% %jitter_amount = 0.2;
% %jittered_xdata = xdata + (rand(size(xdata))-0.5)*(2*jitter_amount);
% hold on
% plot(xdata',plotdata_exp_complex','-','Color',scattercolor_exp_line,'LineWidth',1); hold on
% scatter(xdata(:),plotdata_exp_complex(:), markersize, 'filled', 'MarkerFaceColor', scattercolor_exp_line, 'MarkerEdgeColor', 'k');
% 
% % Mean lines
% meanline_control = plot(1:2,mean(plotdata_control_complex),'-','Color',scattercolor_control_dot,'LineWidth',4);
% meanline_exp = plot(1:2,mean(plotdata_exp_complex),'-','Color',scattercolor_exp_dot,'LineWidth',4);
% 
% title('Complex CS')
% set(gca,'xTick', 1:2,'FontSize',12)
% set(gca,'xTickLabel',{'CS+' 'CS-'},'FontSize',12)
% %ylim([0 1.5e-4])
% ylim([0 3])
% xlim([0.5 2.5])
% legend([meanline_control, meanline_exp],{'Control','Experimental'},'FontSize',10)
% legend('boxoff')
% set(gca,'box','off')
% 
% PAPER = get(fig,'Position');
% set(gcf,'PaperSize',[PAPER(3), PAPER(4)]);
% savefig(gcf,fullfile(opts.expPath,'plots',['SEBR_mean_' sesname '_linkedobs.fig']));
% saveas(gcf,fullfile(opts.expPath,'plots',['SEBR_mean_' sesname '_linkedobs']),'png');
% saveas(gcf,fullfile(opts.expPath,'plots',['SEBR_mean_' sesname '_linkedobs']),'svg');

end