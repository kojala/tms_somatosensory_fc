function SCR_DCM_plot(opts,sessions,groups)

for ses = sessions
    
    for group = groups
    if group == 1
        sList = opts.scr.sList.DCM_control{ses};
        groupname = 'control';
    else
        sList = opts.scr.sList.DCM_experimental{ses};
        groupname = 'experimental';
    end
    condnames = opts.scr.condnames;
    sesname = opts.scr.sessionlist{ses};
    %stimname = {'Simple' 'Complex'};
    
    %% Retrieve GLM stats
    
    dcmpath = fullfile(opts.scr.path.dcm, sesname);
    dcmfile = ['DCM_cond_summary_mean_' groupname '_' sesname '.mat'];
    data = load(fullfile(dcmpath,dcmfile));
    dcm_data = data.dcm_data;
    
    %% Error bars
    clear meanresp_cs meanresp_us
    for cond = 1:size(dcm_data,2)
        meanresp_cs(:,cond) = dcm_data(cond).cs;
        meanresp_us(:,cond) = dcm_data(cond).us;
    end
    
    subavg_cs = nanmean(meanresp_cs,2); % mean over conditions for each sub
    grandavg_cs = nanmean(subavg_cs); % mean over subjects and conditions
    
    subavg_us = nanmean(meanresp_us,2); % mean over conditions for each sub
    grandavg_us = nanmean(subavg_us); % mean over subjects and conditions
    
    newvalues_cs = nan(size(meanresp_cs));
    newvalues_us = nan(size(meanresp_us));
    
    % normalization of subject values
    for cond = 1:size(meanresp_cs,2)
        meanremoved_cs = meanresp_cs(:,cond)-subavg_cs; % remove mean of conditions from each condition value for each sub
        newvalues_cs(:,cond) = meanremoved_cs+repmat(grandavg_cs,[length(sList) 1]); % add grand average over subjects to the values where individual sub average was removed
        plotdata_cs(cond) = nanmean(newvalues_cs(:,cond));
        
        meanremoved_us = meanresp_us(:,cond)-subavg_us; % remove mean of conditions from each condition value for each sub
        newvalues_us(:,cond) = meanremoved_us+repmat(grandavg_us,[length(sList) 1]); % add grand average over subjects to the values where individual sub average was removed
        plotdata_us(cond) = nanmean(newvalues_us(:,cond));
    end
    
    tvalue = tinv(1-0.05, length(sList)-1);
    
    newvar_cs = (cond/(cond-1))*nanvar(newvalues_cs);
    errorbars_cs = squeeze(tvalue*(sqrt(newvar_cs)./sqrt(length(sList)))); % calculate error bars according to Cousineau (2005) with Morey (2005) fix
    
    newvar_us = (cond/(cond-1))*nanvar(newvalues_us);
    errorbars_us = squeeze(tvalue*(sqrt(newvar_us)./sqrt(length(sList)))); % calculate error bars according to Cousineau (2005) with Morey (2005) fix
    
    %% Plot
    
    clr = [255 153 153; 59 100 173; 143 162 212; 255 153 153; 59 100 173; 143 162 212]./255; 
        
    % CR
    figure('Position',[300,300,800,400]);
    hold on
    % Bar graph of mean
    for b = 1:length(plotdata_cs)
        h = bar(b,plotdata_cs(:,b));
        h.FaceColor = clr(b,:);
    end
    % Individual values
    subjects = size(meanresp_cs,1);
    xdata = repmat(1:b,[subjects 1]);
    scatter(xdata(:),meanresp_cs(:), 'filled', 'MarkerFaceColor', [102 178 255]./255, 'MarkerEdgeColor', 'k', 'jitter', 'on', 'jitterAmount', 0.2);
    % Error bars
    errorbar(plotdata_cs,errorbars_cs,'k','LineStyle','none','LineWidth',2.5,'CapSize',0)
    title(['SCR mean CR, ' groupname ' group, N = ' num2str(length(sList))]);
    set(gca,'xTick', 1:length(condnames))
    set(gca,'xTickLabel', condnames)
    savefig(fullfile(dcmpath,['SCR_CR_mean_' sesname '_' groupname '_simple-complex.fig']))
    
    % UR
    figure('Position',[300,300,800,400]);
    hold on
    % Bar graph of mean
    for b = 1:length(plotdata_us)
        h = bar(b,plotdata_us(:,b));
        h.FaceColor = clr(b,:);
    end
    % Individual values
    subjects = size(meanresp_us,1);
    xdata = repmat(1:b,[subjects 1]);
    scatter(xdata(:),meanresp_us(:), 'filled', 'MarkerFaceColor', [102 178 255]./255, 'MarkerEdgeColor', 'k', 'jitter', 'on', 'jitterAmount', 0.2);
    % Error bars
    errorbar(plotdata_us,errorbars_us,'k','LineStyle','none','LineWidth',2.5,'CapSize',0)
    title(['SCR mean UR, ' groupname ' group, N = ' num2str(length(sList))]);
    ylim([0 9])
    set(gca,'xTick', 1:length(condnames))
    set(gca,'xTickLabel', condnames)
    savefig(fullfile(dcmpath,['SCR_UR_mean_' sesname '_' groupname '_simple-complex.fig']))
    
    %% Individual values plots
    %colors = [102 255 255; 153 51 255; 255 0 127;...
    %    0 255 128; 255 255 128; 204 204 0]/255;
%     colors = [255 153 153; 0 102 244; 51 153 255;...
%         255 51 51; 153 204 255; 204 0 0]/255;
%     
%     for stimtype = 1:2
%         
%         % CS
%         figure
%         hold on
%         for cond = 1:3
%             line([0 length(sList)+1],[nanmean(dcm_data(cind(stimtype,cond)).cs) nanmean(dcm_data(cind(stimtype,cond)).cs)],'Color',colors(cond,:),'LineWidth',2)
%             condplot(cond) = plot(dcm_data(cind(stimtype,cond)).cs,'o','MarkerFaceColor',colors(cond,:),'MarkerEdgeColor','k','MarkerSize',8);
%         end
%         xlim([0 length(sList)+1])
%         set(gca,'XTick',1:length(sList))
%         set(gca,'XTickLabel',1:length(sList))
%         xlabel('Subjects')
%         ylabel('Coefficient (a.u.)')
%         legend([condplot(1) condplot(2) condplot(3)],condnames(stimtype,:),'Location','best')
%         title([stimname{stimtype} ' stimuli, SCR individual CR estimates, ' groupname ', ' sesname]);
%         savefig(fullfile(dcmpath,['SCR_CR_individvalues_' sesname '_' groupname '_' stimname{stimtype} '.fig']));
%         
%         % US
%         figure
%         hold on
%         for cond = 1:3
%             line([0 length(sList)+1],[nanmean(dcm_data(cind(stimtype,cond)).us) nanmean(dcm_data(cind(stimtype,cond)).us)],'Color',colors(cond,:),'LineWidth',2)
%             condplot(cond) = plot(dcm_data(cind(stimtype,cond)).us,'o','MarkerFaceColor',colors(cond,:),'MarkerEdgeColor','k','MarkerSize',8);
%         end
%         xlim([0 length(sList)+1])
%         set(gca,'XTick',1:length(sList))
%         set(gca,'XTickLabel',1:length(sList))
%         xlabel('Subjects')
%         ylabel('Coefficient (a.u.)')
%         legend([condplot(1) condplot(2) condplot(3)],condnames(stimtype,:),'Location','best')
%         title([' stimuli, SCR individual UR estimates, ' groupname ', ' sesname]);
%         savefig(fullfile(dcmpath,['SCR_UR_individvalues_' sesname '_' groupname '_' stimname{stimtype} '.fig']));
%         
%     end
    
    end

end

end