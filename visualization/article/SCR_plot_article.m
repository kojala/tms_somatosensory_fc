function SCR_plot_article(opts,sessions)

indir = opts.scr.path.main; % input directory
conditions = 4;
sr = opts.scr.plot.sr; % sampling rate of the data
trials2take = opts.scr.trials2take; % the trials taken into account

clr = [153, 0, 0; 237, 152, 143; 75, 138, 202; ... % CS+US+ simple, CS+US- simple, CS- simple,
    153, 0, 0; 176, 143, 139; 79, 108, 138]./255; % CS+US+ complex, CS+US- complex, CS- complex

%clr = [44, 63, 109; 61, 145, 194; 150, 168, 207; ... % CS+US+ simple, CS+US- simple, CS- simple,
%    76, 47, 65; 143, 59, 92; 186, 155, 167]./255; % CS+US+ complex, CS+US- complex, CS- complex
if conditions == 4; clr = clr([2 3 5 6],:); end

for ses = sessions
    
    sesname = opts.sessionlist{ses};
    
    % Draw figure
    fig = figure('DefaultAxesFontSize', 12, 'DefaultAxesFontName', 'Arial', 'DefaultTextFontName', 'Arial');
    figwidth = 8.8; % 1/2 pdf column = 6, 1 pdf column = 8.5 cm, 1.5 pdf column = 11.6, 2 pdf columns = 17.6
    figheight = 11;
    set(fig, 'Units', 'Centimeters',...
        'PaperUnits', 'Centimeters', ...
        'PaperPositionMode','auto')
    set(fig,'Position', [0, 0, figwidth, figheight])
    set(gca,'LineWidth',1) % axes line width
    
    for group = 1:2
        
        clear scrdata plotdata errordata newvalues
        groupname = opts.groupnames{group};
        
        if length(trials2take) ~= opts.scr.trials
            filename = fullfile(indir,['SCR_preproc_' sesname '_' groupname '_' num2str(length(trials2take)) 'trials.mat']);
        else
            filename = fullfile(indir,['SCR_preproc_' sesname '_' groupname '.mat']);
        end
        data = load(filename);
        
        scrdata_USp = data.scr_alldata_USp;
        scrdata_USm = data.scr_alldata_USm;
        
        % Combine data
        if conditions == 6
            scrdata{1} = squeeze(scrdata_USp(:,:,1,:));
            scrdata{2} = squeeze(scrdata_USm(:,:,1,:));
            scrdata{3} = squeeze(scrdata_USm(:,:,2,:));
            scrdata{4} = squeeze(scrdata_USp(:,:,2,:));
            scrdata{5} = squeeze(scrdata_USm(:,:,3,:));
            scrdata{6} = squeeze(scrdata_USm(:,:,4,:));
        else
%             CSm = cat(1,squeeze(scrdata_USm(:,:,2,:)),squeeze(scrdata_USm(:,:,4,:)));
%             CSm_avg = nanmean(CSm);
%             
%             scrdata{1} = squeeze(scrdata_USm(:,:,1,:))./CSm_avg;
%             scrdata{2} = squeeze(scrdata_USm(:,:,2,:))./CSm_avg;
%             scrdata{3} = squeeze(scrdata_USm(:,:,3,:))./CSm_avg;
%             scrdata{4} = squeeze(scrdata_USm(:,:,4,:))./CSm_avg;

            scrdata{1} = squeeze(scrdata_USm(:,:,1,:));
            scrdata{2} = squeeze(scrdata_USm(:,:,2,:));
            scrdata{3} = squeeze(scrdata_USm(:,:,3,:));
            scrdata{4} = squeeze(scrdata_USm(:,:,4,:));
        end
        
        % Mean data per condition
        for cond = 1:conditions
            plotdata(:,cond) = squeeze(nanmean(nanmean(scrdata{cond},3)));
            errordata(:,cond,:) = squeeze(nanmean(scrdata{cond}));
        end
        
        subjects = size(errordata,3);
    
        %% Plot average timecourses for each condition
        
        % Within-subject error bars for all conditions and time points
        subavg = squeeze(nanmean(errordata,2)); % mean over conditions for each sub for each timepoint
        grandavg = mean(subavg,2); % mean over subjects and conditions for each timepoint
        newvalues = nan(size(errordata));
        
        % normalization of subject values
        for cond = 1:conditions
            meanremoved = squeeze(errordata(:,cond,:))-subavg; % remove mean of conditions from each condition value for each sub for each timepoint
            newvalues(:,cond,:) = meanremoved+repmat(grandavg,[1 subjects 1]); % add grand average over subjects to the values where individual sub average was removed
        end
        
        newvar = (cond/(cond-1))*nanvar(newvalues,[],3);
        tvalue = tinv(1-0.05, subjects-1);
        errorsem = squeeze(tvalue*(sqrt(newvar)./sqrt(subjects))); % calculate error bars
        
        %     ymin = min(min(plotdata))-0.1;
        %     ymax = max(max(plotdata))+0.1;
        if conditions == 6
            ymin = -1;
            ymax = 3;
        else
            if sessions == 1
                ymin = -0.4;
                ymax = 0.4;
            else
                ymin = -0.3;
                ymax = 0.3;
            end
        end
        
        subplot(2,2,group)
%         axis tight
        line([opts.soa opts.soa],[ymin ymax],'LineStyle','--','LineWidth',1.5,'Color',[192 192 192]./255,'HandleVisibility','off')
        
        hold on
        
        for cond = 1:conditions
            x = (1:size(plotdata,1))/sr;
            y = plotdata(:,cond);
            hl(cond) = boundedline(x, y, errorsem(:,cond), 'linewidth', 2, 'cmap', clr(cond,:),'alpha');
        end
        
        hold on
        
        xlabel('Trial time (s)','FontSize',10,'FontWeight','normal')
        if group == 1
            ylabel({'\Delta SCR from baseline (\muS)'},'FontSize',10,'FontWeight','normal')
        end
        groupname = regexprep(lower(groupname),'(\<[a-z])','${upper($1)}');
        title(groupname)
%         if group == 2 % plot legend only on 2nd figure
%             if conditions == 6
%                 leg = legend([hl(1) hl(2) hl(3) hl(4) h1(h5) h(6)],'String',{'CS+US+ simple', 'CS+US- simple', 'CS-US- simple', 'CS+US+ complex', 'CS+US- complex', 'CS-US- complex'},'Orientation','horizontal','NumColumns',2,'Location','south','FontSize',10);
%             else
%                 leg = legend([hl(1) hl(2) hl(3) hl(4)],'String',{'CS+US- simple', 'CS-US- simple', 'CS+US- complex', 'CS-US- complex'},'Orientation','horizontal','NumColumns',1,'Location','best','FontSize',10);
%             end
%             leg.ItemTokenSize = [15,15];
%             legend boxoff
%         end
        
        ylim([ymin ymax])
        xmax = opts.scr.plot.trialtime;
        xlim([0 xmax])
        set(gca,'XTick',0:1:xmax,'FontSize',10)
        set(gca,'YTick',ymin:0.1:ymax,'FontSize',10)
        
    end
    
    %% Plot DCM estimates
    
    condnames = {'CS+CS-' 'CS+CS-'};
    markersize = 14;
    barcolor = [237, 152, 143; 127, 172, 218; 237, 152, 143; 127, 172, 218]./255;
%     barcolor = [103,169,207; 189,201,225; 103,169,207; 189,201,225]./255;
    scattercolor = [192 192 192]./255;
    
    for group = 1:2
        
        if group == 1
            sList = opts.scr.sList.DCM_control{ses};
        else
            sList = opts.scr.sList.DCM_experimental{ses};
        end
        
        subjects = length(sList);
        groupname = opts.groupnames{group};
        
        %% Retrieve stats
        
        dcmpath = fullfile(opts.scr.path.dcm, sesname);
        if length(trials2take) ~= opts.scr.trials
            dcmfile = ['DCM_cond_summary_mean_' groupname '_' sesname '_' num2str(length(trials2take)) 'trials.mat'];
            dcmfile_trial = ['DCM_trial_summary_' groupname '_' sesname '_' num2str(length(trials2take)) 'trials.mat'];
        else
            dcmfile = ['DCM_cond_summary_mean_' groupname '_' sesname '.mat'];
            dcmfile_trial = ['DCM_trial_summary_' groupname '_' sesname '.mat'];
        end
        data = load(fullfile(dcmpath,dcmfile));
        dcm_data = data.dcm_data;
        
        data_trial = load(fullfile(dcmpath,dcmfile_trial));
        dcm_data_trial = data_trial.trial_data_out.cs;
        dcm_data_cond = data_trial.trial_data_out.cond;
        
        clear meanresp subavg grandavg newvalues meanremoved plotdata newvar errorbars
        for cond = 1:size(dcm_data,2)
            meanresp(:,cond) = dcm_data(cond).cs;
            %dcm_data_trial = dcm_data_trial(:,dcm_data_cond==cond);
        end

        % If 4 instead of 6 conditions included (US+ excluded)
        if conditions == 4
            meanresp = meanresp(:,[2 3 5 6]);
        end
        
        %% Experiment average over all trials (trial-wise interpolation)
%         
%         % Interpolate data for each condition within each subject
%         stim_type = [1:3; 1:3; 1:3; 4:6; 4:6; 4:6];
%         
%         for sIDX = 1:subjects
%             for cond = 1:6
%                 
%                 stim_indx = dcm_data_cond(sIDX,:);
%                 stim_indx = find(ismember(stim_indx,stim_type(cond,:)));
%                 
%                 cond_id = dcm_data_cond(sIDX,stim_indx);
%                 cond_indx = find(cond_id==cond);
%                 %nans = sum(isnan(dcm_data_trial(sIDX,stim_indx(cond_indx))))/length(isnan(dcm_data_trial(sIDX,stim_indx(cond_indx))));
%                 %fprintf([num2str(nans) '\n'])
%                 contdata(sIDX,:,cond) = interp1(cond_indx, dcm_data_trial(sIDX,stim_indx(cond_indx)), 1:numel(stim_indx), 'previous', 'extrap');
% 
%             end
%         end
%         
%         % Make sure trials with too few data points are excluded from figures
%         % NOT FUNCTIONAL (not correct result)
%         for cond = 1:6
%             indx = find(squeeze(sum(~isnan(contdata(:,:,cond)))<=10));
%             if numel(indx) > 0
%                 contdata(:,indx,cond) = NaN;
%             end
%         end
    
        %% Error bars
                
        % Calculate within-subject error bars
        subavg = nanmean(meanresp,2); % mean over conditions for each sub
        grandavg = nanmean(subavg); % mean over subjects and conditions
        
        newvalues = nan(size(meanresp));
        
        % normalization of subject values
        clear plotdata
        for cond = 1:conditions
            meanremoved = meanresp(:,cond)-subavg; % remove mean of conditions from each condition value for each sub
            newvalues(:,cond) = meanremoved+repmat(grandavg,[length(sList) 1 1]); % add grand average over subjects to the values where individual sub average was removed
            plotdata(:,cond) = nanmean(newvalues(:,cond));
        end
        
        tvalue = tinv(1-0.025, length(sList)-1);
        newvar = (cond/(cond-1))*nanvar(newvalues);
        errorbars = squeeze(tvalue*(sqrt(newvar)./sqrt(length(sList)))); % calculate error bars according to Cousineau (2005) with Morey (2005) fix
        
        %% Plot
        
%         subplot(2,2,group)
%         
%         ymin = 0;
%         ymax = 2;
%         line([12 12],[ymin ymax],'LineStyle','-','LineWidth',1,'Color',[192 192 192]./255,'HandleVisibility','off')
%         hold on
%         line([24 24],[ymin ymax],'LineStyle','-','LineWidth',1,'Color',[192 192 192]./255,'HandleVisibility','off')
%         line([36 36],[ymin ymax],'LineStyle','-','LineWidth',1,'Color',[192 192 192]./255,'HandleVisibility','off')
%         
%         p_indx = 1;
%         for cond = 1:6
%             if ismember(cond,[2 3 5 6]) % no US+ trials
%                h1(p_indx) = plot(nanmean(contdata(:,:,cond)),'Color',clr(cond,:),'LineWidth',1.5);
%                hold on
%                p_indx = p_indx + 1;
%             end
%         end
%         
%         xlabel('Trial number','FontSize',12,'FontWeight','normal')
%         if group == 1
%             ylabel('SCR amplitude estimate (a.u.)','FontSize',12,'FontWeight','normal')
%         end
%         groupname = regexprep(lower(groupname),'(\<[a-z])','${upper($1)}');
%         title(groupname)
%         if group == 2 % plot legend only on 2nd figure
%             if conditions == 6
%                 leg = legend([h1(1) h1(2) h1(3) h1(4) h1(h5) h1(6)],'String',{'CS+US+ simple', 'CS+US- simple', 'CS-US- simple', 'CS+US+ complex', 'CS+US- complex', 'CS-US- complex'},'Orientation','horizontal','NumColumns',2,'Location','south','FontSize',10);
%             else
%                 leg = legend([h1(1) h1(2) h1(3) h1(4)],'String',{'CS+US- simple', 'CS-US- simple', 'CS+US- complex', 'CS-US- complex'},'Orientation','horizontal','NumColumns',2,'Location','south','FontSize',10);
%             end
%             leg.ItemTokenSize = [15,15];
%             legend boxoff
%         end
%         
%         set(gca,'yTick', 0:0.5:3,'FontSize',12)
%         ylim([ymin ymax])
%         xmax = opts.scr.trials/2;
%         xlim([1 xmax])
%         set(gca,'XTick',0:12:xmax,'FontSize',12)
%         set(gca,'YTick',ymin:0.5:ymax,'FontSize',12)
        
        x = categorical([1 1; 2 2],[1 2],{'Simple', 'Complex'});
        y = [plotdata(1:2); plotdata(3:4)];
        subplot(2,2,group+2)
        bar(x,y);
        h = bar(y,0.8,'LineWidth',1.5,'FaceColor','flat');
        for b = 1:size(y,2)
            ctr(b,:) = bsxfun(@plus, h(b).XData, h(b).XOffset');
            ydt(b,:) = h(b).YData;
            h(1).CData = barcolor(1,:);
            h(2).CData = barcolor(2,:);
        end

%         hold on
%         errorbar(ctr,ydt,errorbars);
%         
%         subplot(2,2,group+2)
%         for b = 1:conditions
%             h = bar(b,plotdata(b),'LineWidth',1.5);
%             hold on
%             h.FaceColor = barcolor(b,:);
%         end
        
        % Individual values
        xdata = repmat([ctr(1,:) ctr(2,:)],[subjects 1]);
        jitter_amount = 0.07;
        jittered_xdata = xdata + (rand(size(xdata))-0.5)*(2*jitter_amount);
        scattercolor = repmat(scattercolor,[size(xdata,1) 1]);
%         columns = 1:size(xdata,1);
        for c = 1:length(plotdata)
            hold on
            beeswarm(jittered_xdata(:,c),meanresp(:,c),'colormap',scattercolor,...
                'MarkerFaceAlpha',1,'MarkerEdgeColor','k','dot_size',.3,...
                'sort_style','rand','use_current_axes',true,'corral_style','rand');
%             axis tight
        end
        
%         jitter_amount = 0.05;
%         jittered_xdata = xdata + (rand(size(xdata))-0.5)*(2*jitter_amount);
%         %plot(jittered_xdata',sebr_group','-','Color',linecolor);
%         hold on
%         for c = 1:length(plotdata)
%             h2(c) = scatter(jittered_xdata(:,c), meanresp(:,c), 'MarkerFaceColor', scattercolor, 'MarkerEdgeColor', 'k');
%             h2(c).SizeData = markersize;
%             hold on
%         end
        
        % Error bars
        errorbar(ctr,ydt,[errorbars(1:2);errorbars(3:4)],'k','LineStyle','none','LineWidth',2.5,'CapSize',0)
%         errorbar([ctr(1,:) ctr(2,:)],[ydt(1,:) ydt(2,:)],errorbars,'k','LineStyle','none','LineWidth',2.5,'CapSize',0)
%         errorbar(plotdata,errorbars,'k','LineStyle','none','LineWidth',2.5,'CapSize',0)
        groupname = regexprep(lower(groupname),'(\<[a-z])','${upper($1)}');
        title(groupname)
        set(gca,'xTick', 1:length(condnames),'FontSize',7)
        set(gca,'xTickLabel', condnames,'FontSize',7)
        if group == 1
            ylabel({'SCR amplitude estimate (a.u.)'},'FontSize',10,'FontWeight','normal')
            xlabel(' Simple  Complex')
        else
            xlabel(' Simple  Complex')
        end
        
        if sessions == 1
            set(gca,'yTick', 0:0.5:3.5,'FontSize',10)
            ylim([0 3.5])
        else
            set(gca,'yTick', 0:0.5:3.0,'FontSize',10)
            ylim([0 3.0])
        end
        set(gca,'box','off')
        
    end
    
    PAPER = get(fig,'Position');
    set(fig,'PaperSize',[PAPER(3), PAPER(4)]);
    if length(trials2take) ~= opts.scr.trials
        plotname = ['SCR_timecourse_dcm_' opts.sessionlist{ses} '_' num2str(length(trials2take)) 'trials'];
    else
        plotname = ['SCR_timecourse_dcm_' opts.sessionlist{ses}];
    end
    savefig(fig,fullfile(opts.expPath,'Plots',plotname));
    saveas(gcf,fullfile(opts.expPath,'Plots',plotname),'png')
    saveas(gcf,fullfile(opts.expPath,'Plots',plotname),'svg')
    
end

end