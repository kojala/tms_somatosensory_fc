function pupil_plot_article(opts,sessions)

indir = opts.psr.path.main; % input directory
conditions = 4;
sr = opts.psr.plot.sr; % sampling rate of the data

for ses = sessions
    
    trials2take = 1:opts.psr.trials2take(ses); % the trials taken into account
    
    sesname = opts.sessionlist{ses};
    
    % Draw figure
    fig = figure('DefaultAxesFontSize', 12, 'DefaultAxesFontName', 'Arial', 'DefaultTextFontName', 'Arial');
    figwidth = 8.8; % 1/2 pdf column = 6, 1 pdf column = 8.5 cm, 1.5 pdf column = 11.6, 2 pdf columns = 17.6
    figheight = 11;
    set(fig, 'Units', 'Centimeters',...
        'PaperUnits', 'Centimeters', ...
        'PaperPositionMode','auto')
    set(fig,'Position', [0, 0, figwidth, figheight])
    set(gca,'LineWidth',1) % axes line width
    
    for group = 1:2
        
        clear scrdata plotdata errordata newvalues
        groupname = opts.groupnames{group};
        
        if length(trials2take) ~= opts.psr.trials(ses)
            filename = fullfile(indir,['pupil_preproc_' sesname '_'  groupname '_' num2str(length(trials2take)) 'trials.mat']);
        else
            filename = fullfile(indir,['pupil_preproc_' sesname '_'  groupname '.mat']);
        end
        data = load(filename);
        
        pupildata_USp = data.pupil_alldata_USp;
        pupildata_USm = data.pupil_alldata_USm;
        
        % Combine data
        if conditions == 6
            pupildata{1} = squeeze(pupildata_USp(:,:,1,:));
            pupildata{2} = squeeze(pupildata_USm(:,:,1,:));
            pupildata{3} = squeeze(pupildata_USm(:,:,2,:));
            pupildata{4} = squeeze(pupildata_USp(:,:,2,:));
            pupildata{5} = squeeze(pupildata_USm(:,:,3,:));
            pupildata{6} = squeeze(pupildata_USm(:,:,4,:));
        else
            pupildata{1} = squeeze(pupildata_USm(:,:,1,:));
            pupildata{2} = squeeze(pupildata_USm(:,:,2,:));
            pupildata{3} = squeeze(pupildata_USm(:,:,3,:));
            pupildata{4} = squeeze(pupildata_USm(:,:,4,:));
        end
        
        % Mean data per condition
        for cond = 1:conditions
            plotdata(:,cond) = squeeze(nanmean(nanmean(pupildata{cond},3)));
            errordata(:,cond,:) = squeeze(nanmean(pupildata{cond}));
        end
        
        %% Plot average timecourses for each condition
        
        % Within-subject error bars for all conditions and time points
        subavg = squeeze(nanmean(errordata,2)); % mean over conditions for each sub for each timepoint
        grandavg = mean(subavg,2); % mean over subjects and conditions for each timepoint
        newvalues = nan(size(errordata));
        subjects = size(errordata,3);
        
        % normalization of subject values
        for cond = 1:conditions
            meanremoved = squeeze(errordata(:,cond,:))-subavg; % remove mean of conditions from each condition value for each sub for each timepoint
            newvalues(:,cond,:) = meanremoved+repmat(grandavg,[1 subjects 1]); % add grand average over subjects to the values where individual sub average was removed
        end
        
        newvar = (cond/(cond-1))*nanvar(newvalues,[],3);
        tvalue = tinv(1-0.05, subjects-1);
        errorsem = squeeze(tvalue*(sqrt(newvar)./sqrt(subjects))); % calculate error bars
        
        %     ymin = min(min(plotdata))-0.1;
        %     ymax = max(max(plotdata))+0.1;
        if conditions == 6
            ymin = -1;
            ymax = 3;
        else
            if sessions == 1
                ymin = -0.1;
                ymax = 0.5;
            else
                ymin = -0.1;
                ymax = 0.4;
            end
        end
        
        subplot(2,2,group)
        line([3.5 3.5],[ymin ymax],'LineStyle','--','LineWidth',1.5,'Color',[192 192 192]./255,'HandleVisibility','off')
        
        hold on

        clr = [153, 0, 0; 237, 152, 143; 75, 138, 202; ... % CS+US+ simple, CS+US- simple, CS- simple,
            153, 0, 0; 176, 143, 139; 79, 108, 138]./255; % CS+US+ complex, CS+US- complex, CS- complex
        if conditions == 4; clr = clr([2 3 5 6],:); end
        
%         clr = [44, 63, 109; 61, 145, 194; 150, 168, 207; ... % CS+US+ simple, CS+US- simple, CS- simple,
%             76, 47, 65; 143, 59, 92; 186, 155, 167]./255; % CS+US+ complex, CS+US- complex, CS- complex
%         if conditions == 4; clr = clr([2 3 5 6],:); end
        
        for cond = 1:conditions
            x = (1:size(plotdata,1))/sr;
            y = plotdata(:,cond);
            hl(cond) = boundedline(x, y, errorsem(:,cond), 'linewidth', 2, 'cmap', clr(cond,:),'alpha');
        end
        
        hold on
        
        xlabel('Trial time (s)','FontSize',10,'FontWeight','normal')
        if group == 1
            ylabel({'\Delta PSR from baseline (mm)'},'FontSize',10,'FontWeight','normal')
        end
        groupname = regexprep(lower(groupname),'(\<[a-z])','${upper($1)}');
        title(groupname)
%         if group == 2 % plot legend only on 2nd figure
%             if conditions == 6
%                 leg = legend([hl(1) hl(2) hl(3) hl(4) h1(h5) h(6)],'String',{'CS+US+ simple', 'CS+US- simple', 'CS-US- simple', 'CS+US+ complex', 'CS+US- complex', 'CS-US- complex'},'Orientation','horizontal','NumColumns',2,'Location','south','FontSize',10);
%             else
%                 leg = legend([hl(1) hl(2) hl(3) hl(4)],'String',{'CS+US- simple', 'CS-US- simple', 'CS+US- complex', 'CS-US- complex'},'Orientation','horizontal','NumColumns',2,'Location','south','FontSize',10);
%             end
%             leg.ItemTokenSize = [15,15];
%             legend boxoff
%         end
        
        ylim([ymin ymax])
        xmax = 5;%opts.psr.plot.trialtime;
        xlim([0 xmax])
        set(gca,'XTick',0:1:xmax,'FontSize',10)
        set(gca,'YTick',ymin:0.1:ymax,'FontSize',10)
        
    end
    
    %% Bar graph plot of conditionwise averages (to accompany ANOVA results)
    
    condnames = {'CS+CS-' 'CS+CS-'};
    markersize = 14;
    barcolor = [237, 152, 143; 127, 172, 218; 237, 152, 143; 127, 172, 218]./255;
%     barcolor = [103,169,207; 189,201,225; 103,169,207; 189,201,225]./255;
    scattercolor = [192 192 192]./255;
    
    %% Retrieve GLM stats
    
    glmpath = fullfile(opts.psr.path.glm_cond, opts.psr.glm_bfname{1}, sesname);
    if length(trials2take) ~= opts.psr.trials(ses)
        glmfile = ['GLM_cond_summary_mean_' sesname '_' num2str(length(trials2take)) 'trials.mat'];
    else
        glmfile = ['GLM_cond_summary_mean_' sesname '.mat'];
    end
    data = load(fullfile(glmpath,glmfile));
    glm_data = data.glm_data;
    
    conditions = 4; % GLM conditions always 4 (otherwise have to change how GLM data is saved)
    sList_all = opts.psr.sList.GLM{ses};
    
    for group = 1:2
        
        if group == 1
            sList = opts.psr.sList.GLM_control{ses};
            sIDX = ismember(sList_all,sList);
            glm_data_group = glm_data.data(sIDX,:);
        else
            sList = opts.psr.sList.GLM_experimental{ses};
            sIDX = ismember(sList_all,sList);
            glm_data_group = glm_data.data(sIDX,:);
        end
        
        groupname = opts.groupnames{group};
        
        %% Error bars
        clear meanresp subavg grandavg newvalues meanremoved plotdata newvar errorbars
        meanresp = glm_data_group;

        % Calculate within-subject error bars
        subavg = nanmean(meanresp,2); % mean over conditions for each sub
        grandavg = nanmean(subavg); % mean over subjects and conditions
        
        newvalues = nan(size(meanresp));
        
        % normalization of subject values
        clear plotdata
        for cond = 1:conditions
            meanremoved = meanresp(:,cond)-subavg; % remove mean of conditions from each condition value for each sub
            newvalues(:,cond) = meanremoved+repmat(grandavg,[length(sList) 1 1]); % add grand average over subjects to the values where individual sub average was removed
            plotdata(:,cond) = nanmean(newvalues(:,cond));
        end
        
        tvalue = tinv(1-0.025, length(sList)-1);
        newvar = (cond/(cond-1))*nanvar(newvalues);
        errorbars = squeeze(tvalue*(sqrt(newvar)./sqrt(length(sList)))); % calculate error bars according to Cousineau (2005) with Morey (2005) fix
        
        subplot(2,2,group+2)
        x = categorical([1 1; 2 2],[1 2],{'Simple', 'Complex'});
        y = [plotdata(1:2); plotdata(3:4)];
        subplot(2,2,group+2)
        bar(x,y);
        h = bar(y,0.8,'LineWidth',1.5,'FaceColor','flat');
        for b = 1:size(y,2)
            ctr(b,:) = bsxfun(@plus, h(b).XData, h(b).XOffset');
            ydt(b,:) = h(b).YData;
            h(1).CData = barcolor(1,:);
            h(2).CData = barcolor(2,:);
        end
        
%         for b = 1:conditions
%             h = bar(b,plotdata(b),'LineWidth',1.5);
%             hold on
%             h.FaceColor = barcolor(b,:);
%         end
        
        % Individual values
        subjects = length(sList);
        xdata = repmat([ctr(1,:) ctr(2,:)],[subjects 1]);
%         xdata = repmat(1:b,[subjects 1]);
        jitter_amount = 0.05;
        jittered_xdata = xdata + (rand(size(xdata))-0.5)*(2*jitter_amount);
        %plot(jittered_xdata',sebr_group','-','Color',linecolor);
        scattercolor = repmat(scattercolor,[size(xdata,1) 1]);
        for c = 1:length(plotdata)
            hold on
            beeswarm(jittered_xdata(:,c),meanresp(:,c),'colormap',scattercolor,...
                'MarkerFaceAlpha',1,'MarkerEdgeColor','k','dot_size',.3,...
                'sort_style','rand','use_current_axes',true,'corral_style','rand');
        end
        
        % Error bars
        errorbar(ctr,ydt,[errorbars(1:2);errorbars(3:4)],'k','LineStyle','none','LineWidth',2.5,'CapSize',0)
%         errorbar(plotdata,errorbars,'k','LineStyle','none','LineWidth',2.5,'CapSize',0)
        groupname = regexprep(lower(groupname),'(\<[a-z])','${upper($1)}');
        title(groupname)
        set(gca,'xTick', 1:length(condnames),'FontSize',7)
        set(gca,'xTickLabel', condnames,'FontSize',7)
        if group == 1
            ylabel({'PSR estimate (a.u.)'},'FontSize',10,'FontWeight','normal')
            xlabel(' Simple Complex')
        else
            xlabel(' Simple Complex')
        end
        
        if sessions == 1
            set(gca,'yTick', -0.4:0.4:1.2,'FontSize',10)
            ylim([-0.4 1.2])
        else
            set(gca,'yTick', -0.4:0.2:1.0,'FontSize',10)
            ylim([-0.4 1.0])
        end
        
        set(gca,'box','off')
        
    end
    
    PAPER = get(fig,'Position');
    set(fig,'PaperSize',[PAPER(3), PAPER(4)]);
    if length(trials2take) ~= opts.psr.trials
        plotname = ['PSR_timecourse_anova_' opts.sessionlist{ses} '_' num2str(length(trials2take)) 'trials'];
    else
        plotname = ['PSR_timecourse_anova_' opts.sessionlist{ses}];
    end
    savefig(fig,fullfile(opts.expPath,'Plots',plotname));
    saveas(gcf,fullfile(opts.expPath,'Plots',plotname),'png')
    saveas(gcf,fullfile(opts.expPath,'Plots',plotname),'svg')
    
end

end