function SEBR_plot_article(opts)

conditions = 4;
trials2take = 24; % the trials taken into account

clr = [153, 0, 0; 237, 152, 143; 75, 138, 202; ... % CS+US+ simple, CS+US- simple, CS- simple,
    153, 0, 0; 176, 143, 139; 79, 108, 138]./255; % CS+US+ complex, CS+US- complex, CS- complex
if conditions == 4; clr = clr([2 3 5 6],:); end

% clr = [61, 145, 194; 150, 168, 207; ... % CS+US+ simple, CS+US- simple, CS- simple,
%        143, 59, 92; 186, 155, 167]./255; % CS+US+ complex, CS+US- complex, CS- complex
%if conditions == 4; clr = clr([2 3 5 6],:); end

% Draw figure
fig = figure('DefaultAxesFontSize', 12, 'DefaultAxesFontName', 'Arial', 'DefaultTextFontName', 'Arial');
figwidth = 11.6; % 1/2 pdf column = 6, 1 pdf column = 8.5 cm, 1.5 pdf column = 11.6, 2 pdf columns = 17.6
figheight = 12;
set(fig, 'Units', 'Centimeters',...
    'PaperUnits', 'Centimeters', ...
    'PaperPositionMode','auto')
set(fig,'Position', [0, 0, figwidth, figheight])
set(gca,'LineWidth',1) % axes line width

condnames = {'CS+CS-' 'CS+CS-'};
markersize = 14;
barcolor = [237, 152, 143; 127, 172, 218; 237, 152, 143; 127, 172, 218]./255;
% barcolor = [103,169,207; 189,201,225; 103,169,207; 189,201,225]./255;
scattercolor = [192 192 192]./255;

for group = 1:2
    
    if group == 1
        sList = opts.sebr.sList.control;
    else
        sList = opts.sebr.sList.experim;
    end
    
    subjects = length(sList);
    groupname = opts.groupnames{group};
    
    %% Retrieve stats
    clear meanresp subavg grandavg newvalues meanremoved plotdata newvar errorbars
    
    data = get_GLM_data(opts,sList,'get');
    
    % Separate into simple and complex stimuli and CS+ and CS-
    amplitude_simple_CSp = data.StartleAmplitude_CSps;
    amplitude_simple_CSm = data.StartleAmplitude_CSms;
    amplitude_complex_CSp = data.StartleAmplitude_CSpc;
    amplitude_complex_CSm = data.StartleAmplitude_CSmc;
    
    meanresp = [amplitude_simple_CSp; amplitude_simple_CSm; amplitude_complex_CSp; amplitude_complex_CSm]';

    %% Experiment average over all trials (trial-wise interpolation)
    
    % Interpolate data for each condition within each subject
    contdata = nan(subjects,trials2take/4,conditions);
    stim_type = [0 0 1 1];
    
    for sIDX = 1:subjects
        
        CStype = data.stimuli_CS(sIDX,:);
        CScomplexity = data.stimuli_complex(sIDX,:);
        CScondition(CStype == 1 & CScomplexity == 0) = 1;
        CScondition(CStype == 0 & CScomplexity == 0) = 2;
        CScondition(CStype == 1 & CScomplexity == 1) = 3;
        CScondition(CStype == 0 & CScomplexity == 1) = 4;
            
        for cond = 1:conditions

            stim_indx = data.stimuli_complex(sIDX,:);
            stim_indx = find(ismember(stim_indx,stim_type(cond)));
            
            cond_id = CScondition(stim_indx);
            cond_indx = cond_id==cond;
            %nans = sum(isnan(data.amplitude(sIDX,stim_indx(cond_indx))))/length(isnan(data.amplitude(sIDX,stim_indx(cond_indx))));
            %fprintf([num2str(sIDX) ' - ' num2str(cond) ' - ' num2str(nans) '\n'])
            %contdata(sIDX,stim_indx(cond_indx),cond) = interp1(cond_indx, data.amplitude(sIDX,stim_indx(cond_indx)), 1:numel(stim_indx), 'previous', 'extrap');
            contdata(sIDX,:,cond) = data.amplitude(sIDX,stim_indx(cond_indx));
            
        end
    end
    
    % Make sure trials with too few data points are excluded from figures
    % NOT FUNCTIONAL (not correct result)
%     for cond = 1:6
%         indx = find(squeeze(sum(~isnan(contdata(:,:,cond)))<=10));
%         if numel(indx) > 0
%             contdata(:,indx,cond) = NaN;
%         end
%     end
    
    %% Error bars
    
    % Calculate within-subject error bars
    subavg = nanmean(meanresp,2); % mean over conditions for each sub
    grandavg = nanmean(subavg); % mean over subjects and conditions
    
    newvalues = nan(size(meanresp));
    
    % normalization of subject values
    clear plotdata
    for cond = 1:conditions
        meanremoved = meanresp(:,cond)-subavg; % remove mean of conditions from each condition value for each sub
        newvalues(:,cond) = meanremoved+repmat(grandavg,[length(sList) 1 1]); % add grand average over subjects to the values where individual sub average was removed
        plotdata(:,cond) = nanmean(newvalues(:,cond));
    end
    
    tvalue = tinv(1-0.025, length(sList)-1);
    newvar = (cond/(cond-1))*nanvar(newvalues);
    errorbars = squeeze(tvalue*(sqrt(newvar)./sqrt(length(sList)))); % calculate error bars according to Cousineau (2005) with Morey (2008) fix
    
    %% Plot
    
    subplot(2,2,group)
    
    ymin = 0;
    ymax = 2.5;
    %line([6 6],[ymin ymax],'LineStyle','-','LineWidth',1,'Color',[192 192 192]./255,'HandleVisibility','off')
    hold on
    %line([12 12],[ymin ymax],'LineStyle','-','LineWidth',1,'Color',[192 192 192]./255,'HandleVisibility','off')
    %line([18 18],[ymin ymax],'LineStyle','-','LineWidth',1,'Color',[192 192 192]./255,'HandleVisibility','off')
    
    for cond = 1:conditions
        h1(cond) = plot(nanmean(contdata(:,:,cond)),'Color',clr(cond,:),'LineWidth',2);
        hold on
    end
    
    xlabel('Trial number','FontSize',12,'FontWeight','normal')
    if group == 1
        ylabel('SEBR amplitude estimate (a.u.)','FontSize',10,'FontWeight','normal')
    end
    groupname = regexprep(lower(groupname),'(\<[a-z])','${upper($1)}');
    title(groupname)
    if group == 2 % plot legend only on 2nd figure
        leg = legend([h1(1) h1(2) h1(3) h1(4)],'String',{'CS+US- simple', 'CS-US- simple', 'CS+US- complex', 'CS-US- complex'},'Orientation','horizontal','NumColumns',2,'Location','south','FontSize',10);
        leg.ItemTokenSize = [15,15];
        legend boxoff
    end
    
    set(gca,'yTick', ymin:0.5:ymax,'FontSize',10)
    ylim([ymin ymax])
    xmax = trials2take/4;
    xlim([1 xmax])
    set(gca,'XTick',0:1:xmax,'FontSize',10)
    set(gca,'YTick',ymin:0.5:ymax,'FontSize',10)
    
    subplot(2,2,group+2)
    x = categorical([1 1; 2 2],[1 2],{'Simple', 'Complex'});
    y = [plotdata(1:2); plotdata(3:4)];
    subplot(2,2,group+2)
    bar(x,y);
    h = bar(y,0.8,'LineWidth',1.5,'FaceColor','flat');
    for b = 1:size(y,2)
        ctr(b,:) = bsxfun(@plus, h(b).XData, h(b).XOffset');
        ydt(b,:) = h(b).YData;
        h(1).CData = barcolor(1,:);
        h(2).CData = barcolor(2,:);
    end
%     for b = 1:conditions
%         h = bar(b,plotdata(b),'LineWidth',1.5);
%         hold on
%         h.FaceColor = barcolor(b,:);
%     end
    
    % Individual values
%     xdata = repmat(1:b,[subjects 1]);
    xdata = repmat([ctr(1,:) ctr(2,:)],[subjects 1]);
    jitter_amount = 0.07;
    jittered_xdata = xdata + (rand(size(xdata))-0.5)*(2*jitter_amount);
    %plot(jittered_xdata',sebr_group','-','Color',linecolor);
%     hold on
    scattercolor = repmat(scattercolor,[size(xdata,1) 1]);
    for c = 1:length(plotdata)
        hold on
        beeswarm(jittered_xdata(:,c),meanresp(:,c),'colormap',scattercolor,...
            'MarkerFaceAlpha',1,'MarkerEdgeColor','k','dot_size',.3,...
            'sort_style','rand','use_current_axes',true,'corral_style','rand');
%         scatter(jittered_xdata(:,c),meanresp(:,c), markersize, 'filled', 'MarkerFaceColor', scattercolor, 'MarkerEdgeColor', 'k');
%         hold on
    end
    
    % Error bars
    errorbar(ctr,ydt,[errorbars(1:2);errorbars(3:4)],'k','LineStyle','none','LineWidth',2.5,'CapSize',0)
%     errorbar(plotdata,errorbars,'k','LineStyle','none','LineWidth',2.5,'CapSize',0)
    groupname = regexprep(lower(groupname),'(\<[a-z])','${upper($1)}');
    title(groupname)
    set(gca,'xTick', 1:length(condnames),'FontSize',7)
    set(gca,'xTickLabel', condnames,'FontSize',7)
    if group == 1
        ylabel({'SEBR amplitude estimate (a.u.)'},'FontSize',10,'FontWeight','normal')
        xlabel(' Simple Complex')
    else
        xlabel(' Simple Complex')
    end
    set(gca,'yTick', 0:0.5:3,'FontSize',10)
    ylim([0 3])
    set(gca,'box','off')
    
end

PAPER = get(fig,'Position');
set(fig,'PaperSize',[PAPER(3), PAPER(4)]);
if length(trials2take) ~= 24
    plotname = ['SEBR_timecourse_anova_' opts.sessionlist{2} '_' num2str(length(trials2take)) 'trials'];
else
    plotname = ['SEBR_timecourse_anova_' opts.sessionlist{2}];
end
savefig(fig,fullfile(opts.expPath,'Plots',plotname));
saveas(gcf,fullfile(opts.expPath,'Plots',plotname),'png')
saveas(gcf,fullfile(opts.expPath,'Plots',plotname),'svg')

end