function SCR_plot_trace(opts,sessions)

for ses = sessions
    
    for group = 1:2
        
        trials2take = opts.scr.trials2take; % the trials taken into account
        if length(trials2take) ~= opts.scr.trials
            filename = fullfile(opts.scr.path.main,['SCR_preproc_' opts.sessionlist{ses} '_' opts.groupnames{group} '_' num2str(length(trials2take)) 'trials.mat']);
        else
            filename = fullfile(opts.scr.path.main,['SCR_preproc_' opts.sessionlist{ses} '_' opts.groupnames{group} '.mat']);
        end
        data = load(filename);
        
        scrdata_USp = data.scr_alldata_USp;
        scrdata_USm = data.scr_alldata_USm;
        sr = data.sr;
        
        %% Plotting
        fig = figure('Position', [100, 100, 1400, 600]);
        %pos = [.03 .53 .40 .40; .5 .53 .40 .40; .03 .04 .40 .40]; % 3 panels
        % axes('Position',[lowleft_x lowleft_y width height]);
        fromdown = .1;
        fromleft = .05;
        height = 0.40;
        width = 0.40;
        pos = [fromleft fromdown+height+0.1 width height; fromleft+width+0.1 fromdown+height+0.1 width height; ...
            fromleft fromdown width height; fromleft+width+0.1 fromdown width height;];
        clr = [9 226 183; 147 0 226; 9 136 239; 226 9 78];
        clr = clr./255;
        
        titles = {'All conditions, simple stimuli'...
            'All conditions, complex stimuli', ...
            'CS+US-  -  CS-US-, simple stimuli',...
            'CS+US-  -  CS-US-, complex stimuli'};
        %pos = axpos(1, 3, .05, .05, .05, .1, .05);
        
        scrdata_CSpUSp_simple = squeeze(nanmean(scrdata_USp(:,:,1,:)));
        scrdata_CSpUSm_simple = squeeze(nanmean(scrdata_USm(:,:,1,:)));
        scrdata_CSmUSm_simple = squeeze(nanmean(scrdata_USm(:,:,2,:)));
        scrdata_CSpUSp_complex = squeeze(nanmean(scrdata_USp(:,:,2,:)));
        scrdata_CSpUSm_complex = squeeze(nanmean(scrdata_USm(:,:,3,:)));
        scrdata_CSmUSm_complex = squeeze(nanmean(scrdata_USm(:,:,4,:)));
        
        xmin = 0;
        xmax = opts.scr.plot.trialtime;
        ymin = -0.5e-6;
        if group == 1 && ses == 1; ymax = 2.5e-6; else; ymax = 2.5e-6; end
        US = 3.5;
        % All conditions, simple stimuli
        iPanel = 1;
        clear plotdata
        
        plotdata(:,1) = nanmean(scrdata_CSpUSp_simple,2);
        plotdata(:,2) = nanmean(scrdata_CSpUSm_simple,2);
        plotdata(:,3) = nanmean(scrdata_CSmUSm_simple,2);
        
        axes('Position', pos(iPanel,:)); hold on;
        %     line([xmin xmax],[0 0],'Color','k','LineWidth',1)
        line([US US],[ymin ymax],'Color','r','LineWidth',1.5)
        for CStype = 1:3
            plot((1:size(plotdata,1))/sr, plotdata(:,CStype), 'Color', clr(CStype,:), 'LineWidth', 1.5);
        end
        colormap spring
        legend('String', {'US onset', 'CS+US+', 'CS+US-', 'CS-US-'},'Location','best');
        legend boxoff
        xlim([xmin xmax])
        ylim([ymin ymax])
        title(titles{iPanel})
        
        % All conditions, complex stimuli
        iPanel = 2;
        clear plotdata
        
        plotdata(:,1) = nanmean(scrdata_CSpUSp_complex,2);
        plotdata(:,2) = nanmean(scrdata_CSpUSm_complex,2);
        plotdata(:,3) = nanmean(scrdata_CSmUSm_complex,2);
        
        axes('Position', pos(iPanel,:)); hold on;
        %     line([xmin xmax],[0 0],'Color','k','LineWidth',1)
        line([US US],[ymin ymax],'Color','r','LineWidth',1.5)
        for CStype = 1:3
            plot((1:size(plotdata,1))/sr, plotdata(:,CStype), 'Color', clr(CStype,:), 'LineWidth', 1.5);
        end
        colormap spring
        legend('String', {'US onset', 'CS+US+', 'CS+US-', 'CS-US-'},'Location','best');
        legend boxoff
        xlim([xmin xmax])
        ylim([ymin ymax])
        title(titles{iPanel})
        
        % CS+US- - CS-US- simple stimuli
        iPanel = 3;
        clear plotdata
        
        plotdata(:,1) = nanmean(scrdata_CSpUSm_simple,2);
        plotdata(:,2) = nanmean(scrdata_CSmUSm_simple,2);
        
        axes('Position', pos(iPanel,:)); hold on;
        %     line([xmin xmax],[0 0],'Color','k','LineWidth',1)
        line([US US],[ymin ymax],'Color','r','LineWidth',1.5)
        plot((1:size(plotdata,1))/sr, plotdata(:,1) - plotdata(:,2), 'Color', clr(1,:), 'LineWidth', 1.5);
        colormap spring
        xlim([xmin xmax])
        ylim([ymin ymax])
        %legend('String', {'33%', '66%', '100%'},'Location','best');
        %legend boxoff
        title(titles{iPanel})
        
        % CS+US- - CS-US- complex stimuli
        iPanel = 4;
        clear plotdata
        
        plotdata(:,1) = nanmean(scrdata_CSpUSm_complex,2);
        plotdata(:,2) = nanmean(scrdata_CSmUSm_complex,2);
        
        axes('Position', pos(iPanel,:)); hold on;
        %     line([xmin xmax],[0 0],'Color','k','LineWidth',1)
        line([US US],[ymin ymax],'Color','r','LineWidth',1.5)
        plot((1:size(plotdata,1))/sr, plotdata(:,1) - plotdata(:,2), 'Color', clr(1,:), 'LineWidth', 1.5);
        colormap spring
        xlim([xmin xmax])
        ylim([ymin ymax])
        %     legend('String', {'33%', '66%', '100%'},'Location','best');
        %     legend boxoff
        title(titles{iPanel})
        
        hold on
        suptitle(['Mean SCR (\muS), ' opts.sessionlist{ses} ', ' opts.groupnames{group} ', N = ' num2str(size(scrdata_USm,4))]);
        
        % Save figure
        %     if opts.psr.plot.valfix; vfname = 'valfix'; else; vfname = 'novalfix'; end
        %     sessavenames = {'AcqMaint' 'Acq2'};
        %     fp = fileparts(opts.expPath);
        %     plotPath = fullfile(fp,'Plots');
        %     if ~exist(plotPath,'dir'); mkdir(plotPath); end
        %     savefig(fig,fullfile(plotPath,['pupil_meanmm_' sessavenames{ses} '_interp_' vfname '_' num2str(size(pupildata_USm,4)) 'subs']));
        
    end
    
end

end