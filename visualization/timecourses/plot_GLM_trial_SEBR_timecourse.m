function plot_GLM_trial_SEBR_timecourse(opts,group)

% if opts.sebr.plot_allorders
%     %sList = opts.sebr.sList.control; % only control subjects, with all orders
%     sList = opts.sebr.sList.allsubs; % all subjects, with all orders
%     allordname = 'all';
% else
%     sList = opts.sebr.sList.control_sameorder; % only control subjects now, with the same order
%     allordname = 'same';
% end

if group == 0 % both together
    sList_simplefirst = opts.sebr.sList.simple_first;
    sList_complexfirst = opts.sebr.sList.complex_first;
elseif group == 1 % control
    sList_simplefirst = opts.sebr.sList.simple_first_control;
    sList_complexfirst = opts.sebr.sList.complex_first_control;
elseif group == 2 % experimental
    sList_simplefirst = opts.sebr.sList.simple_first_experim;
    sList_complexfirst = opts.sebr.sList.complex_first_experim;
end

%% Get GLM data
data_simplefirst = get_GLM_data(opts,sList_simplefirst);
data_complexfirst = get_GLM_data(opts,sList_complexfirst);
data_group.group(1) = data_simplefirst;
data_group.group(2) = data_complexfirst;
    
%% Separate into simple and complex stimuli and CS+ and CS-

if opts.sebr.plot_separate_CStype % separate CS+ and CS- stimuli
    
    for order = 1:2 % Simple and complex stimuli first groups

        data = data_group.group(order);
        
        for sub = 1:size(data.stimuli_CS,1)
            
            CStype = NaN(1,24);
            CStype(data.stimuli_CS(sub,:) == 1 & data.stimuli_complex(sub,:) == 0) = 1; % CS+ simple
            CStype(data.stimuli_CS(sub,:) == 0 & data.stimuli_complex(sub,:) == 0) = 2; % CS- simple
            CStype(data.stimuli_CS(sub,:) == 1 & data.stimuli_complex(sub,:) == 1) = 3; % CS+ complex
            CStype(data.stimuli_CS(sub,:) == 0 & data.stimuli_complex(sub,:) == 1) = 4; % CS- complex
            
            % Initialize data
            amplitude = NaN(4,length(CStype));
            
            % Extrapolate those trials where CS+/CS- did not occur
            for cs_type = 1:4
                amplitude(cs_type,CStype == cs_type) = data.amplitude(sub,CStype == cs_type);
                for trial = 1:length(CStype)
                    prev_trial = 0;
                    if isnan(amplitude(cs_type,trial)) % check if value for this trial for this CS type for this sub is NaN
                        while isnan(amplitude(cs_type,trial-prev_trial)) % go through previous trials until previous non-NaN value found
                            prev_trial = prev_trial+1;
                            if (trial-prev_trial) < 1; break; end % got to the first trial without finding a non-NaN value, stop while loop
                        end
                        if (trial-prev_trial) > 0; amplitude(cs_type,trial) = amplitude(cs_type,trial-prev_trial); end % save amplitude of previous non-NaN trial of the same type for this trial
                    end
                end
            end
            
            amplitude_allsubs(order,sub,:,:) = amplitude;
            
        end
    end
    
    % Format data for ease of use in plotting
    % Simple first subjects
    simplesubs = 1:length(sList_simplefirst);
    amplitude_simplefirst_cstype = squeeze(nanmean(amplitude_allsubs(1,simplesubs,:,:))); % mean over subjects
    amplitude_simplefirst_csp_s = amplitude_simplefirst_cstype(1,:); % Simple CS+
    amplitude_simplefirst_csm_s = amplitude_simplefirst_cstype(2,:); % Simple CS-
    amplitude_simplefirst_csp_c = amplitude_simplefirst_cstype(3,:); % Complex CS+
    amplitude_simplefirst_csm_c = amplitude_simplefirst_cstype(4,:); % Complex CS-
    amplitude_simplefirst_csp([1:6 13:18]) = amplitude_simplefirst_cstype(1,[1:6 13:18]); % Simple stimuli
    amplitude_simplefirst_csp([7:12 19:24]) = amplitude_simplefirst_cstype(3,[7:12 19:24]); % Complex stimuli
    amplitude_simplefirst_csm([1:6 13:18]) = amplitude_simplefirst_cstype(2,[1:6 13:18]); % Simple stimuli
    amplitude_simplefirst_csm([7:12 19:24]) = amplitude_simplefirst_cstype(4,[7:12 19:24]); % Complex stimuli
    % Complex first subjects
    complexsubs = 1:length(sList_complexfirst);
    amplitude_complexfirst_cstype = squeeze(nanmean(amplitude_allsubs(2,complexsubs,:,:)));  % mean over subjects
    amplitude_complexfirst_csp_s = amplitude_complexfirst_cstype(1,:); % Simple CS+
    amplitude_complexfirst_csm_s = amplitude_complexfirst_cstype(2,:); % Simple CS-
    amplitude_complexfirst_csp_c = amplitude_complexfirst_cstype(3,:); % Complex CS+
    amplitude_complexfirst_csm_c = amplitude_complexfirst_cstype(4,:); % Complex CS-
    amplitude_complexfirst_csp([1:6 13:18]) = amplitude_complexfirst_cstype(3,[1:6 13:18]); % Complex stimuli
    amplitude_complexfirst_csp([7:12 19:24]) = amplitude_complexfirst_cstype(1,[7:12 19:24]); % Simple stimuli
    amplitude_complexfirst_csm([1:6 13:18]) = amplitude_complexfirst_cstype(4,[1:6 13:18]); % Complex stimuli
    amplitude_complexfirst_csm([7:12 19:24]) = amplitude_complexfirst_cstype(2,[7:12 19:24]); % Simple stimuli
    
else % CS+ and CS- not separated, one value for each trial already exists, take mean over subjects
    
    amplitude_simplefirst = nanmean(data_simplefirst.amplitude);
    amplitude_complexfirst = nanmean(data_complexfirst.amplitude);
    
end

%% Plot

%colors = [255 51 51; 51 51 255; 255 51 153; 51 153 255]/255;

colors = [255 51 51; 255 153 153; 204 0 0;... % red: medium, light, dark
    0 128 255; 102 178 255; 0 0 204]/255; % blue: medium, light, dark

%% Per subject plots
% % Simple stimuli
% figure
% for sub = 1:length(sList)
%     subplot(ceil(sqrt(length(sList))-1),ceil(sqrt(length(sList))),sub)
%     plot(amplitude_simple_CSp(sub,:),'Color',colors(1,:),'Marker','o','MarkerFaceColor',colors(1,:),'MarkerSize',3,'LineWidth',1.5)
%     hold on
%     plot(amplitude_simple_CSm(sub,:),'Color',colors(2,:),'Marker','o','MarkerFaceColor',colors(2,:),'MarkerSize',3,'LineWidth',1.5)
%     title(['Sub ' num2str(sList(sub))])
%     set(gca,'xTick', 1:6)
%     xlim([1 6])
% end
% suptitle(['SEBR over all simple trials, ' allordname ' randomization orders, N = ' num2str(length(sList))])
% legend({'CS+' 'CS-'},'Location','best')
%
% figure
% for sub = 1:length(sList)
%     subplot(ceil(sqrt(length(sList))-1),ceil(sqrt(length(sList))),sub)
%     plot(amplitude_complex_CSp(sub,:),'Color',colors(1,:),'Marker','o','MarkerFaceColor',colors(1,:),'MarkerSize',3,'LineWidth',1.5)
%     hold on
%     plot(amplitude_complex_CSm(sub,:),'Color',colors(2,:),'Marker','o','MarkerFaceColor',colors(2,:),'MarkerSize',3,'LineWidth',1.5)
%     title(['Sub ' num2str(sList(sub))])
%     set(gca,'xTick', 1:6)
%     xlim([1 6])
% end
% suptitle(['SEBR over all complex trials, ' allordname ' randomization orders, N = ' num2str(length(sList))])
% legend({'CS+' 'CS-'},'Location','best')
%
%% Average plot

% % Simple stimuli first subjects
% figure
% hold on
% plot(mean(amplitude_simple_CSp_s),'Color',colors(1,:),'Marker','o','MarkerFaceColor',colors(1,:),'MarkerSize',8,'LineWidth',2)
% plot(mean(amplitude_simple_CSm_s),'Color',colors(2,:),'Marker','o','MarkerFaceColor',colors(2,:),'MarkerSize',8,'LineWidth',2)
% plot(mean(amplitude_complex_CSp_s),'Color',colors(3,:),'Marker','o','MarkerFaceColor',colors(3,:),'MarkerSize',8,'LineWidth',2)%,'LineStyle','--')
% plot(mean(amplitude_complex_CSm_s),'Color',colors(4,:),'Marker','o','MarkerFaceColor',colors(4,:),'MarkerSize',8,'LineWidth',2)%,'LineStyle','--')
% set(gca,'xTick', 1:6)
% xlim([1 6])
% ylabel('GLM estimate amplitude (a.u.)')
% xlabel('Trials')
% title(['Mean SEBR for all conditions, N = ' num2str(length(sList_simplefirst)) ' with simple stimuli first'])
% legend(condnames,'Location','best')
% ylim([2e-5 14e-5])
%
% % Complex stimuli first subjects
% figure
% hold on
% plot(mean(amplitude_simple_CSp_c),'Color',colors(1,:),'Marker','o','MarkerFaceColor',colors(1,:),'MarkerSize',8,'LineWidth',2)
% plot(mean(amplitude_simple_CSm_c),'Color',colors(2,:),'Marker','o','MarkerFaceColor',colors(2,:),'MarkerSize',8,'LineWidth',2)
% plot(mean(amplitude_complex_CSp_c),'Color',colors(3,:),'Marker','o','MarkerFaceColor',colors(3,:),'MarkerSize',8,'LineWidth',2)%,'LineStyle','--')
% plot(mean(amplitude_complex_CSm_c),'Color',colors(4,:),'Marker','o','MarkerFaceColor',colors(4,:),'MarkerSize',8,'LineWidth',2)%,'LineStyle','--')
% set(gca,'xTick', 1:6)
% xlim([1 6])
% ylabel('GLM estimate amplitude (a.u.)')
% xlabel('Trials')
% title(['Mean SEBR for all conditions, N = ' num2str(length(sList_complexfirst)) ' with complex stimuli first'])
% legend(condnames,'Location','best')
% ylim([2e-5 14e-5])

%% Average plot with all 24 stimuli

figure('Position',[300,300,1000,450])
hold on
grey = [96 96 96]/255;
if group == 0
    y_uplim = 1.5e-5;
elseif group == 1
    y_uplim = 20e-5;
elseif group == 2
    y_uplim = 20e-5;
end

line([6.5 6.5],[0 y_uplim],'Color',grey,'LineWidth',2)
line([12.5 12.5],[0 y_uplim],'Color',grey,'LineWidth',2)
line([18.5 18.5],[0 y_uplim],'Color',grey,'LineWidth',2)

% Colors:
% red (1-3) = simple first, blue (4-6) = complex first
% light (2,5) = simple stimuli, dark (3,6) = complex stimuli, medium (1,4) = line

if opts.sebr.plot_separate_CStype % With CS+ and CS- separately
    
    % Simple first CS+
    p1 = plot(1:24,amplitude_simplefirst_csp,'Color',colors(1,:),'LineStyle','-','LineWidth',2);
    plot(1:6,amplitude_simplefirst_csp_s(1:6),'Color',colors(2,:),'LineStyle','none','Marker','o','MarkerFaceColor',colors(2,:),'MarkerSize',8) % simple
    plot(7:12,amplitude_simplefirst_csp_c(7:12),'Color',colors(3,:),'LineStyle','none','Marker','o','MarkerFaceColor',colors(3,:),'MarkerSize',8) % complex
    plot(13:18,amplitude_simplefirst_csp_s(13:18),'Color',colors(2,:),'LineStyle','none','Marker','o','MarkerFaceColor',colors(2,:),'MarkerSize',8) % simple
    plot(19:24,amplitude_simplefirst_csp_c(19:24),'Color',colors(3,:),'LineStyle','none','Marker','o','MarkerFaceColor',colors(3,:),'MarkerSize',8) % complex
    
    % Simple first CS-
    p2 = plot(1:24,amplitude_simplefirst_csm,'Color',colors(1,:),'LineStyle','--','LineWidth',2);
    plot(1:6,amplitude_simplefirst_csm_s(1:6),'Color',colors(2,:),'LineStyle','none','Marker','o','MarkerFaceColor',colors(2,:),'MarkerSize',8) % simple
    plot(7:12,amplitude_simplefirst_csm_c(7:12),'Color',colors(3,:),'LineStyle','none','Marker','o','MarkerFaceColor',colors(3,:),'MarkerSize',8) % complex
    plot(13:18,amplitude_simplefirst_csm_s(13:18),'Color',colors(2,:),'LineStyle','none','Marker','o','MarkerFaceColor',colors(2,:),'MarkerSize',8) % simple
    plot(19:24,amplitude_simplefirst_csm_c(19:24),'Color',colors(3,:),'LineStyle','none','Marker','o','MarkerFaceColor',colors(3,:),'MarkerSize',8) % complex
    
    % Complex first CS+
    p3 = plot(1:24,amplitude_complexfirst_csp,'Color',colors(4,:),'LineStyle','-','LineWidth',2);
    plot(1:6,amplitude_complexfirst_csp_c(1:6),'Color',colors(6,:),'LineStyle','none','Marker','o','MarkerFaceColor',colors(6,:),'MarkerSize',8) % complex
    plot(7:12,amplitude_complexfirst_csp_s(7:12),'Color',colors(5,:),'LineStyle','none','Marker','o','MarkerFaceColor',colors(5,:),'MarkerSize',8) % simple
    plot(13:18,amplitude_complexfirst_csp_c(13:18),'Color',colors(6,:),'LineStyle','none','Marker','o','MarkerFaceColor',colors(6,:),'MarkerSize',8) % complex
    plot(19:24,amplitude_complexfirst_csp_s(19:24),'Color',colors(5,:),'LineStyle','none','Marker','o','MarkerFaceColor',colors(5,:),'MarkerSize',8) % simple
    
    % Complex first CS-
    p4 = plot(1:24,amplitude_complexfirst_csm,'Color',colors(4,:),'LineStyle','--','LineWidth',2);
    plot(1:6,amplitude_complexfirst_csm_c(1:6),'Color',colors(6,:),'LineStyle','none','Marker','o','MarkerFaceColor',colors(6,:),'MarkerSize',8) % complex
    plot(7:12,amplitude_complexfirst_csm_s(7:12),'Color',colors(5,:),'LineStyle','none','Marker','o','MarkerFaceColor',colors(5,:),'MarkerSize',8) % simple
    plot(13:18,amplitude_complexfirst_csm_c(13:18),'Color',colors(6,:),'LineStyle','none','Marker','o','MarkerFaceColor',colors(6,:),'MarkerSize',8) % complex
    plot(19:24,amplitude_complexfirst_csm_s(19:24),'Color',colors(5,:),'LineStyle','none','Marker','o','MarkerFaceColor',colors(5,:),'MarkerSize',8) % simple
    
else % With CS+ and CS- together
    
    % Simple first
    p1 = plot(1:24,amplitude_simplefirst,'Color',colors(1,:),'LineWidth',2);
    plot(1:6,amplitude_simplefirst(1:6),'Color',colors(2,:),'LineStyle','none','Marker','o','MarkerFaceColor',colors(2,:),'MarkerSize',8) % simple
    plot(7:12,amplitude_simplefirst(7:12),'Color',colors(3,:),'LineStyle','none','Marker','o','MarkerFaceColor',colors(3,:),'MarkerSize',8) % complex
    plot(13:18,amplitude_simplefirst(13:18),'Color',colors(2,:),'LineStyle','none','Marker','o','MarkerFaceColor',colors(2,:),'MarkerSize',8) % simple
    plot(19:24,amplitude_simplefirst(19:24),'Color',colors(3,:),'LineStyle','none','Marker','o','MarkerFaceColor',colors(3,:),'MarkerSize',8) % complex
    
    % Complex first
    p2 = plot(1:24,amplitude_complexfirst,'Color',colors(4,:),'LineWidth',2);
    plot(1:6,amplitude_complexfirst(1:6),'Color',colors(6,:),'LineStyle','none','Marker','o','MarkerFaceColor',colors(6,:),'MarkerSize',8) % complex
    plot(7:12,amplitude_complexfirst(7:12),'Color',colors(5,:),'LineStyle','none','Marker','o','MarkerFaceColor',colors(5,:),'MarkerSize',8) % simple
    plot(13:18,amplitude_complexfirst(13:18),'Color',colors(6,:),'LineStyle','none','Marker','o','MarkerFaceColor',colors(6,:),'MarkerSize',8) % complex
    plot(19:24,amplitude_complexfirst(19:24),'Color',colors(5,:),'LineStyle','none','Marker','o','MarkerFaceColor',colors(5,:),'MarkerSize',8) % simple
    
end

set(gca,'xTick', 1:24)
xlim([1 24])
ylim([0 y_uplim])
ylabel('GLM estimate amplitude (a.u.)')
xlabel('Trial')
title(['Mean SEBR, Simple first N = ' num2str(length(sList_simplefirst)) ', Complex first N = ' num2str(length(sList_complexfirst))])

if opts.sebr.plot_separate_CStype
    condnames = {'Simple first CS+' 'Simple first CS-' 'Complex first CS+' 'Complex first CS-'};
    legend([p1 p2 p3 p4],condnames,'Location','northeast')
else
    condnames = {'Simple first' 'Complex first'};
    legend([p1 p2],condnames,'Location','best')
end

legend('boxoff')
text(20,y_uplim-6e-5,{'Lighter: simple','Darker: complex'})


end