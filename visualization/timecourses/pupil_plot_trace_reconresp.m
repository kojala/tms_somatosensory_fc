function pupil_plot_trace_reconresp(opts,sessions)

for ses = sessions
    
    for group = 1:2
        
        if group == 1
            sList = opts.psr.sList.GLM_control{ses};
        else
            sList = opts.psr.sList.GLM_experimental{ses};
        end

        sesname = opts.psr.sessionlist{ses};
        no_conditions = opts.psr.estimated_conditions*2;
        sr = opts.psr.glm.sr;
        
        glmpath = fullfile(opts.psr.path.glm_cond,opts.psr.glm_bfname{opts.psr.estimated_response},sesname);
        recresp = nan(length(sList),no_conditions,sr*20);
        
        for s = 1:numel(sList)
            
            s_id = num2str(sList(s));
            
            cond = 1;
            
            for stim = 1:2
                
                filename = [opts.psr.glm_filename.cond s_id '_' opts.psr.glm.stimname{stim} '.mat'];
                datafile = fullfile(glmpath,filename);
                
                if exist(datafile,'file')
                    
                    glmdata = load(datafile);
                    
                    recresp(s,cond:cond+2,:) = glmdata.glm.resp(:,1:3)';
                    
                    cond = cond + 3;
                    
                end
                
            end
            
        end
    
    %% Plotting
    fig = figure('Position', [100, 100, 1400, 600]);
    %pos = [.03 .53 .40 .40; .5 .53 .40 .40; .03 .04 .40 .40]; % 3 panels
    % axes('Position',[lowleft_x lowleft_y width height]);
    fromdown = .1;
    fromleft = .05;
    height = 0.40;
    width = 0.40;
    pos = [fromleft fromdown+height+0.1 width height; fromleft+width+0.1 fromdown+height+0.1 width height; ...
        fromleft fromdown width height; fromleft+width+0.1 fromdown width height;];
    clr = [9 226 183; 147 0 226; 9 136 239; 226 9 78];
    clr = clr./255;

    titles = {'All conditions, simple stimuli'...
        'All conditions, complex stimuli', ...
        'CS+US-  -  CS-US-, simple stimuli',...
        'CS+US-  -  CS-US-, complex stimuli'};
    %pos = axpos(1, 3, .05, .05, .05, .1, .05);
    
    pupildata_CSpUSp_simple = recresp(:,1,:);
    pupildata_CSpUSm_simple = recresp(:,2,:);
    pupildata_CSmUSm_simple = recresp(:,3,:);
    pupildata_CSpUSp_complex = recresp(:,4,:);
    pupildata_CSpUSm_complex = recresp(:,5,:);
    pupildata_CSmUSm_complex = recresp(:,6,:);
    
    xmin = 0;
    xmax = 5;
    ymin = -0.6;
    if group == 1 && ses == 1; ymax = 0.6; else; ymax = 1.5; end
    US = 3.5;
    % All conditions, simple stimuli
    iPanel = 1;

    plotdata(:,1) = nanmean(squeeze(pupildata_CSpUSp_simple));
    plotdata(:,2) = nanmean(squeeze(pupildata_CSpUSm_simple));
    plotdata(:,3) = nanmean(squeeze(pupildata_CSmUSm_simple));
    
    axes('Position', pos(iPanel,:)); hold on;
%     line([xmin xmax],[0 0],'Color','k','LineWidth',1)
    line([US US],[ymin ymax],'Color','r','LineWidth',1.5)
    for CStype = 1:3
        plot((1:size(plotdata,1))/sr, plotdata(:,CStype), 'Color', clr(CStype,:), 'LineWidth', 1.5);
    end
    colormap spring
    legend('String', {'US onset', 'CS+US+', 'CS+US-', 'CS-US-'},'Location','best');
    legend boxoff
    xlim([xmin xmax])
    ylim([ymin ymax])
    title(titles{iPanel})
    
    % All conditions, complex stimuli
    iPanel = 2;
    clear plotdata

    plotdata(:,1) = nanmean(squeeze(pupildata_CSpUSp_complex));
    plotdata(:,2) = nanmean(squeeze(pupildata_CSpUSm_complex));
    plotdata(:,3) = nanmean(squeeze(pupildata_CSmUSm_complex));
    
    axes('Position', pos(iPanel,:)); hold on;
%     line([xmin xmax],[0 0],'Color','k','LineWidth',1)
    line([US US],[ymin ymax],'Color','r','LineWidth',1.5)
    for CStype = 1:3
        plot((1:size(plotdata,1))/sr, plotdata(:,CStype), 'Color', clr(CStype,:), 'LineWidth', 1.5);
    end
    colormap spring
    legend('String', {'US onset', 'CS+US+', 'CS+US-', 'CS-US-'},'Location','best');
    legend boxoff
    xlim([xmin xmax])
    ylim([ymin ymax])
    title(titles{iPanel})
        
    % CS+US- - CS-US- simple stimuli
    iPanel = 3;
    clear plotdata
    plotdata(:,1) = nanmean(squeeze(pupildata_CSpUSm_simple));
    plotdata(:,2) = nanmean(squeeze(pupildata_CSmUSm_simple));
    axes('Position', pos(iPanel,:)); hold on;
%     line([xmin xmax],[0 0],'Color','k','LineWidth',1)
    line([US US],[ymin ymax],'Color','r','LineWidth',1.5)
    plot((1:size(plotdata,1))/sr, plotdata(:,1) - plotdata(:,2), 'Color', clr(1,:), 'LineWidth', 1.5);
    colormap spring
    xlim([xmin xmax])
    ylim([ymin ymax])
    %legend('String', {'33%', '66%', '100%'},'Location','best');
    %legend boxoff
    title(titles{iPanel})
    
    % CS+US- - CS-US- complex stimuli
    iPanel = 4;
    clear plotdata
    plotdata(:,1) = nanmean(squeeze(pupildata_CSpUSm_complex));
    plotdata(:,2) = nanmean(squeeze(pupildata_CSmUSm_complex));
    axes('Position', pos(iPanel,:)); hold on;
%     line([xmin xmax],[0 0],'Color','k','LineWidth',1)
    line([US US],[ymin ymax],'Color','r','LineWidth',1.5)
    plot((1:size(plotdata,1))/sr, plotdata(:,1) - plotdata(:,2), 'Color', clr(1,:), 'LineWidth', 1.5);
    colormap spring
    xlim([xmin xmax])
    ylim([ymin ymax])
%     legend('String', {'33%', '66%', '100%'},'Location','best');
%     legend boxoff
    title(titles{iPanel})
    
    hold on
    suptitle(['Mean pupil size (mm), ' opts.psr.sessionlist{ses} ', ' opts.psr.groupnames{group} ', N = ' num2str(size(pupildata_CSpUSp_simple,1))]);
    
    % Save figure
%     if opts.psr.plot.valfix; vfname = 'valfix'; else; vfname = 'novalfix'; end
%     sessavenames = {'AcqMaint' 'Acq2'};
%     fp = fileparts(opts.expPath);
%     plotPath = fullfile(fp,'Plots');
%     if ~exist(plotPath,'dir'); mkdir(plotPath); end
%     savefig(fig,fullfile(plotPath,['pupil_meanmm_' sessavenames{ses} '_interp_' vfname '_' num2str(size(pupildata_USm,4)) 'subs']));
    
    end

end

end