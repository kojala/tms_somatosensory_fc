function plotSCR_sesdiff(opts)

sList = opts.scr.sList.comp;
sesList = opts.scr.sessionlist;
s = 1;

for sIDX = 1:numel(sList)
    
    % File names
    if sList(sIDX) < 100
        fname1 = fullfile(opts.scr.path.trim,['tpspm_ss7b_0' num2str(sList(sIDX)) '_physio_' sesList{1} '_SCR.mat']);
        fname2 = fullfile(opts.scr.path.trim,['tpspm_ss7b_0' num2str(sList(sIDX)) '_physio_' sesList{3} '_SCR.mat']);
        fnamebhv1  = fullfile(opts.bhv.path,['ss7b_0' num2str(sList(sIDX)) '_behav_' sesList{1} '.mat']);
        fnamebhv2  = fullfile(opts.bhv.path,['ss7b_0' num2str(sList(sIDX)) '_behav_' sesList{3} '.mat']);
    else % Check what is correct for when subject numbers go past 99!
        fname1 = fullfile(opts.scr.path.trim,['tpspm_ss7b_' num2str(sList(sIDX)) '_physio_' sesList{1} '_SCR.mat']);
        fname2 = fullfile(opts.scr.path.trim,['tpspm_ss7b_' num2str(sList(sIDX)) '_physio_' sesList{3} '_SCR.mat']);
        fnamebhv1  = fullfile(opts.bhv.path,['ss7b_' num2str(sList(sIDX)) '_behav_' sesList{1} '.mat']);
        fnamebhv2  = fullfile(opts.bhv.path,['ss7b_' num2str(sList(sIDX)) '_behav_' sesList{3} '.mat']);
    end
    
    if exist(fname1,'file') && exist(fname2,'file')
        
        clear data
        % Load day 2 (acquisition) data
        [~,~,data] = pspm_load_data(fname1);
        
        sr = data{opts.scr.channel.scr}.header.sr;
        scrdata1  = data{opts.scr.channel.scr}.data;
        markers1 = data{opts.scr.channel.marker}.data;
        
        % Bandpass & downsampling options
        filt.sr = sr;
        filt.lpfreq = 5;
        filt.lporder = 1;
        filt.hpfreq = 0.0159;
        filt.hporder = 1;
        filt.direction = 'bi';
        filt.down = 10;
        
        clear data
        % Load day 3 (retest) data
        [~,~,data] = pspm_load_data(fname2);
        scrdata2  = data{opts.scr.channel.scr}.data;
        markers2 = data{opts.scr.channel.marker}.data;
        
        % Bandpass & downsampling (for plotting purposes only!)
        [~,scr1_filt,newsr] = pspm_prepdata(scrdata1,filt);
        [~,scr2_filt,~] = pspm_prepdata(scrdata2,filt);
        
        % Collect SCR
        %------------------------------------------------------------------
        
        % Day 2 data
        for trial = 1:numel(markers1)
            t0 = round(markers1(trial)*newsr);
            if t0 == 0; t0 = t0+1; end
            t1 = t0+120; %12000 for original sr
            tbs = t0-5;
            baseline = nanmean(scr1_filt(tbs:t0));
            scr1(:,trial,sIDX) = scr1_filt(t0:t1)-baseline;
            %scrmaxampl1(trial,sIDX) = max(scrdata1(t0:t1));
        end
        
        % Day 3 data
        for trial = 1:numel(markers2)
            t0 = round(markers2(trial)*newsr);
            if t0 == 0; t0 = t0+1; end
            t1 = t0+120; %12000 for original sr
            tbs = t0-5;
            baseline = nanmean(scr2_filt(tbs:t0));
            scr2(:,trial,sIDX) = scr2_filt(t0:t1)-baseline;
            %scrmaxampl2(trial,sIDX) = max(scrdata2(t0:t1));
        end
        
        % Separate for CS+ (US+ and US-) and CS-
        %------------------------------------------------------------------
        % Load behavioural file with event IDs
        % Day 2
        bhvdata1 = load(fnamebhv1);
        
%         CSpUSp = bhvdata1.Data(:,3) == 1 & bhvdata1.Data(:,4) == 1;
%         CSpUSm = bhvdata1.Data(:,3) == 1 & bhvdata1.Data(:,4) == 0;
%         CSm = bhvdata1.Data(:,3) == 2;
        US = bhvdata1.Data(:,4) == 1;
        USm = bhvdata1.Data(:,4) == 0;
        
%         scrCSpUSp1(:,:,sIDX) = scr1(:,CSpUSp,sIDX);
%         scrCSpUSm1(:,:,sIDX) = scr1(:,CSpUSm,sIDX);
%         scrCSm1(:,:,sIDX) = scr1(:,CSm,sIDX);
        
        scrUS1(:,:,sIDX) = scr1(:,US,sIDX);
        scrUSm1(:,:,sIDX) = scr1(:,USm,sIDX);
        %scrmaxamplUS(:,sIDX) = scrmaxampl(US,sIDX);

        % Day 2
        bhvdata2 = load(fnamebhv2);
        
%         CSpUSp = bhvdata2.Data(:,3) == 1 & bhvdata2.Data(:,4) == 1;
%         CSpUSm = bhvdata2.Data(:,3) == 1 & bhvdata2.Data(:,4) == 0;
%         CSm = bhvdata2.Data(:,3) == 2;
        US = bhvdata2.Data(:,4) == 1;
        USm = bhvdata2.Data(:,4) == 0;
        
%         scrCSpUSp2(:,:,sIDX) = scr2(:,CSpUSp,sIDX);
%         scrCSpUSm2(:,:,sIDX) = scr2(:,CSpUSm,sIDX);
%         scrCSm2(:,:,sIDX) = scr2(:,CSm,sIDX);
        
        scrUS2(:,:,sIDX) = scr2(:,US,sIDX);
        scrUSm2(:,:,sIDX) = scr2(:,USm,sIDX);
        
        % Keeping a list of the real subject numbers
        sList2(s) = sList(sIDX);
        s = s + 1;
        
    end
end

% Summarize and plot
%--------------------------------------------------------------------------

% SCR to US per subject normalized to the peak amplitude
allMeansUS1 = squeeze(nanmean(scrUS1(:,:,:),2));
allMeansUS1(:,sum(allMeansUS1)==0) = [];

allMeansUS2 = squeeze(nanmean(scrUS2(:,:,:),2));
allMeansUS2(:,sum(allMeansUS2)==0) = [];

allMeansUSm1 = squeeze(nanmean(scrUSm1(:,:,:),2));
allMeansUSm1(:,sum(allMeansUSm1)==0) = [];

allMeansUSm2 = squeeze(nanmean(scrUSm2(:,:,:),2));
allMeansUSm2(:,sum(allMeansUSm2)==0) = [];

figure
suptitle('SCR to US')
for sIDX = 1:size(allMeansUS1,2)
    subplot(ceil(sqrt(size(allMeansUS1,2))),ceil(sqrt(size(allMeansUS1,2))),sIDX)
    title(['Subject ' int2str(sList2(sIDX))])
    hold on
    plot(allMeansUS1(:,sIDX),'Color',[148,0,211]./255,'LineWidth',1.5)
    plot(allMeansUSm1(:,sIDX),'Color',[148,0,211]./255,'LineWidth',0.5)
    plot(allMeansUS2(:,sIDX),'Color',[0,206,209]./255,'LineWidth',1.5)
    plot(allMeansUSm2(:,sIDX),'Color',[0,206,209]./255,'LineWidth',0.5)
end
%legend('Acquisition','Retest','Location','East')
legend('US+ acq','US- acq','US+ retest','US- retest')

end