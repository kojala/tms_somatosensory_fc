function plotEMG_startleresponse(opts)

% visual inspection of EMG blink startle response for CS+US+. This is
% meant as an overall quality assessment without detailed analyses.
% Steps:
%  1. Cut out windows after startle
%  2. Sort window by CS type
%  3. Peak scoring for maximum amplitude
%  4. Plot startle response to sound
%__________________________________________________________________________

sList = opts.sebr.sList.orig;

s = 1;
    
for sIDX = 1:numel(sList)
    
    if sList(sIDX) < 100
        fnamesebr = fullfile(opts.sebr.path.trim,['tpspm_ss7b_0' num2str(sList(sIDX)) '_physio_retention_SEBR.mat']);
        fnamebhv = fullfile(opts.bhv.path,['ss7b_0' num2str(sList(sIDX)) '_behav_retention.mat']);
    else
        fnamesebr = fullfile(opts.sebr.path.trim,['tpspm_ss7b_' num2str(sList(sIDX)) '_physio_retention_SEBR.mat']);
        fnamebhv = fullfile(opts.bhv.path,['ss7b_' num2str(sList(sIDX)) '_behav_retention.mat']);
    end
    
    if exist(fnamesebr,'file')
        
        % Load preprocessed EMG data
        %------------------------------------------------------------------
        clear data
        [~, ~, data] = pspm_load_data(fnamesebr);
        
        sr = data{opts.sebr.channel.preproc}.header.sr;  % Sampling rate
        emg_pp = data{opts.sebr.channel.preproc}.data;  % Preprocessed data
        
        % Collect EMG responses
        %------------------------------------------------------------------
        
        fprintf('Subject # %3.0f ... \n', sList(sIDX))
        
        markers = data{opts.sebr.channel.marker}.data; % check the correct no. of markers
        markersfound(sIDX,1) = sList(sIDX);
        markersfound(sIDX,2) = numel(markers); % should be 25 at this point
        sprintf(['Found ' num2str(markersfound(sIDX,2)) ' sound markers']);
        %markers(1) = []; % first startle sound is not part of the experiment
        
%         for trial = 1:numel(triggers)
%             t0 = round(triggers(trial)*sr);
%             t1 = t0+300;
%             emg(:,trial,sIDX) = emg_pp(t0:t1);
%         end
        
        % Separate for CS+ and CS-
        %------------------------------------------------------------------
        % Load behavioural file with event IDs
        bhvdata = load(fnamebhv);
        
        %CSall = bhvdata.Data(:,6) == 0;
        CSp = bhvdata.Data(:,3) == 1 & bhvdata.Data(:,6) == 0;
        CSm = bhvdata.Data(:,3) == 2 & bhvdata.Data(:,6) == 0;
        
%         emgCSall(:,:,sIDX) = emg(:,CSall,sIDX);
%         emgCSp(:,:,sIDX) = emg(:,CSp,sIDX);
%         emgCSm(:,:,sIDX) = emg(:,CSm,sIDX);
        
%         % For simple stimuli
%         CSp_s = Data(:,3) == 1 & Data(:,6) == 0;
%         CSm_s = Data(:,3) == 2 & Data(:,6) == 0;
%         emgCSp_s(:,:,sIDX) = emg(:,CSp_s,sIDX);
%         emgCSm_s(:,:,sIDX) = emg(:,CSm_s,sIDX);
%         
%         % For complex stimuli
%         CSp_c = Data(:,3) == 1 & Data(:,6) == 1;
%         CSm_c = Data(:,3) == 2 & Data(:,6) == 1;
%         emgCSp_c(:,:,sIDX) = emg(:,CSp_c,sIDX);
%         emgCSm_c(:,:,sIDX) = emg(:,CSm_c,sIDX);
      
        % Peak scoring for maximum EMG startle amplitude
        %------------------------------------------------------------------

        startletrigger_CSp = markers(CSp);
        startletrigger_CSm = markers(CSm);
        
        % CS+ trials
        for iTrl = 1:numel(startletrigger_CSp)
            winstart = ceil(startletrigger_CSp(iTrl) * sr);
            basewin = winstart + sr * [-0.02 0]; % Baseline 20 ms before to CS onset
            win = winstart + sr * [0 0.2];
            peakwin = winstart + sr * [0.02 0.10];
            emgCSp_blcorr(iTrl,:,s) = emg_pp(peakwin(1):peakwin(2)) - mean(emg_pp(basewin(1):basewin(2))); % Baseline correction
            emgCSp(iTrl,:,s) = emg_pp(peakwin(1):peakwin(2)); % No baseline correction
            [mx, ind] = max(emg_pp(peakwin(1):peakwin(2)));% - mean(emg_pp(basewin(1):basewin(2))));
            emgpeak_CSp(sIDX, iTrl) = abs(mx);
        end
        
        % CS- trials
        for iTrl = 1:numel(startletrigger_CSm)
            winstart = ceil(startletrigger_CSm(iTrl) * sr);
            basewin = winstart + sr * [-0.02 0]; % Baseline 20 ms before to CS onset
            win = winstart + sr * [0 0.2];
            peakwin = winstart + sr * [0.02 0.10];
            emgCSm_blcorr(iTrl,:,s) = emg_pp(peakwin(1):peakwin(2)) - mean(emg_pp(basewin(1):basewin(2))); % Baseline correction
            emgCSm(iTrl,:,s) = emg_pp(peakwin(1):peakwin(2)); % No baseline correction
            [mx, ind] = max(emg_pp(peakwin(1):peakwin(2)));% - mean(emg_pp(basewin(1):basewin(2))));
            emgpeak_CSm(sIDX, iTrl) = abs(mx);
        end
        
        % All trials
        for iTrl = 1:numel(markers)
            winstart = ceil(markers(iTrl) * sr);
            basewin = winstart + sr * [-0.02 0]; % Baseline 20 ms before to CS onset
            win = winstart + sr * [0 0.10]; % From CS onset to 1 s after
            peakwin = winstart + sr * [0.02 0.15]; % From 20 ms to 1.5 s after CS onset
            emg_blcorr(iTrl,:,s) = emg_pp(peakwin(1):peakwin(2)) - mean(emg_pp(basewin(1):basewin(2))); % Baseline correction
            emg(iTrl,:,s) = emg_pp(peakwin(1):peakwin(2)); % No baseline correction
            [mx, ind] = max(emg_pp(peakwin(1):peakwin(2))); % Find peaks
            emgpeak(iTrl,s) = abs(mx);
            emgpeak_blcorr(iTrl,s) = abs(mx) - mean(emg_pp(basewin(1):basewin(2))); 
            emglat(iTrl,s) = ind/sr + 0.02;
        end
        
        % Keeping a list of the real subject numbers
        sList2(s) = sList(sIDX);
        s = s + 1;
        
    end
end


%% Summarize and plot
%--------------------------------------------------------------------------

%% Startle response over all 24 trials normalized to the individual peak
% Mean peak
allMeans = squeeze(mean(emg(:,:,:)));
allMeans(:,sum(allMeans)==0) = [];

for s = 1:size(allMeans,2)
    allMeansnorm(:,s) = (allMeans(:,s)-min(allMeans(:,s)))/(max(allMeans(:,s))-min(allMeans(:,s)));
end
allMeans = allMeansnorm;

%save(fullfile(opts.sebr.path.trim,['EMGdata_' int2str(size(allMeans,2)) 'subs_normalized.mat']),'allMeans')

figure
suptitle('Startle eyeblink response to sound (over 24 trials, CS+ and CS-)')
for sIDX = 1:size(allMeans,2)
    subplot(ceil(sqrt(size(allMeans,2))),ceil(sqrt(size(allMeans,2))),sIDX)
    title(['Subject ' int2str(sList2(sIDX))])
    hold on
    plot(allMeans(:,sIDX),'k')
    %ylim([0 1])
    xlim([0 100])
end

% Maximum of the means
maxallMeans = max(allMeans);
allMeansnorm2=bsxfun(@rdivide,allMeans,maxallMeans);

figure
suptitle('Startle eyeblink response to sound (over 24 trials, CS+ and CS-)')
for sIDX = 1:size(allMeansnorm2,2)
    subplot(ceil(sqrt(size(allMeansnorm2,2))),ceil(sqrt(size(allMeansnorm2,2))),sIDX)
    title(['Subject ' int2str(sList2(sIDX))])
    hold on
    plot(allMeansnorm2(:,sIDX),'k')
    xlim([0 100])
end


%% Maximum amplitude and latency counts
figure;hist(mean(emgpeak, 1)); set(gcf, 'Name', 'EMG peak amplitude');
figure;hist(mean(emglat, 1)); set(gcf, 'Name', 'EMG peak latency');


%% SEBRF plot
grandmean = squeeze(mean(mean(emg_blcorr, 1), 3));
figure; plot(grandmean/max(grandmean),'k-', 'LineWidth', 2); hold on
% coeffModel = [3.5114,... % k of gamma function
%     0.0108,... % theta of gamma function
%     0.0345]; % Onset of the gamma function
%sebrf = pspm_bf_sebrf([0.001, 1, coeffModel]);
sebrf = pspm_bf_sebrf([0.001, 0, 0]);
plot(sebrf/max(sebrf), 'r-', 'LineWidth', 1); hold on

end

%% Old stuff

%emgpeak(:,sum(emgpeak)==0) = [];
%meanEmgpeak = mean(emgpeak);
%allMeans=bsxfun(@rdivide,allMeans,meanEmgpeak);
%allMeans=bsxfun(@rdivide,allMeans,max(emgpeak));

% Startle response over all 24 trials normalized to the peak
% allMeans = squeeze(mean(emg(:,:,:)));
% allMeans(:,sum(allMeans)==0) = [];
% save(fullfile(opts.sebr.path.trim,['EMGdata_' int2str(size(allMeans,2)) 'subs.mat']),'allMeans')
% 
% figure
% suptitle('Startle eyeblink response to sound (over 24 trials, CS+ and CS-)')
% for sIDX = 1:size(allMeans,2)
%     subplot(ceil(sqrt(size(allMeans,2))),ceil(sqrt(size(allMeans,2))),sIDX)
%     title(['Subject ' int2str(sList2(sIDX))])
%     hold on
%     plot(allMeans(:,sIDX),'k')
%     %ylim([-1 1])
%     xlim([0 100])
% end




% % Startle response over all 24 trials normalized to the peak
% % and corrected to baseline
% allMeans = squeeze(mean(emg_blcorr(:,:,:)));
% allMeans(:,sum(allMeans)==0) = [];
% 
% % Mean peak from peak-scoring
% %emgpeak_blcorr(:,sum(emgpeak_blcorr)==0) = [];
% %meanEmgpeak = mean(emgpeak_blcorr);
% %allMeans=bsxfun(@rdivide,allMeans,meanEmgpeak);
% for s = 1:size(allMeans,2)
%     allMeansnorm(:,s) = (allMeans(:,s)-min(allMeans(:,s)))/(max(allMeans(:,s))-min(allMeans(:,s)));
% end
% allMeans = allMeansnorm;
% 
% save(fullfile(opts.sebr.path.trim,['EMGdata_' int2str(size(allMeans,2)) 'subs_normalized_baselinecorrected.mat']),'allMeans')
% 
% figure
% suptitle('Startle eyeblink response to sound (over 24 trials, CS+ and CS-)')
% for sIDX = 1:size(allMeans,2)
%     subplot(ceil(sqrt(size(allMeans,2))),ceil(sqrt(size(allMeans,2))),sIDX)
%     title(['Subject ' int2str(sList2(sIDX))])
%     hold on
%     plot(allMeans(:,sIDX),'k')
%     ylim([0 1])
%     xlim([0 100])
% end
% 
% clear allMeans



% %% Corrected to baseline and normalized to 0-1 range over all subjects
% allMeans = squeeze(mean(emg_blcorr(:,:,:)));
% allMeans(:,sum(allMeans)==0) = [];
% allMeansnorm = (allMeans-min(min(allMeans)))/(max(max(allMeans))-min(min(allMeans)));
% allMeans = allMeansnorm;
% 
% save(fullfile(opts.sebr.path.trim,['EMGdata_' int2str(size(allMeans,2)) 'subs_normalizedto1.mat']),'allMeans')
% 
% figure
% suptitle('Startle eyeblink response to sound (over 24 trials, CS+ and CS-)')
% for sIDX = 1:size(allMeans,2)
%     subplot(ceil(sqrt(size(allMeans,2))),ceil(sqrt(size(allMeans,2))),sIDX)
%     title(['Subject ' int2str(sList2(sIDX))])
%     hold on
%     plot(allMeans(:,sIDX),'k')
%     ylim([0 1])
%     xlim([0 100])
% end



% EMG time series per subject normalized to the peak amplitude
% emgnotrials = reshape(emg_blcorr,size(emg_blcorr,1)*size(emg_blcorr,2),size(emg_blcorr,3));
% emgnotrials(:,sum(emgnotrials)==0) = [];

%maxemg = max(emgnotrials);

%allMeansnorm=bsxfun(@rdivide,emgnotrials,maxemg);
% allMeansnorm=bsxfun(@divide,emgnotrials,emgpeak);
% 
% figure
% suptitle('EMG normalized to the peak amplitude')
% for sIDX = 1:size(allMeansnorm,2)
%     subplot(ceil(sqrt(size(allMeansnorm,2))),ceil(sqrt(size(allMeansnorm,2))),sIDX)
%     title(['Subject ' int2str(sList2(sIDX))])
%     hold on
%     plot(allMeansnorm(:,sIDX),'b')
% end

% Fear-potentiated startle response

% allMeansCSp = squeeze(mean(emgCSp(:,:,:),2));
% allMeansCSp(:,sum(allMeansCSp)==0) = [];
% allMeansCSm = squeeze(mean(emgCSm(:,:,:),2));
% allMeansCSm(:,sum(allMeansCSm)==0) = [];
% 
% figure
% title('Fear-potentiated startle')
% for sIDX = 1:size(allMeansCSp,2)
%     subplot(ceil(sqrt(size(allMeansCSp,2))),ceil(sqrt(size(allMeansCSp,2))),sIDX)
%     title(['Subject ' int2str(sList2(sIDX))])
%     hold on
%     plot(allMeansCSp(:,sIDX),'r')
%     plot(allMeansCSm(:,sIDX),'b')
% end
% legend('CS+','CS-','Location','northeast'); legend boxoff