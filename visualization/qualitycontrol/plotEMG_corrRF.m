%function plotEMG_corrRF(ExpPath,sList)
clear variables

ExpPath = 'I:\TMS_Experiment\SCR_EMG_data_scripts\';
bf = pspm_bf_sebrf(1/1000);
load(fullfile(ExpPath,'EMGdata_57subs_normalized_baselinecorrected.mat'));

%bfmat = repmat(bf,1,size(allMeansnorm,2));

figure
hold on

for sub = 1:size(allMeans,2)

    r(sub) = corr2(allMeans(:,sub),bf(1:131));
    plot(allMeans(:,sub))

end

plot(bf(1:131),'-k','LineWidth',3)
%plot(mean(allMeans,2),'-r','LineWidth',3)

figure
hist(r)

%end