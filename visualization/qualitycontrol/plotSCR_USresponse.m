function plotSCR_USresponse(opts,sessions)

sesList = opts.scr.sessionlist;
s = 1;

for session = sessions

    sList = opts.scr.sList.sessions{session};
    
    for sIDX = 1:numel(sList)
        
        % File names
        if sList(sIDX) < 100
            fname = fullfile(opts.scr.path.trim,['tpspm_ss7b_0' num2str(sList(sIDX)) '_physio_' sesList{session} '_SCR.mat']);
            fnamebhv  = fullfile(opts.bhv.path,['ss7b_0' num2str(sList(sIDX)) '_behav_' sesList{session} '.mat']);
        else
            fname = fullfile(opts.scr.path.trim,['tpspm_ss7b_' num2str(sList(sIDX)) '_physio_' sesList{session} '_SCR.mat']);
            fnamebhv  = fullfile(opts.bhv.path,['ss7b_' num2str(sList(sIDX)) '_behav_' sesList{session} '.mat']);
        end
        
        if exist(fname,'file') && exist(fnamebhv,'file')
            
            clear data
            [~,~,data] = pspm_load_data(fname);
            
            sr = data{opts.scr.channel.scr}.header.sr;
            y_edited  = data{opts.scr.channel.scr}.data;
            
            % Bandpass & downsampling (for plotting purposes only!)
            filt.sr = sr;
            filt.lpfreq = 5;
            filt.lporder = 1;
            filt.hpfreq = 0.0159;
            filt.hporder = 1;
            filt.direction = 'bi';
            filt.down = 10;
            
            [~,y_filtered,newsr] = pspm_prepdata(y_edited,filt);
            
            % Collect SCR
            %------------------------------------------------------------------
            markers = data{opts.scr.channel.marker}.data;
            
            markersfound(sIDX,1) = sList(sIDX);
            markersfound(sIDX,2) = numel(markers); % should be 96
            
            if numel(markers) ~= 96
                fprintf('\nWrong number of markers! Subject %03.0f ... ', sList(sIDX));
            end
            
            for trial = 1:numel(markers)
                t0 = round(markers(trial)*newsr);
                if t0 == 0; t0 = t0+1; end
                t1 = t0+120; %12000 for original sr
                tbs = t0-5;
                baseline = nanmean(y_filtered(tbs:t0));
                scr(:,trial,sIDX) = y_filtered(t0:t1)-baseline;
                scrmaxampl(trial,sIDX) = max(y_filtered(t0:t1));
            end
            
            % Separate for CS+ (US+ and US-) and CS-
            %------------------------------------------------------------------
            % Load behavioural file with event IDs
            bhvdata = load(fnamebhv);
            
            %CSpUSp = bhvdata.Data(:,3) == 1 & bhvdata.Data(:,4) == 1;
            %CSpUSm = bhvdata.Data(:,3) == 1 & bhvdata.Data(:,4) == 0;
            %CSm = bhvdata.Data(:,3) == 2;
            US = bhvdata.Data(:,4) == 1;
            USm = bhvdata.Data(:,4) == 0;
            
            %scrCSpUSp(:,:,sIDX) = scr(:,CSpUSp,sIDX);
            %scrCSpUSm(:,:,sIDX) = scr(:,CSpUSm,sIDX);
            %scrCSm(:,:,sIDX) = scr(:,CSm,sIDX);
            
            scrUS(:,:,sIDX) = scr(:,US,sIDX);
            scrUSm(:,:,sIDX) = scr(:,USm,sIDX);
            scrmaxamplUS(:,sIDX) = scrmaxampl(US,sIDX);
            
            % Keeping a list of the real subject numbers
            sList2(s) = sList(sIDX);
            s = s + 1;
            
        end
    end
    
    % Summarize and plot
    %--------------------------------------------------------------------------
    
    plotsesnames = {'Acquisition','Retention','Retest'};
    % SCR to US per subject normalized to the peak amplitude
    allMeansUS = squeeze(nanmean(scrUS(:,:,:),2));
    allMeansUS(:,sum(allMeansUS)==0) = [];
    %maxscr = max(allMeansUS);
    %scrnorm=bsxfun(@rdivide,allMeansUS,maxscr);
    
    allMeansUSm = squeeze(nanmean(scrUSm(:,:,:),2));
    allMeansUSm(:,sum(allMeansUSm)==0) = [];
    %maxscrUSm = max(allMeansUSm);
    %scrnormUSm=bsxfun(@rdivide,allMeansUSm,maxscrUSm);
    
    figure
    suptitle(['SCR to US+ vs. US-   ' plotsesnames{session}])
    for sIDX = 1:size(allMeansUS,2)
        subplot(ceil(sqrt(size(allMeansUS,2))),ceil(sqrt(size(allMeansUS,2))),sIDX)
        title(['Subject ' int2str(sList2(sIDX))])
        hold on
        plot(allMeansUS(:,sIDX),'r')
        plot(allMeansUSm(:,sIDX),'b')
    end
    legend('US+','US-')
    
end

end