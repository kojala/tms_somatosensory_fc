clear all

opts = get_psychophys_options();

for ses = 2%sessions
    
    sList = opts.psr.sList.sessions{ses};%opts.psr.sList.GLM{ses};
%     sList = opts.scr.sList.orig;
    
    for s = 1:numel(sList)
        
        s_id = num2str(sList(s)); 
        if sList(s) < 100
%             datafile  = fullfile(opts.scr.path.artcorr,['atpspm_ss7b_0' num2str(s_id) '_physio_' opts.psr.sessionlist{ses} '_SCR.mat']);
            datafile  = fullfile(opts.psr.path.valfix,['valfix_tpspm_ss7b_0' num2str(s_id) '_eyelink_' opts.psr.sessionlist{ses} '.mat']); % Trimmed
        else
%             datafile  = fullfile(opts.scr.path.artcorr,['atpspm_ss7b_' num2str(s_id) '_physio_' opts.psr.sessionlist{ses} '_SCR.mat']);
            datafile  = fullfile(opts.psr.path.valfix,['valfix_tpspm_ss7b_' num2str(s_id) '_eyelink_' opts.psr.sessionlist{ses} '.mat']); % Trimmed
        end
        
        if exist(datafile,'file')
            [~, ~, data] = pspm_load_data(datafile);
            
%             channel = opts.scr.channel.scr;
            channel = opts.psr.channel.valfix;
            figure
            hold on
            plot(data{channel}.data);
            title(['Subject ' s_id])
            
        end
    end
end