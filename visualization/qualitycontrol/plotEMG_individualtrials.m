function plotEMG_individualtrials(opts)

% visual inspection of EMG signal for individual trials. This is
% meant as an overall quality assessment without detailed analyses.
%__________________________________________________________________________

sList = opts.sebr.sList;
    
for sIDX = 1:numel(sList)
    
    if sList(sIDX) < 100
        fnamesebr = fullfile(opts.sebr.path.trim,['tpspm_ss7b_0' num2str(sList(sIDX)) '_physio_retention_SEBR.mat']);
    else
        fnamesebr = fullfile(opts.sebr.path.trim,['tpspm_ss7b_' num2str(sList(sIDX)) '_physio_retention_SEBR.mat']);
    end
    
    if exist(fnamesebr,'file')
        
        % Load preprocessed EMG data
        %------------------------------------------------------------------
        clear data
        [~,~,data] = pspm_load_data(fnamesebr);
        
        sr = data{opts.sebr.channel.emg}.header.sr;  % Sampling rate
        emg_pp  = data{opts.sebr.channel.preproc}.data;  % Preprocessed data
        sound   = data{opts.sebr.channel.sound}.data;  % Sound channel
        
        figure
        plot(emg_pp,'b')
        hold on
        plot(sound/1000,'r')
        title(['Subject ' num2str(sList(sIDX))])
 
        % Collect EMG responses
        %------------------------------------------------------------------
        triggers = data{opts.sebr.channel.marker}.data; % check the correct no. of markers
        %triggers(1) = []; % first startle sound is not part of the experiment
        
        % Plot individual trials
        figure
        suptitle(['Subject ' int2str(sList(sIDX))])
        for trial = 1:numel(triggers)
            t0 = round(triggers(trial)*sr);
            t1 = t0+300;
            %emg(:,trial,sIDX) = emg_pp(t0:t1);
            subplot(ceil(sqrt(numel(triggers))),ceil(sqrt(numel(triggers))),trial)
            title(['Trial ' int2str(trial)])
            hold on
            plot(emg_pp(t0:t1),'k')
        end
        
        % Keeping a list of the real subject numbers
%         sList2(s) = sList(sIDX);
%         s = s + 1;
        
    end
end

end