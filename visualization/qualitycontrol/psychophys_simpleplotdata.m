clear all

opts = get_psychophys_options();
ExpPath = fullfile(opts.expPath,'data\labchart\artefact_corrected');
% ExpPath = 'D:\TMS_Experiment\Psychophysiology\data\eyelink\trimmed';
sList = 43;%21:139;

% sessions = 1:2;
sessnames = {'acquisition','retention','retest'}; 
modality = 'SCR';

for ses = 1%sessions
    
    for s = 1:numel(sList)
        
        s_id = num2str(sList(s));
        
        if sList(s) < 100
%             filename = ['atpspm_ss7b_0' s_id '_physio_' sessnames{ses} '_' modality '_sn07.mat'];
            filename = ['atpspm_ss7b_0' s_id '_physio_' sessnames{ses} '_' modality '.mat'];
%             filename = ['tpspm_ss7b_0' s_id '_eyelink_' sessnames{ses} '.mat'];
        else
            filename = ['atpspm_ss7b_' s_id '_physio_' sessnames{ses} '_' modality '.mat'];
%             filename = ['tpspm_ss7b_' s_id '_eyelink_' sessnames{ses} '.mat'];
        end
        
        physfile = fullfile(ExpPath,filename);
        
        if exist(physfile,'file')
            
            [~, ~, data] = pspm_load_data(physfile);
            
            figure
            plot(data{1}.data,'k');
            hold on
            sr = data{1}.header.sr;
            markers = data{3}.data;
            %newmarkers = nan(length(data{1}.data),1);%resample(markers,1000,1);
            for marker = 1:numel(markers)
                %newmarkers(ceil(markers(marker)*sr)) = markers(marker);
                newmarker = markers(marker)*sr;
                %line([newmarker newmarker],[0 max(data{1}.data)],'Color','red');
            end
            %newmarkers(1:1000:end) = markers;
            
            title(['Subject ' num2str(sList(s))])
            
        end
    end
end