function plotPSR_USresponse(opts,sessions)

for ses = sessions
    
    for sIDX = 1:numel(sList)
        
        % File names
        s_id = sList(sIDX);
        if sList(sIDX) < 100
            fixed_file  = fullfile(opts.psr.path.valfix,['ivalfix_tpspm_ss7b_0' num2str(s_id) '_eyelink_' opts.psr.sessionlist{ses} '.mat']); % Valid fixations
            eventfile = fullfile(opts.bhv.path,['ss7b_0' num2str(s_id) '_behav_' opts.psr.sessionlist{ses} '.mat']);
        else
            fixed_file  = fullfile(opts.psr.path.valfix,['ivalfix_tpspm_ss7b_' num2str(s_id) '_eyelink_' opts.psr.sessionlist{ses} '.mat']); % Valid fixations
            eventfile = fullfile(opts.bhv.path,['ss7b_' num2str(s_id) '_behav_' opts.psr.sessionlist{ses} '.mat']);
        end
        
        if exist(fixed_file,'file')
            
            clear data
            [~,~,data] = pspm_load_data(fname);
            
            sr = data{1,1}.header.sr;
            y  = data{1,1}.data;
            
            % Bandpass & downsampling
            %         filt.sr = sr;
            %         filt.lpfreq = 'none';
            %         filt.hpfreq = 'none';
            %         filt.direction = 'bi';
            %         filt.down = 10;
            %
            %         [~,y_filtered,newsr] = pspm_prepdata(y,filt);
            
            y_filtered = y;
            newsr = sr;
            
            figure,plot(y,'b'); title(['Subject ' num2str(s_id)])
            
            % Collect pupil responses
            %------------------------------------------------------------------
            %         markers = data{5,1}.data;
            %         markers(3:2:end) = [];
            %         markers(1) = [];
            %
            %         markersfound(sIDX,1) = sList(sIDX);
            %         markersfound(sIDX,2) = numel(markers); % should be 96
            %
            %         for trial = 1:numel(markers)
            %             t0 = round(markers(trial)*newsr);
            %             if t0 == 0; t0 = t0+1; end
            %             t1 = t0+6000; %12000 for original sr
            %
            %             psr(:,trial,sIDX) = y_filtered(t0:t1);
            %             psrmaxampl(trial,sIDX) = max(y_filtered(t0:t1));
            %         end
            %
            %         % Separate for CS+ (US+ and US-) and CS-
            %         %------------------------------------------------------------------
            %         % Load behavioural file with event IDs
            %         load(fnamebhv)
            %
            %         CSpUSp = Data(:,3) == 1 & Data(:,4) == 1;
            %         CSpUSm = Data(:,3) == 1 & Data(:,4) == 0;
            %         CSm = Data(:,3) == 2;
            %         US = Data(:,4) == 1;
            %         psrCSpUSp(:,:,sIDX) = psr(:,CSpUSp,sIDX);
            %         psrCSpUSm(:,:,sIDX) = psr(:,CSpUSm,sIDX);
            %         psrCSm(:,:,sIDX) = psr(:,CSm,sIDX);
            %
            %         psrUS(:,:,sIDX) = psr(:,US,sIDX);
            %         psrmaxamplUS(:,sIDX) = psrmaxampl(US,sIDX);
            %
            %         % Keeping a list of the real subject numbers
            %         sList2(s) = sList(sIDX);
            %         s = s + 1;
            
        end
    end
    
    % Summarize and plot
    %--------------------------------------------------------------------------
    
    % PSCR to US per subject normalized to the peak amplitude
    % allMeansUS = squeeze(mean(psrUS(:,:,:),2));
    % allMeansUS(:,sum(allMeansUS)==0) = [];
    %
    % maxpsr = max(allMeansUS);
    %
    % psrnorm=bsxfun(@rdivide,allMeansUS,maxpsr);
    %
    % figure
    % title('PSR normalized to the peak amplitude')
    % for sIDX = 1:size(psrnorm,2)
    %     subplot(ceil(sqrt(size(psrnorm,2))),ceil(sqrt(size(psrnorm,2))),sIDX)
    %     title(['Subject ' int2str(sList2(sIDX))])
    %     hold on
    %     plot(psrnorm(:,sIDX),'b')
    % end
    
end

end