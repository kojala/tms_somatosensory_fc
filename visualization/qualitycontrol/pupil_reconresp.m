function pupil_reconresp(opts,sessions)

for ses = sessions
    
    for group = 1:2
        
        if group == 1
            sList = opts.psr.sList.GLM_control{ses};
        else
            sList = opts.psr.sList.GLM_experimental{ses};
        end
        
        groupname = opts.psr.groupnames{group};
        sesname = opts.psr.sessionlist{ses};
        
        glmpath = fullfile(opts.psr.path.glm_cond,opts.psr.glm_bfname{opts.psr.estimated_response},sesname);
        
        no_conditions = opts.psr.estimated_conditions*2;
        unit = 'Pupil size (mm)';
        
        % Condition names
        condnames = opts.psr.glm.condition_names;
        
        %% Retrieve GLM stats
        recresp = nan(length(sList),no_conditions);
        
        for s = 1:numel(sList)
            
            s_id = num2str(sList(s));
            
            cond = 1;
            
            for stim = 1:2
                
                if length(opts.psr.trials2take) ~= opts.psr.trials
                    filename = [opts.psr.glm_filename.cond s_id '_' opts.psr.glm.stimname{stim} '_' num2str(length(opts.psr.trials2take)) 'trials.mat'];
                else
                    filename = [opts.psr.glm_filename.cond s_id '_' opts.psr.glm.stimname{stim} '.mat'];
                end
                datafile = fullfile(glmpath,filename);
                
                if exist(datafile,'file')
                    
                    [~, glm] = pspm_glm_recon(datafile);
                    
                    recresp(s,cond:cond+2) = glm.recon(1:3);
                    
                    cond = cond + 3;
                    
                end
                
            end
            
        end
        
        %% Calculate within-subject error bars
        
        subavg = nanmean(recresp,2); % mean over conditions for each sub
        grandavg = nanmean(subavg); % mean over subjects and conditions
        
        newvalues = nan(size(recresp));
        
        % normalization of subject values
        for cond = 1:size(recresp,2)
            meanremoved = recresp(:,cond)-subavg; % remove mean of conditions from each condition value for each sub
            newvalues(:,cond) = meanremoved+repmat(grandavg,[length(sList) 1 1]); % add grand average over subjects to the values where individual sub average was removed
            plotdata_mean(:,cond) = nanmean(newvalues(:,cond));
            %plotdata_median(:,cond) = nanmedian(recresp(:,cond));
        end
        
        tvalue = tinv(1-0.05, length(sList)-1);
        newvar = (cond/(cond-1))*nanvar(newvalues);
        errorbars = squeeze(tvalue*(sqrt(newvar)./sqrt(length(sList)))); % calculate error bars according to Cousineau (2005) with Morey (2005) fix
        
        %% Plot
        
        clr = [255 153 153; 59 100 173; 143 162 212; 255 153 153; 59 100 173; 143 162 212]./255; % CS+US+ CS+US- CS-US- x 2
        
        figure('Position',[300,300,800,400]);
        hold on
        % Bar graph on means
        for b = 1:length(plotdata_mean)
            h = bar(b,plotdata_mean(:,b));
            h.FaceColor = clr(b,:);
        end
        % Individual values
        subjects = size(recresp,1);
        xdata = repmat(1:b,[subjects 1]);
        scatter(xdata(:),recresp(:), 'filled', 'MarkerFaceColor', [102 178 255]./255, 'MarkerEdgeColor', 'k', 'jitter', 'on', 'jitterAmount', 0.2);
        % Error bars
        errorbar(plotdata_mean,errorbars,'k','LineStyle','none','LineWidth',2.5,'CapSize',0)
        title(['PSR mean CR, ' groupname ' group, N = ' num2str(sum(~isnan(recresp(:,1))))])
        set(gca,'xTick',1:no_conditions)
        set(gca,'xTickLabel', condnames)
        %ylim([0 0.8])
        ylabel(unit)
        if length(opts.psr.trials2take) ~= opts.psr.trials
            savefig(fullfile(glmpath,['PSR_CR_mean_' sesname '_' groupname '_simple-complex_' num2str(length(opts.psr.trials2take)) 'trials.fig']))
        else
            savefig(fullfile(glmpath,['PSR_CR_mean_' sesname '_' groupname '_simple-complex.fig']))
        end
        
    end
    
end

end