function plot_trialsequence(opts,sessions)
% Plot the sequences of trial types to inspect variability across subjects

for ses = sessions
    
    sList = opts.sebr.sList.orig;
    sesname = opts.sessionlist{ses};
    
    for sIDX = 1:numel(sList)
        
        if sList(sIDX) < 100
            fnamebhv = fullfile(opts.bhv.path,['ss7b_0' num2str(sList(sIDX)) '_behav_' sesname '.mat']);
        else
            fnamebhv = fullfile(opts.bhv.path,['ss7b_' num2str(sList(sIDX)) '_behav_' sesname '.mat']);
        end
        
        if exist(fnamebhv,'file')
            
            % Collect trial sequence
            %------------------------------------------------------------------
            clear data
            data = load(fnamebhv);
            data = data.Data;
            
            %CStype = NaN(1,24);
            CStype_simple(sIDX,:) = data(data(:,6)==0,3);
            CStype_complex(sIDX,:) = data(data(:,6)==1,3);
%             CStype(sub,data(:,3) == 1 & data(:,6) == 0) = 1; % CS+ simple
%             CStype(sub,data(:,3) == 2 & data(:,6) == 0) = 2; % CS- simple
%             CStype(sub,data(:,3) == 1 & data(:,6) == 1) = 3; % CS+ complex
%             CStype(sub,data(:,3) == 2 & data(:,6) == 1) = 4; % CS- complex
            
        end
        
    end
    
    % Plot CS+/CS- trial sequences
    %------------------------------------------------------------------
    im = figure;
    imagesc(CStype_simple);
    colormap(im,gray)
    title(['Sequence ' sesname ' simple trials, black = CS+, white = CS-'])
    ylabel('Subject')
    xlabel('Trial')
    %xlim([1 opts.sessiontrials(ses)/2])
    
    im = figure;
    imagesc(CStype_complex);
    colormap(im,gray)
    title(['Sequence ' sesname ' complex trials, black = CS+, white = CS-'])
    ylabel('Subject')
    xlabel('Trial')
    %xlim([1 opts.sessiontrials(ses)/2])
    
end

end