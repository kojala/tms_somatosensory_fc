function pupil_GLM_trial_save(opts,sessions)

for ses = sessions
    
    sesname = opts.psr.sessionlist{ses};
    glmpath = fullfile(opts.psr.path.glm_trial,opts.psr.glm_bfname{opts.psr.estimated_response},sesname);
    trials2take = 1:opts.psr.trials2take(ses);
    
    for group = 1:2
        
        clear glm_est_trial trial_data_out
        
        if group == 1
            sList = opts.psr.sList.GLM_control{ses};
            groupname = 'control';
        else
            sList = opts.psr.sList.GLM_experimental{ses};
            groupname = 'experimental';
        end
        
        bhvPath = opts.bhv.path;
        
        for sIDX = 1:numel(sList)
            
            s_id = num2str(sList(sIDX));
            filename = [opts.psr.glm_filename.trial s_id '.mat'];
            datafile = fullfile(glmpath,filename);
            
            %% Retrieve GLM data
            
            clear recresp
            
            if exist(datafile,'file')
                [~, glm] = pspm_glm_recon(datafile);
                recresp = glm.recon;
            end
            
            trials = 1:length(trials2take);
            recresp = recresp(trials2take);
            
            if sList(sIDX) < 100
                eventfile = fullfile(bhvPath,['ss7b_0' s_id '_behav_' sesname '.mat']);
            else
                eventfile = fullfile(bhvPath,['ss7b_' s_id '_behav_' sesname '.mat']);
            end
            
            bhvdata = load(eventfile);
            bhvdata = bhvdata.Data;
            
            CSPrediction = bhvdata(trials2take,3);
            CSComplexity = bhvdata(trials2take,6);
            US = bhvdata(trials2take,4);
            condition_type = nan(length(US),1);
            
            condition_type(CSPrediction == 1 & CSComplexity == 0 & US == 1) = 1; % Simple CS+US+
            condition_type(CSPrediction == 1 & CSComplexity == 0 & US == 0) = 2; % Simple CS+US-
            condition_type(CSPrediction == 2 & CSComplexity == 0 & US == 0) = 3; % Simple CS-
            
            condition_type(CSPrediction == 1 & CSComplexity == 1 & US == 1) = 4; % Complex CS+
            condition_type(CSPrediction == 1 & CSComplexity == 1 & US == 0) = 5; % Complex CS+
            condition_type(CSPrediction == 2 & CSComplexity == 1 & US == 0) = 6; % Complex CS-
            
            % Trial-by-trial data
            for cond = 1:6
                glm_est_trial{cond}(sIDX,:) = recresp(condition_type==cond);
            end
            
            trial_data_out.subject(sIDX,:) = repmat(sList(sIDX),[length(trials) 1]);
            trial_data_out.group(sIDX,:) = repmat(group,[length(trials) 1]);
            trial_data_out.trial(sIDX,:) = trials;
            trial_data_out.cs(sIDX,:) = recresp;
            trial_data_out.cond(sIDX,:) = condition_type;
            
        end
        
        % Data for MATLAB format saving
        glm_data_trial = glm_est_trial;
        
        %% Save data
        if length(trials2take) == opts.psr.trials(ses)
            savefile_trials = '';
        else
            savefile_trials = ['_' num2str(length(trials2take)) 'trials'];
        end
        
        save(fullfile(glmpath,['GLM_trial_summary_' groupname '_' sesname savefile_trials '.mat']),'glm_data_trial');
        
        % For use in R (long format)
        Subject = trial_data_out.subject';
        Subject = Subject(:);
        Group = trial_data_out.group';
        Group = Group(:);
        data_out_cs = trial_data_out.cs';
        PSR = data_out_cs(:);
        Trial = trial_data_out.trial';
        Trial = Trial(:);
        
        cond = trial_data_out.cond';
        cond = cond(:);
        Condition = cond;
        
        datatable = table(Subject,Group,Trial,Condition,PSR);
        
        summarytablefile2 = fullfile(glmpath,['GLM_trial_summarytable_' groupname '_' sesname savefile_trials '.csv']);
        writetable(datatable,summarytablefile2);
        save(fullfile(glmpath,['GLM_trial_summarytable_' groupname '_' sesname '.mat']),'datatable');
        
    end
    
end

end