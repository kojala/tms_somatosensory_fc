function SCR_DCM_save(opts,sessions)

for ses = sessions
    
    for group = 1:2
        
        clear subs groups
        
        if group == 1
            sList = opts.scr.sList.DCM_control{ses};
            groupname = 'control';
        else
            sList = opts.scr.sList.DCM_experimental{ses};
            groupname = 'experimental';
        end
        
        sesname = opts.scr.sessionlist{ses};
        
        dcmPath = fullfile(opts.scr.path.dcm, sesname);
        bhvPath = opts.bhv.path;
        
        trials2take = opts.scr.trials2take;
        
        %% Retrieve DCM stats
        
        % CSs+US+, CSs+US-, CSs-US-, CSc+US+, CSc+US-, CSc-US-
        trial_divider = [8 8 4 8 8 4]; % total number of trials divided by this number to arrive at condition trial number
        % e.g. 96 trials -> 96/8 = 12 CS+ simple US+ trials
        
        clear dcm_est dcm_est_trial condition_type missing trial_data_out
        
        for cond = 1:6
            dcm_est(cond).cs = nan(length(sList),1);
            dcm_est_trial.cs{cond} = nan(length(sList),opts.scr.dcm_trials(ses)/trial_divider(cond));
            dcm_est(cond).us = nan(length(sList),1);
            dcm_est_trial.us{cond} = nan(length(sList),opts.scr.dcm_trials(ses)/trial_divider(cond));
        end
        
        for sIDX = 1:numel(sList)
            
            s_id = num2str(sList(sIDX));
            
            if ismember(sList(sIDX),opts.scr.sList.DCM_control{1})
                subgroup = 1;
            elseif ismember(sList(sIDX),opts.scr.sList.DCM_experimental{ses})
                subgroup = 2;
            end
            
            subs(sIDX) = sList(sIDX);
            groups(sIDX) = subgroup;
                
            filename = ['dcm_' s_id '.mat'];
            dcmfile = fullfile(dcmPath,filename);
            
            if exist(dcmfile,'file')
                
                dcmdata = load(dcmfile);
                
                trials = 1:length(trials2take);
                
                dcm_est_cs = dcmdata.dcm.stats(trials2take,1);
                dcm_est_us = dcmdata.dcm.stats(trials2take,2);
                
                % Correct scaling
                dcm_est_cs = dcm_est_cs.*opts.scr.datamultiplier;
                dcm_est_us = dcm_est_us.*opts.scr.datamultiplier;
                
                %             figure;plot(dcm_est_cs,'-ok','MarkerSize',6,'MarkerFaceColor','b','MarkerEdgeColor','k','LineWidth',1.5)
                %             xlabel('Trials')
                %             xlim([0 96])
                %             ylabel('SCR DCM estimate')
                %             title(['ACQUISITION, Subject ' num2str(sList(sIDX))])
                
                if sList(sIDX) < 100
                    eventfile = fullfile(bhvPath,['ss7b_0' s_id '_behav_' sesname '.mat']);
                else
                    eventfile = fullfile(bhvPath,['ss7b_' s_id '_behav_' sesname '.mat']);
                end
                
                bhvdata = load(eventfile);
                bhvdata = bhvdata.Data;
                
                CSPrediction = bhvdata(trials2take,3);
                CSComplexity = bhvdata(trials2take,6);
                US = bhvdata(trials2take,4);
                condition_type = nan(length(US),1);
                
                condition_type(CSPrediction == 1 & CSComplexity == 0 & US == 1) = 1; % Simple CS+US+
                condition_type(CSPrediction == 1 & CSComplexity == 0 & US == 0) = 2; % Simple CS+US-
                condition_type(CSPrediction == 2 & CSComplexity == 0 & US == 0) = 3; % Simple CS-
                
                condition_type(CSPrediction == 1 & CSComplexity == 1 & US == 1) = 4; % Complex CS+
                condition_type(CSPrediction == 1 & CSComplexity == 1 & US == 0) = 5; % Complex CS+
                condition_type(CSPrediction == 2 & CSComplexity == 1 & US == 0) = 6; % Complex CS-
                
                % Standardization by individual CS- response
                if opts.scr.standardize == 1
                    
                    CSm_avg = nanmean(dcm_est_cs(condition_type==3|condition_type==6));
                    dcm_est_cs = dcm_est_cs/CSm_avg;
                    
                end
                
                % Trial-by-trial data
                for cond = 1:6
                    dcm_est_trial.cs{cond}(sIDX,:) = dcm_est_cs(condition_type==cond);
                    dcm_est_trial.us{cond}(sIDX,:) = dcm_est_us(condition_type==cond);
                end
                
                trial_data_out.subject(sIDX,:) = repmat(sList(sIDX),[length(trials) 1]);
                trial_data_out.group(sIDX,:) = repmat(group,[length(trials) 1]);
                trial_data_out.trial(sIDX,:) = trials;
                trial_data_out.cs(sIDX,:) = dcm_est_cs;
                trial_data_out.us(sIDX,:) = dcm_est_us;
                trial_data_out.cond(sIDX,:) = condition_type;
                
                % Check for too much missing data and exclude
                
                for cond = 1:6
                    
                    cond_data_cs = dcm_est_cs(condition_type==cond);
                    missing_cs = sum(isnan(cond_data_cs));
                    missing_perc_cs = missing_cs/size(cond_data_cs,1);
                    
                    cond_data_us = dcm_est_us(condition_type==cond);
                    missing_us = sum(isnan(cond_data_us));
                    missing_perc_us = missing_us/size(cond_data_us,1);
                    
                    % Condition-wise average for non-excluded subs
                    if missing_perc_cs > opts.scr.exclude_missingperc
                        dcm_est(cond).cs(sIDX) = NaN;
                    elseif missing_perc_cs <= opts.scr.exclude_missingperc
                        dcm_est(cond).cs(sIDX) = nanmean(cond_data_cs);
                    end
                    
                    if missing_perc_us > opts.scr.exclude_missingperc
                        dcm_est(cond).us(sIDX) = NaN;
                    elseif missing_perc_us <= opts.scr.exclude_missingperc
                        dcm_est(cond).us(sIDX) = nanmean(cond_data_us);
                    end
                    
                    missing{cond}(sIDX,1) = sList(sIDX);
                    missing{cond}(sIDX,2) = missing_perc_cs;
                    missing{cond}(sIDX,3) = missing_perc_us;
                    
                end
                
            end
            
        end
        
        % Data for MATLAB format saving
        dcm_data = dcm_est;
        dcm_data_trial = dcm_est_trial;
        
        %% Save data
        if length(trials2take) == opts.scr.trials
            savefile_trials = '';
        else
            savefile_trials = ['_' num2str(length(trials2take)) 'trials'];
        end
        
        save(fullfile(dcmPath,['DCM_missingvalues_' groupname '_' sesname savefile_trials '.mat']),'missing'); % Save missing data information
        save(fullfile(dcmPath,['DCM_cond_summary_mean_' groupname '_' sesname savefile_trials '.mat']),'dcm_data');
        save(fullfile(dcmPath,['DCM_trial_summary_' groupname '_' sesname savefile_trials '.mat']),'dcm_data_trial','trial_data_out');
        
        % For use in JASP (wide format)
        Subject = trial_data_out.subject(1:length(sList))';
        SCR_CR_Cond1 = dcm_est(1).cs;
        SCR_CR_Cond2 = dcm_est(2).cs;
        SCR_CR_Cond3 = dcm_est(3).cs;
        SCR_CR_Cond4 = dcm_est(4).cs;
        SCR_CR_Cond5 = dcm_est(5).cs;
        SCR_CR_Cond6 = dcm_est(6).cs;
        datatable = table(Subject,SCR_CR_Cond1,SCR_CR_Cond2,SCR_CR_Cond3,SCR_CR_Cond4,SCR_CR_Cond5,SCR_CR_Cond6);
        writetable(datatable,fullfile(dcmPath,['DCM_CR_6cond_summarytable_' groupname '_' sesname savefile_trials '.csv']));
        %     SCR_CR_CS1 = dcm_est_cs1;
        %     SCR_CR_CS2 = mean([dcm_est_cs2,dcm_est_cs3],2);
        %     SCR_CR_CS3 = mean([dcm_est_cs4,dcm_est_cs5],2);
        %     SCR_CR_CS4 = dcm_est_cs6;
        %     datatable = table(Subject,SCR_CR_CS1,SCR_CR_CS2,SCR_CR_CS3,SCR_CR_CS4);
        %     writetable(datatable,fullfile(dcmPath,'DCM_CR_4cond_summarytable.csv'));
        SCR_UR_Cond1 = dcm_est(1).us;
        SCR_UR_Cond2 = dcm_est(2).us;
        SCR_UR_Cond3 = dcm_est(3).us;
        SCR_UR_Cond4 = dcm_est(4).us;
        SCR_UR_Cond5 = dcm_est(5).us;
        SCR_UR_Cond6 = dcm_est(6).us;
        datatable = table(Subject,SCR_UR_Cond1,SCR_UR_Cond2,SCR_UR_Cond3,SCR_UR_Cond4,SCR_UR_Cond5,SCR_UR_Cond6);
        writetable(datatable,fullfile(dcmPath,['DCM_UR_6cond_summarytable_' groupname '_' sesname savefile_trials '.csv']));
        
        % For use in R (long format) - condition-wise
        Subject = [subs subs subs subs]';
        Group = [groups groups groups groups]';
        CSType = [ones(length(subs),1); zeros(length(subs),1); ones(length(subs),1); zeros(length(subs),1);];
        CSComplexity = [zeros(length(subs)*2,1); ones(length(subs)*2,1);];
        SkinConductanceResponse = [dcm_est(2).cs; dcm_est(3).cs; dcm_est(5).cs; dcm_est(6).cs]; % CS+US simple, CS- simple, CS+US- complex, CS- complex
        datatable = table(Subject,Group,CSType,CSComplexity,SkinConductanceResponse);
        
        summarytablefile = fullfile(dcmPath,['DCM_cond_summarytable_' groupname '_' sesname '.csv']);
        writetable(datatable,summarytablefile);
        save(fullfile(dcmPath,['DCM_cond_summarytable_' groupname '_' sesname '.mat']),'datatable');
        
        % For use in R (long format) - trial-wise
        Subject = trial_data_out.subject';
        Subject = Subject(:);
        Group = trial_data_out.group';
        Group = Group(:);
        data_out_cs = trial_data_out.cs';
        SCR_CR = data_out_cs(:);
        data_out_us = trial_data_out.us';
        SCR_UR = data_out_us(:);
        Trial = trial_data_out.trial';
        Trial = Trial(:);
        
        cond = trial_data_out.cond';
        cond = cond(:);
        Condition = cond;
        
        datatable = table(Subject,Group,Trial,Condition,SCR_CR,SCR_UR);
        
        summarytablefile2 = fullfile(dcmPath,['DCM_trial_summarytable_' groupname '_' sesname savefile_trials '.csv']);
        writetable(datatable,summarytablefile2);
        save(fullfile(dcmPath,['DCM_trial_summarytable_' groupname '_' sesname '.mat']),'datatable');
        
    end
    
end

end