function pupil_GLM_cond_save(opts,sessions)

for ses = sessions
    
    clear data_out
    
    sList = opts.psr.sList.GLM{ses};
    sesname = opts.psr.sessionlist{ses};
    glmpath = fullfile(opts.psr.path.glm_cond,opts.psr.glm_bfname{opts.psr.estimated_response},sesname);
    
    %% Retrieve GLM stats
    
    no_conditions = 2; % CS+US-, CS-US-
    
    for sIDX = 1:numel(sList)
        
        clear recresp cs_type cs_complexity
        
        s_id = num2str(sList(sIDX));
        if ismember(sList(sIDX),opts.psr.sList.GLM_control{ses}); group = 1;
        elseif ismember(sList(sIDX),opts.psr.sList.GLM_experimental{ses}); group = 2;
        else; group = 0;
        end
        
        cond = 1;
        
        for stim = 1:2
            if opts.psr.trials2take(ses) ~= opts.psr.trials(ses)
                filename = [opts.psr.glm_filename.cond s_id '_' opts.psr.glm.stimname{stim} '_' num2str(opts.psr.trials2take(ses)) 'trials.mat'];
            else
            filename = [opts.psr.glm_filename.cond s_id '_' opts.psr.glm.stimname{stim} '.mat'];
            end
            datafile = fullfile(glmpath,filename);
            
            if exist(datafile,'file')
                [~, glm] = pspm_glm_recon(datafile);
                recresp(1,cond:cond+1) = glm.recon(end-1:end);
                cs_type(1,cond:cond+1) = 1:2; % 1: CS+, 2: CS-
                cs_complexity(1,cond:cond+1) = stim; % 1: Simple, 2: Complex
                cond = cond + 2;
            end
        end
        
        data_out.subject(sIDX,:) = sList(sIDX);
        data_out.group(sIDX,:) = group;
        data_out.data(sIDX,:) = recresp;
        data_out.cstype(sIDX,:) = cs_type;
        data_out.cscomplexity(sIDX,:) = cs_complexity;
        
    end
    
    % Data for MATLAB format saving
    glm_data = data_out;
    
    %% Save data
    
    % For use in JASP (wide format)
    Subject = data_out.subject;
    Group = data_out.group;
    PSR_Cond1 = data_out.data(:,1);
    PSR_Cond2 = data_out.data(:,2);
    PSR_Cond3 = data_out.data(:,3);
    PSR_Cond4 = data_out.data(:,4);
    PSR_SimpleDiff = PSR_Cond1-PSR_Cond2;
    PSR_ComplexDiff = PSR_Cond3-PSR_Cond4;
    datatable1 = table(Subject,Group,PSR_Cond1,PSR_Cond2,PSR_Cond3,PSR_Cond4,PSR_SimpleDiff,PSR_ComplexDiff);
    
    % For use in R (long format)
    Subject = repmat(data_out.subject,[1 no_conditions*stim])';
    Subject = Subject(:);
    Group = repmat(data_out.group,[1 no_conditions*stim])';
    Group = Group(:);
    PupilSize = data_out.data';
    PupilSize = PupilSize(:);
    CStype = data_out.cstype';
    CStype = CStype(:);
    CScomplexity = data_out.cscomplexity';
    CScomplexity = CScomplexity(:);
    
    datatable = table(Subject,Group,CStype,CScomplexity,PupilSize);
    
    % Save all data
    if opts.psr.trials2take(ses) ~= opts.psr.trials(ses)
        save(fullfile(glmpath,['GLM_cond_summary_mean_' sesname '_' num2str(opts.psr.trials2take(ses)) 'trials.mat']),'glm_data');
        summarytablefile1 = fullfile(glmpath,['GLM_cond_summarytable_wide_' sesname '_' num2str(opts.psr.trials2take(ses)) 'trials.csv']);
        writetable(datatable1,summarytablefile1);
        summarytablefile2 = fullfile(glmpath,['GLM_cond_summarytable_' sesname '_' num2str(opts.psr.trials2take(ses)) 'trials.csv']);
        writetable(datatable,summarytablefile2);
        save(fullfile(glmpath,['GLM_cond_summarytable_' sesname '_' num2str(opts.psr.trials2take(ses)) 'trials.mat']),'datatable');
    else
        save(fullfile(glmpath,['GLM_cond_summary_mean_' sesname '.mat']),'glm_data');
        summarytablefile1 = fullfile(glmpath,['GLM_cond_summarytable_wide_' sesname '.csv']);
        writetable(datatable1,summarytablefile1);
        summarytablefile2 = fullfile(glmpath,['GLM_cond_summarytable_' sesname '.csv']);
        writetable(datatable,summarytablefile2);
        save(fullfile(glmpath,['GLM_cond_summarytable_' sesname '.mat']),'datatable');
    end
    
end

end