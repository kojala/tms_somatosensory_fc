
opts = get_psychophys_options();
ses = 1;
sList = opts.scr.sList.DCM{ses};
dataPath = opts.scr.path.dcm; 

for sIDX = 1:length(sList)
        
        clear data
        
        s_id = num2str(sList(sIDX));
        data = load(fullfile(dataPath,opts.scr.sessionlist{ses},['dcm_removedtrials_sub' s_id '.mat']));
        data = data.removed_trials;
        
        all_removed_trials(sIDX,:) = data;
        
end

sum(all_removed_trials,2)'