function data = get_GLM_data(opts,sList,action)
%%Get GLM data for each subject

summaryfile = fullfile(opts.sebr.path.model,'GLM_trial','GLM_cond_summary'); 
summarytablefile = fullfile(opts.sebr.path.model,'GLM_trial','GLM_cond_summarytable.csv');
summarytablefile2 = fullfile(opts.sebr.path.model,'GLM_trial','GLM_cond_summarytable_JASP.csv');

if (~exist(summaryfile,'file') || ~exist(summarytablefile,'file') || ~exist(summarytablefile2,'file'))
    
    for sIDX = 1:length(sList)
        
        s_id = sList(sIDX);
        
        % Load GLM data
        fname = fullfile(opts.sebr.path.model,'GLM_trial',['GLM_SEBR_trial_' num2str(s_id)]);
        %glmdata = load(fname);
        
        % Load stimulus information
        if s_id < 100
            fnamebhv = fullfile(opts.bhv.path,['ss7b_0' num2str(s_id) '_behav_retention.mat']);
        else
            fnamebhv = fullfile(opts.bhv.path,['ss7b_' num2str(s_id) '_behav_retention.mat']);
        end
        bhvdata = load(fnamebhv);
        stimuli_CS(sIDX,:) = bhvdata.Data(:,3); % CS type (CS+ or CS-)
        stimuli_complex(sIDX,:) = bhvdata.Data(:,6); % stimulus complexity (simple or complex)
        
        % Doesn't make sense to reconstruct response for trialwise data,
        % doesn't work properly
        %     glm.recon = glmdata.glm.recon;
        %     if isempty(glm.recon) % Reconstruct response
        %         [~, glm] = pspm_glm_recon(fname);
        %     end
        %     recresp(sIDX,:) = glm.recon;
        
        if ismember(sList(sIDX),opts.sebr.sList.control)
            subgroup = 1;
        elseif ismember(sList(sIDX),opts.sebr.sList.experim)
            subgroup = 2;
        end
        
        subs(sIDX) = s_id;
        group(sIDX) = subgroup;
        
        
        [~, glm] = pspm_glm_recon(fname);
        
        amplitude(sIDX,:) = glm.recon(1:24);
%         amplitude(sIDX,:) = glm.stats(1:24);
        
        names(sIDX,:) = glm.names(1:24);
        
        %amplitude(sIDX,:) = glmdata.glm.stats(1:24);
        %     latency = stats(25:48);
        
        if opts.sebr.standardize == 1
            CSm_avg = mean(amplitude(sIDX,stimuli_CS(sIDX,:)==2));
            amplitude(sIDX,:) = amplitude(sIDX,:)/CSm_avg;
        end
        
        % take mean over trials of each condition within subject
        StartleAmplitude_CSps(sIDX) = mean(amplitude(sIDX,stimuli_CS(sIDX,:) == 1 & stimuli_complex(sIDX,:) == 0));
        StartleAmplitude_CSms(sIDX) = mean(amplitude(sIDX,stimuli_CS(sIDX,:) == 2 & stimuli_complex(sIDX,:) == 0));
        StartleAmplitude_CSpc(sIDX) = mean(amplitude(sIDX,stimuli_CS(sIDX,:) == 1 & stimuli_complex(sIDX,:) == 1));
        StartleAmplitude_CSmc(sIDX) = mean(amplitude(sIDX,stimuli_CS(sIDX,:) == 2 & stimuli_complex(sIDX,:) == 1));
    
    end
    
    % save data in format suitable for manipulating and plotting in MATLAB
    stimuli_CS(stimuli_CS==2) = 0; % CS- code from 2 to 0 for clarity
    data.subs = subs;
    data.group = group;
    data.names = names;
    data.stimuli_CS = stimuli_CS;
    data.stimuli_complex = stimuli_complex;
    data.amplitude = amplitude;
    data.StartleAmplitude_CSps = StartleAmplitude_CSps;
    data.StartleAmplitude_CSms = StartleAmplitude_CSms;
    data.StartleAmplitude_CSpc = StartleAmplitude_CSpc;
    data.StartleAmplitude_CSmc = StartleAmplitude_CSmc;
    save(summaryfile,'data')
    
    % take the mean over trials of each type
    
    
%     newsize = size(amplitude);
%     newsize(2) = newsize(2)/4;
% %     StartleAmplitude_CSps = mean(reshape(amplitude(stimuli_CS == 1 & stimuli_complex == 0),newsize),2);
%     StartleAmplitude_CSms = mean(reshape(amplitude(stimuli_CS == 0 & stimuli_complex == 0),newsize),2);
%     StartleAmplitude_CSpc = mean(reshape(amplitude(stimuli_CS == 1 & stimuli_complex == 1),newsize),2);
%     StartleAmplitude_CSmc = mean(reshape(amplitude(stimuli_CS == 0 & stimuli_complex == 1),newsize),2);

    % save data in format suitable for importing into R
    % long format of data
    Subjects = [subs subs subs subs]';
    Group = [group group group group]';
    CSType = [ones(length(subs),1); zeros(length(subs),1); ones(length(subs),1); zeros(length(subs),1);];
    CSComplexity = [zeros(length(subs)*2,1); ones(length(subs)*2,1);];
    StartleAmplitude = [StartleAmplitude_CSps StartleAmplitude_CSms StartleAmplitude_CSpc StartleAmplitude_CSmc]';

    datatable.Subjects = Subjects;
    datatable.Group = Group;
    datatable.CSType = CSType;
    datatable.CSComplexity = CSComplexity;
    datatable.StartleAmplitude = StartleAmplitude;
    save([summaryfile 'table'],'datatable')
    datatable = table(Subjects,Group,CSType,CSComplexity,StartleAmplitude);
    
    writetable(datatable,summarytablefile);
    
    % save data for JASP
    % wide format
    Subjects = subs';
    Group = group';

    datatable2 = table(Subjects,Group,StartleAmplitude_CSps',StartleAmplitude_CSms',StartleAmplitude_CSpc',StartleAmplitude_CSmc');
    
    writetable(datatable2,summarytablefile2);
    
elseif strcmp(action,'get')
   
    returndata = load([summaryfile '.mat']);
    data = returndata.data;
    
end
   
end