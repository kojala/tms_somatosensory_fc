function data = get_GLM_data_trial(opts)
%%Get GLM data for each subject

sList = opts.sebr.sList.allsubs;

summaryfile = fullfile(opts.sebr.path.model,'GLM_trial','GLM_trial_summary'); 
summarytablefile = fullfile(opts.sebr.path.model,'GLM_trial','GLM_trial_summarytable.csv');

no_trials = 24;

if ~exist(summaryfile,'file') || ~exist(summarytablefile,'file')
    
    for sIDX = 1:length(sList)
        
        s_id = sList(sIDX);
        
        % Load GLM data
        fname = fullfile(opts.sebr.path.model,'GLM_trial',['GLM_SEBR_trial_' num2str(s_id)]);
        glmdata = load(fname);
        
        % Load stimulus information
        if s_id < 100
            fnamebhv = fullfile(opts.bhv.path,['ss7b_0' num2str(s_id) '_behav_retention.mat']);
        else
            fnamebhv = fullfile(opts.bhv.path,['ss7b_' num2str(s_id) '_behav_retention.mat']);
        end
        bhvdata = load(fnamebhv);
        stimuli_CS(sIDX,:) = bhvdata.Data(:,3); % CS type (CS+ or CS-)
        stimuli_complex(sIDX,:) = bhvdata.Data(:,6); % stimulus complexity (simple or complex)
        
        if ismember(sList(sIDX),opts.sebr.sList.control)
            subgroup = 1;
        elseif ismember(sList(sIDX),opts.sebr.sList.experim)
            subgroup = 2;
        end
        
        subs(sIDX) = s_id;
        group(sIDX) = subgroup;
        names(sIDX,:) = glmdata.glm.names(1:no_trials);
        amplitude(sIDX,:) = glmdata.glm.stats(1:no_trials);
        
        if opts.sebr.standardize == 1
            CSm_avg = mean(amplitude(sIDX,stimuli_CS(sIDX,:)==2));
            amplitude(sIDX,:) = amplitude(sIDX,:)/CSm_avg;
        end
        
    end
    
    % save data in format suitable for manipulating and plotting in MATLAB
    stimuli_CS(stimuli_CS==2) = 0; % CS- code from 2 to 0 for clarity
    data.subs = subs;
    data.group = group;
    data.names = names;
    data.stimuli_CS = stimuli_CS;
    data.stimuli_complex = stimuli_complex;
    data.amplitude = amplitude;
    
    save(summaryfile,'data')
    
    % retain trialwise data values
%     StartleAmplitude_CSps = amplitude(stimuli_CS == 1 & stimuli_complex == 0);
%     StartleAmplitude_CSms = amplitude(stimuli_CS == 0 & stimuli_complex == 0);
%     StartleAmplitude_CSpc = amplitude(stimuli_CS == 1 & stimuli_complex == 1);
%     StartleAmplitude_CSmc = amplitude(stimuli_CS == 0 & stimuli_complex == 1);

    % save data in format suitable for importing into R
    % long format of data
    Subject = repmat(subs,[no_trials 1]);
    Subject = Subject(:);
    Group = repmat(group,[no_trials 1]);
    Group = Group(:);
    Trial = repmat(1:no_trials,[length(subs) 1])';
    Trial = Trial(:);
    stimuli_CS = stimuli_CS';
    CSType = stimuli_CS(:);
    stimuli_complex = stimuli_complex';
    CSComplexity = stimuli_complex(:);
    amplitude = amplitude';
    StartleAmplitude = amplitude(:);
    
    datatable1 = table(Subject,Group,Trial,CSType,CSComplexity,StartleAmplitude);
    writetable(datatable1,summarytablefile);
    
    datatable.Subject = Subject;
    datatable.Group = Group;
    datatable.Trial = Trial;
    datatable.CSType = CSType;
    datatable.CSComplexity = CSComplexity;
    datatable.StartleAmplitude = StartleAmplitude;
    summarytablefile = fullfile(opts.sebr.path.model,'GLM_trial','GLM_trial_summarytable.mat');
    save(summarytablefile,'datatable')
    
else
   
    returndata = load(summaryfile);
    data = returndata.data;
    
end
   
end