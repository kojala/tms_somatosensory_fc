function SCR_GLM_cond(opts,sessions)
%%SCR_GLM_cond First-level GLM per subject

%% GLM for each subject
for ses = sessions
    
    % Output directory
    outdir = fullfile(opts.scr.path.glm_cond,opts.scr.glm_bfname{opts.psr.estimated_response},opts.psr.sessionlist{ses});
    if ~exist(outdir,'dir'); mkdir(outdir); end
    
    sList = opts.scr.sList.GLM{ses};
    
    for sIDX = 1:length(sList)
        
        s_id = num2str(sList(sIDX));
        
        if ismember(sList(sIDX),opts.scr.sList.complex_first)
            stim_order = 1; % simple blocks even, complex blocks odd
        else
            stim_order = 0; % simple blocks odd, complex blocks even
        end
        
        for stim = 1:2 % simple -> complex
            
            %% General settings
            % Find session files with data
            if sList(sIDX) < 100
                fname = ['atpspm_ss7b_0' num2str(s_id) '_physio_' opts.psr.sessionlist{ses} '_SCR'];
                eventfile = fullfile(opts.bhv.path,['ss7b_0' num2str(s_id) '_behav_' opts.psr.sessionlist{ses} '.mat']);
            else
                fname = ['atpspm_ss7b_' num2str(s_id) '_physio_' opts.psr.sessionlist{ses} '_SCR'];
                eventfile = fullfile(opts.bhv.path,['ss7b_' num2str(s_id) '_behav_' opts.psr.sessionlist{ses} '.mat']);
            end
            
            blockfiles = ls(fullfile(opts.scr.path.artcorr,[fname '_sn*.mat']));
            fpath = what(opts.scr.path.artcorr);
            for block = 1:size(blockfiles,1)
                blockfiles_fp(block,:) = [fpath.path '\' blockfiles(block,:)]; % full paths
            end
            
            % Take simple or complex stimuli blocks
            if stim == 2 && stim_order == 1 % complex stimulus loop, complex stimulus first order
                blockfiles_stim = blockfiles_fp(stim_order:2:end,:);
            else
                blockfiles_stim = blockfiles_fp(stim_order+stim:2:end,:);
            end
            
            % Find CS and US timings
            % Define names and onsets
            for blk = 1:size(blockfiles_stim,1)
                
                sn = str2double(blockfiles_stim(blk,end-4));
                breaks = opts.scr.split_sessions_markers{ses};
                if sn <= length(breaks)
                    blockbreak = breaks(sn);
                else
                    blockbreak = breaks(sn-1)+12;
                end
                [~, onsets, conditions] = physio_get_onsets(blockfiles_stim(blk,:),eventfile,blockbreak); % get onsets for trials
                
                CSi = conditions{1};
                US = conditions{2};
                
                CS = nan(length(CSi),1);
                CS(CSi==1 & US == 1) = 1; % CS+US+
                CS(CSi==1 & US == 0) = 2; % CS+US-
                CS(CSi==0 & US == 0) = 3; % CS-US-
        
                nCS = 3;
                cCS = 1;
                
                for iCS = 1:nCS
                    timing{blk}.names{cCS} = opts.scr.glm.condition_names{iCS};
                    timing{blk}.onsets{cCS} = onsets(CS==iCS);
                    cCS = cCS + 1;
                end
                
            end
            
            %% GLM settings
            clear model
            
            model.modelspec = 'scr'; % modality
            model.modelfile = fullfile(outdir,[opts.scr.glm_filename.cond s_id '_' opts.scr.glm.stimname{stim}]);
            model.datafile = cellstr(blockfiles_stim);
            
            model.timing = timing;
            model.timeunits = 'seconds';
            model.channel = opts.scr.channel.scr;
            
%             model.bf.fhandle = 'pspm_bf_scrf_f'; % basis function
%             model.bf.args = [opts.scr.estimated_response 1];
            
%             model.latency = 'fixed';
            
            %options.exclude_missing.segment_length = opts.scr.trialtime;
            %options.exclude_missing.cutoff = 25;
            options.overwrite = 1;
            
            %% Run GLM
            pspm_glm(model, options);
            
        end
        
    end
    
    fprintf('GLM done\n')
    
end

end