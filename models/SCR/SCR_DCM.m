function SCR_DCM(opts,sessions)

pspm_init;

for ses = sessions
    
    sList = opts.scr.sList.DCM{ses};
%     dataPath = opts.scr.path.trim; 
    dataPath = opts.scr.path.artcorr; 
    bhvPath = opts.bhv.path;
    fname_mod = 'atpspm';
    sesname = opts.scr.sessionlist{ses};
    modname = 'SCR';
%     artefactFile = fullfile(opts.expPath,['scr_markedartefacts_' sesname '_compiled.mat']);
%     artefactData = load(artefactFile);
%     artefactData = artefactData.artefact_epochs;
    
    for sIDX = 1:length(sList)
        
        s_id = num2str(sList(sIDX));
        fprintf(['Running DCM for subject ' s_id '... '])
        
        % Find data files
        if sList(sIDX) < 100
            filename   = fullfile(dataPath, [fname_mod '_ss7b_0' s_id '_physio_' sesname '_' modname]);
            eventfile = fullfile(bhvPath,['ss7b_0' s_id '_behav_' sesname '.mat']);
        else
            filename   = fullfile(dataPath, [fname_mod '_ss7b_' s_id '_physio_' sesname '_'  modname]);
            eventfile = fullfile(bhvPath,['ss7b_' s_id '_behav_' sesname '.mat']);
        end
        
%         fulldata = load(filename);
%         allmarkers = fulldata.data{opts.scr.channel.marker}.data;
        
        sessionfiles = ls(fullfile([filename '_sn*.mat']));
        sespath = what(dataPath);
        
        for block = 1:size(sessionfiles,1)
            sessionfiles_fp(block,:) = [sespath.path '\' sessionfiles(block,:)]; % full paths
        end
        
        % Get missing data epochs (artefacts)
%         artefactRow = cell2mat(artefactData(:,1)) == sList(sIDX);
%         artefacts = artefactData{artefactRow,2};
%         artefacts = artefacts./opts.scr.originalsr; % transform into seconds
        
        % Find CS and US (and artefact) timings for each session
        for blk = 1:size(sessionfiles_fp,1)
            
            if blk == size(sessionfiles_fp,1) % last block
                splitind = opts.scr.split_sessions_markers{ses}(blk-1)+12;
            else
                splitind = opts.scr.split_sessions_markers{ses}(blk);
            end
            [~, onsets, ~] = physio_get_onsets(sessionfiles_fp(blk,:),eventfile,splitind); % get onsets for trials
            
%             % Remove too short trials -> Not done because these trials
%             actually not too short/do not contain NaNs during ITI!
%             if sList(sIDX) == 29 && blk == 2
%                 onsets = onsets([1:4 6:12]); % Remove trial 5 of session 2
%             elseif sList(sIDX) == 29 && blk == 7
%                 onsets = onsets([1:8 10:12]); % Remove trial 9 of session 7
%             end
%             onsets = SCR_remove_trials_tooshortITI(opts,sessionfiles_fp(blk,:),onsets);
            
            timing{blk}{1} = onsets;
            %timing{blk}{1}(:, 2) = onsets + opts.soa - 1.5; % model 107 from Filip's model space
            timing{blk}{2} = onsets + opts.soa;
            
%             removed_trials(blk) = 12-numel(onsets);
%             artefacts_block = [];
%             
%             % Find artefact periods for this block and transform timings
%             % into within block timings
%             blkmarkers = allmarkers(opts.scr.blocktrials{blk});
%             artefact_idx = 1;
%             for artefact = 1:size(artefacts,1)
%                 if artefacts(artefact,1) > blkmarkers(1) && artefacts(artefact,2) < blkmarkers(end)+opts.scr.minITI % artefact timing within block (+ minimum ITI after last trial onset)
%                     artefacts_block(artefact_idx,1) = artefacts(artefact,1)-(blkmarkers(1)-opts.scr.trimstart); % artefact timing relative to 5 sec before first trial of the block
%                     artefacts_block(artefact_idx,2) = artefacts(artefact,2)-(blkmarkers(1)-opts.scr.trimstart)+0.006; % for some reason 0.006 s missing from some artefact offsets (better to add a bit than have it too short)
%                     artefact_idx = artefact_idx + 1; % add to index so already added artefact won't be added as empty
%                 elseif artefacts(artefact,1)+opts.scr.trimstart > blkmarkers(1) && artefacts(artefact,2) < blkmarkers(end)+opts.scr.minITI % artefacts that start max. 5 sec before session onset
%                     artefacts_block(artefact_idx,1) = onsets(1); % make artefact start from first trial onset (before first trial shouldn't matter?)
%                     artefact_time_afteronset = (artefacts(artefact,2)-artefacts(artefact,1)) - (blkmarkers(1)-artefacts(artefact,1)); % total artefact time minus time before trial onset
%                     artefacts_block(artefact_idx,2) = onsets(1)+artefact_time_afteronset; % artefact end
%                     artefact_idx = artefact_idx + 1;
%                 end
%             end
%             artefacts_block = sort(artefacts_block,1); % sort artefacts to be in the correct time order (originally in the order of manual artefact marking)
%             missing{blk} = artefacts_block;
            
        end
        
        % File output directory
        outdirname = fullfile(opts.scr.path.dcm, sesname);
        if ~exist(outdirname,'dir'); mkdir(outdirname); end
%         save(fullfile(outdirname,['dcm_removedtrials_sub' s_id '.mat']),'removed_trials');
        
        % DCM options
%         model.missing = missing;
        model.datafile = cellstr(sessionfiles_fp);
        model.modelfile = fullfile(outdirname,['dcm_' s_id]);
        model.timing = timing;
        model.timeunits = 'seconds';
        model.constrained = 1;
        options.esit = 1; % remove trials with too short ITI
%         model.substhresh = 0.5; % 2 s default (splitting into subsessions due to longer NaN periods)
        options.overwrite = 1;
        options.dispwin = 0;
        
        % Run DCM
        if ~exist([model.modelfile '.mat'],'file') && (length(model.datafile) == length(opts.scr.blocks2take))
            pspm_dcm(model,options);
            fprintf('\nModel done.\n')
        else
            fprintf('DCM already exists.\n')
        end
        
    end
    
end

end