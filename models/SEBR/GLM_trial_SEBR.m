function GLM_trial_SEBR(opts)

sList = opts.sebr.sList.orig;

for sIDX = 1:length(sList)
    
    s_id = num2str(sList(sIDX));
    
    % Output directory
    outdir = fullfile(opts.expPath,'models','SEBR','GLM_trial');
    if ~exist(outdir,'dir'); mkdir(outdir); end
    
    % Data file
    if sList(sIDX) < 100
        fnamesebr = fullfile(opts.sebr.path.trim,['tpspm_ss7b_0' num2str(sList(sIDX)) '_physio_retention_SEBR.mat']);
    else
        fnamesebr = fullfile(opts.sebr.path.trim,['tpspm_ss7b_' num2str(sList(sIDX)) '_physio_retention_SEBR.mat']);
    end

    % Load markers
%     [~,~,data] = pspm_load_data(fnamesebr,'events');
%     markers = data{1}.data;
%     markers(1) = []; % 1st startle probe removed
    
    % Find trial timings
    blk_markers = [1:6; 7:12; 13:18; 19:24];
    cCS = 1;
    for blk = 1:4
        %blk_onsets = markers(blk_markers(blk,:));
        for iCS = 1:numel(blk_markers(blk,:))
            timing{1}.timing{blk}.names{iCS} = sprintf('Trial_%03.0f', cCS);
            %timing{1}.timing{blk}.onsets{iCS} = blk_onsets(iCS) - 0.02;
            cCS = cCS + 1;
        end
    end
    
    onsets = [7 13 19]; % first trial of each block
    options = struct('overwrite', 1, 'splitpoints', onsets, ...
        'prefix', -1, 'suffix', 5);
    outnames = pspm_split_sessions(fnamesebr, [], options);
    
    for outf = 1:length(outnames)
        [~,~,data] = pspm_load_data(outnames{outf},'events');
        timing{1}.timing{outf}.onsets = num2cell(data{1}.data' - 0.02); % onsets moved -0.02 s to match Khemka et al. 2017
    end
    
    %% GLM settings
    model.modelfile = fullfile(outdir,['GLM_SEBR_trial_' s_id]);
    model.modelspec = 'sebr'; % modality
    model.channel = opts.sebr.channel.preproc;
    model.datafile = outnames;
    model.timing = timing{1}.timing;
    model.timeunits = 'seconds';
    model.window = 0.15; % onsets moved -0.02 s to match Khemka et al. 2017
    model.latency = 'free';
%     model.latency = 'fixed';
    options.overwrite = 1;
    
    %% Run GLM
    pspm_glm(model, options);
    
end

fprintf('GLM done\n')

end