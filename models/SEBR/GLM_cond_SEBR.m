function GLM_cond_SEBR(opts)

sList = opts.sebr.sList.orig;

for sIDX = 1:length(sList)
    
    s_id = num2str(sList(sIDX));
    
    % Output directory
    outdir = fullfile(opts.expPath,'Models','GLM_cond_SEBR');
    if ~exist(outdir,'dir'); mkdir(outdir); end
    
    % Events
    % timing{1}: names/onsets per condition, in markers per block
    % timing{3}: names/onsets per individual trials, in seconds per block, adjusted
    %            for startle
    
    if sList(sIDX) < 100
        cdata = load(fullfile(opts.bhv.path,['ss7b_0' num2str(sList(sIDX)) '_behav_retention.mat']));
    else
        cdata =  load(fullfile(opts.bhv.path,['ss7b_' num2str(sList(sIDX)) '_behav_retention.mat']));
    end
    
    cs_col = 3;
    us_col = 4;
    
    blks = 1:4;
    nblk = 4;
    blktrials = [1:6; 7:12; 13:18; 19:24];
    nCS = 2;
    
    for iCS = 1:nCS
    % CS that are always reinforced
    if all(cdata.data(cdata.data(:, cs_col) == iCS, us_col) == 1)
        rCS(iCS) = 1;
    % CS that are never reinforced    
    elseif all(cdata.data(cdata.data(:, cs_col) == iCS, us_col) == 0)
        rCS(iCS) = -1;
    end
    % CS with partial reinforcement retain 0 value
    end

    for blk = 1:nblk
        cCS = 1;
        CS = cdata.data(blktrials(blk,:),cs_col);
        US = cdata.data(blktrials(blk,:),us_col);
        for iCS = 1:nCS
            % CS is not always reinforced: model US- trials
            if rCS(iCS) < 1
                timing{1}.timing{blk}.names{cCS} = sprintf('CS%1.0fUS-', iCS);
                timing{1}.timing{blk}.onsets{cCS} = find(CS==iCS & US == 0);
                cCS = cCS + 1;
            end
            % CS is not never reinforced: model US+ trials
            if rCS(iCS) > -1
                timing{1}.timing{blk}.names{cCS} = sprintf('CS%1.0fUS+', iCS);
                timing{1}.timing{blk}.onsets{cCS} = find(CS==iCS & US == 1);
                cCS = cCS + 1;
            end
        end
    end
    
    %% GLM settings
    model.modelfile = fullfile(outdir,['GLM_' s_id]);
    model.modelspec = 'sebr'; % modality
    
    model.timing = timing{3}.timing(soundmarkers == 1);
    model.timeunits = 'seconds';
    %model.window = 0.15;
    %model.latency = 'free';
    model.latency = 'fixed';
    
    model.datafile = outnames(soundmarkers == 1);
    model.channel = opts.sebr.channel.preproc;
    
    options.overwrite = 1;
    
    %% Run GLM
    pspm_glm(model, options);
    
end

fprintf('GLM done\n')

end