function pupil_GLM_trial(opts,sessions)
%%PUPIL_GLM First-level GLM per subject

%% GLM for each subject
for ses = sessions
    
    % Output directory
    outdir = fullfile(opts.psr.path.glm_trial,opts.psr.glm_bfname{opts.psr.estimated_response},opts.sessionlist{ses});
    if ~exist(outdir,'dir'); mkdir(outdir); end
    
    sList = opts.psr.sList.GLM{ses};
    
    for sIDX = 1:length(sList)
        
        s_id = num2str(sList(sIDX));
        
        %% General settings
        % Find session files with data
        if sList(sIDX) < 100
            fname_eye = ['valfix_tpspm_ss7b_0' num2str(s_id) '_eyelink_' opts.psr.sessionlist{ses}];
            eventfile = fullfile(opts.bhv.path,['ss7b_0' num2str(s_id) '_behav_' opts.psr.sessionlist{ses} '.mat']);
        else
            fname_eye = ['valfix_tpspm_ss7b_' num2str(s_id) '_eyelink_' opts.psr.sessionlist{ses}];
            eventfile = fullfile(opts.bhv.path,['ss7b_' num2str(s_id) '_behav_' opts.psr.sessionlist{ses} '.mat']);
        end
        
        blockfiles = ls(fullfile(opts.psr.path.valfix,[fname_eye '_sn*.mat']));
        fpath = what(opts.psr.path.valfix);
        
        cCS = 1;
                    
        for block = 1:size(blockfiles,1)
            
            blockfiles_fp(block,:) = [fpath.path '\' blockfiles(block,:)]; % full paths
            sn = str2double(blockfiles(block,end-4));
            breaks = opts.psr.split_sessions_markers{ses};
            if sn <= length(breaks)
                blockbreak = breaks(sn);
            else
                blockbreak = breaks(sn-1)+opts.psr.trialsperblock(ses);
            end
            [~, onsets, ~] = physio_get_onsets(ses,blockfiles_fp(block,:),eventfile,blockbreak); % get onsets for trials
            
            % Define names and onsets
            for iCS = 1:length(onsets)
                timing{block}.names{iCS} = sprintf('Trial_%03.0f', cCS);
                timing{block}.onsets{iCS} = onsets(iCS);
                cCS = cCS + 1;
            end
            
        end
        
        %% GLM settings
        clear model
        
        model.modelspec = 'ps_fc'; % modality
        model.modelfile = fullfile(outdir,[opts.psr.glm_filename.trial s_id]);
        model.datafile = cellstr(blockfiles_fp);
        
        model.timing = timing;
        model.timeunits = 'seconds';
        model.channel = opts.psr.channel.valfix;
        
        model.bf.fhandle = 'pspm_bf_psrf_fc'; % basis function
        model.bf.args = [opts.psr.estimated_response 1];
        
        model.latency = 'fixed';
        % non-default filter options
        %             % (default: lpfreq 50, down 100, direction 'bi')
        %             filter.lpfreq = 25;
        %             filter.lporder = 1;
        %             filter.hpfreq = nan;
        %             filter.hporder = nan;
        %             filter.direction = 'uni';
        %             filter.down = opts.psr.plot.sr;
        %             model.filter = filter;
        
        options.exclude_missing.segment_length = opts.psr.trialtime;
        options.exclude_missing.cutoff = opts.psr.minpercdata;
        options.overwrite = 1;
        
        %% Run GLM
        pspm_glm(model, options);
        
    end
    
    fprintf('GLM done\n')
    
end

end