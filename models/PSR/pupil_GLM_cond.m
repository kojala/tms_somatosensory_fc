function pupil_GLM_cond(opts,sessions)
%%PUPIL_GLM First-level GLM per subject

%% GLM for each subject
for ses = sessions
    
    % Output directory
    outdir = fullfile(opts.psr.path.glm_cond,opts.psr.glm_bfname{opts.psr.estimated_response},opts.sessionlist{ses});
    if ~exist(outdir,'dir'); mkdir(outdir); end

    sList = opts.psr.sList.GLM{ses};
    
    for sIDX = 1:length(sList)
        
        s_id = num2str(sList(sIDX));
        
        if ismember(sList(sIDX),opts.psr.sList.complex_first)
            stim_order = 1; % simple blocks even, complex blocks odd
        else
            stim_order = 0; % simple blocks odd, complex blocks even
        end
        
        for stim = 1:2 % simple -> complex
            
            clear blockfiles_fp blockfiles_stim timing
            
            %% General settings
            % Find session files with data
            if sList(sIDX) < 100
                fname_eye = ['valfix_tpspm_ss7b_0' num2str(s_id) '_eyelink_' opts.psr.sessionlist{ses}];
                eventfile = fullfile(opts.bhv.path,['ss7b_0' num2str(s_id) '_behav_' opts.psr.sessionlist{ses} '.mat']);
            else
                fname_eye = ['valfix_tpspm_ss7b_' num2str(s_id) '_eyelink_' opts.psr.sessionlist{ses}];
                eventfile = fullfile(opts.bhv.path,['ss7b_' num2str(s_id) '_behav_' opts.psr.sessionlist{ses} '.mat']);
            end
            
            blockfiles = ls(fullfile(opts.psr.path.valfix,[fname_eye '_sn*.mat']));
            fpath = what(opts.psr.path.valfix);
            
            for block = 1:size(blockfiles,1)
                blockfiles_fp(block,:) = [fpath.path '\' blockfiles(block,:)]; % full paths
            end
            
            % Take simple or complex stimuli blocks
            if stim == 2 && stim_order == 1 % complex stimulus loop, complex stimulus first order
                blockfiles_stim = blockfiles_fp(stim_order:2:end,:);
            else
                blockfiles_stim = blockfiles_fp(stim_order+stim:2:end,:);
            end
            
            blockfiles_stim = blockfiles_stim(opts.psr.blocks2take_stim{ses},:);
            % Find CS and US timings
            % Define names and onsets
            
            for blk = 1:size(blockfiles_stim,1)
                
                sn = str2double(blockfiles_stim(blk,end-4));
                breaks = opts.psr.split_sessions_markers{ses};
                if sn <= length(breaks)
                    blockbreak = breaks(sn);
                else
                    blockbreak = breaks(sn-1)+opts.psr.trialsperblock(ses);
                end
                [~, onsets, conditions] = physio_get_onsets(ses,blockfiles_stim(blk,:),eventfile,blockbreak); % get onsets for trials
                
                % Define names and onsets

                CSi = conditions{1};
                US = conditions{2};
                
                CS = nan(length(CSi),1);
                CS(CSi==1 & US == 1) = 1; % CS+US+
                CS(CSi==1 & US == 0) = 2; % CS+US-
                CS(CSi==0 & US == 0) = 3; % CS-US-
        
                if ses == 2
                    nCS = [2 3]; % the CS conditions included
                else
                    nCS = 1:3;
                end
                cCS = 1; % counter
                
                for iCS = nCS
                    timing{blk}.names{cCS} = opts.psr.glm.condition_names{iCS};
                    timing{blk}.onsets{cCS} = onsets(CS==iCS);
                    cCS = cCS + 1;
                end
                
            end
            
            %% GLM settings
            clear model
            
            model.modelspec = 'ps_fc'; % modality
            if opts.psr.trials2take(ses) ~= opts.psr.trials(ses)
                model.modelfile = fullfile(outdir,[opts.psr.glm_filename.cond s_id '_' opts.psr.glm.stimname{stim} '_' num2str(length(opts.psr.trials2take(ses))) 'trials']);
            else
                model.modelfile = fullfile(outdir,[opts.psr.glm_filename.cond s_id '_' opts.psr.glm.stimname{stim}]);
            end
            model.datafile = cellstr(blockfiles_stim);
            
            model.timing = timing;
            model.timeunits = 'seconds';
            model.channel = opts.psr.channel.valfix;
            
            model.bf.fhandle = 'pspm_bf_psrf_fc'; % basis function
            model.bf.args = [opts.psr.estimated_response 1];
            
            model.latency = 'fixed';
            % non-default filter options
%             % (default: lpfreq 50, down 100, direction 'bi')
%             filter.lpfreq = 25;
%             filter.lporder = 1;
%             filter.hpfreq = nan;
%             filter.hporder = nan;
%             filter.direction = 'uni';
%             filter.down = opts.psr.plot.sr;
%             model.filter = filter;
            
            options.exclude_missing.segment_length = opts.psr.trialtime;
            options.exclude_missing.cutoff = opts.psr.minpercdata;
            options.overwrite = 1;
            
            %% Run GLM
            pspm_glm(model, options);
            
        end
        
    end
    
    fprintf('GLM done\n')
    
end

end