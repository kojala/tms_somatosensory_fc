close all;clear;clc

addpath 'D:\spm12\'

sList = dir('D:\TMS_Experiment\fMRI\DataNew');
sList = {sList(:).name}';
sList(1:2) = [];
%startSubj  = 48; % start from this index (check sList to which s# it refers)
startSubj = 2;

for sIDX = startSubj:numel(sList)
    
    cd D:\TMS_Experiment\fMRI\ROIAnalysis
    create_TMS_ROI1(['D:\TMS_Experiment\fMRI\DataNew\' sList{sIDX}])
end

disp('------ BEFORE CONTINUING, SET ORIGIN OF T1 TO AC ------')

% steps:
% - go to D:\data_BLI\Experiment_TMS\ROI\Data\MRI\fearS1tms###\T1_########_#######
% - start SPM fmri -> Display -> load sn_###.nii
% - find rough location of AC and move crosshair there
% - Find the AC precisely. Click on anterior ventricle in sagittal
%   view. Zoom in with second drop down menu (20 x 20 x 20
%   for example). Always scroll through the axial plane to look
%   for the AC, which connects like a bridge across the
%   hemispheres. Click on the centre of the AC in the axial
%   plane. Check all three planes! Should be really precise and
%   centred in all three planes.
% - press "Set Origin"
% - press "Reorient..."

%return

startSubj = 1;

for sIDX = startSubj:numel(sList)
    
    cd D:\TMS_Experiment\fMRI\ROIAnalysis
    create_TMS_ROI2(['D:\TMS_Experiment\fMRI\DataNew\' sList{sIDX}])
end