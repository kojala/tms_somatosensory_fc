function create_TMS_ROI1(SourcePath)

% e.g.
% SourcePath = 'D:\TMS_Experiment\LocalizerRawData\fearS1tms032\';
% create_TMS_ROI(SourcePath)
addpath(fullfile(pwd,'utils'))

% get/set paths
%--------------------------------------------------------------------------
% snip last "\" symbol from input
if strcmp(SourcePath(end),'\'), SourcePath = SourcePath(1:end-1); end
[NewPath,Subj,~] = fileparts(SourcePath);
cd(NewPath); cd ..

ExpPath   = 'D:\temp';
DataPath  = fullfile(ExpPath,'Data');
MRIpath   = fullfile(DataPath,'MRI',Subj);
[~,~]     = mkdir(MRIpath);
ModelPath = fullfile(ExpPath,'Models','GLM_0s_MNI',Subj);
VOIpath   = fullfile(ExpPath,'ROI_TMS',Subj);

% move raw data
%--------------------------------------------------------------------------
copyfile(fullfile(SourcePath,'*.rec'),MRIpath)
copyfile(fullfile(SourcePath,'*.par'),MRIpath)
copyfile(fullfile(SourcePath,'*.mat'),DataPath)


% import and sort data
%--------------------------------------------------------------------------
convert_rec(MRIpath)
pause(20)
sort_MRI_files(MRIpath)

%disp('------ BEFORE CONTINUING, SET ORIGIN OF T1 TO AC ------')
