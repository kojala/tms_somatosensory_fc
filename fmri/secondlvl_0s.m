% create a second level model for all first level contrasts:
% 'd1', 'd2', 'd3', 'd4', 'all fingers', 'left > right', 'right > left';
clear

% launch SPM (add toolbox to path if necessary)
%--------------------------------------------------------------------------
addpath 'D:\spm12\'
spm('defaults','FMRI')

% subjects from all localizer experiments
%--------------------------------------------------------------------------
ExpPath    = 'D:\TMS_Experiment\fMRI\';
%ResultPath = fullfile(ExpPath,'ResultPlots\ExperimenterEffect\');
ResultPath = fullfile(ExpPath,'ResultPlots\');
[s,m]      = mkdir(ResultPath);
ModelPath  = fullfile(ExpPath,'Models','GLM_0s_MNI');
sList      = dir(ModelPath);
sList      = {sList(:).name}';
sList(1:2) = []; % ., ..

%sList = sList(1:75); % Until subject 097
sList = sList(76:end); % From subject 098

% get contrast names and set new model file names
load(fullfile(ModelPath,sList{1},'SPM.mat'))
oldcnames = cellfun(@(x) ['' x(1:end-15)], {SPM.xCon(:).name}', 'Uniformoutput',0);
newcnames = cellfun(@(x) strrep(x,' ',''), oldcnames, 'Uniformoutput',0);
newcnames = cellfun(@(x) strrep(x,'>','-'), newcnames, 'Uniformoutput',0);


%--------------------------------------------------------------------------
% fMRI model specification
%--------------------------------------------------------------------------
for cIDX = 1:numel(newcnames)
    clear matlabbatch
    ModelPathC = fullfile(ExpPath,'Models','GLM_0s_MNI_group_from098',newcnames{cIDX});
    [s,m]      = rmdir(ModelPathC,'s');
    [s,m]      = mkdir(ModelPathC);
    
    fList = cellstr(cellfun(@(x) fullfile(ModelPath,x,sprintf('con_%04i.nii',cIDX)),sList,'Uniformoutput',false));
    
    matlabbatch{1}.spm.stats.factorial_design.dir                    = {ModelPathC};
    matlabbatch{1}.spm.stats.factorial_design.des.t1.scans           = fList;
%     matlabbatch{1}.spm.stats.factorial_design.des.t2.scans1          = fList(1:34);
%     matlabbatch{1}.spm.stats.factorial_design.des.t2.scans2          = fList(35:end);
    matlabbatch{1}.spm.stats.factorial_design.cov                    = struct('c', {}, 'cname', {}, 'iCFI', {}, 'iCC', {});
    matlabbatch{1}.spm.stats.factorial_design.multi_cov              = struct('files', {}, 'iCFI', {}, 'iCC', {});
    matlabbatch{1}.spm.stats.factorial_design.masking.tm.tm_none     = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.im             = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.em             = {''};
    matlabbatch{1}.spm.stats.factorial_design.globalc.g_omit         = 1;
    matlabbatch{1}.spm.stats.factorial_design.globalm.gmsca.gmsca_no = 1;
    matlabbatch{1}.spm.stats.factorial_design.globalm.glonorm        = 1;
    
    
    %----------------------------------------------------------------------
    % Model Estimation
    %----------------------------------------------------------------------
    clear frmi_est
    fmri_est.spmmat(1)        = cfg_dep('Factorial design specification: SPM.mat File', substruct('.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','spmmat'));
    fmri_est.write_residuals  = 0;
    fmri_est.method.Classical = 1;
    
    matlabbatch{2}.spm.stats.fmri_est = fmri_est;
    
    
    %----------------------------------------------------------------------
    % Contrast Manager
    %----------------------------------------------------------------------
    matlabbatch{3}.spm.stats.con.spmmat(1)               = cfg_dep('Model estimation: SPM.mat File', substruct('.','val', '{}',{2}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','spmmat'));
    matlabbatch{3}.spm.stats.con.consess{1}.tcon.name    = oldcnames{cIDX};
    matlabbatch{3}.spm.stats.con.consess{1}.tcon.weights = 1;
    matlabbatch{3}.spm.stats.con.consess{1}.tcon.sessrep = 'none';
    matlabbatch{3}.spm.stats.con.delete                  = 1;
    
    
    %----------------------------------------------------------------------
    % Print result
    %----------------------------------------------------------------------
    matlabbatch{4}.spm.stats.results.spmmat(1) = cfg_dep('Contrast Manager: SPM.mat File', substruct('.','val', '{}',{3}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','spmmat'));
    matlabbatch{4}.spm.stats.results.conspec.titlestr    = oldcnames{cIDX};
    matlabbatch{4}.spm.stats.results.conspec.contrasts   = 1;
    matlabbatch{4}.spm.stats.results.conspec.threshdesc  = 'FWE';
    matlabbatch{4}.spm.stats.results.conspec.thresh      = 0.05;
    matlabbatch{4}.spm.stats.results.conspec.extent      = 0;
    matlabbatch{4}.spm.stats.results.conspec.conjunction = 1;
    matlabbatch{4}.spm.stats.results.conspec.mask.none   = 1;
    matlabbatch{4}.spm.stats.results.units      = 1;
    matlabbatch{4}.spm.stats.results.print      = true;
    matlabbatch{4}.spm.stats.results.write.none = 1;
    
    
    %======================================================================
    % RUN matlabbatch
    %======================================================================
    spm_jobman('run',matlabbatch)
    
    % save figure
    print('-dtiff','-noui', fullfile(ResultPath,[newcnames{cIDX} '.tiff']))
end

