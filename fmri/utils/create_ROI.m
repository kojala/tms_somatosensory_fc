function create_ROI(ModelPath)


%----------------------------------------------------------------------
% paths
%----------------------------------------------------------------------

[NewPath,Subj,~] = fileparts(ModelPath);
cd(NewPath); cd ..; cd ..

ExpPath     = pwd;
DataPath    = fullfile(ExpPath,'Data');
MRIpath     = fullfile(DataPath,'MRI',Subj);
VOIpath     = fullfile(ExpPath,'ROI_TMS',Subj);
[~,~]       = rmdir(VOIpath,'s');
[~,~]       = mkdir(VOIpath);

MaskPath    = 'D:\TMS_Experiment\fMRI';
T1Path      = dir(fullfile(MRIpath,'T1*'));
T1Path      = fullfile(MRIpath,T1Path(1).name);
copyfile(fullfile(T1Path,'sn*.nii'),VOIpath)
copyfile(fullfile(T1Path,'*y_*.nii'),VOIpath)


% go through hemispheres
%------------------------------------------------------------------
hemList = {'L';'R'};
countT  = 1;

for hemIDX = 1:2
    
    Maskfname  = fullfile(MaskPath,'Masks',['FingerMask' hemList{hemIDX} '.nii']);
    
    
    % create thresholded mask in correct resolution
    %--------------------------------------------------------------------------
    clear voi matlabbatch
    
    voi.spmmat = cellstr(fullfile(ModelPath,'SPM.mat'));
    voi.adjust = NaN;
    voi.session = 1; % session 1
    voi.name = ['FingerMask' hemList{hemIDX}];
    voi.roi{1}.spm.spmmat = {''}; % using SPM.mat above
    voi.roi{1}.spm.contrast = 1;
    voi.roi{1}.spm.threshdesc = 'none';
    voi.roi{1}.spm.thresh = 1;
    voi.roi{1}.spm.extent = 0;
    voi.roi{2}.mask.image = {Maskfname};
    voi.roi{2}.mask.threshold = 0.5;
    voi.expression = 'i1 & i2';
    matlabbatch{1}.spm.util.voi = voi;
    
    spm_jobman('run',matlabbatch);
    
    try movefile(fullfile(ModelPath,['*' voi.name '*']),VOIpath,'f'); end
    delete(fullfile(VOIpath,['VOI_' voi.name '_1.mat']))
    delete(fullfile(VOIpath,['VOI_' voi.name '_eigen.nii']))
    
    
    % load masks and get coordinates
    %--------------------------------------------------------------------------
    hMask = spm_vol(Maskfname);
    vMask = spm_read_vols(hMask);
    
    hMask2 = spm_vol(fullfile(VOIpath,['VOI_' voi.name '_mask.nii']));
    vMask2 = spm_read_vols(hMask2);
    
    [r,c,v] = ind2sub(size(vMask2),find(vMask2 == 1));
    list    = [r'; c'; v'];
    
    
    % load T maps and find peak for each contrast
    %--------------------------------------------------------------------------
    fList    = spm_select('ExtFPList',ModelPath,'^spmT',1);
    for fIDX = 1:size(fList,1)
        
        % use only appropriate hemispheres per
        % finger:
        % left brain  -> d3, d4, all fingers, right > left
        % right brain -> d1, d2, all fingers, left  > right
        if (strcmp(hemList{hemIDX},'L') && ismember(fIDX,[3 4 5 7])) || ...
                (strcmp(hemList{hemIDX},'R') && ismember(fIDX,[1 2 5 6]))
            clear peakCoordinate peakCoordinatemm h vol lm VolSphere hSphere Pvol img
            
            
            % load T map and get maxima within mask
            %----------------------------------------------------------------------
            h   = spm_vol(fList(fIDX,:));
            vol = spm_read_vols(h);
            % idx = spm_get_lm(vol,list);
            % vol      - 3(or 2)D volume of statistics (e.g. t or F)
            % list     - 3xn (or 2xn) list of voxel coordinates of tentative local
            %            maxima.
%             try
%                 idx = spm_get_lm(vol,list);
%                 error(lastwarn)
%             catch
%                 foo     = spm_get_data(fList(fIDX,1:end-2),list);
%                 [~,idx] = max(foo);
%             end
            idx = spm_get_lm(vol,list);
            
            
            if ~isempty(idx)
                % list of local maxima
                %----------------------------------------------------------------------
                for ii = 1:numel(idx)
                    lm(ii) = (vol(list(1,idx(ii)),list(2,idx(ii)),list(3,idx(ii))));
                end
                
                
                % get coordinate of global maximum for current contrast
                %----------------------------------------------------------------------
                [Mmax,Imax] = max(lm);
                peakCoordinate = list(:,idx(Imax));
                
            else
                disp('NO LOCAL MAXIMA AVAILABLE!!')
                
                % Take global maximum without a list of local maxima
                foo     = spm_get_data(fList(fIDX,1:end-2),list);
                [~,idx] = max(foo);
                
                for ii = 1:numel(idx)
                    lm(ii) = (vol(list(1,idx(ii)),list(2,idx(ii)),list(3,idx(ii))));
                end
                
                [Mmax,Imax] = max(lm);
                peakCoordinate = list(:,idx(Imax));
                
                %peakCoordinate = [0;0;0] ;
            end
            
            % convert to mm for sphere
            fooSPM = load(fullfile(ModelPath,'SPM.mat'));
            VM     = fooSPM.SPM.VM;
            peakCoordinatemm = VM.mat(1:3,:)*[peakCoordinate; ones(1,size(peakCoordinate,2))];
            
            
            % get P value for T map
            %----------------------------------------------------------------------
            % http://www2.warwick.ac.uk/fac/sci/statistics/staff/academic-research/nichols/scripts/spm/johnsgems2/
            df          = fooSPM.SPM.xX.erdf;
            Pvol        = h;
            Pvol.fname  = strrep(h.fname,'spmT','spm_nltP');
            if strcmp(Pvol.fname,h.fname)
                Pvol.fname = fullfile(spm_str_manip(h.fname,'H'), ...
                    ['nltP' spm_str_manip(h.fname,'t')]);
            end
            Pvol = spm_create_vol(Pvol);
            for i=1:Pvol.dim(3),
                img         = spm_slice_vol(h,spm_matrix([0 0 i]),h.dim(1:2),0);
                img(img==0) = NaN;
                tmp         = find(isfinite(img));
                if ~isempty(tmp)
                    % Create map of P values
                    img(tmp)  = (max(eps,1-spm_Tcdf(img(tmp),df)));
                    
                    % Create map of -log10(P values)
                    % img(tmp)  = -log10(max(eps,1-spm_Tcdf(img(tmp),df)));
                end
                Pvol        = spm_write_plane(Pvol,img,i);
            end;
            
            
            % create sphere around peak coordinate
            %----------------------------------------------------------------------
            disp('Create sphere around centre coordinates (mm):')
            disp(peakCoordinatemm)
            % code snippets copied from spm_run_voi.m
            [x,y,z]   = ndgrid(1:fooSPM.SPM.xVol.DIM(1),1:fooSPM.SPM.xVol.DIM(2),1:fooSPM.SPM.xVol.DIM(3));
            XYZ       = [x(:),y(:),z(:)]'; clear x y z
            XYZmm     = fooSPM.SPM.xVol.M(1:3,:) * [XYZ;ones(1,size(XYZ,2))];
            VolSphere = zeros(fooSPM.SPM.xVol.DIM');
            xyz       = XYZmm;
            Q         = ones(1,size(xyz,2));
            c         = peakCoordinatemm;
            r         = 5;
            VolSphere(sum((xyz - c*Q).^2) <= r^2) = true; % sphere
            
            % write ROI
            hSphere = h;
            hSphere.fname = fullfile(VOIpath,['MNI_ROI_' h.descrip(30:end-15) '_' hemList{hemIDX} '.nii']);
            hSphere.fname = strrep(hSphere.fname,' ','');
            hSphere.fname = strrep(hSphere.fname,'>','-');
            hSphere.descrip = ['MNI_ROI_' h.descrip(30:end-15) '_' hemList{hemIDX}];
            spm_write_vol(hSphere,VolSphere);
            
            
            % inspect result and save image
            %----------------------------------------------------------------------
            spm_check_registration(char({...
                [hMask2.fname ',1']; ...
                [hSphere.fname ',1']; ...
                [h.fname ',1']; ...
                'D:\spm12\canonical\single_subj_T1.nii' ...
                }));
            spm_orthviews('Reposition',peakCoordinatemm); % any coordinates you want {mm}
            allF = findall(0,'Type','Figure');
            
            % save plot
            [~,pngfname,~] = fileparts(hSphere.fname);
            saveas(allF(1),fullfile(VOIpath,[Subj '_' pngfname]),'png')
            
            
            % save information to table
            Subject{countT,1} = Subj;
            peakCoord(countT,:) = peakCoordinate;
            peakCoordmm(countT,:) = peakCoordinatemm;
            peakP(countT,1) = spm_get_data(Pvol.fname,peakCoordinate);
            peakT(countT,1) = spm_get_data(h.fname,peakCoordinate);
            SphereName{countT,1} = pngfname;
            SpherefName{countT,1} = hSphere.fname;
            countT = countT+1;
            
        end
    end
end

ROItable = table(Subject,peakCoord,peakCoordmm,peakP,peakT,SphereName,SpherefName);
save(fullfile(VOIpath,'ROItable.mat'),'ROItable')