function GLM_0s_MNI(MRIpath)

stimdurationM = 0;
TR            = 2;

%----------------------------------------------------------------------
% paths
%----------------------------------------------------------------------

[NewPath,Subj,~] = fileparts(MRIpath);
cd(NewPath); cd ..; cd ..

ExpPath     = pwd;
ModelPath   = fullfile(ExpPath,'Models','GLM_0s_MNI',Subj);
[s,m]       = rmdir(ModelPath,'s');
[s,m]       = mkdir(ModelPath);
DataPath    = fullfile(ExpPath,'Data');
ResultPath  = fullfile(ExpPath,'ResultPlots');

EPIPathList = dir(fullfile(MRIpath,'EPI*'));
EPIPathList = {EPIPathList.name}';

%----------------------------------------------------------------------
% get event times
%----------------------------------------------------------------------

for eIDX = 1:numel(EPIPathList)
    clear timing timingtrial stimvec
    load(fullfile(DataPath,sprintf('%s_localizer_R%i',Subj(end-2:end),eIDX)))
    stimvec = stimvec(1:numel(timing)); % crop
    for d = 1:4
        TD{d,eIDX} = timing(stimvec==d);
    end
end


%----------------------------------------------------------------------
% fMRI model specification
%----------------------------------------------------------------------

clear EPIflist Motionfn fmri_spec

fmri_spec.dir            = cellstr(ModelPath);
fmri_spec.timing.units   = 'secs';
fmri_spec.timing.RT      = TR;
fmri_spec.timing.fmri_t  = 16;
fmri_spec.timing.fmri_t0 = 8;

for eIDX = 1:numel(EPIPathList)
    EPIflist{eIDX,1} = cellstr(spm_select('ExtFPList',fullfile(MRIpath,EPIPathList{eIDX}),'^swarsn.*\.nii$',1));
    Motionfn{eIDX,1} = cellstr(spm_select('FPList',fullfile(MRIpath,EPIPathList{eIDX}),'^rp.*\.txt$'));
    
    fmri_spec.sess(eIDX).scans = EPIflist{eIDX,1};
    for d = 1:4
        fmri_spec.sess(eIDX).cond(d).name     = ['D' num2str(d)];
        fmri_spec.sess(eIDX).cond(d).onset    = TD{d,eIDX};
        fmri_spec.sess(eIDX).cond(d).duration = stimdurationM;
        fmri_spec.sess(eIDX).cond(d).tmod     = 0;
        fmri_spec.sess(eIDX).cond(d).pmod     = struct('name', {}, 'param', {}, 'poly', {});
        fmri_spec.sess(eIDX).cond(d).orth     = 1;
    end
    fmri_spec.sess(eIDX).multi     = {''};
    fmri_spec.sess(eIDX).regress   = struct('name', {}, 'val', {});
    fmri_spec.sess(eIDX).multi_reg = Motionfn{eIDX,1};
    fmri_spec.sess(eIDX).hpf       = 128;
end

fmri_spec.fact             = struct('name', {}, 'levels', {});
fmri_spec.bases.hrf.derivs = [0 0];
fmri_spec.volt             = 1;
fmri_spec.global           = 'None';
fmri_spec.mthresh          = 0.8;
fmri_spec.mask             = {''};
fmri_spec.cvi              = 'AR(1)';

matlabbatch{1}.spm.stats.fmri_spec = fmri_spec;


%----------------------------------------------------------------------
% Model Estimation
%----------------------------------------------------------------------

clear fmri_est
fmri_est.spmmat(1)        = cfg_dep('fMRI model specification: SPM.mat File', substruct('.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','spmmat'));
fmri_est.write_residuals  = 0;
fmri_est.method.Classical = 1;

matlabbatch{2}.spm.stats.fmri_est = fmri_est;


%----------------------------------------------------------------------
% Contrast Manager
%----------------------------------------------------------------------

clear con
con.spmmat(1) = cfg_dep('Model estimation: SPM.mat File', substruct('.','val', '{}',{2}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','spmmat'));
for d = 1:4
    con.consess{d}.tcon.name    = ['d' num2str(d)];
    cwv                         = zeros(1,4);
    cwv(d)                      = 1;
    con.consess{d}.tcon.weights = cwv;
    con.consess{d}.tcon.sessrep = 'repl';
end

con.consess{5}.tcon.name    = 'all fingers';
con.consess{5}.tcon.weights = [1 1 1 1];
con.consess{5}.tcon.sessrep = 'repl';
con.consess{6}.tcon.name    = 'left > right';
con.consess{6}.tcon.weights = [1 1 -1 -1];
con.consess{6}.tcon.sessrep = 'repl';
con.consess{7}.tcon.name    = 'right > left';
con.consess{7}.tcon.weights = [-1 -1 1 1];
con.consess{7}.tcon.sessrep = 'repl';
con.delete                  = 1;

matlabbatch{3}.spm.stats.con = con;


%----------------------------------------------------------------------
% Results Report
%----------------------------------------------------------------------

mSize = numel(matlabbatch);
clear results
for cIDX = 1:2:(numel(con.consess)*2)
    results.spmmat(1) = cfg_dep('Contrast Manager: SPM.mat File', substruct('.','val', '{}',{3}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','spmmat'));
    results.conspec.titlestr = con.consess{(cIDX+1)/2}.tcon.name;
    results.conspec.contrasts = (cIDX+1)/2;
    results.conspec.threshdesc = 'none';
    results.conspec.thresh = 0.001;
    results.conspec.extent = 0;
    results.conspec.conjunction = 1;
    results.conspec.mask.image.name = '';
    results.conspec.mask.image.mtype = 0;
    results.units = 1;
    results.print = 'pdf';
    results.write.none = 1;
    
    matlabbatch{cIDX+mSize}.spm.stats.results = results;
end


%----------------------------------------------------------------------
% Print Results
%----------------------------------------------------------------------

clear print
for cIDX = 2:2:(numel(con.consess)*2)
    print.fname = fullfile(ResultPath,sprintf('c%i_fearS1tms%s',cIDX/2,Subj));
    print.fig.fighandle = NaN;
    print.opts = 'png';
    
    matlabbatch{cIDX+mSize}.spm.util.print = print;
end


%==========================================================================
% RUN matlabbatch
%==========================================================================

save(fullfile(ExpPath,'Batch', ...
    ['batch_GLM_0s_MNI_' Subj '.mat']), ...
    'matlabbatch')

%load D:\temp\Batch\batch_GLM_0s_MNI_fearS1tms073.mat matlabbatch
spm_jobman('run',matlabbatch)

