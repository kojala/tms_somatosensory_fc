function deform_ROI_to_native(VOIpath)

%----------------------------------------------------------------------
% paths
%----------------------------------------------------------------------

[NewPath,Subj,~] = fileparts(VOIpath);
cd(NewPath); cd ..

ExpPath   = pwd;
T1fname   = dir(fullfile(VOIpath,'sn*.nii'));
T1fname   = fullfile(VOIpath,T1fname(1).name);

pfname    = dir(fullfile(VOIpath,'iy*'));
pfname    = fullfile(VOIpath,pfname(1).name);
roifname  = cellstr(spm_select('ExtFPList',VOIpath,'^MNI_ROI',1));
ModelPath = fullfile(ExpPath,'Models','GLM_0s_MNI',Subj);
tfname    = cellstr(spm_select('ExtFPList',ModelPath,'^spmT',1));

%----------------------------------------------------------------------
% Normalize:Write
%----------------------------------------------------------------------

clear matlabbatch
matlabbatch{1}.spm.spatial.normalise.write.subj.def = {pfname};
matlabbatch{1}.spm.spatial.normalise.write.subj.resample = [roifname; tfname];
matlabbatch{1}.spm.spatial.normalise.write.woptions.bb = [-78 -112 -70; 78 76 85];
matlabbatch{1}.spm.spatial.normalise.write.woptions.vox = [2 2 2];
matlabbatch{1}.spm.spatial.normalise.write.woptions.interp = 4;
matlabbatch{1}.spm.spatial.normalise.write.woptions.prefix = 'Native_';

save(fullfile(ExpPath,'Batch',['batch_ROI_MNI2Native' Subj '.mat']),'matlabbatch');
spm_jobman('run',matlabbatch)


%----------------------------------------------------------------------
% cleanup and plot
%----------------------------------------------------------------------

load(fullfile(ModelPath,'SPM.mat'))

% rename t maps and move to VOI path
newtfnames = cellfun(@(x) ['Native_' x(1:end-15)], {SPM.xCon(:).name}', 'Uniformoutput',0);
newtfnames = cellfun(@(x) strrep(x,' ',''), newtfnames, 'Uniformoutput',0);
newtfnames = cellfun(@(x) fullfile(VOIpath,[strrep(x,'>','-') '.nii']), newtfnames, 'Uniformoutput',0);
oldtfnames = cellstr(spm_select('FPList',ModelPath,'^Native_'));
cellfun(@(x,y) movefile(x,y),oldtfnames,newtfnames, 'Uniformoutput',0);

% rename ROIs
oldRoifnames = cellstr(spm_select('FPList',VOIpath,'^Native_MNI_'));
newRoifnames = cellfun(@(x) strrep(x,'MNI_ROI_','ROI_'), oldRoifnames, 'Uniformoutput',0);
cellfun(@(x,y) movefile(x,y),oldRoifnames,newRoifnames, 'Uniformoutput',0);
for fIDX = 1:numel(newRoifnames)
    
    % cleanup edges of ROI after deformation
    hSphere = spm_vol(newRoifnames{fIDX});
    vSphere = spm_read_vols(hSphere);
    vSphere(vSphere<0.1) = 0;
    vSphere(vSphere>=0.1) = 1;
    vSphere(isnan(vSphere)) = 0;
    spm_write_vol(hSphere,vSphere);
end


fnameList = cellstr(spm_select('ExtFPList',VOIpath,'^Native_',1));
for fIDX = 1:numel(fnameList)
    % inspect result and save image
    
    hSphere = spm_vol(fnameList{fIDX});
    vSphere = spm_read_vols(hSphere);
    [r,c,v] = ind2sub(size(vSphere),find(vSphere == max(vSphere(:))));
    peakcoord    = [r'; c'; v'];
    try peakcoord    = peakcoord(:,floor(size(peakcoord,2)/2)); end
    peakcoordmm = SPM.VM.mat(1:3,:)*[peakcoord; ones(1,size(peakcoord,2))];
    
    spm_check_registration(char({...
        fnameList{fIDX}; ...
        T1fname ...
        }));
    spm_orthviews('Reposition',peakcoordmm); % any coordinates you want {mm}
    allF = findall(0,'Type','Figure');
    
    % save plot
    [~,pngfname,~] = fileparts(hSphere.fname);
    saveas(allF(1),fullfile(VOIpath,[Subj '_' pngfname]),'png')
end
