function convert_rec(DataPath)

% example:
% DataPath = 'D:\data_BLI\Experiment_TMS\somstim_fmri_pilot06\data\MRI\fearS1tms041\raw';


% substrings of file names to identify EPI, B0 and T1 files
%--------------------------------------------------------------------------

EPIname = '*fmri_*.rec';
B0name  = '*b0_fieldmap_spliV42*.rec';
T1name  = '*t1w3danat111*.rec';
%T1name  = '*t1w3danat111_splV42.rec';

% convert EPI
%--------------------------------------------------------------------------

fnameEPI = dir(fullfile(DataPath,EPIname));
fnameEPI = {fnameEPI.name}';

for eIDX = 1:numel(fnameEPI)
    command = ['rec2nifti.pl -s -vol ' fullfile(DataPath,fnameEPI{eIDX}) ' &'];
    [status,cmdout]= system(command,'-echo');
end


% convert B0
%--------------------------------------------------------------------------
try
fnameB0 = dir(fullfile(DataPath,B0name));
fnameB0 = {fnameB0.name}';
command     = ['rec2nifti.pl -split ' fullfile(DataPath,fnameB0{1}) ' &'];
[status,cmdout]= system(command,'-echo');
end

% convert T1 using -tms flag
%--------------------------------------------------------------------------
fnameT1 = dir(fullfile(DataPath,T1name));
fnameT1 = {fnameT1.name}';
command     = ['rec2nifti.pl -tms ' fullfile(DataPath,fnameT1{1}) ' &'];
[status,cmdout]= system(command,'-echo');
