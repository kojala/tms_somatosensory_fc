function sort_MRI_files(MRIpath)

% example:
% MRIpath = 'D:\data_BLI\Experiment_TMS\ROI\Data\MRI\fearS1tms032\';

EPIname = '*fmri_*.nii';
B0name  = '*b0_fieldmap_spliV42*.nii';
T1name  = '*t1w3danat111*.nii';
%T1name  = '*t1w3danat111_splV42*.nii';

% EPI std
seqType = 'EPI';
% try
    performSorting(seqType,EPIname,MRIpath)
% end

% T1
seqType = 'T1';
% try
    performSorting(seqType,T1name,MRIpath)
% end

% B0
seqType = 'B0';
% try
    performSorting(seqType,B0name,MRIpath)
% end

% move raw files
raw_destination = fullfile(MRIpath,'raw');
[s,m] = mkdir(raw_destination);
movefile(fullfile(MRIpath, '*.rec'),raw_destination);
movefile(fullfile(MRIpath, '*.par'),raw_destination);
end

function performSorting(seqType,fname1,MRIpath)

% get list of all files
filenames1 = dir(fullfile(MRIpath, fname1));
filenames1 = {filenames1(:).name}';

% regexp for date in filenames
currsub = str2double(MRIpath(end-2:end));

if currsub < 98 % SNS lab scanner update changed the length of this string of numbers from 7 to 6, after sub 97 on 29.5.2017
    expression = '\d{8}_\d{7}';
else
    expression = '\d{8}_\d{6}';
end

% extract date information
filenames1 = cellfun(@(x) cell2mat(regexp(x,expression,'match')), filenames1, 'Uniformoutput',0);

% find unique dates to create folders
[~,idx]=unique(filenames1);
sequences = filenames1(idx,:);

for eIDX = 1:size(sequences,1)
    file_path = fullfile(MRIpath,strcat(seqType,'_',sequences{eIDX,1}));
    [m,s] = mkdir(file_path);
    movefile(fullfile(MRIpath,['*' sequences{eIDX} '*.nii']),file_path)
end
end