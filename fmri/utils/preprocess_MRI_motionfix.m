function preprocess_MRI_motionfix(MRIpath)

nslices = 33;
TR      = 2;
TA      = TR-(TR/nslices);

%--------------------------------------------------------------------------
% Copy files to C:\ (faster SSD disk)
%--------------------------------------------------------------------------

ExpPath  = pwd;
%workPath = pwd;
tempPath = MRIpath; %['D:\data_BLI\' num2str(randi([10^8 10^9]))];
% [~,~]    = rmdir(tempPath,'s');
% [~,~]    = mkdir(tempPath);
% copyfile(MRIpath,tempPath)

T1Path      = dir(fullfile(tempPath,'T1*'));
T1Path      = fullfile(tempPath,T1Path(1).name);
EPIPathList = dir(fullfile(tempPath,'EPI*'));
EPIPathList = {EPIPathList.name}';

% set working directory to D:\
cd(tempPath)

%--------------------------------------------------------------------------
% Realign: Estimate & Reslice
%--------------------------------------------------------------------------

clear EPIflist
for eIDX = 1:numel(EPIPathList)
    EPIflist{eIDX,1} = cellstr(spm_select('ExtFPList',fullfile(tempPath,EPIPathList{eIDX}),'^sn.*\.nii$',1));
end

clear estwrite
estwrite.data             = EPIflist;
estwrite.eoptions.quality = 0.9;
estwrite.eoptions.sep     = 4;
estwrite.eoptions.fwhm    = 5;
estwrite.eoptions.rtm     = 1;
estwrite.eoptions.interp  = 2;
estwrite.eoptions.wrap    = [0 0 0];
estwrite.eoptions.weight  = '';
estwrite.roptions.which   = [2 1];
estwrite.roptions.interp  = 4;
estwrite.roptions.wrap    = [0 0 0];
estwrite.roptions.mask    = 1;
estwrite.roptions.prefix  = 'r';

matlabbatch{1}.spm.spatial.realign.estwrite = estwrite;

%--------------------------------------------------------------------------
% Slice Timing
%--------------------------------------------------------------------------

clear EPIflist
for eIDX = 1:numel(EPIPathList)
    EPIflist{eIDX,1} = cellstr(spm_select('ExtFPList',fullfile(tempPath,EPIPathList{eIDX}),'^rsn.*\.nii$',1));
end

clear st
st.scans       = EPIflist;
st.nslices     = nslices;
st.tr          = TR;
st.ta          = TA;
st.so          = 1:nslices;
st.refslice    = ceil(nslices/2);
st.prefix      = 'a';

matlabbatch{2}.spm.temporal.st = st;

%--------------------------------------------------------------------------
% Coregister: Estimate
%--------------------------------------------------------------------------

T1fname = cellstr(spm_select('ExtFPList',T1Path,'^sn.*nii$',1));

% This mean image is not exactly correct after the replacement of original
% realigned volumes with the repaired ones
meanfname = cellstr(spm_select('ExtFPList',fullfile(tempPath,EPIPathList{1}),'^meansn.*\.nii$',1));

% for eIDX = 1:numel(EPIPathList)
%     estimate.other(eIDX) = spm_select('ExtFPList',fullfile(tempPath,EPIPathList{eIDX}),'^arsn.*\.nii$',1);
% end

clear EPIflist

for eIDX = 1:numel(EPIPathList)
    EPIflist{eIDX,1} = cellstr(spm_select('ExtFPList',fullfile(tempPath,EPIPathList{eIDX}),'^arsn.*\.nii$',1));
end
EPIflist = {cat(1,EPIflist{:})};

clear estimate

estimate.ref               = T1fname;
estimate.source            = meanfname;
estimate.other             = EPIflist{:};
estimate.eoptions.cost_fun = 'nmi';
estimate.eoptions.sep      = [4 2];
estimate.eoptions.tol      = [0.02 0.02 0.02 0.001 0.001 0.001 0.01 0.01 0.01 0.001 0.001 0.001];
estimate.eoptions.fwhm     = [7 7];

matlabbatch{3}.spm.spatial.coreg.estimate = estimate;


%--------------------------------------------------------------------------
% Segment
%--------------------------------------------------------------------------

clear preproc
preproc.channel.vols     = T1fname;
preproc.channel.biasreg  = 0.001;
preproc.channel.biasfwhm = 60;
preproc.channel.write    = [0 0];
preproc.tissue(1).tpm    = {'D:\spm12\tpm\TPM.nii,1'};
preproc.tissue(1).ngaus  = 1;
preproc.tissue(1).native = [1 0];
preproc.tissue(1).warped = [0 0];
preproc.tissue(2).tpm    = {'D:\spm12\tpm\TPM.nii,2'};
preproc.tissue(2).ngaus  = 1;
preproc.tissue(2).native = [1 0];
preproc.tissue(2).warped = [0 0];
preproc.tissue(3).tpm    = {'D:\spm12\tpm\TPM.nii,3'};
preproc.tissue(3).ngaus  = 2;
preproc.tissue(3).native = [1 0];
preproc.tissue(3).warped = [0 0];
preproc.tissue(4).tpm    = {'D:\spm12\tpm\TPM.nii,4'};
preproc.tissue(4).ngaus  = 3;
preproc.tissue(4).native = [1 0];
preproc.tissue(4).warped = [0 0];
preproc.tissue(5).tpm    = {'D:\spm12\tpm\TPM.nii,5'};
preproc.tissue(5).ngaus  = 4;
preproc.tissue(5).native = [1 0];
preproc.tissue(5).warped = [0 0];
preproc.tissue(6).tpm    = {'D:\spm12\tpm\TPM.nii,6'};
preproc.tissue(6).ngaus  = 2;
preproc.tissue(6).native = [0 0];
preproc.tissue(6).warped = [0 0];
preproc.warp.mrf         = 1;
preproc.warp.cleanup     = 1;
preproc.warp.reg         = [0 0.001 0.5 0.05 0.2];
preproc.warp.affreg      = 'mni';
preproc.warp.fwhm        = 0;
preproc.warp.samp        = 3;
preproc.warp.write       = [1 1];

matlabbatch{4}.spm.spatial.preproc = preproc;


%--------------------------------------------------------------------------
% Normalise: Write
%--------------------------------------------------------------------------

clear write
write.subj.def(1)      = cfg_dep('Segment: Forward Deformations', substruct('.','val', '{}',{4}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','fordef', '()',{':'}));

% clear EPIflist
% for eIDX = 1:numel(EPIPathList)
%      EPIflist{eIDX,1} = cellstr(spm_select('ExtFPList',fullfile(tempPath,EPIPathList{eIDX}),'^arsn.*\.nii$',1));
% end

for eIDX = 1:numel(EPIPathList)
    write.subj.resample(eIDX) = cfg_dep(['Slice Timing: Slice Timing Corr. Images (Sess ' num2str(eIDX) ')'], substruct('.','val', '{}',{2}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('()',{eIDX}, '.','files'));
end

%write.subj.resample    = EPIflist;
write.woptions.bb      = [-78 -112 -70
    78 76 85];
write.woptions.vox     = [2 2 2];
write.woptions.interp  = 4;
write.woptions.prefix  = 'w';

matlabbatch{5}.spm.spatial.normalise.write = write;


%--------------------------------------------------------------------------
% Smooth: normalized EPI
%--------------------------------------------------------------------------

clear smooth
smooth.data(1) = cfg_dep('Normalise: Write: Normalised Images (Subj 1)', substruct('.','val', '{}',{5}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('()',{1}, '.','files'));
smooth.fwhm    = [8 8 8];
smooth.dtype   = 0;
smooth.im      = 0;
smooth.prefix  = 's';

matlabbatch{6}.spm.spatial.smooth = smooth;


%==========================================================================
% RUN matlabbatch
%==========================================================================

[~,Subj,~] = fileparts(MRIpath);

save(fullfile('D:\temp','Batch', ...
    ['batch_preprocess_motfix_MRI_' Subj '.mat']), ...
    'matlabbatch')

matlabbatch(1) = [];

%save matlabbatch_preprocess.mat matlabbatch

%load matlabbatch_preprocess.mat matlabbatch
spm_jobman('run',matlabbatch)

% move files back and clean up
% cd(workPath)
% movefile(fullfile(tempPath,'*'),MRIpath,'f')
% [~,~] = rmdir(tempPath,'s');
