function create_TMS_ROI2(SourcePath)

% e.g.
% SourcePath = 'D:\TMS_Experiment\LocalizerRawData\fearS1tms032\';
% create_TMS_ROI(SourcePath)
%addpath(fullfile(pwd,'ROIAnalysis','utils'))

% get/set paths
%--------------------------------------------------------------------------
% snip last "\" symbol from input
if strcmp(SourcePath(end),'\'), SourcePath = SourcePath(1:end-1); end
[NewPath,Subj,~] = fileparts(SourcePath);
cd(NewPath); cd ..

%ExpPath   = pwd;
ExpPath   = 'D:\temp';
DataPath  = fullfile(ExpPath,'Data');
MRIpath   = fullfile(DataPath,'MRI',Subj);
[~,~]     = mkdir(MRIpath);
ModelPath = fullfile(ExpPath,'Models','GLM_0s_MNI',Subj);
VOIpath   = fullfile(ExpPath,'ROI_TMS',Subj);


% launch SPM (add toolbox to path if necessary)
%--------------------------------------------------------------------------
if exist('spm')~=2, addToolboxes(1); end
spm('defaults','fmri');
spm_jobman('initcfg')


% preprocess EPI
%--------------------------------------------------------------------------
preprocess_MRI(MRIpath)
%preprocess_MRI_motionfix(MRIpath)


% create GLM
%--------------------------------------------------------------------------
GLM_0s_MNI(MRIpath)


% create region of interests
%--------------------------------------------------------------------------
create_ROI(ModelPath);


% denormalize to native space
%--------------------------------------------------------------------------
deform_ROI_to_native(VOIpath);