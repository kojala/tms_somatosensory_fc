%% Wrapper script for psychophysiological data analysis
% TMS fear conditioning study
% Authors: Karita E. Ojala, Matthias Staib, Samuel Gerster, Christian C. Ruff, Dominik R. Bach
% Script start:  03.02.2017 Karita Ojala
% Script edited: 26.04.2021 Karita Ojala
% Current e-mail (as of 4/2021): k.ojala@uke.de
% Bach lab website: bachlab.org
% Psychophysiological Modelling (PsPM) toolbox: bachlab.github.io/PsPM/

% This script pipeline was used to sort, preprocess (including import of
% data into PsPM, filtering, data cleaning, artefact correction etc.), run
% PsPM model-based response estimation, and to visualize skin conductance (SCR),
% pupil size (PSR) and startle eye-blink response (SEBR) data

% For a comprehensive explanation of the process for each
% psychophysiological modality, please refer to the latest PsPM manual and
% the articles from the lab: 
% SCR:
% Bach DR, Friston KJ (2013) Model-based analysis of skin conductance responses: Towards causal models in psychophysiology. Psychophysiology 50:15–22.
% Staib M, Castegnetti G, Bach DR (2015) Optimising a model-based approach to inferring fear learning from skin conductance responses. J Neurosci Methods 255:131–138.
% PSR:
% Korn CW, Staib M, Tzovara A, Castegnetti G, Bach DR (2017) A pupil size response model to assess fear learning. Psychophysiology 54:330–343.
% SEBR: 
% Khemka S, Tzovara A, Gerster S, Quednow B, Bach DR (2017) Modelling startle eye-blink electromyogram to assess fear learning. Psychophysiology 54:204–214.

% Preprint/publication of the current study: 
% 

clear all

%% Options for which sessions or groups to run
% Sessions: 
%   1 = Acquisition of fear conditioning (day 1)
%   2 = Memory retention test (day 2)
%   3 = Relearning (day 2)
% Groups: 
%   1 = Control group (CS-ipsilateral TMS)
%   2 = Experimental group (CS-contralateral TMS)
sessions = 2;
group = 1:2;

%% Actions
actions.run_renamefiles          = false; % rename original saved experimental data files to streamline for preprocessing and analysis
actions.run_renamefiles_training = false; % rename training files

%% SCR (Skin Conductance Responses)
actions.run_SCR_preproc         = false; % preprocess SCR data from original LabChart files
actions.run_SCR_plot_UR         = false; % plot unconditioned responses
actions.run_SCR_plot_daydiff    = false; % plot differences in responses between the experimental days/sessions
actions.run_SCR_split_sessions  = false; % fix data with splits due to stopped/interrupted recording in between
actions.run_SCR_DCM             = false; % run Dynamic Causal Model to estimate trialwise SCR amplitudes (takes at least 20 min / participant / session)
actions.run_SCR_DCM_save        = false; % save DCM data in different formats for later analysis and plotting
actions.run_SCR_DCM_plot        = false; % initial check plot of DCM results
actions.run_SCR_DCM_plot_trials = false; % plot DCM results trialwise
actions.run_SCR_GLM_cond        = false; % run General Linear Model to estimate conditionwise SCR amplitudes
actions.run_SCR_GLM_cond_save   = false; % save GLM data in different formats for later analysis and plotting
actions.run_SCR_GLM_cond_plot   = false; % initial check plot of GLM results
actions.run_SCR_preproc_trace   = false; % preprocess SCR timecourse data (e.g. filtering, interpolation)
actions.run_SCR_plot_trace      = false; % plot SCR timecourse traces
actions.run_SCR_plot_article    = false; % create the SCR figure for the article

%% SEBR (Startle Eye-Blink Responses)
actions.run_SEBR_import         = false; % import SEBR data from original LabChart files, including creating markers based on sound detection
actions.run_SEBR_preproc        = false; % preprocess SEBR data, including filtering etc.
actions.run_SEBR_plot_response  = false; % initial plot of SEBR
actions.run_SEBR_plot_indtrials = false; % plot individual trialwise SEBR data
actions.run_SEBR_GLM_cond       = false; % run General Linear Model to estimate conditionwise SEBR amplitudes
actions.run_SEBR_GLM_trial      = false; % run General Linear Model to estimate trialwise SEBR amplitudes
actions.run_SEBR_GLM_plot       = false; % initial plot of GLM results
actions.run_SEBR_GLM_timecourse = false; % plot reconstructed timecourse
actions.run_SEBR_GLM_cond_savedata  = false; % save conditionwise GLM data in different formats for later analysis and plotting
actions.run_SEBR_GLM_trial_savedata = false; % save trialwise GLM data in different formats for later analysis and plotting
actions.run_SEBR_plot_article   = false; % create the SEBR figure for the article

%% PSR (Pupil Size Responses)
actions.run_PSR_import          = false; % import PSR data from original EyeLink eyetracking files
actions.run_PSR_correct         = false; % correcting procedure for PSR data to exclude unlikely/invalid data points
actions.run_PSR_validfix        = false; % find PSR data associated with valid fixations = gaze fixation within certain distance from the center of the screen (fixation point)
actions.run_PSR_plot_validfix   = false; % plot valid fixations for quality control
actions.run_PSR_fixspikes       = false; % remove spikes in PSR data due to lost pupil tracking (participant looking outside the screen/eyetracking lost for some purpose)
actions.run_PSR_validtrials     = false; % find and save information about the valid trials based on enough valid, non-missing data points per trial per session per participant
actions.run_PSR_GLM_cond        = false; % run General Linear Model to estimate conditionwise PSR
actions.run_PSR_GLM_cond_save   = false; % save conditionwise GLM data in different formats for later analysis and plotting 
actions.run_PSR_GLM_trial       = false; % run General Linear Model to estimate trialwise PSR -> tried out but does not work due to low quality and amount of valid data
actions.run_PSR_GLM_trial_save  = false; % save trialwise GLM data in different formats for later analysis and plotting
actions.run_PSR_GLM_plot        = false; % initial plot of GLM results
actions.run_PSR_GLM_plot_trace  = false; % initial plot reconstructed timecourse from the GLM estimates
actions.run_PSR_preproc_trace   = false; % preprocess raw timecourse/trace data
actions.run_PSR_plot_trace      = false; % plot preprocessed timecourse/trace data
actions.run_PSR_plot_UR         = false; % plot unconditioned responses for quality control
actions.run_PSR_plot_article    = true; % create the PSR figure for the article

%% Pain, performance and descriptives

actions.run_pain_performance    = false; % retrieve task performance data (accuracy, reaction time of CS identification), save data for analysis
actions.run_probability_eval    = false; % retrieve data for explicit CS-US contingency ratings at the end of day 1 and day 2, save data for analysis
actions.run_descriptives        = false; % retrieve descriptive information of participants (gender, age, calibrated pain intensity)
actions.run_plot_trialseq       = false; % retrieve and plot individual trial sequence (CS+/CS-) for each block of each participant separately for simple and complex stimuli

%% Define paths
opts = get_psychophys_options(); % options file containing a multitude of parameters for the analysis pipeline
addpath(opts.pspmPath)
addpath(genpath(opts.codePath))

%% Rename and copy files
if actions.run_renamefiles; psychophys_rename_copy(opts,sessions); end

if actions.run_renamefiles_training; psychophys_rename_copy_training(opts); end

%% 1. SCR acquisition (day 2) and retest (day 3) phases

% 1.1   Import and trim SCR data, find markers, remove artefacts
if actions.run_SCR_preproc; preprocessSCR(opts,sessions); end

% 1.2   Filter data and plot mean SCR to US for each subject
if actions.run_SCR_plot_UR; plotSCR_USresponse(opts,sessions); end
% take artefact corrected data, interpolate and then plot!

% 1.3   Plot the difference between day 2 and 3 sessions for each subject
if actions.run_SCR_plot_daydiff; plotSCR_sesdiff(opts); end

% 1.4   Artefact removal: done manually already

% 1.5   Split data into sessions
if actions.run_SCR_split_sessions; SCR_splitsessions(opts,sessions); end

% 1.6   DCM
if actions.run_SCR_DCM; SCR_DCM(opts,sessions); end
if actions.run_SCR_DCM_save; SCR_DCM_save(opts,sessions); end
if actions.run_SCR_DCM_plot; SCR_DCM_plot(opts,sessions,group); end
if actions.run_SCR_DCM_plot_trials; plot_DCM_trial_SCR_timecourse(opts,sessions,group); end

% 1.7   Condition-wise GLM
if actions.run_SCR_GLM_cond; SCR_GLM_cond(opts,sessions); end
if actions.run_SCR_GLM_cond_save; SCR_GLM_cond_save(opts,sessions); end
if actions.run_SCR_GLM_cond_plot; SCR_GLM_cond_plot(opts,sessions); end

% 1.8   Preprocess raw data
if actions.run_SCR_preproc_trace; SCR_preprocess_trace(opts,sessions); end

% 1.9   Plot timecourses
if actions.run_SCR_plot_trace; SCR_plot_trace(opts,sessions); end

% 1.10  Plot for the article
if actions.run_SCR_plot_article; SCR_plot_article(opts,sessions); end

%% 2. Fear-potentiated startle (EMG) retention phase (day 3)

% 2.1   Import and trim data with individual target time intervals and find markers based on sound data
if actions.run_SEBR_import; importEMG(opts); end

% 2.2   Bandpass and notch filter, rectify signal, save data
if actions.run_SEBR_preproc; preprocessEMG_pp(opts); end

% 2.3   Plot original sound channel and preprocessed mean startle response
if actions.run_SEBR_plot_response; plotEMG_startleresponse(opts); end

% 2.4   Plot individual trials for problem subjects (from vis. inspection)
if actions.run_SEBR_plot_indtrials; plotEMG_individualtrials(opts); end

% 2.5   Trial-wise GLM with flexible latency
if actions.run_SEBR_GLM_trial; GLM_trial_SEBR(opts); end

% 2.6   Plot results of the trial-wise GLM
if actions.run_SEBR_GLM_plot; plot_GLM_trial_SEBR(opts); end

% 2.7   Plot timecourse of the trial-wise GLM
if actions.run_SEBR_GLM_timecourse; plot_GLM_trial_SEBR_timecourse(opts,group); end

% 2.8   Save SEBR conditionwise data for ANOVA
if actions.run_SEBR_GLM_cond_savedata; get_GLM_data(opts,opts.sebr.sList.allsubs); end

% 2.9   Save SEBR trialwise data for LME in R
if actions.run_SEBR_GLM_trial_savedata; get_GLM_data_trial(opts); end

% 2.10   Plot for the article
if actions.run_SEBR_plot_article; SEBR_plot_article(opts); end
    
%% 3. Pupil size for all phases (day 2) and retest (day 3) phases

% 3.1   Import and trim pupil data
if actions.run_PSR_import; pupil_import_trim(opts,sessions); end

% 3.2   Preprocessing corrections
if actions.run_PSR_correct; pupil_correct(opts,sessions); end

% 3.2   Find valid fixations
if actions.run_PSR_validfix; pupil_validfixation(opts,sessions); end

% Plot histogram of valid fixation percentages
if actions.run_PSR_plot_validfix; plotPSR_validfix(opts,sessions); end

% 3.3   Fix spike artefacts
if actions.run_PSR_fixspikes; pupil_fixspikes(opts,sessions); end

% 3.4   Segment data and check valid data
if actions.run_PSR_validtrials; pupil_validtrials(opts,sessions); end

% 3.5   Interpolate data for visualization purposes
opts = get_psychophys_options(); % update options in case valids subjects changed in the previous step
if actions.run_PSR_preproc_trace; pupil_preprocess_trace(opts,sessions); end

% 3.6   Plot pupil raw trace
if actions.run_PSR_plot_trace; pupil_plot_trace(opts,sessions); end
    
% 3.7   Plot pupil US response
if actions.run_PSR_plot_UR; plotPSR_USresponse(opts,sessions); end

% 3.8   Conditionwise GLM
if actions.run_PSR_GLM_cond; pupil_GLM_cond(opts,sessions); end
if actions.run_PSR_GLM_cond_save; pupil_GLM_cond_save(opts,sessions); end

% 3.9   Plot GLM results
if actions.run_PSR_GLM_plot; pupil_reconresp(opts,sessions); end
if actions.run_PSR_GLM_plot_trace; pupil_plot_trace_reconresp(opts,sessions); end

% 3.10  Trialwise GLM - not used
if actions.run_PSR_GLM_trial; pupil_GLM_trial(opts,sessions); end
if actions.run_PSR_GLM_trial_save; pupil_GLM_trial_save(opts,sessions); end

% 3.11  Full plot for the article
if actions.run_PSR_plot_article; pupil_plot_article(opts,sessions); end

%% 4. Pain ratings and task performance

% 4.1 Plot trial sequences
if actions.run_plot_trialseq; plot_trialsequence(opts,sessions); end

% 4.2 Plot accuracy and reaction times
if actions.run_pain_performance; analyzePainPerformance(opts,sessions); end

% 4.3. Plot US probability evaluation for CS
if actions.run_probability_eval; analyzeProbabilityEval(opts,sessions); end

%% 5. Statistical analysis
% Done in R