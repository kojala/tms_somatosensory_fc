function analyzePainPerformance(opts,sessions)

% Pain and performance
% Steps:
%  1. Plot shock intensity vs. subjective pain rating for all subjects
%  2. Count incorrect and missed responses and plot them for all subjects
%  3. Plot incorrect and missed responses summed -> overall performance
%  4. Find any subjects that have bad performance (> 35% incorrect/missed)
%__________________________________________________________________________

Bhvpath = opts.bhv.path;
painpath = fullfile(opts.expPath,'pain');

for ses = sessions
    
    s = 1;
    
    %     figure
    %     suptitle('Subjective pain rating vs. Actual shock intensity (mA)')
    %     hold on
    
%     if ses == 1 || ses == 2
%         sList = opts.scr.sList.sessions{ses};
%     else
        sList = opts.sebr.sList.orig;
%     end
    
    for sIDX = 1:numel(sList)
        
        if sList(sIDX) < 100
            fnamebhv  = fullfile(Bhvpath,['ss7b_0' num2str(sList(sIDX)) '_behav_' opts.sessionlist{ses} '.mat']);
        else
            fnamebhv  = fullfile(Bhvpath,['ss7b_' num2str(sList(sIDX)) '_behav_' opts.sessionlist{ses} '.mat']);
        end
        fnamepain = fullfile(painpath,['pain_' num2str(sList(sIDX)) '.mat']);
        
        % PAIN
        %         if exist(fnamepain,'file')
        %             paindata = load(fnamepain);
        %             data = paindata.data;
        %
        %             % Foot shock intensity vs. subjective pain rating
        %             %----------------------------------------------------------------------
        %             %subplotsize = subplotsize + 1;
        %             sqplots = ceil(sqrt(length(sList)));
        %             subplot(sqplots,sqplots,s)
        %             title(['Subject ' int2str(sList(sIDX))])
        %             scatter(data(:,2),data(:,1));hold on; myfit = polyfit(data(:,2),data(:,1),1); x = 0:100; y = myfit(1)*x+myfit(2);plot(x,y,'k');
        %             %xlabel('Subjective pain rating'); ylabel('Shock intensity (mA)');
        %
        %             % Correlating pre- and post-ratings
        %             %----------------------------------------------------------------------
        %             [B,I] = sort(data(:,2));
        %             preratings = [data(I) B]; % Pre-ratings ordered according to subjective rating
        %             % BUT no post-ratings exist for pain stimulus!
        %
        %         end
        
        %% PERFORMANCE
        
        if exist(fnamebhv,'file')
            
            if ses == 4 % training
                perfdata1 = load(fnamebhv);
                fnamebhv2 = fnamebhv;
                fnamebhv2(end-4) = '2';
                perfdata2 = load(fnamebhv2);
                Data = [perfdata1.Data; perfdata2.Data];
            else
                perfdata = load(fnamebhv);
                Data = perfdata.Data;
            end
            % Missed & wrong button presses
            %----------------------------------------------------------------------
            % Data: column 15 button presses, 0 = wrong, 1 = correct, 3 = no response
            
            sList2(s) = sList(sIDX);
            
            if ismember(sList(sIDX),opts.sebr.sList.control)
                group(s) = 1;
            elseif ismember(sList(sIDX),opts.sebr.sList.experim)
                group(s) = 2;
            end
            
            simpleIDX = Data(:,6)==0;
            complexIDX = Data(:,6)==1;
            CSpIDX = Data(:,3)==1;
            CSmIDX = Data(:,3)==2;
            
            resp = Data(:,15);
            resp_simple_CSp = resp(simpleIDX & CSpIDX);
            resp_simple_CSm = resp(simpleIDX & CSmIDX);
            resp_complex_CSp = resp(complexIDX & CSpIDX);
            resp_complex_CSm = resp(complexIDX & CSmIDX);
            %wrong(s,group) = sum(resp==0);
            wrong_simple_CSp(s) = sum(resp_simple_CSp==0);
            wrong_simple_CSm(s) = sum(resp_simple_CSm==0);
            wrong_complex_CSp(s) = sum(resp_complex_CSp==0);
            wrong_complex_CSm(s) = sum(resp_complex_CSm==0);
            %miss(s,group) = sum(resp==3);
            miss_simple_CSp(s) = sum(resp_simple_CSp==3);
            miss_simple_CSm(s) = sum(resp_simple_CSm==3);
            miss_complex_CSp(s) = sum(resp_complex_CSp==3);
            miss_complex_CSm(s) = sum(resp_complex_CSm==3);
            %wrongmiss(s,group) = wrong(s,group)+miss(s,group);
            
            accuracy(s,1) = 1-((miss_simple_CSp(s)+wrong_simple_CSp(s))./(length(Data(:,15))/4));
            accuracy(s,2) = 1-((miss_simple_CSm(s)+wrong_simple_CSm(s))./(length(Data(:,15))/4));
            accuracy(s,3) = 1-((miss_complex_CSp(s)+wrong_complex_CSp(s))./(length(Data(:,15))/4));
            accuracy(s,4) = 1-((miss_complex_CSm(s)+wrong_complex_CSm(s))./(length(Data(:,15))/4));
            
            RT = Data(:,14);
            tooshort(s) = sum(RT < 0.2)/length(RT)*100; % too short responses
            toolong(s) = sum(RT > 3.5)/length(RT)*100; % too long responses
            RT(RT < 0.2) = NaN; 
            RT(RT > 3.5) = NaN; % too short (< 200 ms) or too long (3.5 s) RT trials to NaN
            accuracy(RT < 0.2,:) = NaN; 
            accuracy(RT > 3.5,:) = NaN; % too short (< 200 ms) or too long (3.5 s) RT trials to NaN
            
            reaction_time(s,1) = nanmean(RT(simpleIDX & CSpIDX));
            reaction_time(s,2) = nanmean(RT(simpleIDX & CSmIDX));
            reaction_time(s,3) = nanmean(RT(complexIDX & CSpIDX));
            reaction_time(s,4) = nanmean(RT(complexIDX & CSmIDX));
            subs(s) = sList(s);
            
            s = s + 1;
        end
        
    end
    
    % save as csv for R
    sesname = opts.sessionlist{ses};
    Subjects = [1:length(subs) 1:length(subs) 1:length(subs) 1:length(subs)]';
    Group = [group group group group]';
    CSType = [ones(length(subs),1); zeros(length(subs),1); ones(length(subs),1); zeros(length(subs),1)]; 
    CSComplexity = [ones(length(subs),1); ones(length(subs),1); ones(length(subs),1)*2; ones(length(subs),1)*2]; 
    ReactionTime = reaction_time(:);
    Accuracy = accuracy(:);
    
    datatable.Subjects = Subjects;
    datatable.Group = Group;
    datatable.CSType = CSType;
    datatable.CSComplexity = CSComplexity;
    datatable.ReactionTime = ReactionTime;
    datatable.Accuracy = Accuracy;
    summarytablefile = fullfile(opts.expPath,['RTAccuracy_' sesname '_summarytable.mat']);
    save(summarytablefile,'datatable')
    
%     datatable = table(Subjects,Group,CSComplexity,ReactionTime,Accuracy);
%     summarytablefile = fullfile(opts.expPath,'RTAccuracy_summarytable.csv');
%     writetable(datatable,summarytablefile);

    % Plots
    
    %     figure,bar(wrong_simple); xlabel('Subject number'); ylim([0 15]);ylabel(' incorrect responses'); title('Incorrect button presses per subject SIMPLE');
    %     figure,bar(wrong_complex); xlabel('Subject number'); ylim([0 15]);ylabel(' incorrect responses'); title('Incorrect button presses per subject COMPLEX');
    %
    %     figure,bar(miss_simple); xlabel('Subject number'); ylim([0 15]);ylabel(' missing responses'); title('Missed button presses per subject SIMPLE');
    %     figure,bar(miss_complex); xlabel('Subject number'); ylim([0 15]);ylabel(' missing responses'); title('Missed button presses per subject COMPLEX');
    %
    %     figure,bar(wrongmiss); xlabel('Subject number'); ylim([0 100]);ylabel(' incorrect and missing responses'); title('Incorrect and missed button presses per subject');
    
    % Subjects with bad performance
    %badperf = sList2(wrongmiss > 25);
    %sprintf(['Subjects with bad performance (incorrect + missed responses > %d%%): ' num2str(numel(badperf))],35)
    
    %% Accuracy plots
    
    idx{1} = find(ismember(sList2,opts.sebr.sList.control));
    idx{2} = find(ismember(sList2,opts.sebr.sList.experim));
    groupname = {'Control' 'Experimental'};
    condnames = {'CS+' 'CS-' 'CS+' 'CS-'};
    
    markersize = 14;
    %barcolor = [169 169 169]; grey
    barcolor = [103,169,207; 189,201,225; 103,169,207; 189,201,225]./255; % greenish blues
    %scattercolor = [105 105 105]./255;
    scattercolor = [192 192 192]./255;
    linecolor = [105 105 105]./255;%[169 169 169]; %[211 211 211]; % light grey
    
    fig = figure('DefaultAxesFontSize', 12, 'DefaultAxesFontName', 'Arial', 'DefaultTextFontName', 'Arial');
    
    figwidth = 17.6; % 1/2 pdf column = 6, 1 pdf column = 8.5 cm, 1.5 pdf column = 11.6, 2 pdf columns = 17.6
    figheight = 6;
    
    set(fig, 'Units', 'Centimeters',...
        'PaperUnits', 'Centimeters', ...
        'PaperPositionMode','auto')
    
    set(fig,'Position', [0, 0, figwidth, figheight])
    
    set(gca,'LineWidth',1) % axes line width
    
    for group = 1:2
        
        accuracy_group = accuracy(idx{group},:);
        accuracy_group = accuracy_group*100;
        
        % Calculate within-subject error bars
        subavg = nanmean(accuracy_group,2); % mean over conditions for each sub
        grandavg = nanmean(subavg); % mean over subjects and conditions
        
        newvalues = nan(size(accuracy_group));
        
        % normalization of subject values
        for cond = 1:size(accuracy_group,2)
            meanremoved = accuracy_group(:,cond)-subavg; % remove mean of conditions from each condition value for each sub
            newvalues(:,cond) = meanremoved+repmat(grandavg,[length(idx{group}) 1 1]); % add grand average over subjects to the values where individual sub average was removed
            plotdata(:,cond) = nanmean(newvalues(:,cond));
        end
        
        %cond = cond-1; % remove the filler NaN condition
        
        newvar = (cond/(cond-1))*nanvar(newvalues);
        tvalue = tinv(1-0.05, length(idx{group})-1);
        errorbars = squeeze(tvalue*(sqrt(newvar)./sqrt(length(idx{group})))); % calculate error bars according to Cousineau (2005) with Morey (2005) fix

        % Plot data
        %plotdata = mean(accuracy);
        %clr = [59 100 173; 143 162 212; 59 100 173; 143 162 212]./255;
        
        % Bar graph with individual values
        subplot(1,2,group)
        for b = 1:length(plotdata)
            h = bar(b,plotdata(b),'LineWidth',1.5);
            hold on
            h.FaceColor = barcolor(b,:);
        end
        % Individual values
        subjects = size(accuracy_group,1);
        xdata = repmat(1:b,[subjects 1]);
        
        jitter_amount = 0.2;
        jittered_xdata = xdata + (rand(size(xdata))-0.5)*(2*jitter_amount);
        %plot(jittered_xdata',rating','-','Color',linecolor);
        hold on
        scatter_plot = scatter(jittered_xdata(:),accuracy_group(:), markersize, 'filled', 'MarkerFaceColor', scattercolor, 'MarkerEdgeColor', 'k');
        %scatter(xdata(:),accuracy(:), 'filled', 'MarkerFaceColor', [102 178 255]./255, 'MarkerEdgeColor', 'k', 'jitter', 'on', 'jitterAmount', 0.2);
        % Error bars
        errorbar(plotdata,errorbars,'k','LineStyle','none','LineWidth',2.5,'CapSize',0)
        title(groupname{group},'FontSize',12)
        set(gca,'xTick', 1:length(condnames),'FontSize',12)
        set(gca,'xTickLabel', condnames,'FontSize',12)
        if group == 1
            ylabel('Accuracy (% correct)','FontSize',12)
            xlabel('Simple')
        else
            xlabel('Complex')
        end
        ylim([0 100])
        set(gca,'yTick', 0:25:100,'FontSize',12)
        set(gca,'box','off')
        
    end
    
    PAPER = get(fig,'Position');
    set(gcf,'PaperSize',[PAPER(3), PAPER(4)]);
    savefig(gcf,fullfile(opts.expPath,'plots',['Accuracy_mean_' opts.sessionlist{ses} '.fig']));
    saveas(gcf,fullfile(opts.expPath,'plots',['Accuracy_mean_' opts.sessionlist{ses}]),'png')
    saveas(gcf,fullfile(opts.expPath,'plots',['Accuracy_mean_' opts.sessionlist{ses}]),'svg')
    
    
    %% Reaction time plots
    
    fig = figure('DefaultAxesFontSize', 12, 'DefaultAxesFontName', 'Arial', 'DefaultTextFontName', 'Arial');
    
    figwidth = 17.6; % 1/2 pdf column = 6, 1 pdf column = 8.5 cm, 1.5 pdf column = 11.6, 2 pdf columns = 17.6
    figheight = 6;
    
    set(fig, 'Units', 'Centimeters',...
        'PaperUnits', 'Centimeters', ...
        'PaperPositionMode','auto')
    
    set(fig,'Position', [0, 0, figwidth, figheight])
    
    set(gca,'LineWidth',1) % axes line width
    
    for group = 1:2
        
        reaction_time_group = reaction_time(idx{group},:);
        reaction_time_group = reaction_time_group*1000;
        
        % Calculate within-subject error bars
        subavg = nanmean(reaction_time_group,2); % mean over conditions for each sub
        grandavg = nanmean(subavg); % mean over subjects and conditions
        
        newvalues = nan(size(reaction_time_group));
        
        % normalization of subject values
        for cond = 1:size(reaction_time_group,2)
            meanremoved = reaction_time_group(:,cond)-subavg; % remove mean of conditions from each condition value for each sub
            newvalues(:,cond) = meanremoved+repmat(grandavg,[length(idx{group}) 1 1]); % add grand average over subjects to the values where individual sub average was removed
            plotdata(:,cond) = nanmean(newvalues(:,cond));
        end
        
        %cond = cond-1; % remove the filler NaN condition
        
        newvar = (cond/(cond-1))*nanvar(newvalues);
        tvalue = tinv(1-0.05, length(idx{group})-1);
        errorbars = squeeze(tvalue*(sqrt(newvar)./sqrt(length(idx{group})))); % calculate error bars according to Cousineau (2005) with Morey (2005) fix

        % Plot data
%         plotdata = plotdata*1000;
%         errorbars = errorbars*1000;
%         reaction_time_group = reaction_time_group*1000;
        %clr = [59 100 173; 143 162 212; 59 100 173; 143 162 212]./255;
        
        % Bar graph with individual values
        subplot(1,2,group)
        for b = 1:length(plotdata)
            h = bar(b,plotdata(b),'LineWidth',1.5);
            hold on
            h.FaceColor = barcolor(b,:);
        end
        % Individual values
        subjects = size(reaction_time_group,1);
        xdata = repmat(1:b,[subjects 1]);
        
        jitter_amount = 0.2;
        jittered_xdata = xdata + (rand(size(xdata))-0.5)*(2*jitter_amount);
        %plot(jittered_xdata',rating','-','Color',linecolor);
        hold on
        scatter_plot = scatter(jittered_xdata(:),reaction_time_group(:), markersize, 'filled', 'MarkerFaceColor', scattercolor, 'MarkerEdgeColor', 'k');
        %scatter(xdata(:),accuracy(:), 'filled', 'MarkerFaceColor', [102 178 255]./255, 'MarkerEdgeColor', 'k', 'jitter', 'on', 'jitterAmount', 0.2);
        % Error bars
        errorbar(plotdata,errorbars,'k','LineStyle','none','LineWidth',2.5,'CapSize',0)
        title(groupname{group},'FontSize',12)
        set(gca,'xTick', 1:length(condnames),'FontSize',12)
        set(gca,'xTickLabel', condnames,'FontSize',12)
        if group == 1
            ylabel('Reaction time (ms)','FontSize',12)
            xlabel('Simple')
        else
            xlabel('Complex')
        end
        set(gca,'yTick', 0:500:3000,'FontSize',12)
        ylim([0 3000])
        set(gca,'box','off')
        
    end
    
    PAPER = get(fig,'Position');
    set(gcf,'PaperSize',[PAPER(3), PAPER(4)]);
    savefig(gcf,fullfile(opts.expPath,'plots',['RT_mean_' opts.sessionlist{ses} '.fig']));
    saveas(gcf,fullfile(opts.expPath,'plots',['RT_mean_' opts.sessionlist{ses}]),'png')
    saveas(gcf,fullfile(opts.expPath,'plots',['RT_mean_' opts.sessionlist{ses}]),'svg')
end

end