function analyzeProbabilityEval(opts,sessions)

% Steps:
%  1. Plot probability evaluation at the end of session
%__________________________________________________________________________

Bhvpath = opts.bhv.path;

for ses = sessions
    
    
    %% Get data
    s = 1;
    
    if ses == 1 || ses == 3
        sList = opts.sebr.sList.orig;
        %sList = opts.scr.sList.sessions{ses};
    else
        sList = opts.sebr.sList.orig;
    end
    
    sesname = opts.sessionlist{ses};
    
    for sIDX = 1:numel(sList)
        
        if sList(sIDX) < 100
            fnamebhv  = fullfile(Bhvpath,['ss7b_0' num2str(sList(sIDX)) '_behav_' opts.sessionlist{ses} '.mat']);
        else
            fnamebhv  = fullfile(Bhvpath,['ss7b_' num2str(sList(sIDX)) '_behav_' opts.sessionlist{ses} '.mat']);
        end
       
        % Probability evaluation
        if exist(fnamebhv,'file')
            
            perfdata = load(fnamebhv);
            key = perfdata.Evaluation_probability{:}.key;
            % Keys 5 options: 97, 98, 99, 100, 101 (Numpad 1-5)
            % Correspond to probabilities: 0, 25, 50, 75, 100
            key_p(key==97) = 0;
            key_p(key==98) = 0.25;
            key_p(key==99) = 0.5;
            key_p(key==100) = 0.75;
            key_p(key==101) = 1;
            stim = perfdata.Evaluation_probability{:}.stim;
            % Stimuli 1-4: D1 simple, D2 simple, D1->D2 complex, D2->D1 complex
            % Depends on participant's stimulus order whether CS+ or CS-
            
            % Retrieve stimulus order
            order = perfdata.header.som.order;
            
            % Reorder responses by stimulus order
            % 1: CSs+, 2: CSs-, 3: CSc+, 4: CSc-
            [stim_ordered, idx] = sort(order);
            resp = key_p(idx);
            
            if ismember(sList(sIDX),opts.sebr.sList.control)
                group(s) = 1;
            elseif ismember(sList(sIDX),opts.sebr.sList.experim)
                group(s) = 2;
            else
                group(s) = NaN;
            end
            
            subs(s) = sIDX;
            sList2(s) = sList(sIDX);
            s = s + 1;
            
            prob_eval(sIDX,:) = resp;
            
        end
        
    end
    
%     subs = subs(~isnan(group));
%     sList2 = sList2(~isnan(group));
%     prob_eval = prob_eval(~isnan(group));
%     group = group(~isnan(group));
    
    % Save data
    summarytablefile = fullfile(opts.expPath,['ProbEval_' sesname '_summarytable.mat']);
    summarytablefile2 = fullfile(opts.expPath,['ProbEval_' sesname '_summarytable_JASP.mat']);
    
    % save data in format suitable for importing into R
    % long format of data
    Subjects = [subs subs subs subs]';
    Group = [group group group group]';
    CSType = [ones(length(subs),1); zeros(length(subs),1); ones(length(subs),1); zeros(length(subs),1);];
    CSComplexity = [zeros(length(subs)*2,1); ones(length(subs)*2,1);];
    ProbabilityEval = prob_eval(:);

    %datatable = table(Subjects,Group,CSType,CSComplexity,ProbabilityEval);
    
    datatable.Subjects = Subjects;
    datatable.Group = Group;
    datatable.CSType = CSType;
    datatable.CSComplexity = CSComplexity;
    datatable.ProbabilityEval = ProbabilityEval;
    save(summarytablefile,'datatable')
    %writetable(datatable,summarytablefile);
    
    % save data for JASP
    % wide format
    Subjects = subs';
    Group = group';
    ProbEval_CSps = prob_eval(:,1);
    ProbEval_CSms = prob_eval(:,2);
    ProbEval_CSpc = prob_eval(:,3);
    ProbEval_CSmc = prob_eval(:,4);
    
    datatable2 = table(Subjects,Group,ProbEval_CSps,ProbEval_CSms,ProbEval_CSpc,ProbEval_CSmc);
    
    %writetable(datatable2,summarytablefile2);
    
    %% Plots
    
    clear idx
    idx{1} = find(ismember(sList2,opts.scr.sList.control));
    idx{2} = find(ismember(sList2,opts.scr.sList.experim));
    groupname = {'Control' 'Experimental'};
    condnames = {'CS+' 'CS-' 'CS+' 'CS-'};
    probnames = {'0%' '25%' '50%' '75%' '100%'};
    
    markersize = 14;
    %barcolor = [169 169 169]; grey
    barcolor = [103,169,207; 189,201,225; 103,169,207; 189,201,225]./255; % greenish blues
    %scattercolor = [105 105 105]./255;
    scattercolor = [192 192 192]./255;
    linecolor = [105 105 105]./255;%[169 169 169]; %[211 211 211]; % light grey
    
    %% Groups separately

%     for group = 1:2
%         
%         clear rating
%         
%         rating = prob_eval(idx{group},:);
%         
%         % Calculate within-subject error bars
%         subavg = nanmean(rating,2); % mean over conditions for each sub
%         grandavg = nanmean(subavg); % mean over subjects and conditions
%         
%         newvalues = nan(size(rating));
%         
%         % normalization of subject values
%         for cond = 1:size(rating,2)
%             meanremoved = rating(:,cond)-subavg; % remove mean of conditions from each condition value for each sub
%             newvalues(:,cond) = meanremoved+repmat(grandavg,[length(idx{group}) 1 1]); % add grand average over subjects to the values where individual sub average was removed
%             plotdata(:,cond) = nanmean(newvalues(:,cond));
%         end
%         
%         %cond = cond-1; % remove the filler NaN condition
%         
%         newvar = (cond/(cond-1))*nanvar(newvalues);
%         tvalue = tinv(1-0.05, length(idx{group})-1);
%         errorbars = squeeze(tvalue*(sqrt(newvar)./sqrt(length(idx{group})))); % calculate error bars according to Cousineau (2005) with Morey (2005) fix
% 
%         % Plot data
%         %plotdata = mean(accuracy);
%         %clr = [59 100 173; 143 162 212; 59 100 173; 143 162 212]./255;
%         
%         % Bar graph with individual values
%         fig = figure('DefaultAxesFontSize', 12, 'DefaultAxesFontName', 'Arial', 'DefaultTextFontName', 'Arial');
%         
%         figwidth = 11.5; % 1/2 pdf column = 6, 1 pdf column = 8.5 cm, 1.5 pdf column = 11.6, 2 pdf columns = 17.6
%         figheight = 8;
%         
%         set(fig, 'Units', 'Centimeters',...
%             'PaperUnits', 'Centimeters', ...
%             'PaperPositionMode','auto')
%         
%         set(fig,'Position', [0, 0, figwidth, figheight])
%         
%         set(gca,'LineWidth',1) % axes line width
%         hold on
%         for b = 1:length(plotdata)
%             h = bar(b,plotdata(b),'LineWidth',1.5);
%             h.FaceColor = barcolor(b,:);
%         end
%         % Individual values
%         rating = rating.*100;
%         subjects = size(rating,1);
%         xdata = repmat(1:b,[subjects 1]);
%         scatter(xdata(:),rating(:), 'filled', 'MarkerFaceColor', scattercolor, 'MarkerSize', markersize, 'MarkerEdgeColor', linecolor, 'jitter', 'on', 'jitterAmount', 0.2);
%         % Error bars
%         errorbar(plotdata,errorbars,'k','LineStyle','none','LineWidth',2.5,'CapSize',0)
%         title(['Probability ratings ' groupname{group} ' group, N = ' num2str(length(idx{group}))],'FontSize',12)
%         set(gca,'xTick', 1:length(condnames),'FontSize',12)
%         set(gca,'xTickLabel', condnames,'FontSize',12)
%         set(gca,'YTick', 0:25:100,'FontSize',12)
%         set(gca,'yTickLabel', probnames,'FontSize',12)
%         ylabel('Evaluated US probability','FontSize',12)
%         ylim([0 100])
%         %savefig(fullfile(opts.expPath,'plots',['ProbEval_mean_' opts.sessionlist{ses} '_' groupname{group} '_simple-complex.fig']));
%         
%     end
    
    %% Groups same plot
    
    fig = figure('DefaultAxesFontSize', 12, 'DefaultAxesFontName', 'Arial', 'DefaultTextFontName', 'Arial');
    
    figwidth = 17.6; % 1/2 pdf column = 6, 1 pdf column = 8.5 cm, 1.5 pdf column = 11.6, 2 pdf columns = 17.6
    figheight = 6;
    
    set(fig, 'Units', 'Centimeters',...
        'PaperUnits', 'Centimeters', ...
        'PaperPositionMode','auto')
    
    set(fig,'Position', [0, 0, figwidth, figheight])
    
    set(gca,'LineWidth',1) % axes line width
    
    for group = 1:2
        
        clear rating
        
        rating = prob_eval(idx{group},:);
        
        % Calculate within-subject error bars
        subavg = nanmean(rating,2); % mean over conditions for each sub
        grandavg = nanmean(subavg); % mean over subjects and conditions
        
        newvalues = nan(size(rating));
        
        % normalization of subject values
        for cond = 1:size(rating,2)
            meanremoved = rating(:,cond)-subavg; % remove mean of conditions from each condition value for each sub
            newvalues(:,cond) = meanremoved+repmat(grandavg,[length(idx{group}) 1 1]); % add grand average over subjects to the values where individual sub average was removed
            plotdata(:,cond) = nanmean(newvalues(:,cond));
        end
        
        %cond = cond-1; % remove the filler NaN condition
        
        newvar = (cond/(cond-1))*nanvar(newvalues);
        tvalue = tinv(1-0.05, length(idx{group})-1);
        errorbars = squeeze(tvalue*(sqrt(newvar)./sqrt(length(idx{group})))); % calculate error bars according to Cousineau (2005) with Morey (2005) fix

        % Plot data
        %plotdata = mean(accuracy);
        %clr = [59 100 173; 143 162 212; 59 100 173; 143 162 212]./255;
        
        plotdata = plotdata.*100;
        errorbars = errorbars.*100;
        
        % Bar graph with individual values
        subplot(1,2,group)
        for b = 1:length(plotdata)
            h = bar(b,plotdata(b),'LineWidth',1.5);
            hold on
            h.FaceColor = barcolor(b,:);
        end
        hold on
        % Individual values
        rating = rating.*100;
        subjects = size(rating,1);
        xdata = repmat(1:b,[subjects 1]);
        
        jitter_amount = 0.2;
        jittered_xdata = xdata + (rand(size(xdata))-0.5)*(2*jitter_amount);
        %plot(jittered_xdata',rating','-','Color',linecolor);
        hold on
        scatter_plot = scatter(jittered_xdata(:),rating(:), markersize, 'filled', 'MarkerFaceColor', scattercolor, 'MarkerEdgeColor', 'k');

        %scatter(xdata(:),rating(:), markersize, 'filled', 'MarkerFaceColor', scattercolor, 'MarkerEdgeColor', linecolor, 'jitter', 'on', 'jitterAmount', 0.2);
        % Error bars
        errorbar(plotdata,errorbars,'k','LineStyle','none','LineWidth',2.5,'CapSize',0)
        title(groupname{group},'FontSize',12)
        set(gca,'xTick', 1:length(condnames),'FontSize',12)
        set(gca,'xTickLabel', condnames,'FontSize',12)
        set(gca,'YTick', 0:25:100,'FontSize',12)
        set(gca,'yTickLabel', probnames,'FontSize',12)
        if group == 1
            ylabel('Subjective US probability','FontSize',12)
            xlabel('Simple')
        else
            xlabel('Complex')
        end
        ylim([0 100])
        set(gca,'box','off')

    end
    
    PAPER = get(fig,'Position');
    set(gcf,'PaperSize',[PAPER(3), PAPER(4)]);
    savefig(gcf,fullfile(opts.expPath,'plots',['ProbEval_mean_' opts.sessionlist{ses} '.fig']));
    saveas(gcf,fullfile(opts.expPath,'plots',['ProbEval_mean_' opts.sessionlist{ses}]),'png')
    saveas(gcf,fullfile(opts.expPath,'plots',['ProbEval_mean_' opts.sessionlist{ses}]),'svg')
    
end

end