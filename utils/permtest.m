function pcluster = permtest(Fstat, pstat, Fstatperm, pstatperm)
% function pcluster = permtest(Fstat, pstat, Fstatperm, pstatperm)
% Dominik R Bach
% last edited 12.01.2017
% adapted for 2D testing 14.03.2017

% stack true over permutation models
Fall = [Fstat, Fstatperm]; 
pall = [pstat, pstatperm];
% loop over and permutations
for k = 1:size(Fall, 2)
    % find all supra-threshold clusters and define sum F-value
    L = bwlabeln(pall(:, k) < .05);
    for c = 1:max(unique(L)) % loop over found clusters
        Fsum(c) = sum(Fall(L == c, k)); % take the sum of the cluster's F-values
    end;
    if ~isempty(c)
       [Fsum, clusterindx] = sort(Fsum(:), 1, 'descend');
        % on pass 1, store all values
        % cluster F-values relating to original correctly labeled data
        if k == 1
            Fcluster = Fsum;
            Fselect = L;
            Findx = clusterindx;
        else % otherwise save the largest cluster F-value for each permutation (from randomly shuffled labels data)
            Fclustermax(k-1) = Fsum(1);
        end;
    elseif k == 1
        Fselect = L;
        break
    else
        Fclustermax(k-1) = 0;
    end;
end

% at the end, assess cluster-level significance
% -1: below inclusion threshold, > 0: "exact" value, 0: significant
pcluster = -1 * ones(size(Fstat));
for c = 1:max(unique(Fselect))
    pcluster(Fselect == Findx(c)) = (sum(Fcluster(c) < Fclustermax)/numel(Fclustermax));
    % should change: sum(Fcluster(c) < Fclustermax) to 1 if 0?

    % for each found cluster
    % divide (the number of maximum F-values from all permutations that exceed
    % the cluster's original labels maximum F-value) with the number of permutations
end
