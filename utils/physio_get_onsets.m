function [onsets, onsets_all, conditions] = physio_get_onsets(ses,splitfn,eventfile,splitind)
    
% Load data
[~,~,data] = pspm_load_data(splitfn,'events');
markers = data{1}.data;

sesind = (splitind-length(markers)):splitind-1;

bhvdata = load(eventfile);
sesdata = bhvdata.Data(sesind,:);

if ses == 1
    sub = str2double(eventfile(end-24:end-22));
elseif ses == 2
    sub = str2double(eventfile(end-22:end-20));
else
    sub = str2double(eventfile(end-19:end-17));
end

if ses == 3 && length(markers) < 12 && sub == 98
    missingmarker = sesdata(7,8);
    markers = [markers(1:6); missingmarker-0.003; markers(7:end)];
end

US = sesdata(:,4);
CSPrediction = sesdata(:,3);
%CSComplexity = sesdata(:,6);

CS = nan(length(US),1);
% CS(CSPrediction == 2 & CSComplexity == 0) = 1; % Simple CS-
% CS(CSPrediction == 1 & CSComplexity == 0) = 2; % Simple CS+
% CS(CSPrediction == 2 & CSComplexity == 1) = 3; % Complex CS-
% CS(CSPrediction == 1 & CSComplexity == 1) = 4; % Complex CS+
CS(CSPrediction == 1) = 1; % CS+
CS(CSPrediction == 2) = 0; % CS-

markers_cs1 = markers(CS == 1); % CS+
markers_cs2 = markers(CS == 0); % CS-
% markers_cs3 = markers(CS == 3); 
% markers_cs4 = markers(CS == 4); 

%onsets = {markers_cs1; markers_cs2; markers_cs3; markers_cs4};
onsets = {markers_cs1; markers_cs2};
onsets_all = markers;
conditions{1} = CS;
conditions{2} = US;

end