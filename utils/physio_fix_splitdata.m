function importedfn = physio_fix_splitdata(fnames)
% Some subjects have a stop in LabChart file
% This results in two imported PsPM files
% Need to select the correct one, the second file

[fp,name,ext] = fileparts(fnames{1});

importedfn = fullfile(fp,[name(1:end-6) ext]); % fix
part1_fn = fnames{1};
part2_fn = fnames{2};
movefile(part2_fn,importedfn);
delete(part1_fn);

end