function psychophys_rename_copy_training(opts)

datapath = fullfile(opts.expPath,'data');

sessions_labchart = {'training'};
sessions_cog = {'training'};
sessions_eyelink = {'t'};
sessions_names = {sessions_labchart sessions_cog sessions_eyelink};
sessions_new = {'training1' 'training2'};
labchart_dir = fullfile(datapath,'labchart','labchartmat','training');
cogent_dir = fullfile(datapath,'datainfo','training');
eyelink_dir = fullfile(datapath,'eyelink','asc','training');
datadirs = {labchart_dir cogent_dir eyelink_dir};
% 1 = LabChart, 2 = Cogent, 3 = EyeLink
datatype = {'physio' 'behav' 'eyelink'};

for datadir = 2:length(datadirs)-1 % excluding eyelink for now
    
    cd(datadirs{datadir})
    newdir = fullfile(pwd,'..','renamed');
    if ~exist(newdir,'dir'); mkdir(newdir); end
    
    datatypeses = sessions_names{datadir};
    
    if datadir == 1; filelist = ls(['*' datatypeses{:} '*']);
    elseif datadir == 2; filelist = ls([datatypeses{:} '*']);
    elseif datadir == 3; filelist = ls([datatypeses{:} '*s*.asc']);
        %elseif datadir == 3; filelist = ls(['ss7b*' datatypeses{ses}
        %'asc']); % doesn't work, does all sessions for eyelink data
    end
    
    for f = 1:length(filelist)
        
        oldname = strtrim(filelist(f,:));
        sourcefile = strtrim(fullfile(pwd,oldname));
        
        rename = 0;
        
        if datadir == 1 % LabChart
            subnum = oldname(1:3);
            rename = 1;
        elseif datadir == 2 % Cogent
            subnum = oldname(end-8:end-6); if isempty(str2num(subnum)); subnum = ['0' oldname(end-7:end-6)]; end
            if contains(oldname,'training1'); rename = 1; elseif contains(oldname,'training2'); rename = 2; end % which training session
        elseif datadir == 3 % EyeLink
            subnum = oldname(end-4:end-2); if isempty(str2num(subnum)); subnum = ['0' oldname(end-3:end-2)]; end
            if contains(oldname,'t1'); rename = 1; elseif contains(oldname,'t2'); rename = 2; end % which training session
        end
        
        if rename > 0
            newname = ['ss7b_' subnum '_' datatype{datadir} '_' sessions_new{rename} sourcefile(end-3:end)];
            destfile = fullfile(newdir,newname);
            copyfile(sourcefile,destfile);
        end
    end
    
end

end