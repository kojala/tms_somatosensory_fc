function psychophys_rename_copy(opts,sessions)

datapath = fullfile(opts.expPath,'data');

sessions_labchart = {'experiment' 'retention' 'retest' 'training'};
sessions_cog = {'ss7b' 'retention_ss7b' 'retest_ss7b' 'training1_ss7b' 'training2_ss7b'};
sessions_eyelink = {'.' 'r.' 'n.'};
sessions_names = {sessions_labchart sessions_cog sessions_eyelink};
sessions_new = {'acquisition' 'retention' 'retest' 'training1' 'training2'};
labchart_dir = fullfile(datapath,'labchart','labchartmat');
cogent_dir = fullfile(datapath,'datainfo');
eyelink_dir = fullfile(datapath,'eyelink','asc');
datadirs = {labchart_dir cogent_dir eyelink_dir};
% 1 = LabChart, 2 = Cogent, 3 = EyeLink
datatype = {'physio' 'behav' 'eyelink'};

for datadir = 1:length(datadirs)
    
    cd(datadirs{datadir})
    newdir = fullfile(pwd,'renamed');
    if ~exist(newdir,'dir'); mkdir(newdir); end
    
    for ses = sessions
        
        datatypeses = sessions_names{datadir};
        
        if datadir == 1; filelist = ls(['*' datatypeses{ses} '*']);
        elseif datadir == 2; filelist = ls([datatypeses{ses} '*']);
        elseif datadir == 3; filelist = ls('ss7b*.asc');
        %elseif datadir == 3; filelist = ls(['ss7b*' datatypeses{ses}
        %'asc']); % doesn't work, does all sessions for eyelink data
        end
        
        for f = 1:length(filelist)
            
            oldname = strtrim(filelist(f,:));
            sourcefile = strtrim(fullfile(pwd,oldname));
            
            rename = 0;
            
            if datadir == 1 % LabChart
                subnum = oldname(1:3); 
                rename = 1;
            elseif datadir == 2 % Cogent
                subnum = oldname(end-6:end-4); if isempty(str2num(subnum)); subnum = ['0' oldname(end-5:end-4)]; end
                rename = 1;
            elseif datadir == 3 % EyeLink
                if length(oldname) == 10 % acquisition, subno < 100
                    subnum = ['0' oldname(5:6)];
                    if ses == 1; rename = 1; else; rename = 0; end
                elseif length(oldname) == 11 && ~contains(oldname,'r') && ~contains(oldname,'n') % acquisition, subno => 100
                    subnum = oldname(5:7); 
                    if ses == 1; rename = 1; else; rename = 0; end
                elseif length(oldname) == 11 && contains(oldname,'r') % retention, subnum < 100
                    subnum = ['0' oldname(5:6)];
                    if ses == 2; rename = 1; else; rename = 0; end
                elseif length(oldname) == 11 && contains(oldname,'n') % retest, subnum < 100
                    subnum = ['0' oldname(5:6)];
                    if ses == 3; rename = 1; else; rename = 0; end
                elseif length(oldname) == 12 && contains(oldname,'r') % retention, subnum => 100
                    subnum = oldname(5:7); 
                    if ses == 2; rename = 1; else; rename = 0; end
                elseif length(oldname) == 12 && contains(oldname,'n') % retest, subnum => 100
                    subnum = oldname(5:7); 
                    if ses == 3; rename = 1; else; rename = 0; end
                end
            end
            
            if rename
                newname = ['ss7b_' subnum '_' datatype{datadir} '_' sessions_new{ses} sourcefile(end-3:end)];
                destfile = fullfile(newdir,newname);
                copyfile(sourcefile,destfile);
            end
        end
        
    end
    
end

end