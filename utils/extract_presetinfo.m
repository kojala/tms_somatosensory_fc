clear all

datapath = 'D:\TMS_Experiment\Psychophysiology\data\datainfo';

sList = 1:160;
n = 0;

for s = 1:length(sList)
    
    datafile = fullfile(datapath,['ss7b' num2str(s) '.mat']);
    
    if exist(datafile,'file')
   
        n = n+1; % count of existing subjects
        
        load(datafile)
        
        preset(n,1) = sList(s);
        preset(n,2) = header.options.cond1; % which patterns CS+ and CS-
        if ismember(9,header.options.pattidx); preset(n,3) = 1; % which stimuli used
        else preset(n,3) = 2; end
        preset(n,4) = header.options.cond2; % keys (left/right) for CS+ and CS-
        preset(n,5) = header.options.cond3; % background color (grey/lilac)
        preset(n,6) = header.options.cond4; % order of complexity blocks
        
    end
    
end

%save(['presetinfo_' n 'subs.mat'],'preset')