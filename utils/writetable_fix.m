load('D:\fMRI_Karita\fMRIdata_biascorrected\ROIanalysis\Version_11062018\MeanBetas\MeanBetas_LongFormat_allPE_funcROIs.mat')

ROI = data.ROI;
Condition = data.condition;
Subject = data.subject;
Expectation = data.Expectation;
Outcome = data.Outcome;
FullPE = data.fullPE;
PosPE = data.posPE;
NegPE = data.negPE;
AbsPE = data.absPE;
Beta = data.betas;

table2write = table(ROI,Condition,Subject,Outcome,Expectation,FullPE,PosPE,NegPE,AbsPE,Beta);

summarytablefile = fullfile('D:\fMRI_Karita\fMRIdata_biascorrected\ROIanalysis\Version_11062018\MeanBetas\MeanBetas_LongFormat_allPE_funcROIs.csv');
writetable(table2write,summarytablefile);

load('D:\fMRI_Karita\fMRIdata_biascorrected\ROIanalysis\Version_11062018\MeanBetas\MeanBetas_LongFormat_allPE_anatROIs.mat')

ROI = data.ROI;
Condition = data.condition;
Subject = data.subject;
Expectation = data.Expectation;
Outcome = data.Outcome;
FullPE = data.fullPE;
PosPE = data.posPE;
NegPE = data.negPE;
AbsPE = data.absPE;
Beta = data.betas;

table2write = table(ROI,Condition,Subject,Outcome,Expectation,FullPE,PosPE,NegPE,AbsPE,Beta);

summarytablefile = fullfile('D:\fMRI_Karita\fMRIdata_biascorrected\ROIanalysis\Version_11062018\MeanBetas\MeanBetas_LongFormat_allPE_anatROIs.csv');
writetable(table2write,summarytablefile);


load('D:\fMRI_Karita\fMRIdata_biascorrected\ROIanalysis\Version_11062018\MeanBetas\MeanBetas_LongFormat_unsigPE1.mat')

Subject = data.subject;
US = data.US;
Condition = data.condition;
Beta = data.betas;

table2write = table(Subject,US,Condition,Beta);

summarytablefile = fullfile('D:\fMRI_Karita\fMRIdata_biascorrected\ROIanalysis\Version_11062018\MeanBetas\MeanBetas_LongFormat_unsigPE1.csv');
writetable(table2write,summarytablefile);



load('D:\TMS_Experiment\Psychophysiology\ProbEval_acquisition_summarytable.mat')

Subjects = datatable.Subjects;
Group = datatable.Group;
CSType = datatable.CSType;
CSComplexity = datatable.CSComplexity;
ProbabilityEval = datatable.ProbabilityEval;

table2write = table(Subjects,Group,CSType,CSComplexity,ProbabilityEval);

summarytablefile = fullfile('D:\TMS_Experiment\Psychophysiology','ProbEval_acquisition_summarytable.csv');
 
writetable(table2write,summarytablefile);


load('D:\TMS_Experiment\Psychophysiology\ProbEval_retest_summarytable.mat')

Subjects = datatable.Subjects;
Group = datatable.Group;
CSType = datatable.CSType;
CSComplexity = datatable.CSComplexity;
ProbabilityEval = datatable.ProbabilityEval;

table2write = table(Subjects,Group,CSType,CSComplexity,ProbabilityEval);

summarytablefile = fullfile('D:\TMS_Experiment\Psychophysiology','ProbEval_retest_summarytable.csv');
 
writetable(table2write,summarytablefile);



load('D:\TMS_Experiment\Psychophysiology\RTAccuracy_acquisition_summarytable.mat')

Subjects = datatable.Subjects;
Group = datatable.Group;
CSType = datatable.CSType;
CSComplexity = datatable.CSComplexity;
ReactionTime = datatable.ReactionTime;
Accuracy = datatable.Accuracy;

table2write = table(Subjects,Group,CSType,CSComplexity,ReactionTime,Accuracy);

summarytablefile = fullfile('D:\TMS_Experiment\Psychophysiology','RTAccuracy_acquisition_summarytable.csv');
 
writetable(table2write,summarytablefile);




load('D:\TMS_Experiment\Psychophysiology\RTAccuracy_retention_summarytable.mat')

Subjects = datatable.Subjects;
Group = datatable.Group;
CSType = datatable.CSType;
CSComplexity = datatable.CSComplexity;
ReactionTime = datatable.ReactionTime;
Accuracy = datatable.Accuracy;

table2write = table(Subjects,CSType,Group,CSComplexity,ReactionTime,Accuracy);

summarytablefile = fullfile('D:\TMS_Experiment\Psychophysiology','RTAccuracy_retention_summarytable.csv');
 
writetable(table2write,summarytablefile);




load('D:\TMS_Experiment\Psychophysiology\RTAccuracy_retest_summarytable.mat')

Subjects = datatable.Subjects;
Group = datatable.Group;
CSType = datatable.CSType;
CSComplexity = datatable.CSComplexity;
ReactionTime = datatable.ReactionTime;
Accuracy = datatable.Accuracy;

table2write = table(Subjects,Group,CSType,CSComplexity,ReactionTime,Accuracy);

summarytablefile = fullfile('D:\TMS_Experiment\Psychophysiology','RTAccuracy_retest_summarytable.csv');
 
writetable(table2write,summarytablefile);


load('D:\TMS_Experiment\Psychophysiology\models\SEBR\GLM_trial\GLM_trial_summarytable.mat')

Subjects = datatable.Subjects;
Group = datatable.Group;
CSType = datatable.CSType;
CSComplexity = datatable.CSComplexity;
StartleAmplitude = datatable.StartleAmplitude;

table2write = table(Subjects,CSType,Group,CSComplexity,StartleAmplitude);

summarytablefile = fullfile('D:\TMS_Experiment\Psychophysiology\models\SEBR\GLM_trial\GLM_trial_summarytable_new.csv');
 
writetable(table2write,summarytablefile);


load('D:\TMS_Experiment\Psychophysiology\models\SEBR\GLM_trial\GLM_trial_summarytable.mat')

Subject = datatable.Subject;
Group = datatable.Group;
Trial = datatable.Trial;
CSType = datatable.CSType;
CSComplexity = datatable.CSComplexity;
StartleAmplitude = datatable.StartleAmplitude;

table2write = table(Subject,Group,Trial,CSType,CSComplexity,StartleAmplitude);

summarytablefile = fullfile('D:\TMS_Experiment\Psychophysiology\models\SEBR\GLM_trial\GLM_trial_summarytable.csv');
 
writetable(table2write,summarytablefile);