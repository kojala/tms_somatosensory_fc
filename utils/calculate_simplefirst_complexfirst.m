opts = get_psychophys_options();

% SCR acquisition
perc_complexfirst_scr1 = length(opts.scr.sList.DCM_complexfirst{1})/length(opts.scr.sList.DCM{1});
perc_simplefirst_scr1 = length(opts.scr.sList.DCM_simplefirst{1})/length(opts.scr.sList.DCM{1});

% PSR acquisition
complexfirst = intersect(opts.psr.sList.included{1},opts.psr.sList.complex_first);
simplefirst = intersect(opts.psr.sList.included{1},opts.psr.sList.simple_first);

perc_complexfirst_psr1 = length(complexfirst)/length(opts.psr.sList.included{1});
perc_simplefirst_psr1 = length(simplefirst)/length(opts.psr.sList.included{1});


% SEBR retention
perc_complexfirst_sebr = length(opts.sebr.sList.complex_first)/length(opts.sebr.sList.allsubs);
perc_simplefirst_sebr = length(opts.sebr.sList.simple_first)/length(opts.sebr.sList.allsubs);

% SCR retest
perc_complexfirst_scr3 = length(opts.scr.sList.DCM_complexfirst{3})/length(opts.scr.sList.DCM{3});
perc_simplefirst_scr3 = length(opts.scr.sList.DCM_simplefirst{3})/length(opts.scr.sList.DCM{3});

% PSR retest
complexfirst = intersect(opts.psr.sList.included{3},opts.psr.sList.complex_first);
simplefirst = intersect(opts.psr.sList.included{3},opts.psr.sList.simple_first);

perc_complexfirst_psr3 = length(complexfirst)/length(opts.psr.sList.included{3});
perc_simplefirst_psr3 = length(simplefirst)/length(opts.psr.sList.included{3});

average_complexfirst = mean([perc_complexfirst_scr1 perc_complexfirst_psr1 perc_complexfirst_sebr perc_complexfirst_scr3 perc_complexfirst_psr3]);
average_simplefirst = mean([perc_simplefirst_scr1 perc_simplefirst_psr1 perc_simplefirst_sebr perc_simplefirst_scr3 perc_simplefirst_psr3]);