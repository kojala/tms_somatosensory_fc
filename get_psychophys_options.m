function opts = get_psychophys_options()

%% General
opts.pspmPath = 'C:\Data\Toolboxes\PsPM\pspm-v5.0.0-changed-IR';
% opts.pspmPath = 'C:\Data\Toolboxes\PsPM\pspm-v4.1.1';
% opts.pspmPath = 'C:\Data\Toolboxes\PsPM\pspm-v4.3.0';
%opts.pspmPath = 'D:\PsPM\pspm-svn-r734-trunk';
opts.expPath = 'C:\Data\TMS_Experiment\Psychophysiology';
opts.codePath = fullfile(opts.expPath,'scripts','tms_somatosensory_fc');
opts.bhv.path = fullfile(opts.expPath,'data','datainfo','renamed');

opts.soa = 3.5;
opts.sessionlist = {'acquisition' 'retention' 'retest' 'training1'};
opts.sessiontrials = [96 24 96 24];
opts.groupnames = {'control' 'experimental'};

%% SEBR
opts.sebr.trimstart = -1;
opts.sebr.trimend = 5;
opts.sebr.channel.emg = 1;
opts.sebr.channel.sound = 2;
opts.sebr.channel.marker = 3;
opts.sebr.channel.preproc = 4;

opts.sebr.path.raw = fullfile(opts.expPath,'data','labchart','labchartmat','renamed');
opts.sebr.path.import = fullfile(opts.expPath,'data','labchart','imported');
opts.sebr.path.trim = fullfile(opts.expPath,'data','labchart','trimmed');
opts.sebr.path.model = fullfile(opts.expPath,'models','SEBR');

opts.sebr.sList.orig = [21 22 24 25 35 43 45 46 49 51 53 57 58 65 67 68 69 71 75 ...
    78 79 80 81 83 84 85 87 88 89 91 94 95 99 103 106 107 113 114 115 117 ...
    120 121	123	125	126	129	132	133	134	137	138	139];
opts.sebr.sList.problem = [28 41 42 48 61 84 88 90 92 96 98 100];
opts.sebr.sList.control = [21 25 35 43 46 49 53 69 71 78 79 80 83 84 88 91 ...
    94 95 106 113 114 115 120 126 129 134 137]; % control group subs included
opts.sebr.sList.experim = [22 24 45 51 57 58 65 67 68 75 81 85 87 89 99 103 ...
    107 117 121 123 125 132 133 138 139]; % experimental group subs included
opts.sebr.sList.allsubs = [opts.sebr.sList.control opts.sebr.sList.experim];
opts.sebr.sList.sameorder = [22 43 53 57 58 65 67 68 69 71 75 78 79 80 81 ...
    83 84 85 87 88 89 91 94 95 103 106 107 114 120 123]; % subs with order 2 1 1 1 1 or 2 1 1 2 1 (bg color doesn't matter)
opts.sebr.sList.control_sameorder = intersect(opts.sebr.sList.control,opts.sebr.sList.sameorder);
opts.sebr.sList.complex_first = [22 25 43 46 51 53 57 58 65 67 68 69 71 75 78 ...
    79 80 81 83 84 85 87 88 89 91 94 95 99 103 106 107 113 114 115 117 120 ...
    121 123 125 126 129 132 133 134 137]; % complex stimuli first
opts.sebr.sList.simple_first = [21 24 35 45 49 138 139]; % simple stimuli first

opts.sebr.sList.complex_first_control = opts.sebr.sList.complex_first(ismember(opts.sebr.sList.complex_first,opts.sebr.sList.control));
opts.sebr.sList.complex_first_experim = opts.sebr.sList.complex_first(ismember(opts.sebr.sList.complex_first,opts.sebr.sList.experim));
opts.sebr.sList.simple_first_control = opts.sebr.sList.simple_first(ismember(opts.sebr.sList.simple_first,opts.sebr.sList.control));
opts.sebr.sList.simple_first_experim = opts.sebr.sList.simple_first(ismember(opts.sebr.sList.simple_first,opts.sebr.sList.experim));

opts.sebr.sessionlist = {'retention'};

opts.sebr.standardize = 1; % standardize each subject's values wrt their average CS- response

opts.sebr.plot_allorders = true; % plot subjects with all orders or only the same one
opts.sebr.plot_allconds = true; % plot simple and complex stimuli separately
opts.sebr.plot_separate_CStype = true;

%% PSR
opts.psr.trimstart = -1;
opts.psr.trimend = 7;
opts.psr.channel.pupil = 1;
opts.psr.channel.gaze_x = 2;
opts.psr.channel.gaze_y = 3;
opts.psr.channel.marker = 4;
opts.psr.channel.correct = 5;
opts.psr.channel.gaze_x_cm = 6;
opts.psr.channel.gaze_y_cm = 7;
opts.psr.channel.valfix = 8;

opts.psr.path.main = fullfile(opts.expPath,'data','eyelink');
opts.psr.path.raw = fullfile(opts.expPath,'data','eyelink','asc','renamed');
opts.psr.path.import = fullfile(opts.expPath,'data','eyelink','imported');
opts.psr.path.trim = fullfile(opts.expPath,'data','eyelink','trimmed');
opts.psr.path.valfix = fullfile(opts.expPath,'data','eyelink','valid_fixations');
opts.psr.path.glm_cond = fullfile(opts.expPath,'models','PSR','GLM_cond');
opts.psr.path.glm_trial = fullfile(opts.expPath,'models','PSR','GLM_trial');

opts.psr.sList.orig = [21 22 24 25 26 28 29 30 31 35 36 38 42 43 45 46 47 48 49 51 ...
    53 57 58 65 67 68 69 71 75 78 79 80 81 82 83 84 85 87 88 89 90 ...
    91 92 94 95 96 98 99 100 103 106 107 113 114 115 117 120 121 123 125 ... 
    126 129	132	133	134	137	138	139];
opts.psr.sList.left = [80 88 89 90 134]; % left eye (actually right eye but left in EyeLink)
opts.psr.sList.control = [21 25 29 30 35 38 42 43 46 47 48 49 53 69 71 78 79 80 83 84 88 90 91 ...
    94 95 98 100 106 113 114 115 120 126 129 134 137]; % control group subs included
opts.psr.sList.experim = [22 24 26 28 31 36 45 51 57 58 65 67 68 75 81 82 85 87 89 92 96 99 103 ...
    107 117 121 123 125 132 133 138 139]; % experimental group subs included
opts.psr.sList.complex_first = [22 25 26 29 30 36 38 42 43 46 47 48 51 53 57 58 65 67 68 69 71 75 78 ...
    79 80 81 82 83 84 85 87 88 89 90 91 92 94 95 96 98 99 100 103 106 107 113 114 115 117 120 ...
    121 123 125 126 129 132 133 134 137]; % complex stimuli first
opts.psr.sList.simple_first = [21 24 28 31 35 45 49 138 139]; % simple stimuli first
opts.psr.groupnames = {'control' 'experimental'};

opts.psr.sessionlist = {'acquisition' 'retention' 'retest'};
opts.psr.trials = [96 24 96];
opts.psr.blocks = [8 4 8];
opts.psr.trialsperblock = opts.psr.trials./opts.psr.blocks;
opts.psr.trials2take = [96 24 96] ; % all trials
% opts.psr.trials2take = (opts.psr.trials/2+1):opts.psr.trials; % last half of the experiment
opts.psr.blocks2take = [8 4 8];%5:8; % % which blocks of the experiment
opts.psr.blocks2take_stim = {[1:4] [1:2] [1:4]};%3:4;% % per stimulus complexity type (4 blocks each)

sList_acq = opts.psr.sList.orig;
sList_acq(sList_acq==42 | sList_acq==46) = [];
opts.psr.sList.sessions{1} = sList_acq;
% Sub 42 no data, sub 46 not enough markers

sList_retention = opts.psr.sList.orig;
sList_retention(sList_retention==82 | sList_retention==134) = [];
% 82 retention not completed according to protocol, 134 no data
opts.psr.sList.sessions{2} = sList_retention;

sList_retest = opts.psr.sList.orig;
sList_retest(sList_retest==80 | sList_retest==82 | sList_retest==85 | sList_retest==42 | sList_retest==47 | sList_retest==134) = [];
% Sub 80, 85, 134 restart and missing data, 82 retention not completed
% according to protocol, 42 too few markers
% 47 retest session pupil data too short (6 min, only 24 trials out of 96, although behavioural data includes all trials)

%sList_retest(sList_retest==46) = [];
% 46 almost no data left after import & blink/saccade removal
% 36 very little data
opts.psr.sList.sessions{3} = sList_retest;

% Valid fixations options
opts.psr.screen_resolution = [1280 1024];
opts.psr.screen_aspectratio = [4 3];
opts.psr.screen_size = 18.5;

opts.psr.distance.eyetracker = 800; % eyetracker distance from the subject in mm
opts.psr.distance.screen = 900; % screen distance from the subject in mm
opts.psr.distance_unit = 'mm';
opts.psr.eyelink_width = 38;  % EyeLink calibration window size in cm
opts.psr.eyelink_height = 28; 
opts.psr.box_degrees = 7;     % Visual degrees for valid fixations
opts.psr.convert_unit = 'cm'; % unit for converting gaze coordinates from pixels (to cm)
opts.psr.fixation_opt.eyes = 'right';
opts.psr.fixation_opt.missing = 0;
opts.psr.fixation_opt.plot_gaze_coords = 0; % plot with fix square and data points (for quality control)
opts.psr.fixation_opt.overwrite = 1;
opts.psr.fixation_opt.channel_action = 'replace';
opts.psr.fixation_type = 'default';

% Valid trials
opts.psr.minpercdata = 0.5;
opts.psr.split_sessions_markers = {[13 25 37 49 61 73 85] [7 13 19] [13 25 37 49 61 73 85]};
time_afterUS = 3.5;        % Time in seconds included after US onset
opts.psr.trialtime = opts.soa+time_afterUS;
opts.psr.validtrials.validrunsfn = 'valfix_valid_runs_allsubjects';

% Valid subjects
for ses = 1:length(opts.psr.sessionlist)
    valsubfile = fullfile(opts.psr.path.main,[opts.psr.validtrials.validrunsfn '_' opts.psr.sessionlist{ses} '.mat']);
    if exist(valsubfile,'file'); valsubs = load(valsubfile);
        included = valsubs.validsubs(ses).session(valsubs.validsubs(ses).session(:,end) == opts.psr.blocks(ses),1);
        opts.psr.sList.included{ses} = included';
    else; opts.psr.sList.included{ses} = opts.psr.sList.sessions{ses}; % all subjects ok
    end
end

% Fix spikes
opts.psr.sList.fixspikes{1} = opts.psr.sList.sessions{1};
opts.psr.sList.fixspikes{2} = opts.psr.sList.sessions{2};
opts.psr.sList.fixspikes{3} = opts.psr.sList.sessions{3};
%opts.psr.sList.fixspikes{1} = opts.psr.sList.included{1}(ismember(opts.psr.sList.included{1},opts.psr.sList.control));

% Plotting
opts.psr.plot.valfix = true;
opts.psr.plot.trialtime = 5;
opts.psr.plot.sr = 100;
opts.psr.sList.plot{1} = opts.psr.sList.included{1}(ismember(opts.psr.sList.included{1},opts.psr.sList.control));
opts.psr.sList.plot{2} = opts.psr.sList.included{2}(ismember(opts.psr.sList.included{2},opts.psr.sList.control));
opts.psr.sList.plot{3} = opts.psr.sList.included{3}(ismember(opts.psr.sList.included{3},opts.psr.sList.control));

% GLM
opts.psr.sList.GLM = opts.psr.sList.included;
opts.psr.sList.GLM_control{1} = opts.psr.sList.GLM{1}(ismember(opts.psr.sList.GLM{1},opts.psr.sList.control));
opts.psr.sList.GLM_experimental{1} = opts.psr.sList.GLM{1}(ismember(opts.psr.sList.GLM{1},opts.psr.sList.experim));
opts.psr.sList.GLM_control{2} = opts.psr.sList.GLM{2}(ismember(opts.psr.sList.GLM{2},opts.psr.sList.control));
opts.psr.sList.GLM_experimental{2} = opts.psr.sList.GLM{2}(ismember(opts.psr.sList.GLM{2},opts.psr.sList.experim));
opts.psr.sList.GLM_control{3} = opts.psr.sList.GLM{3}(ismember(opts.psr.sList.GLM{3},opts.psr.sList.control));
opts.psr.sList.GLM_experimental{3} = opts.psr.sList.GLM{3}(ismember(opts.psr.sList.GLM{3},opts.psr.sList.experim));
opts.psr.estimated_response = 1;  % For conditionwise GLM.
                                        % 1 = CS response + derivative, default
                                        % 2 = CS + US response
                                        % 3 = US response + derivative
opts.psr.estimated_conditions = 3;
opts.psr.glm.condition_names = {'CSs+US+','CSs+US-','CSs-US-' 'CSc+US+','CSc+US-','CSc-US-'};
%opts.psr.glm.condition_names = {'CSs+US+','CSs+US-','CSs-US-' 'CSc+US+','CSc+US-','CSc-US-'};
opts.psr.glm_bfname = {'CSderiv' 'CSUS' 'USderiv'}; 
opts.psr.glm.stimname = {'simple' 'complex'};
opts.psr.summarystat = 1;         % For trialwise GLM. 1: mean, 2: median
opts.psr.glm_filename.cond = 'GLM_PSR_cond_';
opts.psr.glm_filename.trial = 'GLM_PSR_trial_';
opts.psr.glm.sr = 100;

% Permutation test
opts.psr.permtest.test_type = 'anova'; % 'anova' or 'ttest'
opts.psr.permtest.test_no = 7; % 7 for ANOVA, 4 for t-tests

%% SCR
opts.scr.artefact_on = 1; % artefact correction on
opts.scr.newsubs = 0; % processing data from new subjects that were included after initial analyses (mistakenly excluded at first for SCR)
opts.scr.trimstart = -5;
opts.scr.trimend = 20;
opts.scr.trimend_block = 12; % for splitting into blocks/sessions, time after last CS onset (4s CS duration + 8 s after offset -> coded into the experiment)
opts.scr.blocks = [8 4 8];
opts.scr.trials = 96;
opts.scr.blocktrials = {1:12; 13:24; 25:36; 37:48; 49:60; 61:72; 73:84; 85:96};
opts.scr.dcm_trials = [96 24 96];%[48 12 48];%
opts.scr.trials2take = 1:96;%(opts.scr.trials/2+1):opts.scr.trials;%
opts.scr.blocks2take = 1:8;%5:8;%
opts.scr.datamultiplier = 1e6; % multiply data with this to arrive at original unit of micro Siemens
opts.scr.originalsr = 1000; % Original 1000 Hz sampling rate of the SCR data
opts.scr.minITI = 7;

opts.scr.path.main = fullfile(opts.expPath,'data','labchart');
opts.scr.path.raw = fullfile(opts.expPath,'data','labchart','labchartmat','renamed');
opts.scr.path.import = fullfile(opts.expPath,'data','labchart','imported');
opts.scr.path.trim = fullfile(opts.expPath,'data','labchart','trimmed');
opts.scr.path.artcorr = fullfile(opts.expPath,'data','labchart','artefact_corrected');
opts.scr.path.dcm = fullfile(opts.expPath,'models','SCR','DCM');
opts.scr.path.glm_cond = fullfile(opts.expPath,'models','SCR','GLM_cond');
opts.scr.path.glm_trial = fullfile(opts.expPath,'models','SCR','GLM_trial');

opts.scr.channel.scr = 1; % scr data
opts.scr.channel.trigger = 2; % raw trigger data
opts.scr.channel.marker = 3; % ready markers

opts.scr.sList.orig = [21 22 24 25 26 28 29 30 31 35 36 38 42 43 45 46 47 ...
    48 49 51 53 57 58 65 67 68 69 71 75 78 79 80 81 82 83 84 85 87 88 ...
    89 90 91 92 94 95 96 98 99 100 103 106 107 113 114 115 117 120 121 ...
    123 125 126	129	132	133	134	137	138	139];
opts.scr.sList.control = [21 25 29 30 35 38 42 43 46 47 48 49 53 69 71 78 79 80 83 84 88 90 91 ...
    94 95 98 100 106 113 114 115 120 126 129 134 137]; % control group subs included
opts.scr.sList.experim = [22 24 26 28 31 36 45 51 57 58 65 67 68 75 81 82 85 87 89 92 96 99 103 ...
    107 117 121 123 125 132 133 138 139]; % experimental group subs included
opts.scr.sList.complex_first = [22 25 26 29 30 36 38 42 43 46 47 48 51 53 57 58 65 67 68 69 71 75 78 ...
    79 80 81 82 83 84 85 87 88 89 90 91 92 94 95 96 98 99 100 103 106 107 113 114 115 117 120 ...
    121 123 125 126 129 132 133 134 137]; % complex stimuli first
opts.scr.sList.simple_first = [21 24 28 31 35 45 49 138 139]; % simple stimuli first
opts.scr.groupnames = {'control' 'experimental'};

sList_acq = opts.scr.sList.orig;
sList_acq(sList_acq==22 | sList_acq==58 | sList_acq == 113) = [];
%  | sList_acq==46
% 22 and 58 no markers, 113 completely missing data, 46??
opts.scr.sList.sessions{1} = intersect(sList_acq, [opts.scr.sList.control opts.scr.sList.experim]);

sList_retention = opts.scr.sList.orig;
sList_retention(sList_retention==38 | sList_retention==82 | sList_retention==99) = [];
% 38 no data, 82 retention not completed to protocol, 99 flat data
opts.scr.sList.sessions{2} = intersect(sList_retention,[opts.scr.sList.control opts.scr.sList.experim]);

sList_retest = opts.scr.sList.orig;
sList_retest(sList_retest==38 | sList_retest==42 | sList_retest==43 | sList_retest==69 | sList_retest==80 | sList_retest==82 | sList_retest==83 | sList_retest==85 | sList_retest==99 | sList_retest==100) = [];
% 38 no data, 42 and 43 not enough markers, 80 and 85 restart, 82 retention not
% completed to protocol, 99 flat data, 100 ??
opts.scr.sList.sessions{3} = intersect(sList_retest,[opts.scr.sList.control opts.scr.sList.experim]);

opts.scr.sList.sessions{4} = opts.scr.sList.orig; % training

% sList common to acquisition and retest
opts.scr.sList.comp = intersect(opts.scr.sList.sessions{1},opts.scr.sList.sessions{3});

% Data
opts.scr.standardize = 1; % standardize each subject's values wrt their average CS- response
opts.scr.sessionlist = {'acquisition' 'retention' 'retest' 'training1'};
opts.scr.split_sessions_markers = {[13 25 37 49 61 73 85] [7 13 19] [13 25 37 49 61 73 85]};
opts.scr.split_sessions_overwrite = 0;
time_afterUS = 0;        % Time in seconds included after US onset
opts.scr.trialtime = opts.soa+time_afterUS;

% DCM
sList_acq(sList_acq==65 | sList_acq==83 | sList_acq==113 | sList_acq==117) = []; % excluded due to data quality problems
opts.scr.sList.DCM{1} = sList_acq;
opts.scr.sList.DCM_control{1} = opts.scr.sList.DCM{1}(ismember(opts.scr.sList.DCM{1},opts.scr.sList.control));
opts.scr.sList.DCM_experimental{1} = opts.scr.sList.DCM{1}(ismember(opts.scr.sList.DCM{1},opts.scr.sList.experim));
opts.scr.sList.DCM_control_simplefirst{1} = intersect(opts.scr.sList.DCM_control{1},opts.scr.sList.simple_first);
opts.scr.sList.DCM_control_complexfirst{1} = intersect(opts.scr.sList.DCM_control{1},opts.scr.sList.complex_first);
opts.scr.sList.DCM_experimental_simplefirst{1} = intersect(opts.scr.sList.DCM_experimental{1},opts.scr.sList.simple_first);
opts.scr.sList.DCM_experimental_complexfirst{1} = intersect(opts.scr.sList.DCM_experimental{1},opts.scr.sList.complex_first);
opts.scr.sList.DCM_simplefirst{1} = intersect(opts.scr.sList.DCM{1},opts.scr.sList.simple_first);
opts.scr.sList.DCM_complexfirst{1} = intersect(opts.scr.sList.DCM{1},opts.scr.sList.complex_first);
sList_retest(sList_retest==58 | sList_retest==65) = []; % excluded due to data quality problems
opts.scr.sList.DCM{3} = sList_retest;
opts.scr.sList.DCM_control{3} = opts.scr.sList.DCM{3}(ismember(opts.scr.sList.DCM{3},opts.scr.sList.control));
opts.scr.sList.DCM_experimental{3} = opts.scr.sList.DCM{3}(ismember(opts.scr.sList.DCM{3},opts.scr.sList.experim));
opts.scr.sList.DCM_control_simplefirst{3} = intersect(opts.scr.sList.DCM_control{3},opts.scr.sList.simple_first);
opts.scr.sList.DCM_control_complexfirst{3} = intersect(opts.scr.sList.DCM_control{3},opts.scr.sList.complex_first);
opts.scr.sList.DCM_experimental_simplefirst{3} = intersect(opts.scr.sList.DCM_experimental{3},opts.scr.sList.simple_first);
opts.scr.sList.DCM_experimental_complexfirst{3} = intersect(opts.scr.sList.DCM_experimental{3},opts.scr.sList.complex_first);
opts.scr.sList.DCM_simplefirst{3} = intersect(opts.scr.sList.DCM{3},opts.scr.sList.simple_first);
opts.scr.sList.DCM_complexfirst{3} = intersect(opts.scr.sList.DCM{3},opts.scr.sList.complex_first);
opts.scr.summarystat = 1;
opts.scr.condnames = {'CSs+US+','CSs+US-','CSs-US-' 'CSc+US+','CSc+US-','CSc-US-'};
opts.scr.exclude_missingperc = 0.5;
opts.scr.plot_separate_CStype = 1;


% GLM
opts.scr.sList.GLM = opts.psr.sList.included;
opts.scr.sList.GLM_control{1} = opts.scr.sList.GLM{1}(ismember(opts.scr.sList.GLM{1},opts.scr.sList.control));
opts.scr.sList.GLM_experimental{1} = opts.scr.sList.GLM{1}(ismember(opts.scr.sList.GLM{1},opts.scr.sList.experim));
opts.scr.sList.GLM_control{3} = opts.scr.sList.GLM{3}(ismember(opts.scr.sList.GLM{3},opts.scr.sList.control));
opts.scr.sList.GLM_experimental{3} = opts.scr.sList.GLM{3}(ismember(opts.scr.sList.GLM{3},opts.scr.sList.experim));
opts.scr.estimated_response = 1;  % For conditionwise GLM.
                                        % 1 = CS response + derivative, default
                                        % 2 = CS + US response
                                        % 3 = US response + derivative
opts.scr.estimated_conditions = 3;
opts.scr.glm.condition_names = {'CSs+US+','CSs+US-','CSs-US-' 'CSc+US+','CSc+US-','CSc-US-'};
%opts.scr.glm.condition_names = {'CSs+US+','CSs+US-','CSs-US-' 'CSc+US+','CSc+US-','CSc-US-'};
opts.scr.glm_bfname = {'CSderiv' 'CSUS' 'USderiv'}; 
opts.scr.glm.stimname = {'simple' 'complex'};
opts.scr.summarystat = 1;         % For trialwise GLM. 1: mean, 2: median
opts.scr.glm_filename.cond = 'GLM_SCR_cond_';
opts.scr.glm_filename.trial = 'GLM_SCR_trial_';
opts.scr.glm.sr = 10;

% Timecourse plots
opts.scr.plot.sr = 50;
opts.scr.plot.trialtime = 9;

end